/*
 Navicat Premium Data Transfer

 Source Server         : Srikandi (RW)
 Source Server Type    : MySQL
 Source Server Version : 50616
 Source Host           : 10.1.1.102:3306
 Source Schema         : db_tpsonline

 Target Server Type    : MySQL
 Target Server Version : 50616
 File Encoding         : 65001

 Date: 18/08/2021 15:01:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL COMMENT '1 = OPERATOR, 2 = AIRLINES',
  `access_domain` enum('1','2') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '1 = MAU, 2 = RA',
  `is_active` int(1) NULL DEFAULT NULL,
  `date_created` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `email`(`email`) USING BTREE,
  INDEX `password`(`password`) USING BTREE,
  INDEX `active`(`is_active`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (9, 'Operational', 'mau@operational.com', 'default.png', '$2y$10$qYXMC0kEljfCqocZNOu85OKlgxcD1Pw96IIpBQB98wcgYqMA5gDby', 1, '1', 1, 1573567854);
INSERT INTO `user` VALUES (15, 'Airlines', 'mau@airlines.com', 'default.png', '$2y$10$HofLJc4vqf3RbNU/9GW/U./c086pag2ebVqdoKV76.ItxCFLoPD2O', 2, '1', 1, 1582022004);
INSERT INTO `user` VALUES (18, 'Operational', 'ra@operational.com', 'default.png', '$2y$10$RWFJ65LnOEfU9/UMCe7iwujtTRRteh836XxR1D9kDDvrR1Gc/Er6C', 1, '2', 1, 1573567854);
INSERT INTO `user` VALUES (19, 'Airlines', 'ra@airlines.com', 'default.png', '$2y$10$1rPJaGl.bWP7oR3eNdXKw.5PtmybWE/.kcchp6QlMd8bMA5tHEKH2', 2, '2', 1, 1582022004);

-- ----------------------------
-- Table structure for user_access_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_access_menu`;
CREATE TABLE `user_access_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `menu_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_access_menu
-- ----------------------------
INSERT INTO `user_access_menu` VALUES (6, 1, 5);
INSERT INTO `user_access_menu` VALUES (14, 1, 11);
INSERT INTO `user_access_menu` VALUES (18, 1, 12);
INSERT INTO `user_access_menu` VALUES (25, 1, 1);
INSERT INTO `user_access_menu` VALUES (34, 2, 1);
INSERT INTO `user_access_menu` VALUES (35, 2, 5);

-- ----------------------------
-- Table structure for user_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_menu`;
CREATE TABLE `user_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `controller` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `icon` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` int(1) NULL DEFAULT 1,
  `have_a_child` int(1) NULL DEFAULT NULL,
  `sort` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_menu
-- ----------------------------
INSERT INTO `user_menu` VALUES (1, 'Dashboard', 'dashboard', 'flaticon-line-graph', 'home/dashboard', 1, 0, '1');
INSERT INTO `user_menu` VALUES (5, 'Report', 'report', 'flaticon-interface-11', 'report', 1, 0, '4');
INSERT INTO `user_menu` VALUES (11, 'Update Status', 'updatestatus', 'flaticon-signs-1', 'status/updateStatus', 1, 0, '5');
INSERT INTO `user_menu` VALUES (12, 'Help', 'help', 'flaticon-share', 'help', 1, 0, '7');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 'Operational');
INSERT INTO `user_role` VALUES (2, 'Airlines');

-- ----------------------------
-- Table structure for user_sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_sub_menu`;
CREATE TABLE `user_sub_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NULL DEFAULT NULL,
  `title` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` int(1) NULL DEFAULT 1,
  `have_a_child` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_sub_sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_sub_sub_menu`;
CREATE TABLE `user_sub_sub_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_menu_id` int(11) NULL DEFAULT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `is_active` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
