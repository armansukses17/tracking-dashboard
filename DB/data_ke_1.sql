/*
 Navicat Premium Data Transfer

 Source Server         : Srikandi (RW)
 Source Server Type    : MySQL
 Source Server Version : 50616
 Source Host           : 10.1.1.102:3306
 Source Schema         : db_tpsonline

 Target Server Type    : MySQL
 Target Server Version : 50616
 File Encoding         : 65001

 Date: 09/07/2021 18:21:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_airlines
-- ----------------------------
DROP TABLE IF EXISTS `m_airlines`;
CREATE TABLE `m_airlines`  (
  `airlinescode` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `airlinesname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `countryID` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kd_tps` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `void` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`airlinescode`) USING BTREE,
  INDEX `countryID`(`countryID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_airlines
-- ----------------------------
INSERT INTO `m_airlines` VALUES ('2Y', 'MYINDO', 'MY', 'BGD1', 0);
INSERT INTO `m_airlines` VALUES ('3K', 'JETSTAR ASIA', 'SG', '', 0);
INSERT INTO `m_airlines` VALUES ('5J', 'CEBU PACIFIC', 'PH', '', 0);
INSERT INTO `m_airlines` VALUES ('8F', 'CARDIG AIR', 'ID', '', 0);
INSERT INTO `m_airlines` VALUES ('8K', 'KMILE', 'MY', '', 0);
INSERT INTO `m_airlines` VALUES ('AK', 'AIR ASIA', 'MY', 'UNX1', 0);
INSERT INTO `m_airlines` VALUES ('BI', 'ROYAL BRUNAI', 'BN', '', 0);
INSERT INTO `m_airlines` VALUES ('BR', 'EVA AIR', 'HK', 'JAS1', 0);
INSERT INTO `m_airlines` VALUES ('CI', 'CHINA AIRLINES', 'CN', 'GRA1', 0);
INSERT INTO `m_airlines` VALUES ('CX', 'CATHAY FASIFIC', 'HK', 'JAS1', 0);
INSERT INTO `m_airlines` VALUES ('CZ', 'CHINA SOUTHERN', 'CN', 'GRA1', 0);
INSERT INTO `m_airlines` VALUES ('D7', 'AIR ASIA X BERHAD', 'MY', 'UNX1', 0);
INSERT INTO `m_airlines` VALUES ('EK', 'EMIRATED AIRLINES', 'AE', '', 0);
INSERT INTO `m_airlines` VALUES ('EY', 'ETIHAD AIRWAYS', 'AE', 'JAS1', 0);
INSERT INTO `m_airlines` VALUES ('FD', 'THAI AIRASIA', 'TH', '', 0);
INSERT INTO `m_airlines` VALUES ('GA', 'GARUDA INDONESIA', 'ID', 'GRA1', 0);
INSERT INTO `m_airlines` VALUES ('HX', 'HONGKONG EXPRESS', 'HK', '', 0);
INSERT INTO `m_airlines` VALUES ('IW', 'WINGS AIR', 'ID', 'GMAU', 0);
INSERT INTO `m_airlines` VALUES ('JT', 'LION AIR', 'ID', 'GMAU', 0);
INSERT INTO `m_airlines` VALUES ('KA', 'CHATAY DRAGON', 'HK', '', 0);
INSERT INTO `m_airlines` VALUES ('KE', 'KOREAN AIR', 'KR', 'GRA1', 0);
INSERT INTO `m_airlines` VALUES ('KL', 'KLM', 'NL', '', 0);
INSERT INTO `m_airlines` VALUES ('MH', 'MALAYSIA AIRLINES', 'MY', 'JAS1', 0);
INSERT INTO `m_airlines` VALUES ('MU', 'CHINA CARGO', 'CN', '', 0);
INSERT INTO `m_airlines` VALUES ('NH', 'ALL NIPPON AIRWAYS', 'JP', '', 0);
INSERT INTO `m_airlines` VALUES ('OZ', 'ASIANA AIRLINES', 'KR', 'UNX1', 0);
INSERT INTO `m_airlines` VALUES ('QG', 'CITILINK', 'ID', 'GMAU', 0);
INSERT INTO `m_airlines` VALUES ('QR', 'QATAR AIRWAYS', 'QA', '', 0);
INSERT INTO `m_airlines` VALUES ('QZ', 'AIR ASIA INDONESIA', 'ID', 'UNX1', 0);
INSERT INTO `m_airlines` VALUES ('SL', 'THAI LION AIR', 'TH', '', 0);
INSERT INTO `m_airlines` VALUES ('SQ', 'SINGAPORE AIRLINES', 'SG', 'JAS1', 0);
INSERT INTO `m_airlines` VALUES ('TG', 'THAI AIRWAYS', 'TH', 'GRA1', 0);
INSERT INTO `m_airlines` VALUES ('TH', 'RAYA AIRWAYS', 'BN', 'JAS1', 0);
INSERT INTO `m_airlines` VALUES ('TK', 'TURKISH AIRLINES', 'TR', '', 0);
INSERT INTO `m_airlines` VALUES ('TR', 'TIGER AIRWAYS', 'SG', '', 0);
INSERT INTO `m_airlines` VALUES ('UR', 'Ukraine AIR ALLIANCE', 'UA', 'BGD1', 0);
INSERT INTO `m_airlines` VALUES ('VN', 'VIETNAM AIRLINES', 'VN', 'JAS1', 0);
INSERT INTO `m_airlines` VALUES ('XT', 'INDONESIA AIRASIA EXTRA', 'ID', '', 0);
INSERT INTO `m_airlines` VALUES ('ZH', 'SHENZHEN AIRLINES', 'CN', 'GRA1', 0);

-- ----------------------------
-- Table structure for m_beacukai
-- ----------------------------
DROP TABLE IF EXISTS `m_beacukai`;
CREATE TABLE `m_beacukai`  (
  `Noid` int(11) NOT NULL AUTO_INCREMENT,
  `kd_KBPC` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Nama_KBPC` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `Kota` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Eselon` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `NamaProgram` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `Registrasi` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `void` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`Noid`) USING BTREE,
  INDEX `kd_KBPC`(`kd_KBPC`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_beacukai
-- ----------------------------
INSERT INTO `m_beacukai` VALUES (2, '000000', 'Kantor Pusat DJBC', 0, 'Jakarta', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (3, '010000', 'Kanwil DJBC Sumatera Utara', 0, 'Belawan', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (4, '010700', 'KPPBC Belawan', 0, 'Belawan', '010000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (5, '010800', 'KPPBC Medan', 0, 'Medan', '010000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (6, '010900', 'KPPBC Pangkalan Susu', 0, 'Pangkalan Susu', '010000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (7, '011000', 'KPPBC Pematang Siantar', 0, 'Pematang Siantar', '010000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (8, '011100', 'KPPBC Teluk Nibung', 0, 'Teluk Nibung', '010000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (9, '011200', 'KPPBC Kuala Tanjung', 0, 'Kuala Tanjung', '010000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (10, '011300', 'KPPBC Sibolga', 0, 'Sibolga', '010000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (11, '011500', 'KPPBC Teluk Bayur', 0, 'Teluk Bayur', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (12, '020000', 'Kanwil DJBC Kepulauan Riau', 0, 'Pekanbaru', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (13, '020100', 'KPPBC Tanjung Balai Karimun', 0, 'Tanjung Balai Karimun', '020000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (14, '020200', 'KPPBC Sambu Belakang Padang', 0, 'Sambu Belakang Padang', '020000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (15, '020300', 'KPPBC Selat Panjang', 0, 'Selat Panjang', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (16, '020400', 'KPU Batam', 1, 'Batam', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (17, '020500', 'KPPBC Tanjung Pinang', 0, 'Tanjung Pinang', '020000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (18, '020800', 'KPPBC Dabo Singkep', 0, 'Dabo Singkep', '020000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (19, '020900', 'KPPBC Dumai', 0, 'Dumai', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (20, '021000', 'KPPBC Bagan Siapiapi', 0, 'Bagan Siapiapi', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (21, '021100', 'KPPBC Bengkalis', 0, 'Bengkalis', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (22, '021200', 'KPPBC Pekanbaru', 0, 'Pekanbaru', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (23, '021300', 'KPPBC Siak Sri Indrapura', 0, 'Siak Sri Indrapura', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (24, '021500', 'KPPBC Tembilahan', 0, 'Tembilahan', '140000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (25, '021700', 'KPPBC Tarempa ', 0, 'Tarempa', '020000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (26, '030000', 'Kanwil DJBC Sumatera Bagian Selatan', 0, 'Jakarta', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (27, '030100', 'KPPBC Palembang', 0, 'Palembang', '030000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (28, '030200', 'KPPBC Bengkulu', 0, 'Bengkulu', '030000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (29, '030300', 'KPPBC Pangkal Pinang', 0, 'Pangkal Pinang', '030000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (30, '030500', 'KPPBC Tanjung Pandan', 0, 'Tanjung Pandan', '030000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (31, '030600', 'KPPBC Jambi', 0, 'Jambi', '030000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (32, '030700', 'KPPBC Bandar Lampung', 1, 'Bandar Lampung', '030000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (33, '040000', 'Kanwil DJBC Jakarta', 1, 'Jakarta', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (34, '040300', 'KPU Tanjung Priok', 1, 'Jakarta', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (35, '040400', 'KPPBC Jakarta', 1, 'Jakarta', '040000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (36, '040500', 'Balai Pengujian dan Identifikasi Ba', 1, 'Jakarta', '040000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (37, '040600', 'KPPBC Kantor Pos Pasar Baru', 1, 'Jakarta', '040000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (38, '050000', 'Kanwil DJBC Jawa Barat', 1, 'Bandung', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (39, '050100', 'KPPBC Soekarno-Hatta', 1, 'Jakarta', '150000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (40, '050300', 'KPPBC Bogor', 1, 'Bogor', '050000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (41, '050400', 'KPPBC Merak', 1, 'Merak', '150000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (42, '050500', 'KPPBC Bandung', 1, 'Bandung', '050000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (43, '050600', 'KPPBC Tasikmalaya', 1, 'Tasikmalaya', '050000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (44, '050700', 'KPPBC Cirebon', 1, 'Cirebon', '050000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (45, '050800', 'KPPBC Purwakarta', 1, 'Purwakarta', '050000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (46, '050900', 'KPPBC Bekasi', 1, 'Bekasi', '050000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (47, '060000', 'Kanwil DJBC Jawa Tengah dan DI Yogy', 1, 'Semarang', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (48, '060100', 'KPPBC Tanjung Emas', 1, 'Semarang', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (49, '060200', 'KPPBC Pekalongan', 1, 'Pekalongan', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (50, '060300', 'KPPBC Kudus', 1, 'Kudus', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (51, '060400', 'KPPBC Cilacap', 1, 'Cilacap', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (52, '060600', 'KPPBC Surakarta', 1, 'Surakarta', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (53, '060700', 'KPPBC Yogyakarta', 1, 'Yogyakarta', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (54, '061000', 'KPPBC Tegal', 1, 'Tegal', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (55, '062000', 'KPPBC Purwokerto', 1, 'Purwokerto', '060000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (56, '070000', 'Kanwil DJBC Jawa Timur I', 1, 'Surabaya', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (57, '070100', 'KPPBC Tanjung Perak', 1, 'Surabaya', '070000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (58, '070200', 'KPPBC Kalianget', 1, 'Kalianget', '070000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (59, '070300', 'KPPBC Gresik', 1, 'Gresik', '070000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (60, '070400', 'KPPBC Bojonegoro', 1, 'Bojonegoro', '070000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (61, '070500', 'KPPBC Juanda', 1, 'Surabaya', '070000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (62, '070600', 'KPPBC Malang', 0, 'Malang', '160000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (63, '070700', 'KPPBC Blitar', 0, 'Blitar', '160000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (64, '070800', 'KPPBC Kediri', 0, 'Kediri', '160000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (65, '070900', 'KPPBC Tulung Agung', 0, 'Tulung Agung', '160000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (66, '071000', 'KPPBC Madiun', 0, 'Madiun', '160000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (67, '071100', 'KPPBC Panarukan', 0, 'Panarukan', '160000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (68, '071200', 'KPPBC Probolinggo', 0, 'Probolinggo', '160000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (69, '071300', 'KPPBC Pasuruan', 0, 'Pasuruan', '070000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (70, '080000', 'Kanwil DJBC Bali, NTB dan NTT', 1, 'Denpasar', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (71, '080100', 'KPPBC Ngurah Rai', 1, 'Denpasar', '080000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (72, '080300', 'KPPBC Mataram', 0, 'Mataram', '080000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (73, '080400', 'KPPBC Bima', 1, 'Bima', '080000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (74, '080500', 'KPPBC Kupang', 1, 'Kupang', '080000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (75, '080700', 'KPPBC Maumere', 0, 'Maumere', '080000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (76, '081200', 'KPPBC Benoa ', 0, 'Benoa', '080000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (77, '081300', 'KPPBC Atapupu', 0, 'Atapupu', '080000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (78, '090000', 'Kanwil DJBC Kalimantan Bagian Barat', 0, 'Pontianak', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (79, '090100', 'KPPBC Pontianak', 0, 'Pontianak', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (80, '090200', 'KPPBC Entikong', 0, 'Entikong', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (81, '090300', ' Kantor Bantu Teluk Air (PL)', 0, 'Teluk Air', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (82, '090400', 'KPPBC Ketapang', 0, 'Ketapang', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (83, '090500', 'KPPBC Sintete', 0, 'Sintete', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (84, '090700', 'KPPBC Sampit', 0, 'Sampit', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (85, '090800', 'KPPBC Pangkalan Buun', 0, 'Pangkalan Buun', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (86, '090900', 'KPPBC Pulang Pisau', 0, 'Pulang Pisau', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (87, '092000', 'KPPBC Jagoi Babang', 0, 'Jagoi Babang', '090000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (88, '100000', 'Kanwil DJBC Kalimantan Bagian Timur', 0, 'Balikpapan', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (89, '100100', 'KPPBC Banjarmasin', 0, 'Banjarmasin', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (90, '100200', 'KPPBC Kotabaru', 0, 'Kotabaru', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (91, '100300', 'KPPBC Balikpapan', 0, 'Balikpapan', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (92, '100500', 'KPPBC Samarinda', 0, 'Samarinda', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (93, '100600', 'KPPBC Bontang', 0, 'Bontang', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (94, '100800', 'KPPBC Tarakan', 0, 'Tarakan', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (95, '100900', 'KPPBC Nunukan', 0, 'Nunukan', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (96, '101000', 'KPPBC Sangata', 0, 'Sangata', '100000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (97, '110000', 'Kanwil DJBC Sulawesi', 0, 'Makasar', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (98, '110100', 'KPPBC Makasar', 0, 'Makasar', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (99, '110300', 'KPPBC Pare-pare', 0, 'Pare-pare', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (100, '110400', 'KPPBC Malili', 0, 'Malili', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (101, '110500', 'KPPBC Bajoe', 0, 'Bajoe', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (102, '110600', 'KPPBC Kendari', 0, 'Kendari', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (103, '110700', 'KPPBC Pomalaa', 0, 'Pomalaa', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (104, '110800', 'KPPBC Pantoloan', 0, 'Pantoloan', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (105, '110900', 'KPPBC Poso', 0, 'Poso', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (106, '111000', 'KPPBC Luwuk', 0, 'Luwuk', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (107, '111100', 'KPPBC Bitung', 0, 'Bitung', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (108, '111200', 'KPPBC Manado', 0, 'Manado', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (109, '111300', 'KPPBC Gorontalo', 0, 'Gorontalo', '110000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (110, '120000', 'Kanwil DJBC Maluku, Papua dan Irian', 0, 'Ambon', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (111, '120100', 'KPPBC Ambon', 0, 'Ambon', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (112, '120200', 'KPPBC Ternate', 0, 'Ternate', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (113, '120300', 'KPPBC Sorong', 0, 'Sorong', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (114, '120400', 'KPPBC Manokwari', 0, 'Manokwari', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (115, '120500', 'KPPBC Fak-Fak', 0, 'Fak-Fak', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (116, '120600', 'KPPBC Jayapura', 0, 'Jayapura', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (117, '120700', 'KPPBC Merauke', 0, 'Merauke', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (118, '120800', 'KPPBC Amamapare', 0, 'Timika', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (119, '120900', 'KPPBC Biak', 0, 'Biak', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (120, '121000', 'KPPBC Tual', 0, 'Tual', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (121, '122000', 'KPPBC Bintuni', 0, 'Bintuni', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (122, '122100', 'KPPBC Kaimana', 0, 'Kaimana', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (123, '122200', 'KPPBC Nabire', 0, 'Nabire', '120000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (124, '130000', 'Kanwil DJBC Nangroe Aceh Darussalam', 0, 'Banda Aceh', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (125, '130100', 'KPPBC Banda Aceh', 0, 'Banda Aceh', '130000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (126, '130300', 'KPPBC Sabang', 0, 'Sabang', '130000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (127, '130400', 'KPPBC Meulaboh', 0, 'Meulaboh', '130000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (128, '130500', 'KPPBC Lhok Seumawe', 0, 'Lhok Seumawe', '130000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (129, '130600', 'KPPBC Kuala Langsa', 0, 'Kuala Langsa', '130000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (130, '140000', 'Kanwil DJBC Riau dan Sumatra Barat', 0, 'Pekanbaru', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (131, '150000', 'Kantor Wilayah DJBC Banten', 1, 'Tangerang', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (132, '150300', 'KPPBC Tangerang', 1, 'Tangerang', '150000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (133, '160000', 'Kanwil DJBC Jawa Timur II', 1, 'Malang', '000000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (134, '160200', 'KPPBC Sunda Kelapa', 1, 'Jakarta', '040000', '', '', 0);
INSERT INTO `m_beacukai` VALUES (135, '160700', 'KPPBC Banyuwangi', 0, 'Banyuwangi', '160000', '', '', 0);

-- ----------------------------
-- Table structure for m_flight
-- ----------------------------
DROP TABLE IF EXISTS `m_flight`;
CREATE TABLE `m_flight`  (
  `noid` int(11) NOT NULL AUTO_INCREMENT,
  `airlinescode` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `flightno` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `TimeDeparture` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `TimeArrival` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `route` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `void` tinyint(1) NOT NULL DEFAULT 0,
  `In_Out` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`noid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 145 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_flight
-- ----------------------------
INSERT INTO `m_flight` VALUES (1, 'GA', '863', '17:10', '21:10', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (2, 'SQ', '0986', '00:30', '01:15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (3, 'SQ', '0952', '07:40', '08:05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (4, 'SQ', '0956', '09:25', '09:50', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (5, 'CX', '776', '14:25', '20:25', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (6, 'CX', '718', '08:20', '14:20', NULL, 0, 'O');
INSERT INTO `m_flight` VALUES (7, 'GA', '873', '09:00', '13:00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (8, 'CX', '753', '00:11', '01:13', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (9, 'CX', '777', '09:20', '13:10', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (10, 'CX', '719', '16:00', '19:50', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (11, 'KL', '8044', '12:00', '14:00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (12, 'KL', '8046', '10:00', '12:00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (14, 'CX', '752', '05:15', '11:05', NULL, 0, 'O');
INSERT INTO `m_flight` VALUES (15, '2Y', '925', '04:30', '05:30', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (16, '2Y', '921', '07:40', '08:40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (18, 'CX', '797', '19:05', '22:45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (19, 'CX', '3241', '10:20', '13:56', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (21, 'GA', '421', '19.40', '20.45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (22, 'GA', '857', '15.45', '20.50', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (23, 'CX', '3249', '09.30', '13.26', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (24, 'GA', '425', '21.40', '22.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (25, 'GA', '409', '14.05', '15.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (26, 'GA', '419', '17.20', '18.30', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (27, 'GA', '839', '20.20', '21.15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (28, 'MH', '717', '10.15', '11.20', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (29, 'SQ', '7861', '04:00', '07:40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (30, 'SQ', '0966', '18:30', '19:20', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (31, 'AK', '129', '03:20', '07:15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (32, 'AK', '139', '11:45', '15:40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (33, 'AK', '388', '09.20', '10.25', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (34, 'QZ', '203', '09.50', '10.45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (35, 'AK', '382', '23.55', '01.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (36, 'AK', '380', '07:00', '08.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (37, 'AK', '384', '13.05', '14.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (38, 'AK', '386', '18.50', '20:00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (39, 'QZ', '201', '14.55', '16.10', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (40, 'XT', '209', '17.50', '19.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (41, 'MH', '711', '09.10', '10.15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (42, 'QZ', '207', '22.25', '23.30', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (43, 'MH', '725', '18.05', '19.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (47, 'MH', '721', '14.31', '15.41', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (48, 'MH', '723', '16.30', '17.35', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (49, '8F', '781', '06:00', '10:28', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (50, 'MH', '713', '07.30', '09.31', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (51, 'MH', '727', '10.00', '11.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (53, 'BI', '735', '11.40', '13.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (54, '3K', '203', '13:35', '14:30', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (55, 'TR', '2274', '08.00', '09.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (56, 'TR', '2278', '10:20', '11:10', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (57, '3K', '201', '08:12', '08:54', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (58, 'GA', '417', '18:35', '19:50', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (59, 'CZ', '8352', '19.15', '23.20', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (60, 'CZ', '8353', '19.15', '23.20', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (61, 'GA', '407', '12:00', '12:55', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (62, 'CI', '679', '17.20', '21.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (63, 'GA', '319', '15:30', '16:45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (64, 'TG', '435', '14:25', '17:55', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (65, 'EK', '542', '09:50', '13:25', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (66, 'TG', '433', '08:26', '12:00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (67, 'GA', '411', '01:15', '16:35', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (68, 'SQ', '0960', '15.15', '16.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (69, 'SQ', '0891', '12.25', '16.20', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (70, 'SQ', '0968', '21.55', '22.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (71, 'EK', '0356', '04.29', '15.33', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (72, 'KL', '0809', '20.48', '17.25', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (75, '2Y', '923', '08.50', '09.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (77, 'BR', '0237', '08.52', '13.31', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (78, 'EY', '474', '03.34', '15.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (79, 'CI', '5853', '07.55', '12.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (80, 'KE', '627', '15.30', '20.30', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (81, '5J', '759', '20.35', '23.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (82, 'OZ', '761', '17.15', '22.15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (83, 'SQ', '0950', '06.20', '07.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (84, 'SQ', '0958', '12.30', '13.15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (85, 'VN', '631', '09.50', '13.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (86, 'QR', '956', '02.25', '15.25', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (87, 'CI', '761', '08.45', '13.10', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (88, 'QZ', '269', '22.00', '22.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (89, 'GA', '899', '15.45', '20.10', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (90, 'AK', '353', '19.50', '20.45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (91, 'TK', '056', '02.43', '18.29', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (92, 'SQ', '0853', '01.20', '04.50', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (93, 'NH', '871', '23.30', '05.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (94, 'GA', '897', '02.20', '07.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (95, 'NH', '835', '18.05', '23.55', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (96, '3K', '207', '15.45', '16.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (97, 'TH', 'TH1021', '09.00', '10.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (98, '3K', '211', '18.53', '19.41', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (99, '3K', '205', '20.00', '20.55', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (100, 'QZ', '263', '10.20', '11.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (101, 'QZ', '209', '17.30', '19.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (102, 'ZH', '9047', '23.00', '02.20', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (103, 'GA', '4012', '11.00', '12.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (104, 'GA', '879', '0.35', '15.45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (105, 'CX', '793', '00.50', '04.35', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (106, 'SQ', '0964', '17.20', '18.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (107, 'D7', '688', '09.45', '10.45', NULL, 0, '1');
INSERT INTO `m_flight` VALUES (108, '8K', '8K542', '14.30', '19.45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (109, 'UR', 'UKL408', '02.47', '02.47', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (110, '8K', '8K541', '14.30', '16.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (111, 'SQ', '0962', '16.10', '16.55', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (112, 'SL', '116', '08.20', '11.50', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (114, 'HX', 'HX707', '12.22', '17.15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (115, 'HX', 'HX709', '19.20', '00.30', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (116, 'CX', '785', '10.05', '15.00', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (117, 'GA', '857', '15.45', '20.45', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (118, '3K', '243', '07.05', '09.55', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (119, 'SQ', '0938', '08.20', '11.05', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (120, 'CI', '771', '09.06', '14.24', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (121, 'BR', '0255', '10.00', '15.20', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (122, 'KE', '633', '16.25', '22.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (123, 'TG', '431', '09.04', '14.16', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (124, 'FD', '396', '06.11', '11.15', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (125, 'CZ', '6065', '18.25', '23.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (126, 'QZ', '551', '10.30', '13.35', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (127, 'MU', '5029', '18.00', '00.30', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (128, 'KA', '350   ', '12.30', '17.53', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (129, 'BI', '737', '11.40', '12.40', NULL, 0, 'I');
INSERT INTO `m_flight` VALUES (131, 'QG', 'QG1511', '0', '17:05', 'BDOSUB', 0, 'K');
INSERT INTO `m_flight` VALUES (132, 'QG', 'QG432', '0', '08:00', 'BDOBPN', 0, 'K');
INSERT INTO `m_flight` VALUES (133, 'QG', 'QG955', '0', '13:05', 'BDOPKU', 0, 'K');
INSERT INTO `m_flight` VALUES (134, 'QG', 'QG815', '0', '18:35', 'BDOPLM', 0, 'K');
INSERT INTO `m_flight` VALUES (135, 'QG', 'QG813', '0', '09:30', 'BDOKNO', 0, 'K');
INSERT INTO `m_flight` VALUES (136, 'QG', 'QG825', '0', '15:25', 'BDODPS', 0, 'K');
INSERT INTO `m_flight` VALUES (137, 'QG', 'QG1510', '0', '16:30', 'SUBBDO', 0, 'M');
INSERT INTO `m_flight` VALUES (138, 'QG', 'QG810', '0', '7:25', 'PLMBDO', 0, 'M');
INSERT INTO `m_flight` VALUES (139, 'QG', 'QG433', '0', '11:40', 'BPNBDO', 0, 'M');
INSERT INTO `m_flight` VALUES (140, 'QG', 'QG954', '0', '17:15', 'PKUBDO', 0, 'M');
INSERT INTO `m_flight` VALUES (141, 'QG', 'QG822', '0', '8:55', 'DPSBDO', 0, 'M');
INSERT INTO `m_flight` VALUES (142, 'QG', 'QG812', '0', '14:50', 'KNOBDO', 0, 'M');
INSERT INTO `m_flight` VALUES (144, 'MU', '7179', '18:00', '00:30', NULL, 0, 'I');

-- ----------------------------
-- Table structure for m_gudang
-- ----------------------------
DROP TABLE IF EXISTS `m_gudang`;
CREATE TABLE `m_gudang`  (
  `Noid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kd_Gudang` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Nama_Gudang` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Active` tinyint(1) NULL DEFAULT 1,
  `void` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`Noid`) USING BTREE,
  INDEX `kd_Gudang`(`kd_Gudang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_gudang
-- ----------------------------
INSERT INTO `m_gudang` VALUES (1, 'TA11', 'FAJAR INSAN NUSANTARA', 0, 0);
INSERT INTO `m_gudang` VALUES (2, 'TB15', 'GUDANG CARDIC LOGISTICS INDONESIA', 0, 0);
INSERT INTO `m_gudang` VALUES (3, 'LOGW', 'GUDANG LOGWIN AIR & OCEAN INDONESIA', 0, 0);
INSERT INTO `m_gudang` VALUES (4, 'LWPH', 'GUDANG LOGWIN AIR & OCEAN INDONESIA', 0, 0);
INSERT INTO `m_gudang` VALUES (5, 'TA10', 'K LINE AIR SERVICE INDONESIA', 0, 0);
INSERT INTO `m_gudang` VALUES (6, 'TB13', 'KINTETSU WORLD EX. IND', 0, 0);
INSERT INTO `m_gudang` VALUES (7, 'TA01', 'NIPPON EXPRESS INDONESIA', 0, 0);
INSERT INTO `m_gudang` VALUES (8, 'GPPL', 'PANAH PERDANA LOGISINDO', 0, 0);
INSERT INTO `m_gudang` VALUES (9, 'TA05', 'PANALPINA NUSAJAYA TRN', 0, 0);
INSERT INTO `m_gudang` VALUES (10, 'TE03', 'PRIMA INTERNATIONAL C', 0, 0);
INSERT INTO `m_gudang` VALUES (11, 'APRI', 'PT ANGKASA PURA II', 1, 0);
INSERT INTO `m_gudang` VALUES (12, 'APRE', 'PT ANGKASA PURA II', 1, 0);
INSERT INTO `m_gudang` VALUES (13, 'GIBS', 'PT INTI BANGUN SELARAS', 1, 0);
INSERT INTO `m_gudang` VALUES (14, 'GGWI', 'PT. GEODIS WILSON INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (15, 'TB12', 'PT. AGILITY INTERNATIONAL', 1, 0);
INSERT INTO `m_gudang` VALUES (16, 'BDEK', 'PT. BANGUN DESA LOGISTINDO EKSPOR', 1, 0);
INSERT INTO `m_gudang` VALUES (17, 'BDIM', 'PT. BANGUN DESA LOGISTINDO IMPORT', 1, 0);
INSERT INTO `m_gudang` VALUES (18, 'BDJT', 'PT. BANGUN DESA LOGISTINDO PJT', 1, 0);
INSERT INTO `m_gudang` VALUES (19, 'GBDL', 'PT. BANGUN DESA LOGISTINDO', 1, 0);
INSERT INTO `m_gudang` VALUES (20, 'GBGF', 'PT. BANTEN GLOBAL DEVELOPMENT   UPS', 1, 0);
INSERT INTO `m_gudang` VALUES (21, 'GBGD', 'PT. BANTEN GLOBAL DEVELOPMENT IMPORT', 1, 0);
INSERT INTO `m_gudang` VALUES (22, 'GBGE', 'PT. BANTEN GLOBAL DEVELOPMENT EKSPOR', 1, 0);
INSERT INTO `m_gudang` VALUES (23, 'GBGT', 'PT. BANTEN GLOBAL DEVELOPMENT  PJT', 1, 0);
INSERT INTO `m_gudang` VALUES (24, 'DXKT', 'PT. DEXTER EUREKATAMA', 1, 0);
INSERT INTO `m_gudang` VALUES (25, 'TA04', 'PT. DFDS TRANSPORT INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (26, 'TB05', 'PT. DHL GLOBAL FORWARDING INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (27, 'GDHL', 'PT. DHL INDONESIA (BIROTIKA)', 1, 0);
INSERT INTO `m_gudang` VALUES (28, 'TB06', 'PT. EXPEDITORS INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (29, 'GPRA', 'PT. GAPURA ANGKASA IMPORT', 1, 0);
INSERT INTO `m_gudang` VALUES (30, 'GPRE', 'PT. GAPURA ANGKASA EKSPOR', 1, 0);
INSERT INTO `m_gudang` VALUES (31, 'GPRH', 'PT. GAPURA ANGKASA RUSH HANDLING', 1, 0);
INSERT INTO `m_gudang` VALUES (32, 'GAEK', 'PT. GARUDA INDONESIA EKSPOR', 1, 0);
INSERT INTO `m_gudang` VALUES (33, 'GAJT', 'PT. GARUDA INDONESIA PJT', 1, 0);
INSERT INTO `m_gudang` VALUES (34, 'GIMP', 'PT. GARUDA INDONESIA IMPORT', 1, 0);
INSERT INTO `m_gudang` VALUES (35, 'GARH', 'PT. GARUDA INDONESIA RUSH HANDLING', 1, 0);
INSERT INTO `m_gudang` VALUES (36, 'TA15', 'PT. GREEN AIR PACIFIC', 1, 0);
INSERT INTO `m_gudang` VALUES (37, 'CIMP', 'PT. JASA ANGKASA SEMESTA IMPORT', 1, 0);
INSERT INTO `m_gudang` VALUES (38, 'CIRH', 'PT. JASA ANGKASA SEMESTA RUSH HANDLING', 1, 0);
INSERT INTO `m_gudang` VALUES (39, 'CIMR', 'PT. JASA ANGKASA SEMESTA PJT', 1, 0);
INSERT INTO `m_gudang` VALUES (40, 'CIME', 'PT. JASA ANGKASA SEMESTA EKSPOR', 1, 0);
INSERT INTO `m_gudang` VALUES (41, 'KENA', 'PT. KARGO EXPRESS NUSANTARA', 1, 0);
INSERT INTO `m_gudang` VALUES (42, 'TB09', 'PT. MONANG SIANIPAR ABADI', 1, 0);
INSERT INTO `m_gudang` VALUES (43, 'POSE', 'PT. POS INDONESIA KANTOR TUKAR SOEKARNO HATTA', 1, 0);
INSERT INTO `m_gudang` VALUES (44, 'POSI', 'PT. POS INDONESIA KANTOR TUKAR SOEKARNO HATTA', 1, 0);
INSERT INTO `m_gudang` VALUES (45, 'PNMI', 'PT. PUNINAR MSE INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (46, 'TB10', 'PT. RITRA CARGO INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (47, 'TRKI', 'PT. TRANS KARGO INTERNASIONAL', 1, 0);
INSERT INTO `m_gudang` VALUES (48, 'UNEX', 'PT. UNEX INTI INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (49, 'GUCT', 'PT. UNGGUL CIPTA TRANS', 1, 0);
INSERT INTO `m_gudang` VALUES (50, 'TA02', 'SCHENKER PETROLOG U', 1, 0);
INSERT INTO `m_gudang` VALUES (51, 'TE11', 'SDV LOGISTICS INDONESIA (BOLLORE)', 1, 0);
INSERT INTO `m_gudang` VALUES (52, 'TSLM', 'SKY LIGHT MULTITRADA', 1, 0);
INSERT INTO `m_gudang` VALUES (53, 'SPLI', 'SPEEDMARK LOGISTIC INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (54, 'TA12', 'TABHITA EXPRESS', 1, 0);
INSERT INTO `m_gudang` VALUES (55, 'GLFC', 'TNS GLOBAL FREIGHT CONSOLIDATAMA', 1, 0);
INSERT INTO `m_gudang` VALUES (56, 'TE08', 'TNS YUSEN AIR & SEA INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (57, 'TB11', 'TNT SKYPAK INTERNATIONAL', 1, 0);
INSERT INTO `m_gudang` VALUES (58, 'TB08', 'UNGGUL CIPTA TRANS', 1, 0);
INSERT INTO `m_gudang` VALUES (59, 'TA03', 'UNIAR INDOTAMA', 1, 0);
INSERT INTO `m_gudang` VALUES (60, 'TUTI', 'UNION TRANS INTERNUSA', 1, 0);
INSERT INTO `m_gudang` VALUES (61, 'VALI', 'VINFLAIR LOGISTICS INDONESIA', 1, 0);
INSERT INTO `m_gudang` VALUES (62, 'GDEK', 'WAHANA DIRGANTARA INDONESIA EKSPOR', 1, 0);
INSERT INTO `m_gudang` VALUES (63, 'GDJT', 'WAHANA DIRGANTARA INDONESIA PJT', 1, 0);
INSERT INTO `m_gudang` VALUES (64, 'GDWI', 'WAHANA DIRGANTARA INDONESIA IMPORT', 1, 0);
INSERT INTO `m_gudang` VALUES (65, 'GDWF', 'WAHANA DIRGANTARA INDONESIA FEDEX', 1, 0);
INSERT INTO `m_gudang` VALUES (66, 'GDWD', 'WAHANA DIRGANTARA INDONESIA DOMESTIK', 1, 0);
INSERT INTO `m_gudang` VALUES (67, 'GATI', 'PT GATRANS IMPORT', 1, 0);
INSERT INTO `m_gudang` VALUES (68, 'GATE', 'PT GATRANS EKSPOR', 1, 0);
INSERT INTO `m_gudang` VALUES (70, 'BPKU', 'PT. BALI PANDAWA KENCANA UTAMA', 1, 0);
INSERT INTO `m_gudang` VALUES (71, 'BANG', 'KRISNA MULTI LINTAS CEMERLANG', 1, 0);
INSERT INTO `m_gudang` VALUES (72, 'GCAR', 'PT. JASA ANGKASA SEMESTA (BALI)', 1, 0);
INSERT INTO `m_gudang` VALUES (73, 'GDWD', 'WAHANA DIRGANTARA INDONESIA IMPORT', 1, 0);

-- ----------------------------
-- Table structure for m_kd_dok
-- ----------------------------
DROP TABLE IF EXISTS `m_kd_dok`;
CREATE TABLE `m_kd_dok`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` int(5) NOT NULL,
  `nama` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `keterangan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `nilai_2`(`nilai`) USING BTREE,
  INDEX `nilai`(`nilai`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kd_dok
-- ----------------------------
INSERT INTO `m_kd_dok` VALUES (1, 1, 'Coari Discarge', 'Daftar Timbun Import', '2016-10-10 15:15:34');
INSERT INTO `m_kd_dok` VALUES (2, 2, 'Coari Loading', 'Daftar Muat Export', '2016-10-10 15:16:23');
INSERT INTO `m_kd_dok` VALUES (3, 3, 'Codeco Import', 'Gate Out Import Lini 1', '2016-10-10 16:42:02');
INSERT INTO `m_kd_dok` VALUES (4, 4, 'Codeco Export ', 'Gate In Export Lini 1', '2016-10-10 16:42:37');
INSERT INTO `m_kd_dok` VALUES (5, 5, 'Gate In Import Lini 2', 'Gate In Import Lini 2', '2016-10-10 16:44:48');
INSERT INTO `m_kd_dok` VALUES (6, 6, 'Gate Out Import Lini 2', 'Gate Out Import Lini 2', '2016-10-10 16:45:15');
INSERT INTO `m_kd_dok` VALUES (7, 7, 'Gate In Export Lini 2', 'Gate In Export Lini 2', '2016-10-10 16:45:49');
INSERT INTO `m_kd_dok` VALUES (8, 8, 'Gate Out Export Lini 2', 'Gate Out Export Lini 2', '2016-10-10 16:46:15');
INSERT INTO `m_kd_dok` VALUES (9, 9, 'Codeco Out Export ', 'Gate Out Export Lini 1 Batal Export', '2016-10-10 16:47:07');

-- ----------------------------
-- Table structure for m_kd_dok_inout
-- ----------------------------
DROP TABLE IF EXISTS `m_kd_dok_inout`;
CREATE TABLE `m_kd_dok_inout`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` int(10) NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kd_dok_inout
-- ----------------------------
INSERT INTO `m_kd_dok_inout` VALUES (1, 42, 'Persetujuan Pengeluaran Barang Kiriman (PPBK)', 'Impor', '2019-07-22 11:55:33');
INSERT INTO `m_kd_dok_inout` VALUES (2, 2, 'SPPB BC 2.3', 'Impor', '2016-10-11 16:24:52');
INSERT INTO `m_kd_dok_inout` VALUES (3, 1, 'SPPB PIB (BC 2.0)', 'Impor', '2019-07-22 11:56:55');
INSERT INTO `m_kd_dok_inout` VALUES (4, 4, 'SPPB BC 1.2', 'Impor', '2016-10-11 16:25:30');
INSERT INTO `m_kd_dok_inout` VALUES (5, 5, 'BCF 2.6 A (pemeriksaan di lokasi importir/SPPF)', 'Impor', '2016-10-11 16:26:37');
INSERT INTO `m_kd_dok_inout` VALUES (6, 3, 'Persetujuan PLP', 'Impor', '2019-07-22 11:56:59');
INSERT INTO `m_kd_dok_inout` VALUES (7, 7, 'PKBE', 'Ekspor', '2016-10-11 16:27:22');
INSERT INTO `m_kd_dok_inout` VALUES (8, 8, 'PPB', 'Ekspor', '2016-10-11 16:27:35');
INSERT INTO `m_kd_dok_inout` VALUES (9, 9, 'BCF 1.5 (Pindah lokasi dari TPS ke TPP untuk Kontainer > 30hari)', 'Impor', '2016-10-11 16:29:15');
INSERT INTO `m_kd_dok_inout` VALUES (10, 6, 'NPE', 'Ekspor', '2019-07-22 11:54:46');
INSERT INTO `m_kd_dok_inout` VALUES (11, 11, 'SPPBE (batal expor countainer sudah masuk TPS)', 'Ekspor', '2016-10-11 16:31:04');
INSERT INTO `m_kd_dok_inout` VALUES (12, 12, 'SPPBE (Pindah muat barang Expor)', 'Ekspor', '2016-10-11 16:31:38');
INSERT INTO `m_kd_dok_inout` VALUES (13, 13, 'PIBK', 'Impor', '2016-10-11 16:31:51');
INSERT INTO `m_kd_dok_inout` VALUES (14, 10, 'Empty Container ', 'Impor', '2019-07-22 11:53:52');
INSERT INTO `m_kd_dok_inout` VALUES (16, 16, 'Persetujuan Short Shipp', 'Impor', '2016-10-11 16:33:13');
INSERT INTO `m_kd_dok_inout` VALUES (17, 17, 'Persetujuan Part of (Importir Mita)', 'Impor', '2016-10-11 16:33:48');
INSERT INTO `m_kd_dok_inout` VALUES (18, 18, 'Persetujuan Part of (Importir Non-Mita)', 'Impor', '2016-10-11 16:34:09');
INSERT INTO `m_kd_dok_inout` VALUES (19, 19, 'SPJM', 'Impor', '2016-10-11 16:34:23');
INSERT INTO `m_kd_dok_inout` VALUES (20, 20, 'Dokumen BC 1.1 A/SP3B Impor', 'Impor', '2016-10-11 16:35:11');
INSERT INTO `m_kd_dok_inout` VALUES (21, 21, 'Pengeluaran dengan PIB manual (Cukai)', 'Impor', '2016-10-11 16:35:45');
INSERT INTO `m_kd_dok_inout` VALUES (22, 22, 'Paket Pos', 'Impor', '2016-10-11 16:35:59');
INSERT INTO `m_kd_dok_inout` VALUES (23, 23, 'Pengeluaran Barang Untuk Dimusnahkan', 'Impor', '2016-10-11 16:36:25');
INSERT INTO `m_kd_dok_inout` VALUES (24, 24, 'Pengeluaran barang untuk barang bukti Pengadilan', 'Impor', '2016-10-11 16:37:01');
INSERT INTO `m_kd_dok_inout` VALUES (25, 25, 'Pengeluaran barang Hibah', 'Impor', '2016-10-11 16:37:24');
INSERT INTO `m_kd_dok_inout` VALUES (26, 26, 'Pengeluaran barang milik Negara & barang tidak dikuasai yang di Lelang', 'Impor', '2016-10-11 16:38:32');
INSERT INTO `m_kd_dok_inout` VALUES (27, 27, 'Pengeluaran barang Pers Release', 'Impor', '2016-10-11 16:39:38');
INSERT INTO `m_kd_dok_inout` VALUES (28, 28, 'Re-Ekspor (BC 1.2) belum aju PIB', 'Impor', '2016-10-11 16:40:26');
INSERT INTO `m_kd_dok_inout` VALUES (29, 29, 'Pengeluaran barang eks pergantian kontainer', 'Impor', '2016-10-11 16:40:55');
INSERT INTO `m_kd_dok_inout` VALUES (30, 30, 'Pengeluaran Barang Penegahan (sebagian)', 'Impor', '2016-10-11 16:41:33');
INSERT INTO `m_kd_dok_inout` VALUES (31, 31, 'Pengeluaran Barang Penegahan (seluruhnya)', 'Impor', '2016-10-11 16:41:52');
INSERT INTO `m_kd_dok_inout` VALUES (32, 32, 'Empty Container (expor)', 'Ekspor', '2016-10-11 16:44:06');
INSERT INTO `m_kd_dok_inout` VALUES (33, 33, 'Returnable Package (RP-Expor)', 'Ekspor', '2016-10-11 16:45:08');
INSERT INTO `m_kd_dok_inout` VALUES (34, 34, 'Dokumen BC 1.1B/SP3B Export', 'Ekspor', '2016-10-11 16:45:44');
INSERT INTO `m_kd_dok_inout` VALUES (35, 40, 'Pengeluaran Kontainer eks stripping', 'Impor', '2019-02-27 09:42:33');
INSERT INTO `m_kd_dok_inout` VALUES (39, 99, 'Tidak Tps Online', 'impor', '2019-04-09 11:19:44');
INSERT INTO `m_kd_dok_inout` VALUES (40, 14, 'Retunable Package(RP)', 'Impor', '2019-07-22 11:51:27');
INSERT INTO `m_kd_dok_inout` VALUES (41, 15, 'Penimbunan diluar kawasab Pabean', 'Impor', '2019-07-22 11:51:53');
INSERT INTO `m_kd_dok_inout` VALUES (42, 0, 'Tidak TPS ONLINE', 'Impor', '2019-10-10 15:29:15');
INSERT INTO `m_kd_dok_inout` VALUES (43, 41, 'Document BC 1.6', 'Impor', '2019-10-10 15:29:13');
INSERT INTO `m_kd_dok_inout` VALUES (44, 43, 'Surat Perintah Penetapan Bea Masuk Cukai dan atau Pajak (SPPBMCP)', 'Impor', '2019-07-22 11:53:39');

-- ----------------------------
-- Table structure for m_kemasan
-- ----------------------------
DROP TABLE IF EXISTS `m_kemasan`;
CREATE TABLE `m_kemasan`  (
  `Noid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kd_kemasan` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_kemasan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` tinyint(1) NULL DEFAULT 0,
  `void` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`Noid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kemasan
-- ----------------------------
INSERT INTO `m_kemasan` VALUES (1, 'AE', 'Aerosol', 0, 0);
INSERT INTO `m_kemasan` VALUES (2, 'AM', 'Ampoule, non protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (3, 'AP', 'Ampoule, protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (4, 'AT', 'Atomizer', 0, 0);
INSERT INTO `m_kemasan` VALUES (5, 'BA', 'Barrel', 0, 0);
INSERT INTO `m_kemasan` VALUES (6, 'BB', 'Bobbin', 0, 0);
INSERT INTO `m_kemasan` VALUES (7, 'BC', 'Bottlecrate, bottlerack', 0, 0);
INSERT INTO `m_kemasan` VALUES (8, 'BD', 'Board', 0, 0);
INSERT INTO `m_kemasan` VALUES (9, 'BE', 'Bundle', 0, 0);
INSERT INTO `m_kemasan` VALUES (10, 'BF', 'Balloon, non-protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (11, 'BG', 'Bag', 0, 0);
INSERT INTO `m_kemasan` VALUES (12, 'BH', 'Bunch', 0, 0);
INSERT INTO `m_kemasan` VALUES (13, 'BI', 'Bin', 0, 0);
INSERT INTO `m_kemasan` VALUES (14, 'BJ', 'Bucket', 0, 0);
INSERT INTO `m_kemasan` VALUES (15, 'BK', 'Basket', 0, 0);
INSERT INTO `m_kemasan` VALUES (16, 'BL', 'Bale, compressed', 0, 0);
INSERT INTO `m_kemasan` VALUES (17, 'BN', 'Bale, non -compressed', 0, 0);
INSERT INTO `m_kemasan` VALUES (18, 'BO', 'Bottle, non-protected, cylindrical', 0, 0);
INSERT INTO `m_kemasan` VALUES (19, 'BP', 'Balloon, protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (20, 'BQ', 'Bottle, protected cylindrical', 0, 0);
INSERT INTO `m_kemasan` VALUES (21, 'BR', 'Bar', 0, 0);
INSERT INTO `m_kemasan` VALUES (22, 'BS', 'Bottle, non-protected, bulbous', 0, 0);
INSERT INTO `m_kemasan` VALUES (23, 'BT', 'Bolt', 0, 0);
INSERT INTO `m_kemasan` VALUES (24, 'BU', 'Butt', 0, 0);
INSERT INTO `m_kemasan` VALUES (25, 'BV', 'Bottle, protected bulbous', 0, 0);
INSERT INTO `m_kemasan` VALUES (26, 'BX', 'Box', 1, 0);
INSERT INTO `m_kemasan` VALUES (27, 'BY', 'Board, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (28, 'BZ', 'Bars, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (29, 'CA', 'Can, rectangular', 0, 0);
INSERT INTO `m_kemasan` VALUES (30, 'CB', 'Beer crate', 0, 0);
INSERT INTO `m_kemasan` VALUES (31, 'CC', 'Churn', 0, 0);
INSERT INTO `m_kemasan` VALUES (32, 'CE', 'Creel', 0, 0);
INSERT INTO `m_kemasan` VALUES (33, 'CF', 'Coffer', 0, 0);
INSERT INTO `m_kemasan` VALUES (34, 'CG', 'Cage', 0, 0);
INSERT INTO `m_kemasan` VALUES (35, 'CH', 'Chest', 0, 0);
INSERT INTO `m_kemasan` VALUES (36, 'CI', 'Canister', 0, 0);
INSERT INTO `m_kemasan` VALUES (37, 'CJ', 'Coffin', 0, 0);
INSERT INTO `m_kemasan` VALUES (38, 'CK', 'Cask', 0, 0);
INSERT INTO `m_kemasan` VALUES (39, 'CL', 'Coil', 0, 0);
INSERT INTO `m_kemasan` VALUES (40, 'CO', 'Carboy, non-protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (41, 'CP', 'Carboy, protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (42, 'CR', 'Crate', 0, 0);
INSERT INTO `m_kemasan` VALUES (43, 'CS', 'Case', 0, 0);
INSERT INTO `m_kemasan` VALUES (44, 'CT', 'Carton', 0, 0);
INSERT INTO `m_kemasan` VALUES (45, 'CU', 'Cup', 0, 0);
INSERT INTO `m_kemasan` VALUES (46, 'CV', 'Cover', 0, 0);
INSERT INTO `m_kemasan` VALUES (47, 'CX', 'Can, cylindical', 0, 0);
INSERT INTO `m_kemasan` VALUES (48, 'CY', 'Cylinder', 0, 0);
INSERT INTO `m_kemasan` VALUES (49, 'CZ', 'Canvas', 0, 0);
INSERT INTO `m_kemasan` VALUES (50, 'DJ', 'Demijohn, non-protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (51, 'DP', 'Demijohn, protected', 0, 0);
INSERT INTO `m_kemasan` VALUES (52, 'DR', 'Drum', 0, 0);
INSERT INTO `m_kemasan` VALUES (53, 'EN', 'Envelope', 0, 0);
INSERT INTO `m_kemasan` VALUES (54, 'FC', 'Fruit crate', 0, 0);
INSERT INTO `m_kemasan` VALUES (55, 'FD', 'Framed crate', 0, 0);
INSERT INTO `m_kemasan` VALUES (56, 'FI', 'Firkin', 0, 0);
INSERT INTO `m_kemasan` VALUES (57, 'FL', 'Flask', 0, 0);
INSERT INTO `m_kemasan` VALUES (58, 'FO', 'Footlocker', 0, 0);
INSERT INTO `m_kemasan` VALUES (59, 'FP', 'Filmpack', 0, 0);
INSERT INTO `m_kemasan` VALUES (60, 'FR', 'Frame', 0, 0);
INSERT INTO `m_kemasan` VALUES (61, 'GB', 'Gas bottle', 0, 0);
INSERT INTO `m_kemasan` VALUES (62, 'GI', 'Girder', 0, 0);
INSERT INTO `m_kemasan` VALUES (63, 'GZ', 'Girders, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (64, 'HG', 'Hogshead', 0, 0);
INSERT INTO `m_kemasan` VALUES (65, 'HR', 'Hamper', 0, 0);
INSERT INTO `m_kemasan` VALUES (66, 'IN', 'Ingot', 0, 0);
INSERT INTO `m_kemasan` VALUES (67, 'IZ', 'ingots, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (68, 'JC', 'Jerrican, rectangular', 0, 0);
INSERT INTO `m_kemasan` VALUES (69, 'JG', 'Jug', 0, 0);
INSERT INTO `m_kemasan` VALUES (70, 'JR', 'Jar', 0, 0);
INSERT INTO `m_kemasan` VALUES (71, 'JT', 'Jutebag', 0, 0);
INSERT INTO `m_kemasan` VALUES (72, 'JY', 'Jerrican, cylindrical', 0, 0);
INSERT INTO `m_kemasan` VALUES (73, 'KG', 'Keg', 1, 0);
INSERT INTO `m_kemasan` VALUES (74, 'LG', 'Log', 1, 0);
INSERT INTO `m_kemasan` VALUES (75, 'LZ', 'Logs, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (76, 'MB', 'Multiply bag', 0, 0);
INSERT INTO `m_kemasan` VALUES (77, 'MC', 'milk crate', 0, 0);
INSERT INTO `m_kemasan` VALUES (78, 'MS', 'Multiwall sack', 0, 0);
INSERT INTO `m_kemasan` VALUES (79, 'MT', 'Mat', 0, 0);
INSERT INTO `m_kemasan` VALUES (80, 'MX', 'Macth box', 0, 0);
INSERT INTO `m_kemasan` VALUES (81, 'NE', 'Unpacked or unpackaged', 0, 0);
INSERT INTO `m_kemasan` VALUES (82, 'NS', 'Nest', 0, 0);
INSERT INTO `m_kemasan` VALUES (83, 'NT', 'Net', 0, 0);
INSERT INTO `m_kemasan` VALUES (84, 'PA', 'Packet', 1, 0);
INSERT INTO `m_kemasan` VALUES (85, 'PC', 'Parcel', 1, 0);
INSERT INTO `m_kemasan` VALUES (86, 'PG', 'Plate', 0, 0);
INSERT INTO `m_kemasan` VALUES (87, 'PH', 'Pitcher', 0, 0);
INSERT INTO `m_kemasan` VALUES (88, 'PI', 'Pipe', 0, 0);
INSERT INTO `m_kemasan` VALUES (89, 'PK', 'Package', 1, 0);
INSERT INTO `m_kemasan` VALUES (90, 'PL', 'Pail', 0, 0);
INSERT INTO `m_kemasan` VALUES (91, 'PN', 'Plank', 0, 0);
INSERT INTO `m_kemasan` VALUES (92, 'PO', 'Pouch', 0, 0);
INSERT INTO `m_kemasan` VALUES (93, 'PT', 'Pot', 0, 0);
INSERT INTO `m_kemasan` VALUES (94, 'PU', 'Tray pack', 0, 0);
INSERT INTO `m_kemasan` VALUES (95, 'PY', 'Plates, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (96, 'PZ', 'Planks/Pipes, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (97, 'RD', 'Rod', 0, 0);
INSERT INTO `m_kemasan` VALUES (98, 'RG', 'Ring', 0, 0);
INSERT INTO `m_kemasan` VALUES (99, 'RL', 'Reel', 0, 0);
INSERT INTO `m_kemasan` VALUES (100, 'RO', 'Roll', 0, 0);
INSERT INTO `m_kemasan` VALUES (101, 'RT', 'Rednet', 0, 0);
INSERT INTO `m_kemasan` VALUES (102, 'RZ', 'Rods, in bundle/ bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (103, 'SA', 'Sack', 0, 0);
INSERT INTO `m_kemasan` VALUES (104, 'SC', 'Shallow crate', 0, 0);
INSERT INTO `m_kemasan` VALUES (105, 'SD', 'Spindle', 0, 0);
INSERT INTO `m_kemasan` VALUES (106, 'SE', 'Sea-chest', 0, 0);
INSERT INTO `m_kemasan` VALUES (107, 'SH', 'Sachet', 0, 0);
INSERT INTO `m_kemasan` VALUES (108, 'SK', 'Skeleton case', 0, 0);
INSERT INTO `m_kemasan` VALUES (109, 'SL', 'Slipsheet', 0, 0);
INSERT INTO `m_kemasan` VALUES (110, 'SM', 'Sheetmetal', 0, 0);
INSERT INTO `m_kemasan` VALUES (111, 'ST', 'Sheet', 0, 0);
INSERT INTO `m_kemasan` VALUES (112, 'SU', 'Suitcase', 0, 0);
INSERT INTO `m_kemasan` VALUES (113, 'SW', 'Shrinkwrapped', 0, 0);
INSERT INTO `m_kemasan` VALUES (114, 'SZ', 'Sheets, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (115, 'TB', 'Tub', 0, 0);
INSERT INTO `m_kemasan` VALUES (116, 'TC', 'Tea-chest', 0, 0);
INSERT INTO `m_kemasan` VALUES (117, 'TD', 'Tube, collapsible', 0, 0);
INSERT INTO `m_kemasan` VALUES (118, 'TK', 'Tank, rectangular', 0, 0);
INSERT INTO `m_kemasan` VALUES (119, 'TN', 'Tin', 0, 0);
INSERT INTO `m_kemasan` VALUES (120, 'TO', 'Tun', 0, 0);
INSERT INTO `m_kemasan` VALUES (121, 'TP', 'Tray', 0, 0);
INSERT INTO `m_kemasan` VALUES (122, 'TR', 'Trunk', 0, 0);
INSERT INTO `m_kemasan` VALUES (123, 'TS', 'Truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (124, 'TU', 'Tube', 0, 0);
INSERT INTO `m_kemasan` VALUES (125, 'TY', 'Tank, cylindrical', 0, 0);
INSERT INTO `m_kemasan` VALUES (126, 'TZ', 'Tubes, in bundle/bunch/truss', 0, 0);
INSERT INTO `m_kemasan` VALUES (127, 'VA', 'Vat', 0, 0);
INSERT INTO `m_kemasan` VALUES (128, 'VG', 'Bulk, gas ( at 1031 mbar and 15C )', 0, 0);
INSERT INTO `m_kemasan` VALUES (129, 'VI', 'Vial', 0, 0);
INSERT INTO `m_kemasan` VALUES (130, 'VL', 'Bulk, liquid', 0, 0);
INSERT INTO `m_kemasan` VALUES (131, 'VO', 'Bulk, solid, large particles (\"nodules\")', 0, 0);
INSERT INTO `m_kemasan` VALUES (132, 'VP', 'Vacuumpacked', 0, 0);
INSERT INTO `m_kemasan` VALUES (133, 'VQ', 'Bulk, liquefied gas (at abnormal temperature)', 1, 0);
INSERT INTO `m_kemasan` VALUES (134, 'VR', 'Bulk, solid, granular particles (\"grains\")', 1, 0);
INSERT INTO `m_kemasan` VALUES (135, 'VY', 'Bulk, solid, fine particles (\"powders\")', 1, 0);
INSERT INTO `m_kemasan` VALUES (136, 'WB', 'Wickerbottle', 0, 0);

-- ----------------------------
-- Table structure for m_report
-- ----------------------------
DROP TABLE IF EXISTS `m_report`;
CREATE TABLE `m_report`  (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `TypeReport` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`_id`, `TypeReport`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_report
-- ----------------------------
INSERT INTO `m_report` VALUES (1, 'outbond');
INSERT INTO `m_report` VALUES (2, 'inbound');
INSERT INTO `m_report` VALUES (3, 'transit');
INSERT INTO `m_report` VALUES (4, 'RA-Weighing');
INSERT INTO `m_report` VALUES (5, 'RA-CSD');
INSERT INTO `m_report` VALUES (6, 'RA-Delivery');

-- ----------------------------
-- Table structure for m_status
-- ----------------------------
DROP TABLE IF EXISTS `m_status`;
CREATE TABLE `m_status`  (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `shipment_type` enum('inbound','outbound','transit') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for m_status_tracking
-- ----------------------------
DROP TABLE IF EXISTS `m_status_tracking`;
CREATE TABLE `m_status_tracking`  (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `proses_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `urutan_status` int(11) NULL DEFAULT NULL,
  `status_kode` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_status_tracking
-- ----------------------------
INSERT INTO `m_status_tracking` VALUES (1, 'REGULATED', 1, 'RA001', 'Acceptance confirmation (before security screening)');
INSERT INTO `m_status_tracking` VALUES (2, 'REGULATED', 2, 'RA002', 'Security screening process');
INSERT INTO `m_status_tracking` VALUES (3, 'REGULATED', 3, 'RA003', 'Digital weighing scale');
INSERT INTO `m_status_tracking` VALUES (4, 'REGULATED', 4, 'RA004', 'Updated rejected item');
INSERT INTO `m_status_tracking` VALUES (5, 'REGULATED', 5, 'RA005', 'RA storage position');
INSERT INTO `m_status_tracking` VALUES (6, 'REGULATED', 6, 'RA006', 'Consignment security declaration (CSD) issuance');
INSERT INTO `m_status_tracking` VALUES (7, 'REGULATED', 7, 'RA007', 'Loading process to vehicle');
INSERT INTO `m_status_tracking` VALUES (8, 'REGULATED', 8, 'RA008', 'Transportation to warehouse lini-1');
INSERT INTO `m_status_tracking` VALUES (9, 'REGULATED', 9, 'RA009', 'Hand-over to airline');
INSERT INTO `m_status_tracking` VALUES (10, 'INBOUND', 1, 'IN001', 'Delivery from aircraft to incoming warehouse');
INSERT INTO `m_status_tracking` VALUES (11, 'INBOUND', 2, 'IN002', 'Arrival at Incoming warehouse');
INSERT INTO `m_status_tracking` VALUES (12, 'INBOUND', 3, 'IN003', 'Storage');
INSERT INTO `m_status_tracking` VALUES (13, 'INBOUND', 4, 'IN004', 'Custom & quarantine clearance');
INSERT INTO `m_status_tracking` VALUES (14, 'INBOUND', 5, 'IN005', 'Received by consignee');
INSERT INTO `m_status_tracking` VALUES (15, 'OUTBOUND', 1, 'OU001', 'Acceptance confirmation');
INSERT INTO `m_status_tracking` VALUES (16, 'OUTBOUND', 2, 'OU002', 'Weighing scale');
INSERT INTO `m_status_tracking` VALUES (17, 'OUTBOUND', 3, 'OU003', 'Manifesting');
INSERT INTO `m_status_tracking` VALUES (18, 'OUTBOUND', 4, 'OU004', 'Storage position');
INSERT INTO `m_status_tracking` VALUES (19, 'OUTBOUND', 5, 'OU005', 'Build up process');
INSERT INTO `m_status_tracking` VALUES (20, 'OUTBOUND', 6, 'OU006', 'Delivery to staging area');
INSERT INTO `m_status_tracking` VALUES (21, 'OUTBOUND', 7, 'OU007', 'Delivery to aircraft');
INSERT INTO `m_status_tracking` VALUES (22, 'OUTBOUND', 8, 'OU008', 'Loading to aircraft');
INSERT INTO `m_status_tracking` VALUES (23, 'TRANSIT', 1, 'TR001', 'Delivery from aircraft to incoming warehouse');
INSERT INTO `m_status_tracking` VALUES (24, 'TRANSIT', 2, 'TR002', 'Arrival at Incoming warehouse');
INSERT INTO `m_status_tracking` VALUES (25, 'TRANSIT', 3, 'TR003', 'Storage');
INSERT INTO `m_status_tracking` VALUES (26, 'TRANSIT', 4, 'TR004', 'Manifesting');
INSERT INTO `m_status_tracking` VALUES (27, 'TRANSIT', 5, 'TR005', 'Build up process');
INSERT INTO `m_status_tracking` VALUES (28, 'TRANSIT', 6, 'TR006', 'Delivery to staging area');
INSERT INTO `m_status_tracking` VALUES (29, 'TRANSIT', 7, 'TR007', 'Delivery to aircarft');
INSERT INTO `m_status_tracking` VALUES (30, 'TRANSIT', 8, 'TR008', 'Loading to aircraft');
INSERT INTO `m_status_tracking` VALUES (31, 'INBOUND', 0, 'IN006', 'Void data');
INSERT INTO `m_status_tracking` VALUES (32, 'OUTBOUND', 0, 'OU009', 'Void data');
INSERT INTO `m_status_tracking` VALUES (33, 'REGULATED', 0, 'RA010', 'Void data');
INSERT INTO `m_status_tracking` VALUES (34, 'TRANSIT', 0, 'TR009', 'Void data');

-- ----------------------------
-- Table structure for m_tps
-- ----------------------------
DROP TABLE IF EXISTS `m_tps`;
CREATE TABLE `m_tps`  (
  `Noid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kd_tps` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kd_gudang` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kd_kbpc` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `asAsal` int(1) NULL DEFAULT 1,
  `void` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`Noid`) USING BTREE,
  INDEX `kd_tps`(`kd_tps`) USING BTREE,
  INDEX `kd_gudang`(`kd_gudang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_tps
-- ----------------------------
INSERT INTO `m_tps` VALUES (1, 'FIN1', 'TA11', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (2, 'GCL1', 'TB15', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (3, 'LGWN', 'LOGW', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (4, 'LGWN', 'LWPH', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (5, 'KLA1', 'TA10', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (6, 'KNTS', 'TB13', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (7, 'NEI1', 'TA01', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (8, 'PPL1', 'GPPL', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (9, 'PNPN', 'TA05', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (10, 'PRIM', 'TE03', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (11, 'APR2', 'APRI', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (12, 'APR2', 'APRE', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (13, 'IBS1', 'GIBS', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (14, 'GWI1', 'GGWI', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (15, 'AGL1', 'TB12', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (16, 'BDL1', 'BDEK', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (17, 'BDL1', 'BDIM', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (18, 'BDL1', 'BDJT', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (19, 'BDL1', 'GBDL', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (20, 'BGD1', 'GBGF', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (21, 'BGD1', 'GBGD', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (22, 'BGD1', 'GBGE', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (23, 'BGD1', 'GBGT', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (24, 'DXET', 'DXKT', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (25, 'DTI1', 'TA04', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (26, 'DGF1', 'TB05', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (27, 'DHL1', 'GDHL', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (28, 'EXP1', 'TB06', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (29, 'GRA1', 'GPRA', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (30, 'GRA1', 'GPRE', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (31, 'GRA1', 'GPRH', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (32, 'GRD1', 'GAEK', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (33, 'GRD1', 'GAJT', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (34, 'GRD1', 'GIMP', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (35, 'GRD1', 'GARH', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (36, 'GAP1', 'TA15', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (37, 'JAS1', 'CIMP', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (38, 'JAS1', 'CIRH', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (39, 'JAS1', 'CIMR', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (40, 'JAS1', 'CIME', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (41, 'KEN1', 'KENA', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (42, 'MSA1', 'TB09', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (43, 'POS1', 'POSE', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (44, 'POS1', 'POSI', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (45, 'PMI1', 'PNMI', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (46, 'RCI1', 'TB10', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (47, 'TRK1', 'TRKI', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (48, 'UNX1', 'UNEX', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (49, 'UCT2', 'GUCT', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (50, 'SPU1', 'TA02', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (51, 'SDVL', 'TE11', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (52, 'SKLM', 'TSLM', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (53, 'SLI1', 'SPLI', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (54, 'TBEX', 'TA12', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (55, 'GFC1', 'GLFC', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (56, 'YAS1', 'TE08', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (57, 'TNTS', 'TB11', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (58, 'UCT1', 'TB08', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (59, 'UNR1', 'TA03', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (60, 'UTI1', 'TUTI', '050100', 1, 1);
INSERT INTO `m_tps` VALUES (61, 'VLG1', 'VALI', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (62, 'WHD1', 'GDEK', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (63, 'WHD1', 'GDJT', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (64, 'WHD1', 'GDWI', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (65, 'WHD1', 'GDWF', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (66, 'WHD1', 'GDWD', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (67, 'GATR', 'GATI', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (68, 'GATR', 'GATE', '050100', 1, 0);
INSERT INTO `m_tps` VALUES (72, 'GPAN', 'GPAN', '080100', 1, 0);
INSERT INTO `m_tps` VALUES (73, 'BANG', 'BANG', '080100', 1, 0);
INSERT INTO `m_tps` VALUES (74, 'GCAR', 'GCAR', '080100', 1, 0);
INSERT INTO `m_tps` VALUES (75, 'BPKU', 'BPKU', '080100', 1, 0);

-- ----------------------------
-- Table structure for master_bc11
-- ----------------------------
DROP TABLE IF EXISTS `master_bc11`;
CREATE TABLE `master_bc11`  (
  `no_bc11` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tgl_bc11` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nm_angkut` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tgl_tiba` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `no_voy_flight` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for master_refnum
-- ----------------------------
DROP TABLE IF EXISTS `master_refnum`;
CREATE TABLE `master_refnum`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(4) NULL DEFAULT 0,
  `keterangan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for status_upload
-- ----------------------------
DROP TABLE IF EXISTS `status_upload`;
CREATE TABLE `status_upload`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('mau','ra') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `file_path` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `cron_status` int(11) NULL DEFAULT NULL COMMENT '0 = file disimpan didirectory & sedang diproses, \r\n1 = transfer file / insert selesai, \r\n2 = done,\r\n--------------------------------------------\r\n9 = gagal / jml sheet tidak sesuai\r\n11 = gagal proses import / proses insert',
  `user` int(11) NULL DEFAULT NULL,
  `add_info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `key_upload` int(11) NULL DEFAULT NULL COMMENT 'No. upload (untuk info di header)',
  `created_at` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `updated_at` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 966 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of status_upload
-- ----------------------------
INSERT INTO `status_upload` VALUES (1, 'mau', 'data_mau3-20210210091108.xlsx', 'file/data_mau3-20210210091108.xlsx', 2, 9, NULL, 1, '2021-02-10 09:11:08', '2021-02-10 09:11:08');
INSERT INTO `status_upload` VALUES (2, 'mau', 'data_mau2-20210210091505.xlsx', 'file/data_mau2-20210210091505.xlsx', 2, 9, NULL, 2, '2021-02-10 09:15:05', '2021-02-10 09:15:05');
INSERT INTO `status_upload` VALUES (3, 'mau', 'data_mau_11022021-20210211100705.xlsx', 'file/data_mau_11022021-20210211100705.xlsx', 2, 9, NULL, 3, '2021-02-11 10:07:05', '2021-02-11 10:07:05');
INSERT INTO `status_upload` VALUES (4, 'mau', 'data_mau_110220011122-20210211112504.xlsx', 'file/data_mau_110220011122-20210211112504.xlsx', 2, 9, NULL, 4, '2021-02-11 11:25:04', '2021-02-11 11:25:04');
INSERT INTO `status_upload` VALUES (5, 'mau', 'traning syistem-20210211121019.xlsx', 'file/traning syistem-20210211121019.xlsx', 2, 9, NULL, 5, '2021-02-11 12:10:19', '2021-02-11 12:10:19');
INSERT INTO `status_upload` VALUES (6, 'ra', 'data_RA_1102210402-20210211160322.xlsx', 'file/data_RA_1102210402-20210211160322.xlsx', 2, 18, NULL, 6, '2021-02-11 16:03:22', '2021-02-11 16:03:22');
INSERT INTO `status_upload` VALUES (7, 'ra', '22121823-20210211202505.xlsx', 'file/22121823-20210211202505.xlsx', 2, 18, NULL, 7, '2021-02-11 20:25:05', '2021-02-11 20:25:05');
INSERT INTO `status_upload` VALUES (8, 'ra', 'qz-20210211203733.xlsx', 'file/qz-20210211203733.xlsx', 2, 18, NULL, 8, '2021-02-11 20:37:33', '2021-02-11 20:37:33');
INSERT INTO `status_upload` VALUES (9, 'ra', 'qg1-20210211204324.xlsx', 'file/qg1-20210211204324.xlsx', 2, 18, NULL, 9, '2021-02-11 20:43:24', '2021-02-11 20:43:24');
INSERT INTO `status_upload` VALUES (10, '', 'qg2-20210211231212.xlsx', 'file/qg2-20210211231212.xlsx', 0, NULL, NULL, 10, '2021-02-11 23:12:12', '2021-02-11 23:12:12');
INSERT INTO `status_upload` VALUES (11, 'ra', 'qg3-20210212001043.xlsx', 'file/qg3-20210212001043.xlsx', 2, 18, NULL, 11, '2021-02-12 00:10:43', '2021-02-12 00:10:43');
INSERT INTO `status_upload` VALUES (12, 'ra', 'qg2-20210212001115.xlsx', 'file/qg2-20210212001115.xlsx', 2, 18, NULL, 12, '2021-02-12 00:11:15', '2021-02-12 00:11:15');
INSERT INTO `status_upload` VALUES (13, 'ra', 'qg4-20210212041017.xlsx', 'file/qg4-20210212041017.xlsx', 2, 18, NULL, 13, '2021-02-12 04:10:17', '2021-02-12 04:10:17');
INSERT INTO `status_upload` VALUES (14, 'ra', 'qg5-20210212075834.xlsx', 'file/qg5-20210212075834.xlsx', 2, 18, NULL, 14, '2021-02-12 07:58:34', '2021-02-12 07:58:34');
INSERT INTO `status_upload` VALUES (15, 'ra', '123-20210212080336.xlsx', 'file/123-20210212080336.xlsx', 2, 18, NULL, 15, '2021-02-12 08:03:36', '2021-02-12 08:03:36');
INSERT INTO `status_upload` VALUES (16, 'ra', '123-20210212084839.xlsx', 'file/123-20210212084839.xlsx', 2, 18, NULL, 16, '2021-02-12 08:48:39', '2021-02-12 08:48:39');
INSERT INTO `status_upload` VALUES (17, 'ra', 'sssss-20210212085248.xlsx', 'file/sssss-20210212085248.xlsx', 2, 18, NULL, 17, '2021-02-12 08:52:48', '2021-02-12 08:52:48');
INSERT INTO `status_upload` VALUES (18, 'ra', 'data_RA_120221_1201-20210212120311.xlsx', 'file/data_RA_120221_1201-20210212120311.xlsx', 2, 18, NULL, 18, '2021-02-12 12:03:11', '2021-02-12 12:03:11');
INSERT INTO `status_upload` VALUES (19, 'ra', 'agu-20210212123418.xlsx', 'file/agu-20210212123418.xlsx', 2, 18, NULL, 19, '2021-02-12 12:34:18', '2021-02-12 12:34:18');
INSERT INTO `status_upload` VALUES (20, 'ra', 'agung-20210212140329.xlsx', 'file/agung-20210212140329.xlsx', 2, 18, NULL, 20, '2021-02-12 14:03:29', '2021-02-12 14:03:29');
INSERT INTO `status_upload` VALUES (21, 'ra', 'henc-20210212160420.xlsx', 'file/henc-20210212160420.xlsx', 2, 18, NULL, 21, '2021-02-12 16:04:20', '2021-02-12 16:04:20');
INSERT INTO `status_upload` VALUES (22, 'ra', 'ggg-20210212193116.xlsx', 'file/ggg-20210212193116.xlsx', 9, 18, NULL, 22, '2021-02-12 19:31:16', '2021-02-12 19:31:16');
INSERT INTO `status_upload` VALUES (23, 'ra', 'D 8714 FH 2040323-20210213004110.xlsx', 'file/D 8714 FH 2040323-20210213004110.xlsx', 2, 18, NULL, 23, '2021-02-13 00:41:10', '2021-02-13 00:41:10');
INSERT INTO `status_upload` VALUES (24, 'ra', 'D 8714 FH 2040323-20210213004355.xlsx', 'file/D 8714 FH 2040323-20210213004355.xlsx', 2, 18, NULL, 24, '2021-02-13 00:43:55', '2021-02-13 00:43:55');
INSERT INTO `status_upload` VALUES (25, 'ra', 'D 8713 FH 2040324-20210213030214.xlsx', 'file/D 8713 FH 2040324-20210213030214.xlsx', 2, 18, NULL, 25, '2021-02-13 03:02:14', '2021-02-13 03:02:14');
INSERT INTO `status_upload` VALUES (26, 'ra', 'D 8713 FH 2040324-20210213030608.xlsx', 'file/D 8713 FH 2040324-20210213030608.xlsx', 2, 18, NULL, 26, '2021-02-13 03:06:08', '2021-02-13 03:06:08');
INSERT INTO `status_upload` VALUES (27, 'ra', 'D 8713 FH 2040326-20210213052916.xlsx', 'file/D 8713 FH 2040326-20210213052916.xlsx', 2, 18, NULL, 27, '2021-02-13 05:29:16', '2021-02-13 05:29:16');
INSERT INTO `status_upload` VALUES (28, 'ra', 'D 8713 FH 2040326-20210213053110.xlsx', 'file/D 8713 FH 2040326-20210213053110.xlsx', 2, 18, NULL, 28, '2021-02-13 05:31:10', '2021-02-13 05:31:10');
INSERT INTO `status_upload` VALUES (29, 'ra', 'D 8714 FH 2040327-20210213064017.xlsx', 'file/D 8714 FH 2040327-20210213064017.xlsx', 2, 18, NULL, 29, '2021-02-13 06:40:17', '2021-02-13 06:40:17');
INSERT INTO `status_upload` VALUES (30, 'ra', 'gunk-20210213094550.xlsx', 'file/gunk-20210213094550.xlsx', 2, 18, NULL, 30, '2021-02-13 09:45:50', '2021-02-13 09:45:50');
INSERT INTO `status_upload` VALUES (31, 'ra', '130321-20210213101855.xlsx', 'file/130321-20210213101855.xlsx', 2, 18, NULL, 31, '2021-02-13 10:18:55', '2021-02-13 10:18:55');
INSERT INTO `status_upload` VALUES (32, 'ra', '3021-20210213122055.xlsx', 'file/3021-20210213122055.xlsx', 2, 18, NULL, 32, '2021-02-13 12:20:55', '2021-02-13 12:20:55');
INSERT INTO `status_upload` VALUES (33, 'ra', '3021-20210213122224.xlsx', 'file/3021-20210213122224.xlsx', 2, 18, NULL, 33, '2021-02-13 12:22:24', '2021-02-13 12:22:24');
INSERT INTO `status_upload` VALUES (34, 'mau', 'DATA 13FEB21-20210213131009.xlsx', 'file/DATA 13FEB21-20210213131009.xlsx', 2, 9, NULL, 34, '2021-02-13 13:10:09', '2021-02-13 13:10:09');
INSERT INTO `status_upload` VALUES (35, 'ra', 'D 8713 FH 2040331-20210214000344.xlsx', 'file/D 8713 FH 2040331-20210214000344.xlsx', 2, 18, NULL, 35, '2021-02-14 00:03:44', '2021-02-14 00:03:44');
INSERT INTO `status_upload` VALUES (36, 'mau', 'data 14feb210127-20210214013053.xlsx', 'file/data 14feb210127-20210214013053.xlsx', 2, 9, NULL, 36, '2021-02-14 01:30:53', '2021-02-14 01:30:53');
INSERT INTO `status_upload` VALUES (37, 'ra', 'D 8713 FH 2040332-20210214035644.xlsx', 'file/D 8713 FH 2040332-20210214035644.xlsx', 2, 18, NULL, 37, '2021-02-14 03:56:44', '2021-02-14 03:56:44');
INSERT INTO `status_upload` VALUES (38, 'ra', 'D 8713 FH 2040334-20210214063319.xlsx', 'file/D 8713 FH 2040334-20210214063319.xlsx', 2, 18, NULL, 38, '2021-02-14 06:33:19', '2021-02-14 06:33:19');
INSERT INTO `status_upload` VALUES (39, 'mau', 'data 14feb210127-20210214065444.xlsx', 'file/data 14feb210127-20210214065444.xlsx', 2, 9, NULL, 39, '2021-02-14 06:54:44', '2021-02-14 06:54:44');
INSERT INTO `status_upload` VALUES (40, 'mau', 'data 14feb210127-20210214065731.xlsx', 'file/data 14feb210127-20210214065731.xlsx', 2, 9, NULL, 40, '2021-02-14 06:57:31', '2021-02-14 06:57:31');
INSERT INTO `status_upload` VALUES (41, 'mau', 'data 14feb210127-20210214070003.xlsx', 'file/data 14feb210127-20210214070003.xlsx', 2, 9, NULL, 41, '2021-02-14 07:00:03', '2021-02-14 07:00:03');
INSERT INTO `status_upload` VALUES (42, 'mau', 'data 14feb 21  0647-20210214070433.xlsx', 'file/data 14feb 21  0647-20210214070433.xlsx', 2, 9, NULL, 42, '2021-02-14 07:04:33', '2021-02-14 07:04:33');
INSERT INTO `status_upload` VALUES (43, 'mau', 'DATA 14FEB21 0918-20210214091919.xlsx', 'file/DATA 14FEB21 0918-20210214091919.xlsx', 2, 9, NULL, 43, '2021-02-14 09:19:19', '2021-02-14 09:19:19');
INSERT INTO `status_upload` VALUES (44, 'ra', 'D 8714 FH 2040335-20210214104433.xlsx', 'file/D 8714 FH 2040335-20210214104433.xlsx', 2, 18, NULL, 44, '2021-02-14 10:44:33', '2021-02-14 10:44:33');
INSERT INTO `status_upload` VALUES (45, 'ra', 'D 8713 FH 2040336-20210214113346.xlsx', 'file/D 8713 FH 2040336-20210214113346.xlsx', 2, 18, NULL, 45, '2021-02-14 11:33:46', '2021-02-14 11:33:46');
INSERT INTO `status_upload` VALUES (46, 'ra', 'D 8713 FH 2040336-20210214113440.xlsx', 'file/D 8713 FH 2040336-20210214113440.xlsx', 2, 18, NULL, 46, '2021-02-14 11:34:40', '2021-02-14 11:34:40');
INSERT INTO `status_upload` VALUES (47, 'ra', 'D 8714 FH 2040337-20210214170307.xlsx', 'file/D 8714 FH 2040337-20210214170307.xlsx', 2, 18, NULL, 47, '2021-02-14 17:03:08', '2021-02-14 17:03:08');
INSERT INTO `status_upload` VALUES (48, '', 'data 14feb21 2146-20210214214728.xlsx', 'file/data 14feb21 2146-20210214214728.xlsx', 0, NULL, NULL, 48, '2021-02-14 21:47:28', '2021-02-14 21:47:28');
INSERT INTO `status_upload` VALUES (49, 'mau', 'data 14feb21 2146-20210214215456.xlsx', 'file/data 14feb21 2146-20210214215456.xlsx', 2, 9, NULL, 49, '2021-02-14 21:54:56', '2021-02-14 21:54:56');
INSERT INTO `status_upload` VALUES (50, 'ra', 'D 8714 FH 2040338-20210215010441.xlsx', 'file/D 8714 FH 2040338-20210215010441.xlsx', 2, 18, NULL, 50, '2021-02-15 01:04:41', '2021-02-15 01:04:41');
INSERT INTO `status_upload` VALUES (51, '', 'DATA 15FEB21 0244-20210215030100.xlsx', 'file/DATA 15FEB21 0244-20210215030100.xlsx', 0, NULL, NULL, 51, '2021-02-15 03:01:00', '2021-02-15 03:01:00');
INSERT INTO `status_upload` VALUES (52, 'mau', 'DATA 15FEB21 0244-20210215030152.xlsx', 'file/DATA 15FEB21 0244-20210215030152.xlsx', 2, 9, NULL, 52, '2021-02-15 03:01:52', '2021-02-15 03:01:52');
INSERT INTO `status_upload` VALUES (53, '', 'D 8714 FH 2040339-20210215032715.xlsx', 'file/D 8714 FH 2040339-20210215032715.xlsx', 0, NULL, NULL, 53, '2021-02-15 03:27:15', '2021-02-15 03:27:15');
INSERT INTO `status_upload` VALUES (54, 'ra', 'D 8714 FH 2040339-20210215032906.xlsx', 'file/D 8714 FH 2040339-20210215032906.xlsx', 2, 18, NULL, 54, '2021-02-15 03:29:06', '2021-02-15 03:29:06');
INSERT INTO `status_upload` VALUES (55, 'mau', 'DATA 15FEB21 0244-20210215032945.xlsx', 'file/DATA 15FEB21 0244-20210215032945.xlsx', 2, 9, NULL, 55, '2021-02-15 03:29:45', '2021-02-15 03:29:45');
INSERT INTO `status_upload` VALUES (56, 'ra', 'D 8714 FH 2040340-20210215044637.xlsx', 'file/D 8714 FH 2040340-20210215044637.xlsx', 2, 18, NULL, 56, '2021-02-15 04:46:37', '2021-02-15 04:46:37');
INSERT INTO `status_upload` VALUES (57, 'mau', 'DATA 15FEB21 0544 UPDATE-20210215054513.xlsx', 'file/DATA 15FEB21 0544 UPDATE-20210215054513.xlsx', 2, 9, NULL, 57, '2021-02-15 05:45:13', '2021-02-15 05:45:13');
INSERT INTO `status_upload` VALUES (58, 'ra', 'D 8054 ER 2040341-20210215102514.xlsx', 'file/D 8054 ER 2040341-20210215102514.xlsx', 2, 18, NULL, 58, '2021-02-15 10:25:14', '2021-02-15 10:25:14');
INSERT INTO `status_upload` VALUES (59, 'mau', 'DATA 15FEB21 1218-20210215121856.xlsx', 'file/DATA 15FEB21 1218-20210215121856.xlsx', 2, 9, NULL, 59, '2021-02-15 12:18:56', '2021-02-15 12:18:56');
INSERT INTO `status_upload` VALUES (60, 'mau', 'DATA 15FEB21 1217-20210215124515.xlsx', 'file/DATA 15FEB21 1217-20210215124515.xlsx', 2, 9, NULL, 60, '2021-02-15 12:45:15', '2021-02-15 12:45:15');
INSERT INTO `status_upload` VALUES (61, 'ra', 'D 8054 ER 2040342-20210215142912.xlsx', 'file/D 8054 ER 2040342-20210215142912.xlsx', 2, 18, NULL, 61, '2021-02-15 14:29:12', '2021-02-15 14:29:12');
INSERT INTO `status_upload` VALUES (62, '', 'DATA 15FEB21 1622-20210215162231.xlsx', 'file/DATA 15FEB21 1622-20210215162231.xlsx', 0, NULL, NULL, 62, '2021-02-15 16:22:31', '2021-02-15 16:22:31');
INSERT INTO `status_upload` VALUES (63, 'mau', 'DATA 15FEB21 1622-20210215162337.xlsx', 'file/DATA 15FEB21 1622-20210215162337.xlsx', 2, 9, NULL, 63, '2021-02-15 16:23:37', '2021-02-15 16:23:37');
INSERT INTO `status_upload` VALUES (64, 'ra', 'D 8714 FH 2040343-20210216015350.xlsx', 'file/D 8714 FH 2040343-20210216015350.xlsx', 2, 18, NULL, 64, '2021-02-16 01:53:50', '2021-02-16 01:53:50');
INSERT INTO `status_upload` VALUES (65, 'mau', 'DATA 16FEB21 0307 PAGI-20210216030919.xlsx', 'file/DATA 16FEB21 0307 PAGI-20210216030919.xlsx', 2, 9, NULL, 65, '2021-02-16 03:09:19', '2021-02-16 03:09:19');
INSERT INTO `status_upload` VALUES (66, 'ra', 'D 8714 FH 2040344-20210216045140.xlsx', 'file/D 8714 FH 2040344-20210216045140.xlsx', 2, 18, NULL, 66, '2021-02-16 04:51:40', '2021-02-16 04:51:40');
INSERT INTO `status_upload` VALUES (67, 'ra', 'D 8054 ER 2040345-20210216060434.xlsx', 'file/D 8054 ER 2040345-20210216060434.xlsx', 2, 18, NULL, 67, '2021-02-16 06:04:34', '2021-02-16 06:04:34');
INSERT INTO `status_upload` VALUES (68, 'mau', 'DATA 16FEB21 0555 PAGI-20210216061008.xlsx', 'file/DATA 16FEB21 0555 PAGI-20210216061008.xlsx', 2, 9, NULL, 68, '2021-02-16 06:10:08', '2021-02-16 06:10:08');
INSERT INTO `status_upload` VALUES (69, 'mau', 'DATA 16FEB21 0555 PAGI-20210216061321.xlsx', 'file/DATA 16FEB21 0555 PAGI-20210216061321.xlsx', 2, 9, NULL, 69, '2021-02-16 06:13:21', '2021-02-16 06:13:21');
INSERT INTO `status_upload` VALUES (70, 'mau', 'DATA 16FEB21 0555 PAGI-20210216061707.xlsx', 'file/DATA 16FEB21 0555 PAGI-20210216061707.xlsx', 2, 9, NULL, 70, '2021-02-16 06:17:07', '2021-02-16 06:17:07');
INSERT INTO `status_upload` VALUES (71, 'mau', 'DATA 16FEB21 0555 PAGI-20210216062201.xlsx', 'file/DATA 16FEB21 0555 PAGI-20210216062201.xlsx', 2, 9, NULL, 71, '2021-02-16 06:22:01', '2021-02-16 06:22:01');
INSERT INTO `status_upload` VALUES (72, 'mau', 'data 16feb21 0620-20210216065618.xlsx', 'file/data 16feb21 0620-20210216065618.xlsx', 2, 9, NULL, 72, '2021-02-16 06:56:18', '2021-02-16 06:56:18');
INSERT INTO `status_upload` VALUES (73, 'ra', 'qg6-20210216113608.xlsx', 'file/qg6-20210216113608.xlsx', 2, 18, NULL, 73, '2021-02-16 11:36:08', '2021-02-16 11:36:08');
INSERT INTO `status_upload` VALUES (74, 'ra', 'qg6-20210216113858.xlsx', 'file/qg6-20210216113858.xlsx', 2, 18, NULL, 74, '2021-02-16 11:38:58', '2021-02-16 11:38:58');
INSERT INTO `status_upload` VALUES (75, 'mau', 'data 16feb21 1300-20210216133254.xlsx', 'file/data 16feb21 1300-20210216133254.xlsx', 2, 9, NULL, 75, '2021-02-16 13:32:54', '2021-02-16 13:32:54');
INSERT INTO `status_upload` VALUES (76, 'ra', 'qg7-20210216141058.xlsx', 'file/qg7-20210216141058.xlsx', 2, 18, NULL, 76, '2021-02-16 14:10:58', '2021-02-16 14:10:58');
INSERT INTO `status_upload` VALUES (77, 'ra', 'qg8-20210216161823.xlsx', 'file/qg8-20210216161823.xlsx', 2, 18, NULL, 77, '2021-02-16 16:18:23', '2021-02-16 16:18:23');
INSERT INTO `status_upload` VALUES (78, 'ra', 'qg9-20210216184827.xlsx', 'file/qg9-20210216184827.xlsx', 2, 18, NULL, 78, '2021-02-16 18:48:27', '2021-02-16 18:48:27');
INSERT INTO `status_upload` VALUES (79, 'ra', 'gunk 123-20210217005340.xlsx', 'file/gunk 123-20210217005340.xlsx', 2, 18, NULL, 79, '2021-02-17 00:53:40', '2021-02-17 00:53:40');
INSERT INTO `status_upload` VALUES (80, 'ra', 'gunk 123-20210217010024.xlsx', 'file/gunk 123-20210217010024.xlsx', 2, 18, NULL, 80, '2021-02-17 01:00:24', '2021-02-17 01:00:24');
INSERT INTO `status_upload` VALUES (81, 'mau', 'DATA 17FEB21 0242-20210217024314.xlsx', 'file/DATA 17FEB21 0242-20210217024314.xlsx', 2, 9, NULL, 81, '2021-02-17 02:43:14', '2021-02-17 02:43:14');
INSERT INTO `status_upload` VALUES (82, 'mau', 'DATA 17FEB21 0600-20210217064231.xlsx', 'file/DATA 17FEB21 0600-20210217064231.xlsx', 2, 9, NULL, 82, '2021-02-17 06:42:31', '2021-02-17 06:42:31');
INSERT INTO `status_upload` VALUES (83, 'ra', 'data_RA_170221-20210217110410.xlsx', 'file/data_RA_170221-20210217110410.xlsx', 2, 18, NULL, 83, '2021-02-17 11:04:10', '2021-02-17 11:04:10');
INSERT INTO `status_upload` VALUES (84, 'mau', 'data 17feb21 1130-20210217114641.xlsx', 'file/data 17feb21 1130-20210217114641.xlsx', 2, 9, NULL, 84, '2021-02-17 11:46:41', '2021-02-17 11:46:41');
INSERT INTO `status_upload` VALUES (85, '', 'data_RA_170221_02-20210217131555.xlsx', 'file/data_RA_170221_02-20210217131555.xlsx', 0, NULL, NULL, 85, '2021-02-17 13:15:55', '2021-02-17 13:15:55');
INSERT INTO `status_upload` VALUES (86, 'ra', 'data_RA_170221_02-20210217131622.xlsx', 'file/data_RA_170221_02-20210217131622.xlsx', 2, 18, NULL, 86, '2021-02-17 13:16:22', '2021-02-17 13:16:22');
INSERT INTO `status_upload` VALUES (87, 'mau', 'DATA 18FEB21 0306-20210218031646.xlsx', 'file/DATA 18FEB21 0306-20210218031646.xlsx', 2, 9, NULL, 87, '2021-02-18 03:16:46', '2021-02-18 03:16:46');
INSERT INTO `status_upload` VALUES (88, 'ra', 'qg10-20210218035705.xlsx', 'file/qg10-20210218035705.xlsx', 2, 18, NULL, 88, '2021-02-18 03:57:05', '2021-02-18 03:57:05');
INSERT INTO `status_upload` VALUES (89, 'ra', 'qg10-20210218041053.xlsx', 'file/qg10-20210218041053.xlsx', 2, 18, NULL, 89, '2021-02-18 04:10:53', '2021-02-18 04:10:53');
INSERT INTO `status_upload` VALUES (90, 'ra', 'qg11-20210218045857.xlsx', 'file/qg11-20210218045857.xlsx', 2, 18, NULL, 90, '2021-02-18 04:58:57', '2021-02-18 04:58:57');
INSERT INTO `status_upload` VALUES (91, 'ra', 'qg12-20210218062045.xlsx', 'file/qg12-20210218062045.xlsx', 2, 18, NULL, 91, '2021-02-18 06:20:45', '2021-02-18 06:20:45');
INSERT INTO `status_upload` VALUES (92, '', 'DATA 18FEB21 0630-20210218063239.xlsx', 'file/DATA 18FEB21 0630-20210218063239.xlsx', 0, NULL, NULL, 92, '2021-02-18 06:32:39', '2021-02-18 06:32:39');
INSERT INTO `status_upload` VALUES (93, 'mau', 'DATA 18FEB21 0630-20210218063302.xlsx', 'file/DATA 18FEB21 0630-20210218063302.xlsx', 2, 9, NULL, 93, '2021-02-18 06:33:02', '2021-02-18 06:33:02');
INSERT INTO `status_upload` VALUES (94, 'mau', 'DATA 18FEB21 0858-20210218085956.xlsx', 'file/DATA 18FEB21 0858-20210218085956.xlsx', 2, 9, NULL, 94, '2021-02-18 08:59:56', '2021-02-18 08:59:56');
INSERT INTO `status_upload` VALUES (95, 'ra', 'data_RA_180221_01-20210218124454.xlsx', 'file/data_RA_180221_01-20210218124454.xlsx', 2, 18, NULL, 95, '2021-02-18 12:44:54', '2021-02-18 12:44:54');
INSERT INTO `status_upload` VALUES (96, 'ra', 'data_RA_180221_02-20210218134923.xlsx', 'file/data_RA_180221_02-20210218134923.xlsx', 2, 18, NULL, 96, '2021-02-18 13:49:23', '2021-02-18 13:49:23');
INSERT INTO `status_upload` VALUES (97, '', 'DATA 18FEB21 1556 INCOMING-20210218155640.xlsx', 'file/DATA 18FEB21 1556 INCOMING-20210218155640.xlsx', 0, NULL, NULL, 97, '2021-02-18 15:56:40', '2021-02-18 15:56:40');
INSERT INTO `status_upload` VALUES (98, 'mau', 'DATA 18FEB21 1556 INCOMING-20210218155702.xlsx', 'file/DATA 18FEB21 1556 INCOMING-20210218155702.xlsx', 2, 9, NULL, 98, '2021-02-18 15:57:02', '2021-02-18 15:57:02');
INSERT INTO `status_upload` VALUES (99, '', 'DATA 18FEB21 1827 INCOMING-20210218182810.xlsx', 'file/DATA 18FEB21 1827 INCOMING-20210218182810.xlsx', 0, NULL, NULL, 99, '2021-02-18 18:28:10', '2021-02-18 18:28:10');
INSERT INTO `status_upload` VALUES (100, 'mau', 'DATA 18FEB21 1827 INCOMING-20210218182834.xlsx', 'file/DATA 18FEB21 1827 INCOMING-20210218182834.xlsx', 2, 9, NULL, 100, '2021-02-18 18:28:34', '2021-02-18 18:28:34');
INSERT INTO `status_upload` VALUES (101, 'mau', 'DATA 18FEB21 1827 INCOMING-20210218183943.xlsx', 'file/DATA 18FEB21 1827 INCOMING-20210218183943.xlsx', 2, 9, NULL, 101, '2021-02-18 18:39:43', '2021-02-18 18:39:43');
INSERT INTO `status_upload` VALUES (102, 'ra', 'data_RA_190221_01-20210219015032.xlsx', 'file/data_RA_190221_01-20210219015032.xlsx', 2, 18, NULL, 102, '2021-02-19 01:50:32', '2021-02-19 01:50:32');
INSERT INTO `status_upload` VALUES (103, '', 'DATA 19FEB21 0200-20210219032556.xlsx', 'file/DATA 19FEB21 0200-20210219032556.xlsx', 0, NULL, NULL, 103, '2021-02-19 03:25:56', '2021-02-19 03:25:56');
INSERT INTO `status_upload` VALUES (104, 'mau', 'DATA 19FEB21 0200-20210219032806.xlsx', 'file/DATA 19FEB21 0200-20210219032806.xlsx', 2, 9, NULL, 104, '2021-02-19 03:28:06', '2021-02-19 03:28:06');
INSERT INTO `status_upload` VALUES (105, 'ra', 'data_RA_190221_02-20210219040603.xlsx', 'file/data_RA_190221_02-20210219040603.xlsx', 2, 18, NULL, 105, '2021-02-19 04:06:03', '2021-02-19 04:06:03');
INSERT INTO `status_upload` VALUES (106, 'ra', 'data_RA_190221_03-20210219055107.xlsx', 'file/data_RA_190221_03-20210219055107.xlsx', 2, 18, NULL, 106, '2021-02-19 05:51:07', '2021-02-19 05:51:07');
INSERT INTO `status_upload` VALUES (107, 'mau', 'data 19feb21 0630-20210219063841.xlsx', 'file/data 19feb21 0630-20210219063841.xlsx', 2, 9, NULL, 107, '2021-02-19 06:38:41', '2021-02-19 06:38:41');
INSERT INTO `status_upload` VALUES (108, 'ra', '38-20210219102000.xlsx', 'file/38-20210219102000.xlsx', 2, 18, NULL, 108, '2021-02-19 10:20:00', '2021-02-19 10:20:00');
INSERT INTO `status_upload` VALUES (109, '', 'DATA 19FEB21 1119-20210219112025.xlsx', 'file/DATA 19FEB21 1119-20210219112025.xlsx', 0, NULL, NULL, 109, '2021-02-19 11:20:25', '2021-02-19 11:20:25');
INSERT INTO `status_upload` VALUES (110, 'mau', 'DATA 19FEB21 1119-20210219112047.xlsx', 'file/DATA 19FEB21 1119-20210219112047.xlsx', 2, 9, NULL, 110, '2021-02-19 11:20:47', '2021-02-19 11:20:47');
INSERT INTO `status_upload` VALUES (111, 'mau', 'DATA 19FEB21 1334-20210219133523.xlsx', 'file/DATA 19FEB21 1334-20210219133523.xlsx', 2, 9, NULL, 111, '2021-02-19 13:35:23', '2021-02-19 13:36:01');
INSERT INTO `status_upload` VALUES (112, 'mau', 'DATA 19FEB21 1334-20210219133714.xlsx', 'file/DATA 19FEB21 1334-20210219133714.xlsx', 2, 9, NULL, 112, '2021-02-19 13:37:14', '2021-02-19 13:38:02');
INSERT INTO `status_upload` VALUES (113, 'ra', 'data_RA_190221_69-20210219171804.xlsx', 'file/data_RA_190221_69-20210219171804.xlsx', 2, 18, NULL, 113, '2021-02-19 17:18:04', '2021-02-19 17:19:01');
INSERT INTO `status_upload` VALUES (114, 'ra', 'data_RA_200221_01-20210220003323.xlsx', 'file/data_RA_200221_01-20210220003323.xlsx', 2, 18, NULL, 114, '2021-02-20 00:33:23', '2021-02-20 00:34:01');
INSERT INTO `status_upload` VALUES (115, 'mau', 'data 20feb21 0000-20210220013742.xlsx', 'file/data 20feb21 0000-20210220013742.xlsx', 2, 9, NULL, 115, '2021-02-20 01:37:42', '2021-02-20 01:38:01');
INSERT INTO `status_upload` VALUES (116, 'ra', 'data_RA_200221_02-20210220042059.xlsx', 'file/data_RA_200221_02-20210220042059.xlsx', 2, 18, NULL, 116, '2021-02-20 04:20:59', '2021-02-20 04:21:01');
INSERT INTO `status_upload` VALUES (117, 'ra', 'data_RA_200221_03-20210220061156.xlsx', 'file/data_RA_200221_03-20210220061156.xlsx', 2, 18, NULL, 117, '2021-02-20 06:11:56', '2021-02-20 06:12:01');
INSERT INTO `status_upload` VALUES (118, 'mau', 'data 20feb21 0630-20210220063931.xlsx', 'file/data 20feb21 0630-20210220063931.xlsx', 2, 9, NULL, 118, '2021-02-20 06:39:31', '2021-02-20 06:40:01');
INSERT INTO `status_upload` VALUES (119, 'ra', 'data_RA_200221_04-20210220104733.xlsx', 'file/data_RA_200221_04-20210220104733.xlsx', 2, 18, NULL, 119, '2021-02-20 10:47:33', '2021-02-20 10:48:02');
INSERT INTO `status_upload` VALUES (120, 'mau', 'DATA 20FEB21 1130-20210220113224.xlsx', 'file/DATA 20FEB21 1130-20210220113224.xlsx', 2, 9, NULL, 120, '2021-02-20 11:32:24', '2021-02-20 11:33:01');
INSERT INTO `status_upload` VALUES (121, 'mau', 'DATA 20FEB21 1130-20210220113353.xlsx', 'file/DATA 20FEB21 1130-20210220113353.xlsx', 2, 9, NULL, 121, '2021-02-20 11:33:53', '2021-02-20 11:34:02');
INSERT INTO `status_upload` VALUES (122, 'ra', 'data_RA_200221_05-20210220125440.xlsx', 'file/data_RA_200221_05-20210220125440.xlsx', 2, 18, NULL, 122, '2021-02-20 12:54:40', '2021-02-20 12:55:01');
INSERT INTO `status_upload` VALUES (123, 'mau', 'DATA 20FEB21 1400-20210220140334.xlsx', 'file/DATA 20FEB21 1400-20210220140334.xlsx', 2, 9, NULL, 123, '2021-02-20 14:03:34', '2021-02-20 14:04:01');
INSERT INTO `status_upload` VALUES (124, 'ra', 'data_RA_200221_06-20210220231129.xlsx', 'file/data_RA_200221_06-20210220231129.xlsx', 2, 18, NULL, 124, '2021-02-20 23:11:29', '2021-02-20 23:12:01');
INSERT INTO `status_upload` VALUES (125, 'mau', 'DATA 21FEB21 0313-20210221031434.xlsx', 'file/DATA 21FEB21 0313-20210221031434.xlsx', 2, 9, NULL, 125, '2021-02-21 03:14:34', '2021-02-21 03:15:02');
INSERT INTO `status_upload` VALUES (126, 'mau', 'DATA 21FEB21 0313-20210221031555.xlsx', 'file/DATA 21FEB21 0313-20210221031555.xlsx', 2, 9, NULL, 126, '2021-02-21 03:15:55', '2021-02-21 03:16:01');
INSERT INTO `status_upload` VALUES (127, 'ra', 'data_RA_210221_01-20210221035018.xlsx', 'file/data_RA_210221_01-20210221035018.xlsx', 2, 18, NULL, 127, '2021-02-21 03:50:18', '2021-02-21 03:51:02');
INSERT INTO `status_upload` VALUES (128, 'mau', 'DATA 21FEB21 0432 EDIT-20210221043959.xlsx', 'file/DATA 21FEB21 0432 EDIT-20210221043959.xlsx', 2, 9, NULL, 128, '2021-02-21 04:39:59', '2021-02-21 04:40:01');
INSERT INTO `status_upload` VALUES (129, 'ra', 'data_RA_210221_02-20210221051650.xlsx', 'file/data_RA_210221_02-20210221051650.xlsx', 2, 18, NULL, 129, '2021-02-21 05:16:50', '2021-02-21 05:17:01');
INSERT INTO `status_upload` VALUES (130, 'ra', 'data_RA_210221_03-20210221060745.xlsx', 'file/data_RA_210221_03-20210221060745.xlsx', 2, 18, NULL, 130, '2021-02-21 06:07:45', '2021-02-21 06:08:02');
INSERT INTO `status_upload` VALUES (131, 'mau', 'DATA 21FEB21 0618-20210221061850.xlsx', 'file/DATA 21FEB21 0618-20210221061850.xlsx', 2, 9, NULL, 131, '2021-02-21 06:18:50', '2021-02-21 06:19:01');
INSERT INTO `status_upload` VALUES (132, 'mau', 'DATA 21FEB21 0432 EDITT-20210221063547.xlsx', 'file/DATA 21FEB21 0432 EDITT-20210221063547.xlsx', 2, 9, NULL, 132, '2021-02-21 06:35:47', '2021-02-21 06:36:01');
INSERT INTO `status_upload` VALUES (133, 'mau', 'DATA 21FEB21 0701-20210221070126.xlsx', 'file/DATA 21FEB21 0701-20210221070126.xlsx', 2, 9, NULL, 133, '2021-02-21 07:01:26', '2021-02-21 07:02:01');
INSERT INTO `status_upload` VALUES (134, 'ra', 'data_RA_210221_04-20210221083931.xlsx', 'file/data_RA_210221_04-20210221083931.xlsx', 2, 18, NULL, 134, '2021-02-21 08:39:31', '2021-02-21 08:40:01');
INSERT INTO `status_upload` VALUES (135, 'ra', 'data_RA_210221_05-20210221101229.xlsx', 'file/data_RA_210221_05-20210221101229.xlsx', 2, 18, NULL, 135, '2021-02-21 10:12:29', '2021-02-21 10:13:02');
INSERT INTO `status_upload` VALUES (136, 'ra', 'data_RA_210221_06-20210221110300.xlsx', 'file/data_RA_210221_06-20210221110300.xlsx', 2, 18, NULL, 136, '2021-02-21 11:03:00', '2021-02-21 11:03:01');
INSERT INTO `status_upload` VALUES (137, 'mau', 'DATA 21FEB21 1140-20210221114934.xlsx', 'file/DATA 21FEB21 1140-20210221114934.xlsx', 2, 9, NULL, 137, '2021-02-21 11:49:34', '2021-02-21 11:50:02');
INSERT INTO `status_upload` VALUES (138, 'ra', 'qg13-20210221132133.xlsx', 'file/qg13-20210221132133.xlsx', 2, 18, NULL, 138, '2021-02-21 13:21:33', '2021-02-21 13:22:01');
INSERT INTO `status_upload` VALUES (139, '', 'DATA 21FEB21 1500-20210221152111.xlsx', 'file/DATA 21FEB21 1500-20210221152111.xlsx', 0, NULL, NULL, 139, '2021-02-21 15:21:11', '2021-02-21 15:21:11');
INSERT INTO `status_upload` VALUES (140, 'mau', 'DATA 21FEB21 1500-20210221152143.xlsx', 'file/DATA 21FEB21 1500-20210221152143.xlsx', 2, 9, NULL, 140, '2021-02-21 15:21:43', '2021-02-21 15:22:01');
INSERT INTO `status_upload` VALUES (141, 'mau', 'DATA 22FEB21 0159-20210222020010.xlsx', 'file/DATA 22FEB21 0159-20210222020010.xlsx', 2, 9, NULL, 141, '2021-02-22 02:00:10', '2021-02-22 02:01:02');
INSERT INTO `status_upload` VALUES (142, 'ra', 'data_RA_210221_58-20210222032855.xlsx', 'file/data_RA_210221_58-20210222032855.xlsx', 2, 18, NULL, 142, '2021-02-22 03:28:55', '2021-02-22 03:29:01');
INSERT INTO `status_upload` VALUES (143, 'mau', 'DATA 22FEB21 0404-20210222041416.xlsx', 'file/DATA 22FEB21 0404-20210222041416.xlsx', 2, 9, NULL, 143, '2021-02-22 04:14:16', '2021-02-22 04:15:01');
INSERT INTO `status_upload` VALUES (144, 'mau', 'DATA 22FEB21 0510-20210222051500.xlsx', 'file/DATA 22FEB21 0510-20210222051500.xlsx', 2, 9, NULL, 144, '2021-02-22 05:15:00', '2021-02-22 05:15:01');
INSERT INTO `status_upload` VALUES (145, 'mau', 'DATA 22FEB21 0510-20210222055121.xlsx', 'file/DATA 22FEB21 0510-20210222055121.xlsx', 2, 9, NULL, 145, '2021-02-22 05:51:21', '2021-02-22 05:52:01');
INSERT INTO `status_upload` VALUES (146, 'mau', 'DATA 22FEB21 0404-20210222060226.xlsx', 'file/DATA 22FEB21 0404-20210222060226.xlsx', 2, 9, NULL, 146, '2021-02-22 06:02:26', '2021-02-22 06:03:01');
INSERT INTO `status_upload` VALUES (147, 'mau', 'DATA 22FEB21 1100-20210222111008.xlsx', 'file/DATA 22FEB21 1100-20210222111008.xlsx', 2, 9, NULL, 147, '2021-02-22 11:10:08', '2021-02-22 11:11:01');
INSERT INTO `status_upload` VALUES (148, 'mau', 'DATA 23FEB21 0100-20210223010349.xlsx', 'file/DATA 23FEB21 0100-20210223010349.xlsx', 2, 9, NULL, 148, '2021-02-23 01:03:49', '2021-02-23 01:04:01');
INSERT INTO `status_upload` VALUES (149, 'ra', 'data_RA_210221_99-20210223014144.xlsx', 'file/data_RA_210221_99-20210223014144.xlsx', 2, 18, NULL, 149, '2021-02-23 01:41:44', '2021-02-23 01:42:02');
INSERT INTO `status_upload` VALUES (150, 'ra', 'data_RA_210221_91-20210223044232.xlsx', 'file/data_RA_210221_91-20210223044232.xlsx', 2, 18, NULL, 150, '2021-02-23 04:42:32', '2021-02-23 04:43:01');
INSERT INTO `status_upload` VALUES (151, 'mau', 'data 23 feb 21 0530-20210223053438.xlsx', 'file/data 23 feb 21 0530-20210223053438.xlsx', 2, 9, NULL, 151, '2021-02-23 05:34:38', '2021-02-23 05:35:01');
INSERT INTO `status_upload` VALUES (152, 'mau', 'DATA 23FEB21 0635-20210223063353.xlsx', 'file/DATA 23FEB21 0635-20210223063353.xlsx', 2, 9, NULL, 152, '2021-02-23 06:33:53', '2021-02-23 06:34:01');
INSERT INTO `status_upload` VALUES (153, 'ra', 'data_RA_210221_92-20210223070944.xlsx', 'file/data_RA_210221_92-20210223070944.xlsx', 2, 18, NULL, 153, '2021-02-23 07:09:44', '2021-02-23 07:10:01');
INSERT INTO `status_upload` VALUES (154, 'mau', 'DATA23FEB21 1100-20210223115931.xlsx', 'file/DATA23FEB21 1100-20210223115931.xlsx', 2, 9, NULL, 154, '2021-02-23 11:59:31', '2021-02-23 12:00:01');
INSERT INTO `status_upload` VALUES (155, 'ra', 'qg14-20210223123255.xlsx', 'file/qg14-20210223123255.xlsx', 2, 18, NULL, 155, '2021-02-23 12:32:55', '2021-02-23 12:33:01');
INSERT INTO `status_upload` VALUES (156, 'ra', 'qg15-20210223125343.xlsx', 'file/qg15-20210223125343.xlsx', 2, 18, NULL, 156, '2021-02-23 12:53:43', '2021-02-23 12:54:02');
INSERT INTO `status_upload` VALUES (157, 'mau', 'DATA23FEB21 1315-20210223134621.xlsx', 'file/DATA23FEB21 1315-20210223134621.xlsx', 2, 9, NULL, 157, '2021-02-23 13:46:21', '2021-02-23 13:47:01');
INSERT INTO `status_upload` VALUES (158, 'mau', 'DATA 23FEB21 1600-20210223165939.xlsx', 'file/DATA 23FEB21 1600-20210223165939.xlsx', 2, 9, NULL, 158, '2021-02-23 16:59:39', '2021-02-23 17:00:02');
INSERT INTO `status_upload` VALUES (159, 'mau', 'DATA 24FEB21 0235-20210224041100.xlsx', 'file/DATA 24FEB21 0235-20210224041100.xlsx', 2, 9, NULL, 159, '2021-02-24 04:11:00', '2021-02-24 04:11:02');
INSERT INTO `status_upload` VALUES (160, 'ra', 'data_RA_240221_01-20210224042326.xlsx', 'file/data_RA_240221_01-20210224042326.xlsx', 2, 18, NULL, 160, '2021-02-24 04:23:26', '2021-02-24 04:24:02');
INSERT INTO `status_upload` VALUES (161, 'mau', 'DATA 24FEB21 0527-20210224052942.xlsx', 'file/DATA 24FEB21 0527-20210224052942.xlsx', 2, 9, NULL, 161, '2021-02-24 05:29:42', '2021-02-24 05:30:01');
INSERT INTO `status_upload` VALUES (162, 'mau', 'DATA 24FEB21 0527-20210224073313.xlsx', 'file/DATA 24FEB21 0527-20210224073313.xlsx', 2, 9, NULL, 162, '2021-02-24 07:33:13', '2021-02-24 07:34:01');
INSERT INTO `status_upload` VALUES (163, '', 'DATA 24FEB21 1136-20210224113701.xlsx', 'file/DATA 24FEB21 1136-20210224113701.xlsx', 0, NULL, NULL, 163, '2021-02-24 11:37:01', '2021-02-24 11:37:01');
INSERT INTO `status_upload` VALUES (164, 'mau', 'DATA 24FEB21 1136-20210224113720.xlsx', 'file/DATA 24FEB21 1136-20210224113720.xlsx', 2, 9, NULL, 164, '2021-02-24 11:37:20', '2021-02-24 11:38:01');
INSERT INTO `status_upload` VALUES (165, 'ra', 'data_RA_240221_400-20210224122314.xlsx', 'file/data_RA_240221_400-20210224122314.xlsx', 2, 18, NULL, 165, '2021-02-24 12:23:14', '2021-02-24 12:24:01');
INSERT INTO `status_upload` VALUES (166, 'ra', 'data_RA_240221_401-20210224140005.xlsx', 'file/data_RA_240221_401-20210224140005.xlsx', 2, 18, NULL, 166, '2021-02-24 14:00:05', '2021-02-24 14:01:02');
INSERT INTO `status_upload` VALUES (167, 'mau', 'DATA 24FEB21 1449-20210224145838.xlsx', 'file/DATA 24FEB21 1449-20210224145838.xlsx', 2, 9, NULL, 167, '2021-02-24 14:58:38', '2021-02-24 14:59:01');
INSERT INTO `status_upload` VALUES (168, 'ra', 'data_RA_250221_402-20210225004719.xlsx', 'file/data_RA_250221_402-20210225004719.xlsx', 2, 18, NULL, 168, '2021-02-25 00:47:19', '2021-02-25 00:48:01');
INSERT INTO `status_upload` VALUES (169, 'ra', 'qg16-20210225012832.xlsx', 'file/qg16-20210225012832.xlsx', 2, 18, NULL, 169, '2021-02-25 01:28:32', '2021-02-25 01:29:01');
INSERT INTO `status_upload` VALUES (170, 'mau', 'DATA 25FEB21 0230-20210225030141.xlsx', 'file/DATA 25FEB21 0230-20210225030141.xlsx', 2, 9, NULL, 170, '2021-02-25 03:01:41', '2021-02-25 03:02:01');
INSERT INTO `status_upload` VALUES (171, 'mau', 'DATA 25FEB21 0600-20210225063350.xlsx', 'file/DATA 25FEB21 0600-20210225063350.xlsx', 2, 9, NULL, 171, '2021-02-25 06:33:50', '2021-02-25 06:34:01');
INSERT INTO `status_upload` VALUES (172, 'ra', 'data_RA_250221_404-20210225114532.xlsx', 'file/data_RA_250221_404-20210225114532.xlsx', 2, 18, NULL, 172, '2021-02-25 11:45:32', '2021-02-25 11:46:01');
INSERT INTO `status_upload` VALUES (173, 'mau', 'Report-20210225130752.xls', 'file/Report-20210225130752.xls', 9, 9, NULL, 173, '2021-02-25 13:07:52', '2021-02-25 13:07:52');
INSERT INTO `status_upload` VALUES (174, 'mau', 'DATA 25FEB21 1359 EDIT-20210225140355.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225140355.xlsx', 2, 9, NULL, 174, '2021-02-25 14:03:55', '2021-02-25 14:04:01');
INSERT INTO `status_upload` VALUES (175, 'ra', 'data_RA_250221_407-20210225140720.xlsx', 'file/data_RA_250221_407-20210225140720.xlsx', 2, 18, NULL, 175, '2021-02-25 14:07:20', '2021-02-25 14:08:02');
INSERT INTO `status_upload` VALUES (176, 'mau', 'DATA 25FEB21 1359 EDIT-20210225141028.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225141028.xlsx', 9, 9, NULL, 176, '2021-02-25 14:10:28', '2021-02-25 14:11:01');
INSERT INTO `status_upload` VALUES (177, 'mau', 'DATA 25FEB21 1359 EDIT-20210225141255.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225141255.xlsx', 9, 9, NULL, 177, '2021-02-25 14:12:55', '2021-02-25 14:13:01');
INSERT INTO `status_upload` VALUES (178, 'mau', 'DATA 25FEB21 1359 EDIT-20210225141355.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225141355.xlsx', 9, 9, NULL, 178, '2021-02-25 14:13:55', '2021-02-25 14:14:01');
INSERT INTO `status_upload` VALUES (179, 'mau', 'DATA 25FEB21 1359 EDIT-20210225141445.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225141445.xlsx', 2, 9, NULL, 179, '2021-02-25 14:14:45', '2021-02-25 14:15:01');
INSERT INTO `status_upload` VALUES (180, 'mau', 'DATA 25FEB21 1359 EDIT-20210225141844.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225141844.xlsx', 9, 9, NULL, 180, '2021-02-25 14:18:44', '2021-02-25 14:18:44');
INSERT INTO `status_upload` VALUES (181, 'mau', 'DATA 25FEB21 1359 EDIT-20210225142213.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225142213.xlsx', 9, 9, NULL, 181, '2021-02-25 14:22:13', '2021-02-25 14:23:01');
INSERT INTO `status_upload` VALUES (182, 'mau', 'DATA 25FEB21 1359 EDIT-20210225142549.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225142549.xlsx', 9, 9, NULL, 182, '2021-02-25 14:25:49', '2021-02-25 14:26:02');
INSERT INTO `status_upload` VALUES (183, 'mau', 'DATA 25FEB21 1359 EDIT-20210225142620.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225142620.xlsx', 9, 9, NULL, 183, '2021-02-25 14:26:20', '2021-02-25 14:27:01');
INSERT INTO `status_upload` VALUES (184, 'mau', 'DATA 25FEB21 1359 EDIT-20210225142839.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225142839.xlsx', 9, 9, NULL, 184, '2021-02-25 14:28:39', '2021-02-25 14:29:01');
INSERT INTO `status_upload` VALUES (185, 'mau', 'DATA 25FEB21 1359 EDIT-20210225143103.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225143103.xlsx', 9, 9, NULL, 185, '2021-02-25 14:31:03', '2021-02-25 14:31:25');
INSERT INTO `status_upload` VALUES (186, 'mau', 'DATA 25FEB21 1359 EDIT-20210225143208.xlsx', 'file/DATA 25FEB21 1359 EDIT-20210225143208.xlsx', 9, 9, NULL, 186, '2021-02-25 14:32:08', '2021-02-25 14:33:01');
INSERT INTO `status_upload` VALUES (187, 'mau', 'DATA 25FEB21 1359-20210225143405.xlsx', 'file/DATA 25FEB21 1359-20210225143405.xlsx', 2, 9, NULL, 187, '2021-02-25 14:34:05', '2021-02-25 14:35:01');
INSERT INTO `status_upload` VALUES (188, 'mau', 'DATA 25FEB21 1359-20210225144135.xlsx', 'file/DATA 25FEB21 1359-20210225144135.xlsx', 2, 9, NULL, 188, '2021-02-25 14:41:35', '2021-02-25 14:42:01');
INSERT INTO `status_upload` VALUES (189, 'mau', 'DATA 25FEB21 1359-20210225144314.xlsx', 'file/DATA 25FEB21 1359-20210225144314.xlsx', 2, 9, NULL, 189, '2021-02-25 14:43:14', '2021-02-25 14:44:01');
INSERT INTO `status_upload` VALUES (190, 'mau', 'DATA 25FEB21 1359-20210225144741.xlsx', 'file/DATA 25FEB21 1359-20210225144741.xlsx', 2, 9, NULL, 190, '2021-02-25 14:47:41', '2021-02-25 14:48:02');
INSERT INTO `status_upload` VALUES (191, 'mau', 'DATA 25FEB21 1359 T-20210225154938.xlsx', 'file/DATA 25FEB21 1359 T-20210225154938.xlsx', 2, 9, NULL, 191, '2021-02-25 15:49:38', '2021-02-25 15:50:01');
INSERT INTO `status_upload` VALUES (192, 'ra', 'data_RA_250221_409-20210226020537.xlsx', 'file/data_RA_250221_409-20210226020537.xlsx', 2, 18, NULL, 192, '2021-02-26 02:05:37', '2021-02-26 02:06:01');
INSERT INTO `status_upload` VALUES (193, '', 'DATA 26FEB21 0130-20210226023216.xlsx', 'file/DATA 26FEB21 0130-20210226023216.xlsx', 0, NULL, NULL, 193, '2021-02-26 02:32:16', '2021-02-26 02:32:16');
INSERT INTO `status_upload` VALUES (194, 'mau', 'DATA 26FEB21 0130-20210226023541.xlsx', 'file/DATA 26FEB21 0130-20210226023541.xlsx', 2, 9, NULL, 194, '2021-02-26 02:35:41', '2021-02-26 02:36:01');
INSERT INTO `status_upload` VALUES (195, 'ra', 'data_RA_250221_408-20210226041007.xlsx', 'file/data_RA_250221_408-20210226041007.xlsx', 2, 18, NULL, 195, '2021-02-26 04:10:07', '2021-02-26 04:11:01');
INSERT INTO `status_upload` VALUES (196, 'ra', 'data_RA_250221_410-20210226051754.xlsx', 'file/data_RA_250221_410-20210226051754.xlsx', 2, 18, NULL, 196, '2021-02-26 05:17:54', '2021-02-26 05:18:01');
INSERT INTO `status_upload` VALUES (197, 'mau', 'DATA 26FEB21 0530-20210226055925.xlsx', 'file/DATA 26FEB21 0530-20210226055925.xlsx', 2, 9, NULL, 197, '2021-02-26 05:59:25', '2021-02-26 06:00:01');
INSERT INTO `status_upload` VALUES (198, 'ra', 'data_RA_250221_411-20210226062946.xlsx', 'file/data_RA_250221_411-20210226062946.xlsx', 2, 18, NULL, 198, '2021-02-26 06:29:46', '2021-02-26 06:30:02');
INSERT INTO `status_upload` VALUES (199, 'mau', 'DATA 26FEB21 0900-20210226094648.xlsx', 'file/DATA 26FEB21 0900-20210226094648.xlsx', 2, 9, NULL, 199, '2021-02-26 09:46:48', '2021-02-26 09:47:01');
INSERT INTO `status_upload` VALUES (200, 'mau', 'DATA 26FEB21 1312-20210226131354.xlsx', 'file/DATA 26FEB21 1312-20210226131354.xlsx', 2, 9, NULL, 200, '2021-02-26 13:13:54', '2021-02-26 13:14:01');
INSERT INTO `status_upload` VALUES (201, 'mau', 'DATA 26FEB21 1312 edit pku-20210226140809.xlsx', 'file/DATA 26FEB21 1312 edit pku-20210226140809.xlsx', 2, 9, NULL, 201, '2021-02-26 14:08:09', '2021-02-26 14:09:01');
INSERT INTO `status_upload` VALUES (202, 'mau', 'DATA 26FEB21 1312 edit pku-20210226141112.xlsx', 'file/DATA 26FEB21 1312 edit pku-20210226141112.xlsx', 2, 9, NULL, 202, '2021-02-26 14:11:12', '2021-02-26 14:12:01');
INSERT INTO `status_upload` VALUES (203, 'mau', 'DATA 26FEB21 1520-20210226152306.xlsx', 'file/DATA 26FEB21 1520-20210226152306.xlsx', 2, 9, NULL, 203, '2021-02-26 15:23:06', '2021-02-26 15:24:01');
INSERT INTO `status_upload` VALUES (204, 'ra', 'data_RA_250221_415-20210227000536.xlsx', 'file/data_RA_250221_415-20210227000536.xlsx', 2, 18, NULL, 204, '2021-02-27 00:05:36', '2021-02-27 00:06:01');
INSERT INTO `status_upload` VALUES (205, 'mau', 'DATA 27FEB21 0143 1-20210227015245.xlsx', 'file/DATA 27FEB21 0143 1-20210227015245.xlsx', 2, 9, NULL, 205, '2021-02-27 01:52:45', '2021-02-27 01:53:01');
INSERT INTO `status_upload` VALUES (206, 'ra', 'data_RA_250221_416-20210227024403.xlsx', 'file/data_RA_250221_416-20210227024403.xlsx', 2, 18, NULL, 206, '2021-02-27 02:44:03', '2021-02-27 02:45:02');
INSERT INTO `status_upload` VALUES (207, 'mau', 'DATA 27FEB21 0143 2-20210227040017.xlsx', 'file/DATA 27FEB21 0143 2-20210227040017.xlsx', 2, 9, NULL, 207, '2021-02-27 04:00:17', '2021-02-27 04:01:02');
INSERT INTO `status_upload` VALUES (208, 'ra', 'data_RA_250221_417-20210227040917.xlsx', 'file/data_RA_250221_417-20210227040917.xlsx', 2, 18, NULL, 208, '2021-02-27 04:09:17', '2021-02-27 04:10:02');
INSERT INTO `status_upload` VALUES (209, 'mau', 'DATA 27FEB21 0506-20210227051549.xlsx', 'file/DATA 27FEB21 0506-20210227051549.xlsx', 2, 9, NULL, 209, '2021-02-27 05:15:49', '2021-02-27 05:16:01');
INSERT INTO `status_upload` VALUES (210, 'ra', 'data_RA_250221_418-20210227070227.xlsx', 'file/data_RA_250221_418-20210227070227.xlsx', 2, 18, NULL, 210, '2021-02-27 07:02:27', '2021-02-27 07:03:01');
INSERT INTO `status_upload` VALUES (211, 'ra', 'qg17-20210227111128.xlsx', 'file/qg17-20210227111128.xlsx', 2, 18, NULL, 211, '2021-02-27 11:11:28', '2021-02-27 11:12:01');
INSERT INTO `status_upload` VALUES (212, 'ra', 'qg18-20210227123249.xlsx', 'file/qg18-20210227123249.xlsx', 2, 18, NULL, 212, '2021-02-27 12:32:49', '2021-02-27 12:33:01');
INSERT INTO `status_upload` VALUES (213, 'mau', 'DATA 27FEB21 1635-20210227164930.xlsx', 'file/DATA 27FEB21 1635-20210227164930.xlsx', 2, 9, NULL, 213, '2021-02-27 16:49:30', '2021-02-27 16:50:02');
INSERT INTO `status_upload` VALUES (214, 'mau', 'DATA 27FEB21 1630-20210227165433.xlsx', 'file/DATA 27FEB21 1630-20210227165433.xlsx', 2, 9, NULL, 214, '2021-02-27 16:54:33', '2021-02-27 16:55:01');
INSERT INTO `status_upload` VALUES (215, 'mau', 'DATA 28FEB21 0219-20210228022033.xlsx', 'file/DATA 28FEB21 0219-20210228022033.xlsx', 2, 9, NULL, 215, '2021-02-28 02:20:33', '2021-02-28 02:21:02');
INSERT INTO `status_upload` VALUES (216, '', 'DATA 28FEB21 0705-20210228070638.xlsx', 'file/DATA 28FEB21 0705-20210228070638.xlsx', 0, NULL, NULL, 216, '2021-02-28 07:06:38', '2021-02-28 07:06:38');
INSERT INTO `status_upload` VALUES (217, 'mau', 'DATA 28FEB21 0705-20210228070751.xlsx', 'file/DATA 28FEB21 0705-20210228070751.xlsx', 2, 9, NULL, 217, '2021-02-28 07:07:51', '2021-02-28 07:08:02');
INSERT INTO `status_upload` VALUES (218, 'mau', 'DATA 28FEB21 0955-20210228095353.xlsx', 'file/DATA 28FEB21 0955-20210228095353.xlsx', 2, 9, NULL, 218, '2021-02-28 09:53:53', '2021-02-28 09:54:01');
INSERT INTO `status_upload` VALUES (219, '', 'data_RA_260221_01-20210228104734.xlsx', 'file/data_RA_260221_01-20210228104734.xlsx', 0, NULL, NULL, 219, '2021-02-28 10:47:34', '2021-02-28 10:47:34');
INSERT INTO `status_upload` VALUES (220, 'ra', 'data_RA_260221_01-20210228104842.xlsx', 'file/data_RA_260221_01-20210228104842.xlsx', 2, 18, NULL, 220, '2021-02-28 10:48:42', '2021-02-28 10:49:01');
INSERT INTO `status_upload` VALUES (221, 'ra', 'data_RA_260221_02-20210228105052.xlsx', 'file/data_RA_260221_02-20210228105052.xlsx', 2, 18, NULL, 221, '2021-02-28 10:50:52', '2021-02-28 10:51:02');
INSERT INTO `status_upload` VALUES (222, 'ra', 'data_RA_260221_03-20210228124007.xlsx', 'file/data_RA_260221_03-20210228124007.xlsx', 2, 18, NULL, 222, '2021-02-28 12:40:07', '2021-02-28 12:41:01');
INSERT INTO `status_upload` VALUES (223, 'mau', 'DATA 01MAR21 0145-20210301014533.xlsx', 'file/DATA 01MAR21 0145-20210301014533.xlsx', 2, 9, NULL, 223, '2021-03-01 01:45:33', '2021-03-01 01:46:01');
INSERT INTO `status_upload` VALUES (224, 'mau', 'DATA 01MAR21 0310-20210301031919.xlsx', 'file/DATA 01MAR21 0310-20210301031919.xlsx', 2, 9, NULL, 224, '2021-03-01 03:19:19', '2021-03-01 03:20:02');
INSERT INTO `status_upload` VALUES (225, 'mau', 'DATA 01MAR21 0430-20210301043000.xlsx', 'file/DATA 01MAR21 0430-20210301043000.xlsx', 2, 9, NULL, 225, '2021-03-01 04:30:00', '2021-03-01 04:30:01');
INSERT INTO `status_upload` VALUES (226, 'ra', 'data_RA_260221_430-20210301044807.xlsx', 'file/data_RA_260221_430-20210301044807.xlsx', 2, 18, NULL, 226, '2021-03-01 04:48:07', '2021-03-01 04:49:01');
INSERT INTO `status_upload` VALUES (227, 'mau', 'DATA 01MAR21 0530-20210301053207.xlsx', 'file/DATA 01MAR21 0530-20210301053207.xlsx', 2, 9, NULL, 227, '2021-03-01 05:32:07', '2021-03-01 05:33:01');
INSERT INTO `status_upload` VALUES (228, 'ra', 'Book1-20210301075208.xlsx', 'file/Book1-20210301075208.xlsx', 2, 18, NULL, 228, '2021-03-01 07:52:08', '2021-03-01 07:53:01');
INSERT INTO `status_upload` VALUES (229, 'ra', 'data_RA_010321_431-20210301104345.xlsx', 'file/data_RA_010321_431-20210301104345.xlsx', 2, 18, NULL, 229, '2021-03-01 10:43:45', '2021-03-01 10:44:01');
INSERT INTO `status_upload` VALUES (230, 'ra', 'data_RA_010321_432-20210301143125.xlsx', 'file/data_RA_010321_432-20210301143125.xlsx', 2, 18, NULL, 230, '2021-03-01 14:31:25', '2021-03-01 14:32:01');
INSERT INTO `status_upload` VALUES (231, 'mau', 'DATA 02MAR21 0310-20210302031324.xlsx', 'file/DATA 02MAR21 0310-20210302031324.xlsx', 2, 9, NULL, 231, '2021-03-02 03:13:24', '2021-03-02 03:14:01');
INSERT INTO `status_upload` VALUES (232, 'ra', 'qg19-20210302051102.xlsx', 'file/qg19-20210302051102.xlsx', 2, 18, NULL, 232, '2021-03-02 05:11:02', '2021-03-02 05:12:02');
INSERT INTO `status_upload` VALUES (233, 'ra', 'qg20-20210302060450.xlsx', 'file/qg20-20210302060450.xlsx', 2, 18, NULL, 233, '2021-03-02 06:04:50', '2021-03-02 06:05:01');
INSERT INTO `status_upload` VALUES (234, 'ra', 'qg19-20210302060515.xlsx', 'file/qg19-20210302060515.xlsx', 2, 18, NULL, 234, '2021-03-02 06:05:15', '2021-03-02 06:06:01');
INSERT INTO `status_upload` VALUES (235, 'mau', 'DATA 02MAR21 0620-20210302062156.xlsx', 'file/DATA 02MAR21 0620-20210302062156.xlsx', 2, 9, NULL, 235, '2021-03-02 06:21:56', '2021-03-02 06:22:01');
INSERT INTO `status_upload` VALUES (236, 'mau', 'DATA 02MAR21 0650-20210302065511.xlsx', 'file/DATA 02MAR21 0650-20210302065511.xlsx', 2, 9, NULL, 236, '2021-03-02 06:55:11', '2021-03-02 06:56:01');
INSERT INTO `status_upload` VALUES (237, 'ra', 'data_RA_020321_436-20210302104257.xlsx', 'file/data_RA_020321_436-20210302104257.xlsx', 2, 18, NULL, 237, '2021-03-02 10:42:57', '2021-03-02 10:43:01');
INSERT INTO `status_upload` VALUES (238, 'mau', 'DATA 02MAR21 11500-20210302115233.xlsx', 'file/DATA 02MAR21 11500-20210302115233.xlsx', 2, 9, NULL, 238, '2021-03-02 11:52:33', '2021-03-02 11:53:01');
INSERT INTO `status_upload` VALUES (239, 'ra', 'data_RA_020321_438-20210302133040.xlsx', 'file/data_RA_020321_438-20210302133040.xlsx', 2, 18, NULL, 239, '2021-03-02 13:30:40', '2021-03-02 13:31:02');
INSERT INTO `status_upload` VALUES (240, '', 'DATA 02MAR21 1357-20210302135734.xlsx', 'file/DATA 02MAR21 1357-20210302135734.xlsx', 0, NULL, NULL, 240, '2021-03-02 13:57:34', '2021-03-02 13:57:34');
INSERT INTO `status_upload` VALUES (241, 'mau', 'DATA 02MAR21 1357-20210302135751.xlsx', 'file/DATA 02MAR21 1357-20210302135751.xlsx', 2, 9, NULL, 241, '2021-03-02 13:57:51', '2021-03-02 13:58:01');
INSERT INTO `status_upload` VALUES (242, 'ra', 'qg21-20210303013042.xlsx', 'file/qg21-20210303013042.xlsx', 2, 18, NULL, 242, '2021-03-03 01:30:42', '2021-03-03 01:31:02');
INSERT INTO `status_upload` VALUES (243, 'mau', 'DATA 03MAR21 0130-20210303023742.xlsx', 'file/DATA 03MAR21 0130-20210303023742.xlsx', 2, 9, NULL, 243, '2021-03-03 02:37:42', '2021-03-03 02:38:01');
INSERT INTO `status_upload` VALUES (244, 'ra', 'qg22-20210303043037.xlsx', 'file/qg22-20210303043037.xlsx', 2, 18, NULL, 244, '2021-03-03 04:30:37', '2021-03-03 04:31:01');
INSERT INTO `status_upload` VALUES (245, 'ra', 'qg23-20210303054448.xlsx', 'file/qg23-20210303054448.xlsx', 2, 18, NULL, 245, '2021-03-03 05:44:48', '2021-03-03 05:45:01');
INSERT INTO `status_upload` VALUES (246, 'mau', 'DATA 03MAR21 0600-20210303075259.xlsx', 'file/DATA 03MAR21 0600-20210303075259.xlsx', 2, 9, NULL, 246, '2021-03-03 07:52:59', '2021-03-03 07:53:01');
INSERT INTO `status_upload` VALUES (247, 'mau', 'DATA 03MAR21 0844-20210303084554.xlsx', 'file/DATA 03MAR21 0844-20210303084554.xlsx', 2, 9, NULL, 247, '2021-03-03 08:45:54', '2021-03-03 08:46:01');
INSERT INTO `status_upload` VALUES (248, 'ra', 'data_RA_260221_443-20210303100500.xlsx', 'file/data_RA_260221_443-20210303100500.xlsx', 2, 18, NULL, 248, '2021-03-03 10:05:00', '2021-03-03 10:05:01');
INSERT INTO `status_upload` VALUES (249, '', 'DATA 03MAR21 08440-20210303111224.xlsx', 'file/DATA 03MAR21 08440-20210303111224.xlsx', 0, NULL, NULL, 249, '2021-03-03 11:12:24', '2021-03-03 11:12:24');
INSERT INTO `status_upload` VALUES (250, 'mau', 'DATA 03MAR21 08440-20210303111239.xlsx', 'file/DATA 03MAR21 08440-20210303111239.xlsx', 2, 9, NULL, 250, '2021-03-03 11:12:39', '2021-03-03 11:13:01');
INSERT INTO `status_upload` VALUES (251, 'ra', 'data_RA_260221_444-20210303111249.xlsx', 'file/data_RA_260221_444-20210303111249.xlsx', 2, 18, NULL, 251, '2021-03-03 11:12:49', '2021-03-03 11:13:01');
INSERT INTO `status_upload` VALUES (252, 'mau', '03MAR21 12480-20210303125119.xlsx', 'file/03MAR21 12480-20210303125119.xlsx', 2, 9, NULL, 252, '2021-03-03 12:51:19', '2021-03-03 12:52:01');
INSERT INTO `status_upload` VALUES (253, 'ra', 'data_RA_260221_445-20210304012834.xlsx', 'file/data_RA_260221_445-20210304012834.xlsx', 2, 18, NULL, 253, '2021-03-04 01:28:34', '2021-03-04 01:29:01');
INSERT INTO `status_upload` VALUES (254, 'ra', 'data_RA_260221_446-20210304013813.xlsx', 'file/data_RA_260221_446-20210304013813.xlsx', 9, 18, NULL, 254, '2021-03-04 01:38:13', '2021-03-04 01:39:01');
INSERT INTO `status_upload` VALUES (255, 'mau', 'DATA 04MAR21 0200-20210304030122.xlsx', 'file/DATA 04MAR21 0200-20210304030122.xlsx', 2, 9, NULL, 255, '2021-03-04 03:01:22', '2021-03-04 03:02:01');
INSERT INTO `status_upload` VALUES (256, 'ra', 'data_RA_260221_447-20210304044226.xlsx', 'file/data_RA_260221_447-20210304044226.xlsx', 2, 18, NULL, 256, '2021-03-04 04:42:26', '2021-03-04 04:43:01');
INSERT INTO `status_upload` VALUES (257, 'ra', 'data_RA_260221_448-20210304060550.xlsx', 'file/data_RA_260221_448-20210304060550.xlsx', 2, 18, NULL, 257, '2021-03-04 06:05:50', '2021-03-04 06:06:02');
INSERT INTO `status_upload` VALUES (258, 'mau', 'DATA 04MAR21 0600-20210304070817.xlsx', 'file/DATA 04MAR21 0600-20210304070817.xlsx', 2, 9, NULL, 258, '2021-03-04 07:08:17', '2021-03-04 07:09:01');
INSERT INTO `status_upload` VALUES (259, 'mau', 'DATA 04MAR21 0600-20210304090726.xlsx', 'file/DATA 04MAR21 0600-20210304090726.xlsx', 2, 9, NULL, 259, '2021-03-04 09:07:26', '2021-03-04 09:08:01');
INSERT INTO `status_upload` VALUES (260, 'mau', 'DATA 04MAR21 1218-20210304121916.xlsx', 'file/DATA 04MAR21 1218-20210304121916.xlsx', 2, 9, NULL, 260, '2021-03-04 12:19:16', '2021-03-04 12:20:02');
INSERT INTO `status_upload` VALUES (261, 'ra', 'QG24-20210304130317.xlsx', 'file/QG24-20210304130317.xlsx', 2, 18, NULL, 261, '2021-03-04 13:03:17', '2021-03-04 13:04:02');
INSERT INTO `status_upload` VALUES (262, 'mau', 'DATA 04MAR21 1420-20210304142111.xlsx', 'file/DATA 04MAR21 1420-20210304142111.xlsx', 2, 9, NULL, 262, '2021-03-04 14:21:11', '2021-03-04 14:22:01');
INSERT INTO `status_upload` VALUES (263, 'ra', 'data_RA_260221_453-20210305001336.xlsx', 'file/data_RA_260221_453-20210305001336.xlsx', 2, 18, NULL, 263, '2021-03-05 00:13:36', '2021-03-05 00:14:01');
INSERT INTO `status_upload` VALUES (264, 'mau', 'DATA 05MAR21 05050-20210305021308.xlsx', 'file/DATA 05MAR21 05050-20210305021308.xlsx', 2, 9, NULL, 264, '2021-03-05 02:13:08', '2021-03-05 02:14:01');
INSERT INTO `status_upload` VALUES (265, 'ra', 'data_RA_260221_454-20210305043735.xlsx', 'file/data_RA_260221_454-20210305043735.xlsx', 2, 18, NULL, 265, '2021-03-05 04:37:35', '2021-03-05 04:38:01');
INSERT INTO `status_upload` VALUES (266, 'ra', 'data_RA_260221_455-20210305053953.xlsx', 'file/data_RA_260221_455-20210305053953.xlsx', 2, 18, NULL, 266, '2021-03-05 05:39:53', '2021-03-05 05:40:01');
INSERT INTO `status_upload` VALUES (267, 'mau', 'DATA 5MAR21 06170-20210305061927.xlsx', 'file/DATA 5MAR21 06170-20210305061927.xlsx', 2, 9, NULL, 267, '2021-03-05 06:19:27', '2021-03-05 06:20:02');
INSERT INTO `status_upload` VALUES (268, 'mau', 'DATA 05MA21 07120-20210305071400.xlsx', 'file/DATA 05MA21 07120-20210305071400.xlsx', 2, 9, NULL, 268, '2021-03-05 07:14:00', '2021-03-05 07:14:01');
INSERT INTO `status_upload` VALUES (269, 'ra', 'D 8714 FH 2040346-20210305113650.xlsx', 'file/D 8714 FH 2040346-20210305113650.xlsx', 2, 18, NULL, 269, '2021-03-05 11:36:50', '2021-03-05 11:37:02');
INSERT INTO `status_upload` VALUES (270, 'mau', 'DATA 05MAR21 1245-20210305124252.xlsx', 'file/DATA 05MAR21 1245-20210305124252.xlsx', 2, 9, NULL, 270, '2021-03-05 12:42:52', '2021-03-05 12:43:01');
INSERT INTO `status_upload` VALUES (271, 'mau', 'DATA 06MAR21 04040-20210306010955.xlsx', 'file/DATA 06MAR21 04040-20210306010955.xlsx', 2, 9, NULL, 271, '2021-03-06 01:09:55', '2021-03-06 01:10:01');
INSERT INTO `status_upload` VALUES (272, 'mau', 'DATA 06MAR21 040400-20210306025354.xlsx', 'file/DATA 06MAR21 040400-20210306025354.xlsx', 2, 9, NULL, 272, '2021-03-06 02:53:54', '2021-03-06 02:54:01');
INSERT INTO `status_upload` VALUES (273, '', 'DATA 06MAR21 05370-20210306054034.xlsx', 'file/DATA 06MAR21 05370-20210306054034.xlsx', 0, NULL, NULL, 273, '2021-03-06 05:40:34', '2021-03-06 05:40:34');
INSERT INTO `status_upload` VALUES (274, 'mau', 'DATA 06MAR21 05370-20210306054155.xlsx', 'file/DATA 06MAR21 05370-20210306054155.xlsx', 2, 9, NULL, 274, '2021-03-06 05:41:55', '2021-03-06 05:42:01');
INSERT INTO `status_upload` VALUES (275, 'ra', 'qg25-20210306072842.xlsx', 'file/qg25-20210306072842.xlsx', 2, 18, NULL, 275, '2021-03-06 07:28:42', '2021-03-06 07:29:01');
INSERT INTO `status_upload` VALUES (276, 'mau', 'DATA 06MAR21 1100-20210306114234.xlsx', 'file/DATA 06MAR21 1100-20210306114234.xlsx', 2, 9, NULL, 276, '2021-03-06 11:42:34', '2021-03-06 11:43:02');
INSERT INTO `status_upload` VALUES (277, 'mau', 'DATA 06MAR21 1315-20210306132433.xlsx', 'file/DATA 06MAR21 1315-20210306132433.xlsx', 2, 9, NULL, 277, '2021-03-06 13:24:33', '2021-03-06 13:25:02');
INSERT INTO `status_upload` VALUES (278, 'mau', 'DATA 07MAR21 0530-20210307053605.xlsx', 'file/DATA 07MAR21 0530-20210307053605.xlsx', 2, 9, NULL, 278, '2021-03-07 05:36:05', '2021-03-07 05:37:01');
INSERT INTO `status_upload` VALUES (279, 'mau', 'DATA 07MAR21 0625-20210307062708.xlsx', 'file/DATA 07MAR21 0625-20210307062708.xlsx', 2, 9, NULL, 279, '2021-03-07 06:27:08', '2021-03-07 06:28:01');
INSERT INTO `status_upload` VALUES (280, 'mau', 'DATA 07MAR21 0645-20210307064659.xlsx', 'file/DATA 07MAR21 0645-20210307064659.xlsx', 2, 9, NULL, 280, '2021-03-07 06:46:59', '2021-03-07 06:47:01');
INSERT INTO `status_upload` VALUES (281, 'ra', 'data_RA_260221_468-20210308001242.xlsx', 'file/data_RA_260221_468-20210308001242.xlsx', 2, 18, NULL, 281, '2021-03-08 00:12:42', '2021-03-08 00:13:01');
INSERT INTO `status_upload` VALUES (282, 'mau', 'DATA 08MAR21 0130-20210308013908.xlsx', 'file/DATA 08MAR21 0130-20210308013908.xlsx', 2, 9, NULL, 282, '2021-03-08 01:39:08', '2021-03-08 01:40:01');
INSERT INTO `status_upload` VALUES (283, 'ra', 'data_RA_260221_470-20210308032109.xlsx', 'file/data_RA_260221_470-20210308032109.xlsx', 2, 18, NULL, 283, '2021-03-08 03:21:09', '2021-03-08 03:22:01');
INSERT INTO `status_upload` VALUES (284, 'mau', 'DATA 08MAR21 0330-20210308033614.xlsx', 'file/DATA 08MAR21 0330-20210308033614.xlsx', 2, 9, NULL, 284, '2021-03-08 03:36:14', '2021-03-08 03:37:02');
INSERT INTO `status_upload` VALUES (285, 'mau', 'DATA 08MAR21 0500-20210308050958.xlsx', 'file/DATA 08MAR21 0500-20210308050958.xlsx', 2, 9, NULL, 285, '2021-03-08 05:09:58', '2021-03-08 05:10:01');
INSERT INTO `status_upload` VALUES (286, 'mau', 'DATA 08MAR21 0650-20210308065700.xlsx', 'file/DATA 08MAR21 0650-20210308065700.xlsx', 2, 9, NULL, 286, '2021-03-08 06:57:00', '2021-03-08 06:57:01');
INSERT INTO `status_upload` VALUES (287, 'ra', 'data_RA_260221_471-20210308073640.xlsx', 'file/data_RA_260221_471-20210308073640.xlsx', 2, 18, NULL, 287, '2021-03-08 07:36:40', '2021-03-08 07:37:01');
INSERT INTO `status_upload` VALUES (288, 'ra', 'data_RA_260221_472-20210308132657.xlsx', 'file/data_RA_260221_472-20210308132657.xlsx', 2, 18, NULL, 288, '2021-03-08 13:26:57', '2021-03-08 13:27:02');
INSERT INTO `status_upload` VALUES (289, '', 'DATA 08MAR21 13550-20210308135629.xlsx', 'file/DATA 08MAR21 13550-20210308135629.xlsx', 0, NULL, NULL, 289, '2021-03-08 13:56:29', '2021-03-08 13:56:29');
INSERT INTO `status_upload` VALUES (290, 'mau', 'DATA 08MAR21 13550-20210308135647.xlsx', 'file/DATA 08MAR21 13550-20210308135647.xlsx', 2, 9, NULL, 290, '2021-03-08 13:56:47', '2021-03-08 13:57:01');
INSERT INTO `status_upload` VALUES (291, 'mau', 'DATA 09MAR21 0500-20210309053723.xlsx', 'file/DATA 09MAR21 0500-20210309053723.xlsx', 2, 9, NULL, 291, '2021-03-09 05:37:23', '2021-03-09 05:38:02');
INSERT INTO `status_upload` VALUES (292, 'mau', 'DATA 09MAR21 0744-20210309074638.xlsx', 'file/DATA 09MAR21 0744-20210309074638.xlsx', 2, 9, NULL, 292, '2021-03-09 07:46:38', '2021-03-09 07:47:01');
INSERT INTO `status_upload` VALUES (293, '', 'DATA 09MAR21 11310-20210309113309.xlsx', 'file/DATA 09MAR21 11310-20210309113309.xlsx', 0, NULL, NULL, 293, '2021-03-09 11:33:09', '2021-03-09 11:33:09');
INSERT INTO `status_upload` VALUES (294, 'mau', 'DATA 09MAR21 11310-20210309113326.xlsx', 'file/DATA 09MAR21 11310-20210309113326.xlsx', 2, 9, NULL, 294, '2021-03-09 11:33:26', '2021-03-09 11:34:01');
INSERT INTO `status_upload` VALUES (295, 'ra', 'data_RA_260221_477-20210309114107.xlsx', 'file/data_RA_260221_477-20210309114107.xlsx', 2, 18, NULL, 295, '2021-03-09 11:41:07', '2021-03-09 11:42:01');
INSERT INTO `status_upload` VALUES (296, 'ra', 'data_RA_100321_01-20210310014405.xlsx', 'file/data_RA_100321_01-20210310014405.xlsx', 2, 18, NULL, 296, '2021-03-10 01:44:05', '2021-03-10 01:45:01');
INSERT INTO `status_upload` VALUES (297, 'ra', 'data_RA_100321_02-20210310025957.xlsx', 'file/data_RA_100321_02-20210310025957.xlsx', 2, 18, NULL, 297, '2021-03-10 02:59:57', '2021-03-10 03:00:01');
INSERT INTO `status_upload` VALUES (298, 'ra', 'data_RA_100321_03-20210310041146.xlsx', 'file/data_RA_100321_03-20210310041146.xlsx', 2, 18, NULL, 298, '2021-03-10 04:11:46', '2021-03-10 04:12:01');
INSERT INTO `status_upload` VALUES (299, 'mau', 'DATA 10MARET21 0300-20210310045121.xlsx', 'file/DATA 10MARET21 0300-20210310045121.xlsx', 2, 9, NULL, 299, '2021-03-10 04:51:21', '2021-03-10 04:52:01');
INSERT INTO `status_upload` VALUES (300, 'ra', 'data_RA_100321_04-20210310052954.xlsx', 'file/data_RA_100321_04-20210310052954.xlsx', 2, 18, NULL, 300, '2021-03-10 05:29:54', '2021-03-10 05:30:01');
INSERT INTO `status_upload` VALUES (301, 'mau', 'DATA 10MAR21 1145-20210310114746.xlsx', 'file/DATA 10MAR21 1145-20210310114746.xlsx', 2, 9, NULL, 301, '2021-03-10 11:47:46', '2021-03-10 11:48:01');
INSERT INTO `status_upload` VALUES (302, 'ra', 'data_RA_110321_01-20210311014827.xlsx', 'file/data_RA_110321_01-20210311014827.xlsx', 2, 18, NULL, 302, '2021-03-11 01:48:27', '2021-03-11 01:49:02');
INSERT INTO `status_upload` VALUES (303, 'mau', 'DATA 11MAR21 03260-20210311033030.xlsx', 'file/DATA 11MAR21 03260-20210311033030.xlsx', 2, 9, NULL, 303, '2021-03-11 03:30:30', '2021-03-11 03:31:01');
INSERT INTO `status_upload` VALUES (304, 'ra', 'data_RA_110321_02-20210311040119.xlsx', 'file/data_RA_110321_02-20210311040119.xlsx', 2, 18, NULL, 304, '2021-03-11 04:01:19', '2021-03-11 04:02:02');
INSERT INTO `status_upload` VALUES (305, 'ra', 'data_RA_110321_03-20210311053750.xlsx', 'file/data_RA_110321_03-20210311053750.xlsx', 2, 18, NULL, 305, '2021-03-11 05:37:50', '2021-03-11 05:38:01');
INSERT INTO `status_upload` VALUES (306, '', 'DATA 11MAR21 06440-20210311064439.xlsx', 'file/DATA 11MAR21 06440-20210311064439.xlsx', 0, NULL, NULL, 306, '2021-03-11 06:44:39', '2021-03-11 06:44:39');
INSERT INTO `status_upload` VALUES (307, 'mau', 'DATA 11MAR21 06440-20210311064459.xlsx', 'file/DATA 11MAR21 06440-20210311064459.xlsx', 2, 9, NULL, 307, '2021-03-11 06:44:59', '2021-03-11 06:45:01');
INSERT INTO `status_upload` VALUES (308, 'mau', 'DATA 11MAR21 06440-20210311064716.xlsx', 'file/DATA 11MAR21 06440-20210311064716.xlsx', 2, 9, NULL, 308, '2021-03-11 06:47:16', '2021-03-11 06:48:01');
INSERT INTO `status_upload` VALUES (309, 'ra', 'data_RA_110321_04-20210311101550.xlsx', 'file/data_RA_110321_04-20210311101550.xlsx', 2, 18, NULL, 309, '2021-03-11 10:15:50', '2021-03-11 10:16:01');
INSERT INTO `status_upload` VALUES (310, 'mau', 'DATA 11MAR21 1335-20210311133534.xlsx', 'file/DATA 11MAR21 1335-20210311133534.xlsx', 2, 9, NULL, 310, '2021-03-11 13:35:34', '2021-03-11 13:36:01');
INSERT INTO `status_upload` VALUES (311, 'mau', 'DATA 12MAR21 01520-20210312015618.xlsx', 'file/DATA 12MAR21 01520-20210312015618.xlsx', 2, 9, NULL, 311, '2021-03-12 01:56:18', '2021-03-12 01:57:02');
INSERT INTO `status_upload` VALUES (312, 'ra', 'data_RA_260221_490-20210312023032.xlsx', 'file/data_RA_260221_490-20210312023032.xlsx', 2, 18, NULL, 312, '2021-03-12 02:30:32', '2021-03-12 02:31:01');
INSERT INTO `status_upload` VALUES (313, 'mau', 'DATA 12MAR21 03260-20210312032721.xlsx', 'file/DATA 12MAR21 03260-20210312032721.xlsx', 2, 9, NULL, 313, '2021-03-12 03:27:21', '2021-03-12 03:28:02');
INSERT INTO `status_upload` VALUES (314, 'ra', 'data_RA_260221_491-20210312053603.xlsx', 'file/data_RA_260221_491-20210312053603.xlsx', 2, 18, NULL, 314, '2021-03-12 05:36:03', '2021-03-12 05:37:01');
INSERT INTO `status_upload` VALUES (315, '', 'DATA 12MAR21 03260-20210312061409.xlsx', 'file/DATA 12MAR21 03260-20210312061409.xlsx', 0, NULL, NULL, 315, '2021-03-12 06:14:09', '2021-03-12 06:14:09');
INSERT INTO `status_upload` VALUES (316, 'mau', 'DATA 12MAR21 03260-20210312061425.xlsx', 'file/DATA 12MAR21 03260-20210312061425.xlsx', 2, 9, NULL, 316, '2021-03-12 06:14:25', '2021-03-12 06:15:01');
INSERT INTO `status_upload` VALUES (317, 'mau', 'DATA 12MAR21 0645-20210312064740.xlsx', 'file/DATA 12MAR21 0645-20210312064740.xlsx', 2, 9, NULL, 317, '2021-03-12 06:47:40', '2021-03-12 06:48:01');
INSERT INTO `status_upload` VALUES (318, 'ra', 'data_RA_260221_492-20210312070022.xlsx', 'file/data_RA_260221_492-20210312070022.xlsx', 2, 18, NULL, 318, '2021-03-12 07:00:22', '2021-03-12 07:01:01');
INSERT INTO `status_upload` VALUES (319, 'ra', 'data_RA_120321_01-20210312102801.xlsx', 'file/data_RA_120321_01-20210312102801.xlsx', 2, 18, NULL, 319, '2021-03-12 10:28:01', '2021-03-12 10:29:01');
INSERT INTO `status_upload` VALUES (320, 'mau', 'DATA 12MAR21 1000-20210312114613.xlsx', 'file/DATA 12MAR21 1000-20210312114613.xlsx', 2, 9, NULL, 320, '2021-03-12 11:46:13', '2021-03-12 11:47:02');
INSERT INTO `status_upload` VALUES (321, 'ra', 'data_RA_130321_01-20210313011437.xlsx', 'file/data_RA_130321_01-20210313011437.xlsx', 2, 18, NULL, 321, '2021-03-13 01:14:37', '2021-03-13 01:15:01');
INSERT INTO `status_upload` VALUES (322, 'ra', 'data_RA_130321_02-20210313021423.xlsx', 'file/data_RA_130321_02-20210313021423.xlsx', 2, 18, NULL, 322, '2021-03-13 02:14:23', '2021-03-13 02:15:01');
INSERT INTO `status_upload` VALUES (323, 'mau', 'DATA 13MAR21 0200-20210313024706.xlsx', 'file/DATA 13MAR21 0200-20210313024706.xlsx', 2, 9, NULL, 323, '2021-03-13 02:47:06', '2021-03-13 02:48:01');
INSERT INTO `status_upload` VALUES (324, 'ra', 'data_RA_130321_03-20210313044456.xlsx', 'file/data_RA_130321_03-20210313044456.xlsx', 2, 18, NULL, 324, '2021-03-13 04:44:56', '2021-03-13 04:45:02');
INSERT INTO `status_upload` VALUES (325, 'mau', 'DATA 13MAR21 0400-20210313045815.xlsx', 'file/DATA 13MAR21 0400-20210313045815.xlsx', 2, 9, NULL, 325, '2021-03-13 04:58:15', '2021-03-13 04:59:01');
INSERT INTO `status_upload` VALUES (326, 'ra', 'data_RA_130321_04-20210313052032.xlsx', 'file/data_RA_130321_04-20210313052032.xlsx', 2, 18, NULL, 326, '2021-03-13 05:20:32', '2021-03-13 05:21:01');
INSERT INTO `status_upload` VALUES (327, 'mau', 'DATA 13MAR21 0530-20210313060141.xlsx', 'file/DATA 13MAR21 0530-20210313060141.xlsx', 2, 9, NULL, 327, '2021-03-13 06:01:41', '2021-03-13 06:02:01');
INSERT INTO `status_upload` VALUES (328, 'mau', 'DATA 13MAR21 0615-20210313062938.xlsx', 'file/DATA 13MAR21 0615-20210313062938.xlsx', 2, 9, NULL, 328, '2021-03-13 06:29:38', '2021-03-13 06:30:02');
INSERT INTO `status_upload` VALUES (329, 'mau', 'DATA 13MAR21 0615-20210313095647.xlsx', 'file/DATA 13MAR21 0615-20210313095647.xlsx', 2, 9, NULL, 329, '2021-03-13 09:56:47', '2021-03-13 09:57:01');
INSERT INTO `status_upload` VALUES (330, 'ra', 'data_RA_130321_05-20210313101415.xlsx', 'file/data_RA_130321_05-20210313101415.xlsx', 2, 18, NULL, 330, '2021-03-13 10:14:15', '2021-03-13 10:15:01');
INSERT INTO `status_upload` VALUES (331, 'mau', 'DATA 13MAR21 1100-20210313120949.xlsx', 'file/DATA 13MAR21 1100-20210313120949.xlsx', 2, 9, NULL, 331, '2021-03-13 12:09:49', '2021-03-13 12:10:01');
INSERT INTO `status_upload` VALUES (332, 'mau', 'DATA 14MAR21 0350-20210314035754.xlsx', 'file/DATA 14MAR21 0350-20210314035754.xlsx', 2, 9, NULL, 332, '2021-03-14 03:57:54', '2021-03-14 03:58:01');
INSERT INTO `status_upload` VALUES (333, 'mau', 'DATA 14MAR21 0550-20210314060449.xlsx', 'file/DATA 14MAR21 0550-20210314060449.xlsx', 2, 9, NULL, 333, '2021-03-14 06:04:49', '2021-03-14 06:05:01');
INSERT INTO `status_upload` VALUES (334, 'ra', 'qg26-20210314063552.xlsx', 'file/qg26-20210314063552.xlsx', 2, 18, NULL, 334, '2021-03-14 06:35:52', '2021-03-14 06:36:01');
INSERT INTO `status_upload` VALUES (335, 'mau', 'DATA 14,AR21 0714-20210314071609.xlsx', 'file/DATA 14,AR21 0714-20210314071609.xlsx', 2, 9, NULL, 335, '2021-03-14 07:16:09', '2021-03-14 07:17:01');
INSERT INTO `status_upload` VALUES (336, 'ra', 'data_RA_140321_01-20210314124644.xlsx', 'file/data_RA_140321_01-20210314124644.xlsx', 2, 18, NULL, 336, '2021-03-14 12:46:44', '2021-03-14 12:47:02');
INSERT INTO `status_upload` VALUES (337, '', 'DATA 14MAR21 1321-20210314132413.xlsx', 'file/DATA 14MAR21 1321-20210314132413.xlsx', 0, NULL, NULL, 337, '2021-03-14 13:24:13', '2021-03-14 13:24:13');
INSERT INTO `status_upload` VALUES (338, 'mau', 'DATA 14MAR21 1321-20210314132432.xlsx', 'file/DATA 14MAR21 1321-20210314132432.xlsx', 2, 9, NULL, 338, '2021-03-14 13:24:32', '2021-03-14 13:25:01');
INSERT INTO `status_upload` VALUES (339, 'mau', 'DATA 14MAR21 1321-20210314132617.xlsx', 'file/DATA 14MAR21 1321-20210314132617.xlsx', 2, 9, NULL, 339, '2021-03-14 13:26:17', '2021-03-14 13:27:01');
INSERT INTO `status_upload` VALUES (340, 'mau', 'DATA 14MAR21 1437-20210314143835.xlsx', 'file/DATA 14MAR21 1437-20210314143835.xlsx', 2, 9, NULL, 340, '2021-03-14 14:38:35', '2021-03-14 14:39:01');
INSERT INTO `status_upload` VALUES (341, 'ra', 'data_RA_150321_01-20210315001256.xlsx', 'file/data_RA_150321_01-20210315001256.xlsx', 2, 18, NULL, 341, '2021-03-15 00:12:56', '2021-03-15 00:13:01');
INSERT INTO `status_upload` VALUES (342, 'mau', 'DATA 15MAR21 0100-20210315014850.xlsx', 'file/DATA 15MAR21 0100-20210315014850.xlsx', 2, 9, NULL, 342, '2021-03-15 01:48:50', '2021-03-15 01:49:02');
INSERT INTO `status_upload` VALUES (343, 'ra', 'data_RA_150321_02-20210315032457.xlsx', 'file/data_RA_150321_02-20210315032457.xlsx', 2, 18, NULL, 343, '2021-03-15 03:24:57', '2021-03-15 03:25:01');
INSERT INTO `status_upload` VALUES (344, 'mau', 'DATA 15MAR21 0300-20210315045753.xlsx', 'file/DATA 15MAR21 0300-20210315045753.xlsx', 2, 9, NULL, 344, '2021-03-15 04:57:53', '2021-03-15 04:58:01');
INSERT INTO `status_upload` VALUES (345, 'ra', 'data_RA_150321_03-20210315111618.xlsx', 'file/data_RA_150321_03-20210315111618.xlsx', 2, 18, NULL, 345, '2021-03-15 11:16:18', '2021-03-15 11:17:02');
INSERT INTO `status_upload` VALUES (346, '', 'DATA 15MAR21 1205-20210315120608.xlsx', 'file/DATA 15MAR21 1205-20210315120608.xlsx', 0, NULL, NULL, 346, '2021-03-15 12:06:08', '2021-03-15 12:06:08');
INSERT INTO `status_upload` VALUES (347, 'mau', 'DATA 15MAR21 1205-20210315120624.xlsx', 'file/DATA 15MAR21 1205-20210315120624.xlsx', 2, 9, NULL, 347, '2021-03-15 12:06:24', '2021-03-15 12:07:01');
INSERT INTO `status_upload` VALUES (348, 'ra', 'data_RA_160321_01-20210316005623.xlsx', 'file/data_RA_160321_01-20210316005623.xlsx', 2, 18, NULL, 348, '2021-03-16 00:56:23', '2021-03-16 00:57:01');
INSERT INTO `status_upload` VALUES (349, 'mau', 'DATA 16MAR21 1200-20210316031123.xlsx', 'file/DATA 16MAR21 1200-20210316031123.xlsx', 2, 9, NULL, 349, '2021-03-16 03:11:23', '2021-03-16 03:12:01');
INSERT INTO `status_upload` VALUES (350, 'ra', 'data_RA_160321_02-20210316051624.xlsx', 'file/data_RA_160321_02-20210316051624.xlsx', 2, 18, NULL, 350, '2021-03-16 05:16:24', '2021-03-16 05:17:01');
INSERT INTO `status_upload` VALUES (351, 'ra', 'data_RA_160321_03-20210316061119.xlsx', 'file/data_RA_160321_03-20210316061119.xlsx', 2, 18, NULL, 351, '2021-03-16 06:11:19', '2021-03-16 06:12:01');
INSERT INTO `status_upload` VALUES (352, 'mau', 'DATA 16MAR21 0900-20210316092533.xlsx', 'file/DATA 16MAR21 0900-20210316092533.xlsx', 2, 9, NULL, 352, '2021-03-16 09:25:33', '2021-03-16 09:26:01');
INSERT INTO `status_upload` VALUES (353, 'mau', 'Book1 data 16mar21 1140-20210316114315.xlsx', 'file/Book1 data 16mar21 1140-20210316114315.xlsx', 2, 9, NULL, 353, '2021-03-16 11:43:15', '2021-03-16 11:44:01');
INSERT INTO `status_upload` VALUES (354, 'mau', 'DATA 16MAR21 1420-20210316142911.xlsx', 'file/DATA 16MAR21 1420-20210316142911.xlsx', 2, 9, NULL, 354, '2021-03-16 14:29:11', '2021-03-16 14:30:01');
INSERT INTO `status_upload` VALUES (355, 'ra', 'data_RA_170321_01-20210317011251.xlsx', 'file/data_RA_170321_01-20210317011251.xlsx', 2, 18, NULL, 355, '2021-03-17 01:12:51', '2021-03-17 01:13:01');
INSERT INTO `status_upload` VALUES (356, 'mau', 'DATA 17MAR21 0251-20210317025540.xlsx', 'file/DATA 17MAR21 0251-20210317025540.xlsx', 2, 9, NULL, 356, '2021-03-17 02:55:40', '2021-03-17 02:56:01');
INSERT INTO `status_upload` VALUES (357, 'ra', 'D 8714 FH 2040519-20210317050223.xlsx', 'file/D 8714 FH 2040519-20210317050223.xlsx', 2, 18, NULL, 357, '2021-03-17 05:02:23', '2021-03-17 05:03:02');
INSERT INTO `status_upload` VALUES (358, 'ra', 'D 8713 FH 2040520-20210317062723.xlsx', 'file/D 8713 FH 2040520-20210317062723.xlsx', 2, 18, NULL, 358, '2021-03-17 06:27:23', '2021-03-17 06:28:01');
INSERT INTO `status_upload` VALUES (359, '', 'DATA 17MAR21 0757-20210317075746.xlsx', 'file/DATA 17MAR21 0757-20210317075746.xlsx', 0, NULL, NULL, 359, '2021-03-17 07:57:46', '2021-03-17 07:57:46');
INSERT INTO `status_upload` VALUES (360, 'mau', 'DATA 17MAR21 0757-20210317075817.xlsx', 'file/DATA 17MAR21 0757-20210317075817.xlsx', 2, 9, NULL, 360, '2021-03-17 07:58:17', '2021-03-17 07:59:02');
INSERT INTO `status_upload` VALUES (361, 'ra', 'qg27-20210317110040.xlsx', 'file/qg27-20210317110040.xlsx', 2, 18, NULL, 361, '2021-03-17 11:00:40', '2021-03-17 11:01:01');
INSERT INTO `status_upload` VALUES (362, 'mau', 'DATA 17MAR21 1220-20210317122248.xlsx', 'file/DATA 17MAR21 1220-20210317122248.xlsx', 2, 9, NULL, 362, '2021-03-17 12:22:48', '2021-03-17 12:23:01');
INSERT INTO `status_upload` VALUES (363, 'ra', 'data_RA_260221_452-20210318022719.xlsx', 'file/data_RA_260221_452-20210318022719.xlsx', 2, 18, NULL, 363, '2021-03-18 02:27:19', '2021-03-18 02:28:02');
INSERT INTO `status_upload` VALUES (364, 'mau', 'DATA 18MAR21 0500-20210318050611.xlsx', 'file/DATA 18MAR21 0500-20210318050611.xlsx', 2, 9, NULL, 364, '2021-03-18 05:06:11', '2021-03-18 05:07:01');
INSERT INTO `status_upload` VALUES (365, 'ra', 'data_RA_260221_525-20210318055154.xlsx', 'file/data_RA_260221_525-20210318055154.xlsx', 2, 18, NULL, 365, '2021-03-18 05:51:54', '2021-03-18 05:52:01');
INSERT INTO `status_upload` VALUES (366, 'mau', 'DATA 18MAR21 0556-20210318055734.xlsx', 'file/DATA 18MAR21 0556-20210318055734.xlsx', 2, 9, NULL, 366, '2021-03-18 05:57:34', '2021-03-18 05:58:01');
INSERT INTO `status_upload` VALUES (367, 'ra', 'data_RA_260221_526-20210318062214.xlsx', 'file/data_RA_260221_526-20210318062214.xlsx', 2, 18, NULL, 367, '2021-03-18 06:22:14', '2021-03-18 06:23:01');
INSERT INTO `status_upload` VALUES (368, '', 'DATA 18MAR21 0700-20210318090459.xlsx', 'file/DATA 18MAR21 0700-20210318090459.xlsx', 0, NULL, NULL, 368, '2021-03-18 09:04:59', '2021-03-18 09:04:59');
INSERT INTO `status_upload` VALUES (369, 'mau', 'DATA 18MAR21 0700-20210318095225.xlsx', 'file/DATA 18MAR21 0700-20210318095225.xlsx', 2, 9, NULL, 369, '2021-03-18 09:52:25', '2021-03-18 09:53:01');
INSERT INTO `status_upload` VALUES (370, 'ra', 'qg28-20210318130732.xlsx', 'file/qg28-20210318130732.xlsx', 2, 18, NULL, 370, '2021-03-18 13:07:32', '2021-03-18 13:08:01');
INSERT INTO `status_upload` VALUES (371, 'mau', 'DATA 18MAR21 1500-20210318165617.xlsx', 'file/DATA 18MAR21 1500-20210318165617.xlsx', 2, 9, NULL, 371, '2021-03-18 16:56:17', '2021-03-18 16:57:01');
INSERT INTO `status_upload` VALUES (372, 'ra', 'data_RA_260221_529-20210319010856.xlsx', 'file/data_RA_260221_529-20210319010856.xlsx', 2, 18, NULL, 372, '2021-03-19 01:08:56', '2021-03-19 01:09:01');
INSERT INTO `status_upload` VALUES (373, 'mau', 'DATA 19MAR21 0250-20210319025213.xlsx', 'file/DATA 19MAR21 0250-20210319025213.xlsx', 2, 9, NULL, 373, '2021-03-19 02:52:13', '2021-03-19 02:53:02');
INSERT INTO `status_upload` VALUES (374, 'mau', 'DATA 19MAR21 0600-20210319060023.xlsx', 'file/DATA 19MAR21 0600-20210319060023.xlsx', 2, 9, NULL, 374, '2021-03-19 06:00:23', '2021-03-19 06:01:01');
INSERT INTO `status_upload` VALUES (375, 'ra', 'data_RA_260221_530-20210319060751.xlsx', 'file/data_RA_260221_530-20210319060751.xlsx', 2, 18, NULL, 375, '2021-03-19 06:07:51', '2021-03-19 06:08:01');
INSERT INTO `status_upload` VALUES (376, 'mau', 'DATA 19MAR21 0659-20210319070120.xlsx', 'file/DATA 19MAR21 0659-20210319070120.xlsx', 2, 9, NULL, 376, '2021-03-19 07:01:20', '2021-03-19 07:02:01');
INSERT INTO `status_upload` VALUES (377, 'mau', 'DATA 19MAR21 1030-20210319120117.xlsx', 'file/DATA 19MAR21 1030-20210319120117.xlsx', 2, 9, NULL, 377, '2021-03-19 12:01:17', '2021-03-19 12:02:01');
INSERT INTO `status_upload` VALUES (378, 'mau', 'DATA 20MAR21 0200-20210320023429.xlsx', 'file/DATA 20MAR21 0200-20210320023429.xlsx', 2, 9, NULL, 378, '2021-03-20 02:34:29', '2021-03-20 02:35:01');
INSERT INTO `status_upload` VALUES (379, 'ra', 'QG29-20210320040126.xlsx', 'file/QG29-20210320040126.xlsx', 2, 18, NULL, 379, '2021-03-20 04:01:26', '2021-03-20 04:02:01');
INSERT INTO `status_upload` VALUES (380, 'mau', 'DATA 20MAR21 0540-20210320054319.xlsx', 'file/DATA 20MAR21 0540-20210320054319.xlsx', 2, 9, NULL, 380, '2021-03-20 05:43:19', '2021-03-20 05:44:01');
INSERT INTO `status_upload` VALUES (381, '', 'qg30-20210320061451.xlsx', 'file/qg30-20210320061451.xlsx', 0, NULL, NULL, 381, '2021-03-20 06:14:51', '2021-03-20 06:14:51');
INSERT INTO `status_upload` VALUES (382, 'ra', 'qg30-20210320061519.xlsx', 'file/qg30-20210320061519.xlsx', 2, 18, NULL, 382, '2021-03-20 06:15:19', '2021-03-20 06:16:01');
INSERT INTO `status_upload` VALUES (383, 'mau', 'DATA 20MAR21 0655-20210320065547.xlsx', 'file/DATA 20MAR21 0655-20210320065547.xlsx', 2, 9, NULL, 383, '2021-03-20 06:55:47', '2021-03-20 06:56:01');
INSERT INTO `status_upload` VALUES (384, '', 'data_RA_260221_536-20210320102339.xlsx', 'file/data_RA_260221_536-20210320102339.xlsx', 0, NULL, NULL, 384, '2021-03-20 10:23:39', '2021-03-20 10:23:39');
INSERT INTO `status_upload` VALUES (385, 'ra', 'data_RA_260221_536-20210320102420.xlsx', 'file/data_RA_260221_536-20210320102420.xlsx', 2, 18, NULL, 385, '2021-03-20 10:24:20', '2021-03-20 10:25:01');
INSERT INTO `status_upload` VALUES (386, 'mau', 'DATA 20MAR21 1134-20210320113539.xlsx', 'file/DATA 20MAR21 1134-20210320113539.xlsx', 2, 9, NULL, 386, '2021-03-20 11:35:39', '2021-03-20 11:36:01');
INSERT INTO `status_upload` VALUES (387, 'ra', 'data_RA_260221_537-20210321005248.xlsx', 'file/data_RA_260221_537-20210321005248.xlsx', 2, 18, NULL, 387, '2021-03-21 00:52:48', '2021-03-21 00:53:01');
INSERT INTO `status_upload` VALUES (388, 'mau', 'DATA 21MAR21 0015-20210321053130.xlsx', 'file/DATA 21MAR21 0015-20210321053130.xlsx', 2, 9, NULL, 388, '2021-03-21 05:31:30', '2021-03-21 05:32:01');
INSERT INTO `status_upload` VALUES (389, 'mau', 'DATA 21MAR21 0600-20210321063432.xlsx', 'file/DATA 21MAR21 0600-20210321063432.xlsx', 2, 9, NULL, 389, '2021-03-21 06:34:32', '2021-03-21 06:35:01');
INSERT INTO `status_upload` VALUES (390, 'ra', 'data_RA_260221_538-20210321072522.xlsx', 'file/data_RA_260221_538-20210321072522.xlsx', 2, 18, NULL, 390, '2021-03-21 07:25:22', '2021-03-21 07:26:01');
INSERT INTO `status_upload` VALUES (391, 'mau', 'DATA 21MAR21 0733-20210321073429.xlsx', 'file/DATA 21MAR21 0733-20210321073429.xlsx', 2, 9, NULL, 391, '2021-03-21 07:34:29', '2021-03-21 07:35:01');
INSERT INTO `status_upload` VALUES (392, 'mau', 'DATA 21MAR21 0925-20210321092740.xlsx', 'file/DATA 21MAR21 0925-20210321092740.xlsx', 2, 9, NULL, 392, '2021-03-21 09:27:40', '2021-03-21 09:28:01');
INSERT INTO `status_upload` VALUES (393, 'mau', 'DATA 21MAR21 1156-20210321115832.xlsx', 'file/DATA 21MAR21 1156-20210321115832.xlsx', 2, 9, NULL, 393, '2021-03-21 11:58:32', '2021-03-21 11:59:01');
INSERT INTO `status_upload` VALUES (394, 'mau', 'DATA 21MAR21 1305-20210321130755.xlsx', 'file/DATA 21MAR21 1305-20210321130755.xlsx', 2, 9, NULL, 394, '2021-03-21 13:07:55', '2021-03-21 13:08:01');
INSERT INTO `status_upload` VALUES (395, 'mau', 'DATA 21MAR21 1608-20210321160939.xlsx', 'file/DATA 21MAR21 1608-20210321160939.xlsx', 2, 9, NULL, 395, '2021-03-21 16:09:39', '2021-03-21 16:10:02');
INSERT INTO `status_upload` VALUES (396, 'ra', 'qg31-20210322052517.xlsx', 'file/qg31-20210322052517.xlsx', 2, 18, NULL, 396, '2021-03-22 05:25:17', '2021-03-22 05:26:02');
INSERT INTO `status_upload` VALUES (397, 'mau', 'DATA 22MAR21 0400-20210322054156.xlsx', 'file/DATA 22MAR21 0400-20210322054156.xlsx', 2, 9, NULL, 397, '2021-03-22 05:41:56', '2021-03-22 05:42:01');
INSERT INTO `status_upload` VALUES (398, '', 'DATA 22MAR21 1306-20210322130710.xlsx', 'file/DATA 22MAR21 1306-20210322130710.xlsx', 0, NULL, NULL, 398, '2021-03-22 13:07:10', '2021-03-22 13:07:10');
INSERT INTO `status_upload` VALUES (399, 'ra', 'data_RA_260221_550-20210322133758.xlsx', 'file/data_RA_260221_550-20210322133758.xlsx', 2, 18, NULL, 399, '2021-03-22 13:37:58', '2021-03-22 13:38:01');
INSERT INTO `status_upload` VALUES (400, 'mau', 'DATA 22MAR21 1306-20210322153911.xlsx', 'file/DATA 22MAR21 1306-20210322153911.xlsx', 2, 9, NULL, 400, '2021-03-22 15:39:11', '2021-03-22 15:40:01');
INSERT INTO `status_upload` VALUES (401, 'mau', 'DATA 23MAR21 0248-20210323025248.xlsx', 'file/DATA 23MAR21 0248-20210323025248.xlsx', 2, 9, NULL, 401, '2021-03-23 02:52:48', '2021-03-23 02:53:01');
INSERT INTO `status_upload` VALUES (402, 'mau', 'DATA 23MAR21 0628-20210323063441.xlsx', 'file/DATA 23MAR21 0628-20210323063441.xlsx', 2, 9, NULL, 402, '2021-03-23 06:34:41', '2021-03-23 06:35:01');
INSERT INTO `status_upload` VALUES (403, 'ra', 'data_RA_260221_553-20210323064923.xlsx', 'file/data_RA_260221_553-20210323064923.xlsx', 2, 18, NULL, 403, '2021-03-23 06:49:23', '2021-03-23 06:50:01');
INSERT INTO `status_upload` VALUES (404, 'mau', 'DATA 23MAR21 0953-20210323095406.xlsx', 'file/DATA 23MAR21 0953-20210323095406.xlsx', 2, 9, NULL, 404, '2021-03-23 09:54:06', '2021-03-23 09:55:01');
INSERT INTO `status_upload` VALUES (405, 'ra', 'qg32-20210323124046.xlsx', 'file/qg32-20210323124046.xlsx', 2, 18, NULL, 405, '2021-03-23 12:40:46', '2021-03-23 12:41:01');
INSERT INTO `status_upload` VALUES (406, 'ra', 'data_RA_260221_557-20210324010645.xlsx', 'file/data_RA_260221_557-20210324010645.xlsx', 2, 18, NULL, 406, '2021-03-24 01:06:45', '2021-03-24 01:07:02');
INSERT INTO `status_upload` VALUES (407, 'mau', 'DATA 24MAR21 0131-20210324013423.xlsx', 'file/DATA 24MAR21 0131-20210324013423.xlsx', 2, 9, NULL, 407, '2021-03-24 01:34:23', '2021-03-24 01:35:01');
INSERT INTO `status_upload` VALUES (408, 'mau', 'DATA 24MAR21 0251-20210324025418.xlsx', 'file/DATA 24MAR21 0251-20210324025418.xlsx', 2, 9, NULL, 408, '2021-03-24 02:54:18', '2021-03-24 02:55:02');
INSERT INTO `status_upload` VALUES (409, 'mau', 'DATA 24MAR21 0420-20210324042153.xlsx', 'file/DATA 24MAR21 0420-20210324042153.xlsx', 2, 9, NULL, 409, '2021-03-24 04:21:53', '2021-03-24 04:22:01');
INSERT INTO `status_upload` VALUES (410, 'ra', 'data_RA_260221_559-20210324043101.xlsx', 'file/data_RA_260221_559-20210324043101.xlsx', 2, 18, NULL, 410, '2021-03-24 04:31:01', '2021-03-24 04:32:01');
INSERT INTO `status_upload` VALUES (411, 'mau', 'DATA 24MAR21 0607-20210324060908.xlsx', 'file/DATA 24MAR21 0607-20210324060908.xlsx', 2, 9, NULL, 411, '2021-03-24 06:09:08', '2021-03-24 06:10:01');
INSERT INTO `status_upload` VALUES (412, 'ra', 'data_RA_260221_562-20210324065529.xlsx', 'file/data_RA_260221_562-20210324065529.xlsx', 2, 18, NULL, 412, '2021-03-24 06:55:29', '2021-03-24 06:56:01');
INSERT INTO `status_upload` VALUES (413, 'mau', 'DATA 24MAR21 0700-20210324092341.xlsx', 'file/DATA 24MAR21 0700-20210324092341.xlsx', 2, 9, NULL, 413, '2021-03-24 09:23:41', '2021-03-24 09:24:01');
INSERT INTO `status_upload` VALUES (414, 'mau', 'DATA 25MAR21 0245-20210325025003.xlsx', 'file/DATA 25MAR21 0245-20210325025003.xlsx', 2, 9, NULL, 414, '2021-03-25 02:50:03', '2021-03-25 02:51:01');
INSERT INTO `status_upload` VALUES (415, 'mau', 'DATA 25MAR21 0600-20210325060905.xlsx', 'file/DATA 25MAR21 0600-20210325060905.xlsx', 2, 9, NULL, 415, '2021-03-25 06:09:05', '2021-03-25 06:10:01');
INSERT INTO `status_upload` VALUES (416, 'mau', 'DATA 25MAR21 0730-20210325123657.xlsx', 'file/DATA 25MAR21 0730-20210325123657.xlsx', 2, 9, NULL, 416, '2021-03-25 12:36:57', '2021-03-25 12:37:01');
INSERT INTO `status_upload` VALUES (417, 'ra', 'data_RA_260321_01-20210326012251.xlsx', 'file/data_RA_260321_01-20210326012251.xlsx', 2, 18, NULL, 417, '2021-03-26 01:22:51', '2021-03-26 01:23:02');
INSERT INTO `status_upload` VALUES (418, 'mau', 'DATA 26MAR21 0500-20210326051313.xlsx', 'file/DATA 26MAR21 0500-20210326051313.xlsx', 2, 9, NULL, 418, '2021-03-26 05:13:13', '2021-03-26 05:14:01');
INSERT INTO `status_upload` VALUES (419, 'ra', 'data_RA_260321_02-20210326052213.xlsx', 'file/data_RA_260321_02-20210326052213.xlsx', 2, 18, NULL, 419, '2021-03-26 05:22:13', '2021-03-26 05:23:01');
INSERT INTO `status_upload` VALUES (420, 'mau', 'DATA 26 MAR21 0627-20210326062916.xlsx', 'file/DATA 26 MAR21 0627-20210326062916.xlsx', 2, 9, NULL, 420, '2021-03-26 06:29:16', '2021-03-26 06:30:01');
INSERT INTO `status_upload` VALUES (421, 'mau', '26MAR21 1221-20210326122243.xlsx', 'file/26MAR21 1221-20210326122243.xlsx', 2, 9, NULL, 421, '2021-03-26 12:22:43', '2021-03-26 12:23:01');
INSERT INTO `status_upload` VALUES (422, 'ra', 'data_RA_260321_03-20210326133154.xlsx', 'file/data_RA_260321_03-20210326133154.xlsx', 2, 18, NULL, 422, '2021-03-26 13:31:54', '2021-03-26 13:32:01');
INSERT INTO `status_upload` VALUES (423, 'mau', 'DATA  26MAR21 1541-20210326154324.xlsx', 'file/DATA  26MAR21 1541-20210326154324.xlsx', 2, 9, NULL, 423, '2021-03-26 15:43:24', '2021-03-26 15:44:02');
INSERT INTO `status_upload` VALUES (424, 'ra', 'data_RA_270321_01-20210327014434.xlsx', 'file/data_RA_270321_01-20210327014434.xlsx', 2, 18, NULL, 424, '2021-03-27 01:44:34', '2021-03-27 01:45:01');
INSERT INTO `status_upload` VALUES (425, 'mau', 'DATA 27MAR21 0200-20210327034427.xlsx', 'file/DATA 27MAR21 0200-20210327034427.xlsx', 2, 9, NULL, 425, '2021-03-27 03:44:27', '2021-03-27 03:45:01');
INSERT INTO `status_upload` VALUES (426, 'ra', 'data_RA_270321_02-20210327045309.xlsx', 'file/data_RA_270321_02-20210327045309.xlsx', 2, 18, NULL, 426, '2021-03-27 04:53:09', '2021-03-27 04:54:01');
INSERT INTO `status_upload` VALUES (427, 'ra', 'data_RA_270321_03-20210327061538.xlsx', 'file/data_RA_270321_03-20210327061538.xlsx', 2, 18, NULL, 427, '2021-03-27 06:15:38', '2021-03-27 06:16:01');
INSERT INTO `status_upload` VALUES (428, 'mau', 'DATA 27MAR21 0713-20210327071438.xlsx', 'file/DATA 27MAR21 0713-20210327071438.xlsx', 2, 9, NULL, 428, '2021-03-27 07:14:38', '2021-03-27 07:15:01');
INSERT INTO `status_upload` VALUES (429, '', 'DATA 27MAR21 1107-20210327110856.xlsx', 'file/DATA 27MAR21 1107-20210327110856.xlsx', 0, NULL, NULL, 429, '2021-03-27 11:08:56', '2021-03-27 11:08:56');
INSERT INTO `status_upload` VALUES (430, 'mau', 'DATA 27MAR21 1107-20210327110910.xlsx', 'file/DATA 27MAR21 1107-20210327110910.xlsx', 2, 9, NULL, 430, '2021-03-27 11:09:10', '2021-03-27 11:10:01');
INSERT INTO `status_upload` VALUES (431, 'ra', 'data_RA_270321_04-20210327113153.xlsx', 'file/data_RA_270321_04-20210327113153.xlsx', 2, 18, NULL, 431, '2021-03-27 11:31:53', '2021-03-27 11:32:01');
INSERT INTO `status_upload` VALUES (432, 'mau', 'data 27mar21 1219-20210327122028.xlsx', 'file/data 27mar21 1219-20210327122028.xlsx', 2, 9, NULL, 432, '2021-03-27 12:20:28', '2021-03-27 12:21:01');
INSERT INTO `status_upload` VALUES (433, 'mau', 'DATA 27MAR21 1306-20210327130747.xlsx', 'file/DATA 27MAR21 1306-20210327130747.xlsx', 2, 9, NULL, 433, '2021-03-27 13:07:47', '2021-03-27 13:08:01');
INSERT INTO `status_upload` VALUES (434, 'ra', 'data_RA_270321_05-20210327131258.xlsx', 'file/data_RA_270321_05-20210327131258.xlsx', 2, 18, NULL, 434, '2021-03-27 13:12:58', '2021-03-27 13:13:01');
INSERT INTO `status_upload` VALUES (435, 'mau', 'DATA 27MAR21 1400-20210327140209.xlsx', 'file/DATA 27MAR21 1400-20210327140209.xlsx', 2, 9, NULL, 435, '2021-03-27 14:02:09', '2021-03-27 14:03:01');
INSERT INTO `status_upload` VALUES (436, '', 'data_RA_270321_06-20210327233401.xlsx', 'file/data_RA_270321_06-20210327233401.xlsx', 0, NULL, NULL, 436, '2021-03-27 23:34:01', '2021-03-27 23:34:01');
INSERT INTO `status_upload` VALUES (437, 'ra', 'data_RA_270321_06-20210327233429.xlsx', 'file/data_RA_270321_06-20210327233429.xlsx', 2, 18, NULL, 437, '2021-03-27 23:34:29', '2021-03-27 23:35:02');
INSERT INTO `status_upload` VALUES (438, 'mau', 'DATA 28MAR21 0000-20210328004840.xlsx', 'file/DATA 28MAR21 0000-20210328004840.xlsx', 2, 9, NULL, 438, '2021-03-28 00:48:40', '2021-03-28 00:49:01');
INSERT INTO `status_upload` VALUES (439, 'ra', 'data_RA_280321_01-20210328050023.xlsx', 'file/data_RA_280321_01-20210328050023.xlsx', 2, 18, NULL, 439, '2021-03-28 05:00:23', '2021-03-28 05:01:01');
INSERT INTO `status_upload` VALUES (440, '', 'DATA 28MART21 0530-20210328061034.xlsx', 'file/DATA 28MART21 0530-20210328061034.xlsx', 0, NULL, NULL, 440, '2021-03-28 06:10:34', '2021-03-28 06:10:34');
INSERT INTO `status_upload` VALUES (441, 'mau', 'DATA 28MART21 0530-20210328061208.xlsx', 'file/DATA 28MART21 0530-20210328061208.xlsx', 2, 9, NULL, 441, '2021-03-28 06:12:08', '2021-03-28 06:13:01');
INSERT INTO `status_upload` VALUES (442, 'ra', 'data_RA_280321_02-20210328065107.xlsx', 'file/data_RA_280321_02-20210328065107.xlsx', 2, 18, NULL, 442, '2021-03-28 06:51:07', '2021-03-28 06:52:01');
INSERT INTO `status_upload` VALUES (443, 'ra', 'D 8713 FH 2040591-20210328102305.xlsx', 'file/D 8713 FH 2040591-20210328102305.xlsx', 2, 18, NULL, 443, '2021-03-28 10:23:05', '2021-03-28 10:24:01');
INSERT INTO `status_upload` VALUES (444, 'mau', 'DATA 28MAR21 1400-20210328184507.xlsx', 'file/DATA 28MAR21 1400-20210328184507.xlsx', 2, 9, NULL, 444, '2021-03-28 18:45:07', '2021-03-28 18:46:02');
INSERT INTO `status_upload` VALUES (445, 'ra', 'data_RA_280321_04-20210328193540.xlsx', 'file/data_RA_280321_04-20210328193540.xlsx', 2, 18, NULL, 445, '2021-03-28 19:35:40', '2021-03-28 19:36:02');
INSERT INTO `status_upload` VALUES (446, 'ra', 'data_RA_290321_01-20210329004304.xlsx', 'file/data_RA_290321_01-20210329004304.xlsx', 2, 18, NULL, 446, '2021-03-29 00:43:04', '2021-03-29 00:44:01');
INSERT INTO `status_upload` VALUES (447, 'mau', '29MAR21 0137-20210329014013.xlsx', 'file/29MAR21 0137-20210329014013.xlsx', 2, 9, NULL, 447, '2021-03-29 01:40:13', '2021-03-29 01:41:01');
INSERT INTO `status_upload` VALUES (448, 'ra', 'data_RA_290321_02-20210329041642.xlsx', 'file/data_RA_290321_02-20210329041642.xlsx', 2, 18, NULL, 448, '2021-03-29 04:16:42', '2021-03-29 04:17:01');
INSERT INTO `status_upload` VALUES (449, 'mau', 'DATA 29MAR21 0503-20210329050512.xlsx', 'file/DATA 29MAR21 0503-20210329050512.xlsx', 2, 9, NULL, 449, '2021-03-29 05:05:12', '2021-03-29 05:06:01');
INSERT INTO `status_upload` VALUES (450, 'ra', 'data_RA_290321_03-20210329063754.xlsx', 'file/data_RA_290321_03-20210329063754.xlsx', 2, 18, NULL, 450, '2021-03-29 06:37:54', '2021-03-29 06:38:02');
INSERT INTO `status_upload` VALUES (451, 'mau', 'DATA 29MAR21 0900-20210329090412.xlsx', 'file/DATA 29MAR21 0900-20210329090412.xlsx', 2, 9, NULL, 451, '2021-03-29 09:04:12', '2021-03-29 09:05:01');
INSERT INTO `status_upload` VALUES (452, 'ra', 'data_RA_290321_04-20210329124151.xlsx', 'file/data_RA_290321_04-20210329124151.xlsx', 2, 18, NULL, 452, '2021-03-29 12:41:51', '2021-03-29 12:42:01');
INSERT INTO `status_upload` VALUES (453, 'mau', 'DATA 29MAR21 1300-20210329133124.xlsx', 'file/DATA 29MAR21 1300-20210329133124.xlsx', 2, 9, NULL, 453, '2021-03-29 13:31:24', '2021-03-29 13:32:02');
INSERT INTO `status_upload` VALUES (454, 'mau', 'DATA 29MAR21 1400-20210329142035.xlsx', 'file/DATA 29MAR21 1400-20210329142035.xlsx', 2, 9, NULL, 454, '2021-03-29 14:20:35', '2021-03-29 14:21:01');
INSERT INTO `status_upload` VALUES (455, 'ra', 'data_RA_300321_01-20210330015355.xlsx', 'file/data_RA_300321_01-20210330015355.xlsx', 2, 18, NULL, 455, '2021-03-30 01:53:55', '2021-03-30 01:54:01');
INSERT INTO `status_upload` VALUES (456, 'mau', 'DATA 30MAR21 0322-20210330032348.xlsx', 'file/DATA 30MAR21 0322-20210330032348.xlsx', 2, 9, NULL, 456, '2021-03-30 03:23:48', '2021-03-30 03:24:01');
INSERT INTO `status_upload` VALUES (457, 'mau', 'DATA 30MAR31 0631-20210330063311.xlsx', 'file/DATA 30MAR31 0631-20210330063311.xlsx', 2, 9, NULL, 457, '2021-03-30 06:33:11', '2021-03-30 06:34:01');
INSERT INTO `status_upload` VALUES (458, 'ra', 'QG33-20210330111823.xlsx', 'file/QG33-20210330111823.xlsx', 2, 18, NULL, 458, '2021-03-30 11:18:23', '2021-03-30 11:19:02');
INSERT INTO `status_upload` VALUES (459, 'mau', 'DATA 30MAR21 1000-20210330120700.xlsx', 'file/DATA 30MAR21 1000-20210330120700.xlsx', 2, 9, NULL, 459, '2021-03-30 12:07:00', '2021-03-30 12:07:01');
INSERT INTO `status_upload` VALUES (460, 'mau', 'DATA 30MAR21 1200-20210330124454.xlsx', 'file/DATA 30MAR21 1200-20210330124454.xlsx', 2, 9, NULL, 460, '2021-03-30 12:44:54', '2021-03-30 12:45:01');
INSERT INTO `status_upload` VALUES (461, 'mau', 'DATA 30MAR21 1600-20210330163832.xlsx', 'file/DATA 30MAR21 1600-20210330163832.xlsx', 2, 9, NULL, 461, '2021-03-30 16:38:32', '2021-03-30 16:39:02');
INSERT INTO `status_upload` VALUES (462, 'ra', 'data_RA_300321_02-20210331022948.xlsx', 'file/data_RA_300321_02-20210331022948.xlsx', 2, 18, NULL, 462, '2021-03-31 02:29:48', '2021-03-31 02:30:01');
INSERT INTO `status_upload` VALUES (463, 'mau', 'DATA 31MAR21 0400-20210331053336.xlsx', 'file/DATA 31MAR21 0400-20210331053336.xlsx', 2, 9, NULL, 463, '2021-03-31 05:33:36', '2021-03-31 05:34:01');
INSERT INTO `status_upload` VALUES (464, 'mau', 'DATA 31MAR21 0001-20210331053619.xlsx', 'file/DATA 31MAR21 0001-20210331053619.xlsx', 2, 9, NULL, 464, '2021-03-31 05:36:19', '2021-03-31 05:37:01');
INSERT INTO `status_upload` VALUES (465, 'ra', 'data_RA_300321_03-20210331062507.xlsx', 'file/data_RA_300321_03-20210331062507.xlsx', 2, 18, NULL, 465, '2021-03-31 06:25:07', '2021-03-31 06:26:01');
INSERT INTO `status_upload` VALUES (466, 'mau', 'DATA 31MAR21 0700-20210331113932.xlsx', 'file/DATA 31MAR21 0700-20210331113932.xlsx', 2, 9, NULL, 466, '2021-03-31 11:39:32', '2021-03-31 11:40:01');
INSERT INTO `status_upload` VALUES (467, 'ra', 'qg34-20210331133509.xlsx', 'file/qg34-20210331133509.xlsx', 2, 18, NULL, 467, '2021-03-31 13:35:09', '2021-03-31 13:36:01');
INSERT INTO `status_upload` VALUES (468, 'ra', 'qg35-20210401012422.xlsx', 'file/qg35-20210401012422.xlsx', 2, 18, NULL, 468, '2021-04-01 01:24:22', '2021-04-01 01:25:01');
INSERT INTO `status_upload` VALUES (469, 'mau', 'DATA 01 APRIL 2021 0600-20210401063511.xlsx', 'file/DATA 01 APRIL 2021 0600-20210401063511.xlsx', 2, 9, NULL, 469, '2021-04-01 06:35:11', '2021-04-01 06:36:01');
INSERT INTO `status_upload` VALUES (470, 'ra', 'qg36-20210401070221.xlsx', 'file/qg36-20210401070221.xlsx', 2, 18, NULL, 470, '2021-04-01 07:02:21', '2021-04-01 07:03:01');
INSERT INTO `status_upload` VALUES (471, 'mau', 'DATA 01 APRIL 2021 0600-20210401111851.xlsx', 'file/DATA 01 APRIL 2021 0600-20210401111851.xlsx', 2, 9, NULL, 471, '2021-04-01 11:18:51', '2021-04-01 11:19:01');
INSERT INTO `status_upload` VALUES (472, 'mau', 'DATA 01APR21 1112-20210401112026.xlsx', 'file/DATA 01APR21 1112-20210401112026.xlsx', 2, 9, NULL, 472, '2021-04-01 11:20:26', '2021-04-01 11:21:02');
INSERT INTO `status_upload` VALUES (473, 'mau', 'DATA 01APRIL21 1246-20210401124856.xlsx', 'file/DATA 01APRIL21 1246-20210401124856.xlsx', 2, 9, NULL, 473, '2021-04-01 12:48:56', '2021-04-01 12:49:01');
INSERT INTO `status_upload` VALUES (474, 'mau', '01APRIL21 1447-20210401144751.xlsx', 'file/01APRIL21 1447-20210401144751.xlsx', 2, 9, NULL, 474, '2021-04-01 14:47:51', '2021-04-01 14:48:01');
INSERT INTO `status_upload` VALUES (475, 'mau', 'DATA 01APRIL21 1546-20210401154641.xlsx', 'file/DATA 01APRIL21 1546-20210401154641.xlsx', 2, 9, NULL, 475, '2021-04-01 15:46:41', '2021-04-01 15:47:02');
INSERT INTO `status_upload` VALUES (476, 'ra', 'data_RA_300321_04-20210402015550.xlsx', 'file/data_RA_300321_04-20210402015550.xlsx', 2, 18, NULL, 476, '2021-04-02 01:55:50', '2021-04-02 01:56:01');
INSERT INTO `status_upload` VALUES (477, 'mau', 'DATA 02 APRIL21 0200-20210402050713.xlsx', 'file/DATA 02 APRIL21 0200-20210402050713.xlsx', 2, 9, NULL, 477, '2021-04-02 05:07:13', '2021-04-02 05:08:01');
INSERT INTO `status_upload` VALUES (478, 'ra', 'data_RA_300321_05-20210402070516.xlsx', 'file/data_RA_300321_05-20210402070516.xlsx', 2, 18, NULL, 478, '2021-04-02 07:05:16', '2021-04-02 07:06:01');
INSERT INTO `status_upload` VALUES (479, 'mau', 'DATA 02APRIL21 0851-20210402085334.xlsx', 'file/DATA 02APRIL21 0851-20210402085334.xlsx', 2, 9, NULL, 479, '2021-04-02 08:53:34', '2021-04-02 08:54:02');
INSERT INTO `status_upload` VALUES (480, 'mau', 'DATA 02APRIL21 0937-20210402093744.xlsx', 'file/DATA 02APRIL21 0937-20210402093744.xlsx', 2, 9, NULL, 480, '2021-04-02 09:37:44', '2021-04-02 09:38:01');
INSERT INTO `status_upload` VALUES (481, 'mau', '02APRIL21 1228-20210402122833.xlsx', 'file/02APRIL21 1228-20210402122833.xlsx', 2, 9, NULL, 481, '2021-04-02 12:28:33', '2021-04-02 12:29:01');
INSERT INTO `status_upload` VALUES (482, 'mau', 'DATA 02APRIL21 1424-20210402142438.xlsx', 'file/DATA 02APRIL21 1424-20210402142438.xlsx', 2, 9, NULL, 482, '2021-04-02 14:24:38', '2021-04-02 14:25:01');
INSERT INTO `status_upload` VALUES (483, 'ra', 'qg37-20210403041304.xlsx', 'file/qg37-20210403041304.xlsx', 2, 18, NULL, 483, '2021-04-03 04:13:04', '2021-04-03 04:14:01');
INSERT INTO `status_upload` VALUES (484, 'mau', 'DATA 03 APRIL21 0100-20210403043912.xlsx', 'file/DATA 03 APRIL21 0100-20210403043912.xlsx', 2, 9, NULL, 484, '2021-04-03 04:39:12', '2021-04-03 04:40:01');
INSERT INTO `status_upload` VALUES (485, 'mau', 'DATA 03APRIL21 1430-20210403144353.xlsx', 'file/DATA 03APRIL21 1430-20210403144353.xlsx', 2, 9, NULL, 485, '2021-04-03 14:43:53', '2021-04-03 14:44:01');
INSERT INTO `status_upload` VALUES (486, 'mau', 'DATA 03APRIL21 1500-20210403150704.xlsx', 'file/DATA 03APRIL21 1500-20210403150704.xlsx', 2, 9, NULL, 486, '2021-04-03 15:07:04', '2021-04-03 15:08:01');
INSERT INTO `status_upload` VALUES (487, 'mau', 'DATA 04APRIL21 0137-20210404014030.xlsx', 'file/DATA 04APRIL21 0137-20210404014030.xlsx', 2, 9, NULL, 487, '2021-04-04 01:40:30', '2021-04-04 01:41:01');
INSERT INTO `status_upload` VALUES (488, 'mau', 'DATA 04APRIL21 0246-20210404024651.xlsx', 'file/DATA 04APRIL21 0246-20210404024651.xlsx', 2, 9, NULL, 488, '2021-04-04 02:46:51', '2021-04-04 02:47:02');
INSERT INTO `status_upload` VALUES (489, 'ra', 'data_RA_300321_06-20210404041555.xlsx', 'file/data_RA_300321_06-20210404041555.xlsx', 2, 18, NULL, 489, '2021-04-04 04:15:55', '2021-04-04 04:16:02');
INSERT INTO `status_upload` VALUES (490, 'ra', 'D 8714 FH 2040641-20210404043156.xlsx', 'file/D 8714 FH 2040641-20210404043156.xlsx', 2, 18, NULL, 490, '2021-04-04 04:31:56', '2021-04-04 04:32:01');
INSERT INTO `status_upload` VALUES (491, 'mau', 'DATA 04APRIL21 0524-20210404052431.xlsx', 'file/DATA 04APRIL21 0524-20210404052431.xlsx', 2, 9, NULL, 491, '2021-04-04 05:24:31', '2021-04-04 05:25:01');
INSERT INTO `status_upload` VALUES (492, 'ra', 'data_RA_300321_07-20210404071144.xlsx', 'file/data_RA_300321_07-20210404071144.xlsx', 2, 18, NULL, 492, '2021-04-04 07:11:44', '2021-04-04 07:12:01');
INSERT INTO `status_upload` VALUES (493, 'mau', 'DATA 04APRIL21 1400-20210404140020.xlsx', 'file/DATA 04APRIL21 1400-20210404140020.xlsx', 2, 9, NULL, 493, '2021-04-04 14:00:20', '2021-04-04 14:01:01');
INSERT INTO `status_upload` VALUES (494, 'mau', 'DATA 04APRIL21 1520-20210404152530.xlsx', 'file/DATA 04APRIL21 1520-20210404152530.xlsx', 2, 9, NULL, 494, '2021-04-04 15:25:30', '2021-04-04 15:26:01');
INSERT INTO `status_upload` VALUES (495, 'ra', 'data_RA_300321_08-20210404232335.xlsx', 'file/data_RA_300321_08-20210404232335.xlsx', 2, 18, NULL, 495, '2021-04-04 23:23:35', '2021-04-04 23:24:01');
INSERT INTO `status_upload` VALUES (496, 'mau', 'DATA 05APRIL21 0031-20210405003140.xlsx', 'file/DATA 05APRIL21 0031-20210405003140.xlsx', 2, 9, NULL, 496, '2021-04-05 00:31:40', '2021-04-05 00:32:02');
INSERT INTO `status_upload` VALUES (497, 'mau', 'DATA 05APRIL21 0139-20210405014013.xlsx', 'file/DATA 05APRIL21 0139-20210405014013.xlsx', 2, 9, NULL, 497, '2021-04-05 01:40:13', '2021-04-05 01:41:01');
INSERT INTO `status_upload` VALUES (498, 'mau', 'DATA 05APRIL21 0317-20210405031738.xlsx', 'file/DATA 05APRIL21 0317-20210405031738.xlsx', 2, 9, NULL, 498, '2021-04-05 03:17:38', '2021-04-05 03:18:01');
INSERT INTO `status_upload` VALUES (499, 'ra', 'data_RA_300321_09-20210405031750.xlsx', 'file/data_RA_300321_09-20210405031750.xlsx', 2, 18, NULL, 499, '2021-04-05 03:17:50', '2021-04-05 03:18:01');
INSERT INTO `status_upload` VALUES (500, 'mau', 'DATA 05APRIL21 0438-20210405043909.xlsx', 'file/DATA 05APRIL21 0438-20210405043909.xlsx', 2, 9, NULL, 500, '2021-04-05 04:39:09', '2021-04-05 04:40:01');
INSERT INTO `status_upload` VALUES (501, 'mau', 'DATA 05APRIL21 0659-20210405065951.xlsx', 'file/DATA 05APRIL21 0659-20210405065951.xlsx', 2, 9, NULL, 501, '2021-04-05 06:59:51', '2021-04-05 07:00:01');
INSERT INTO `status_upload` VALUES (502, '', 'data_RA_300321_10-20210405073837.xlsx', 'file/data_RA_300321_10-20210405073837.xlsx', 0, NULL, NULL, 502, '2021-04-05 07:38:37', '2021-04-05 07:38:37');
INSERT INTO `status_upload` VALUES (503, 'ra', 'data_RA_300321_10-20210405073919.xlsx', 'file/data_RA_300321_10-20210405073919.xlsx', 2, 18, NULL, 503, '2021-04-05 07:39:19', '2021-04-05 07:40:01');
INSERT INTO `status_upload` VALUES (504, 'ra', 'qg38-20210405110658.xlsx', 'file/qg38-20210405110658.xlsx', 2, 18, NULL, 504, '2021-04-05 11:06:58', '2021-04-05 11:07:01');
INSERT INTO `status_upload` VALUES (505, 'mau', 'DATA 05APRIL21 0730-20210405113343.xlsx', 'file/DATA 05APRIL21 0730-20210405113343.xlsx', 2, 9, NULL, 505, '2021-04-05 11:33:43', '2021-04-05 11:34:01');
INSERT INTO `status_upload` VALUES (506, 'mau', 'DATA 05APRIL 1500-20210405151956.xlsx', 'file/DATA 05APRIL 1500-20210405151956.xlsx', 2, 9, NULL, 506, '2021-04-05 15:19:56', '2021-04-05 15:20:01');
INSERT INTO `status_upload` VALUES (507, 'ra', 'data_RA_300321_11-20210406003729.xlsx', 'file/data_RA_300321_11-20210406003729.xlsx', 2, 18, NULL, 507, '2021-04-06 00:37:29', '2021-04-06 00:38:02');
INSERT INTO `status_upload` VALUES (508, 'mau', 'DATA 06APRIL21 0500-20210406053226.xlsx', 'file/DATA 06APRIL21 0500-20210406053226.xlsx', 2, 9, NULL, 508, '2021-04-06 05:32:26', '2021-04-06 05:33:02');
INSERT INTO `status_upload` VALUES (509, 'mau', 'DATA 06APRIL21 0640-20210406064224.xlsx', 'file/DATA 06APRIL21 0640-20210406064224.xlsx', 2, 9, NULL, 509, '2021-04-06 06:42:24', '2021-04-06 06:43:01');
INSERT INTO `status_upload` VALUES (510, 'ra', 'data_RA_300321_12-20210406071024.xlsx', 'file/data_RA_300321_12-20210406071024.xlsx', 2, 18, NULL, 510, '2021-04-06 07:10:24', '2021-04-06 07:11:01');
INSERT INTO `status_upload` VALUES (511, 'mau', 'DATA 06APRIL21 0715-20210406094253.xlsx', 'file/DATA 06APRIL21 0715-20210406094253.xlsx', 2, 9, NULL, 511, '2021-04-06 09:42:53', '2021-04-06 09:43:01');
INSERT INTO `status_upload` VALUES (512, 'ra', 'qg39-20210406184203.xlsx', 'file/qg39-20210406184203.xlsx', 2, 18, NULL, 512, '2021-04-06 18:42:03', '2021-04-06 18:43:01');
INSERT INTO `status_upload` VALUES (513, 'ra', 'data_RA_070421_01-20210406235530.xlsx', 'file/data_RA_070421_01-20210406235530.xlsx', 2, 18, NULL, 513, '2021-04-06 23:55:30', '2021-04-06 23:56:01');
INSERT INTO `status_upload` VALUES (514, 'ra', 'data_RA_070421_02-20210407023715.xlsx', 'file/data_RA_070421_02-20210407023715.xlsx', 2, 18, NULL, 514, '2021-04-07 02:37:15', '2021-04-07 02:38:02');
INSERT INTO `status_upload` VALUES (515, 'mau', 'DATA 07APRIL21 0200-20210407034743.xlsx', 'file/DATA 07APRIL21 0200-20210407034743.xlsx', 2, 9, NULL, 515, '2021-04-07 03:47:43', '2021-04-07 03:48:01');
INSERT INTO `status_upload` VALUES (516, 'ra', 'data_RA_070421_03-20210407055738.xlsx', 'file/data_RA_070421_03-20210407055738.xlsx', 2, 18, NULL, 516, '2021-04-07 05:57:38', '2021-04-07 05:58:01');
INSERT INTO `status_upload` VALUES (517, 'mau', 'DATA 07APRIL21 0500-20210407063424.xlsx', 'file/DATA 07APRIL21 0500-20210407063424.xlsx', 2, 9, NULL, 517, '2021-04-07 06:34:24', '2021-04-07 06:35:01');
INSERT INTO `status_upload` VALUES (518, 'ra', 'data_RA_070421_04-20210407072513.xlsx', 'file/data_RA_070421_04-20210407072513.xlsx', 2, 18, NULL, 518, '2021-04-07 07:25:13', '2021-04-07 07:26:01');
INSERT INTO `status_upload` VALUES (519, 'mau', 'DATA 07APRIL21 1058-20210407105907.xlsx', 'file/DATA 07APRIL21 1058-20210407105907.xlsx', 2, 9, NULL, 519, '2021-04-07 10:59:07', '2021-04-07 11:00:02');
INSERT INTO `status_upload` VALUES (520, 'mau', 'DATA 07ARPIL21 1340-20210407134051.xlsx', 'file/DATA 07ARPIL21 1340-20210407134051.xlsx', 2, 9, NULL, 520, '2021-04-07 13:40:51', '2021-04-07 13:41:01');
INSERT INTO `status_upload` VALUES (521, 'mau', 'DATA 07APRIL20211413-20210407141344.xlsx', 'file/DATA 07APRIL20211413-20210407141344.xlsx', 2, 9, NULL, 521, '2021-04-07 14:13:44', '2021-04-07 14:14:01');
INSERT INTO `status_upload` VALUES (522, 'ra', 'data_RA_070421_05-20210407142733.xlsx', 'file/data_RA_070421_05-20210407142733.xlsx', 2, 18, NULL, 522, '2021-04-07 14:27:33', '2021-04-07 14:28:02');
INSERT INTO `status_upload` VALUES (523, 'ra', 'data_RA_070421_06-20210407231135.xlsx', 'file/data_RA_070421_06-20210407231135.xlsx', 2, 18, NULL, 523, '2021-04-07 23:11:35', '2021-04-07 23:12:01');
INSERT INTO `status_upload` VALUES (524, 'ra', 'data_RA_080421_01-20210408020949.xlsx', 'file/data_RA_080421_01-20210408020949.xlsx', 2, 18, NULL, 524, '2021-04-08 02:09:49', '2021-04-08 02:10:02');
INSERT INTO `status_upload` VALUES (525, 'mau', 'DATA APRIL21 0000-20210408040856.xlsx', 'file/DATA APRIL21 0000-20210408040856.xlsx', 2, 9, NULL, 525, '2021-04-08 04:08:56', '2021-04-08 04:09:01');
INSERT INTO `status_upload` VALUES (526, 'ra', 'data_RA_080421_02-20210408043512.xlsx', 'file/data_RA_080421_02-20210408043512.xlsx', 2, 18, NULL, 526, '2021-04-08 04:35:12', '2021-04-08 04:36:01');
INSERT INTO `status_upload` VALUES (527, 'mau', 'DATA 08APRIL21 0430-20210408054038.xlsx', 'file/DATA 08APRIL21 0430-20210408054038.xlsx', 2, 9, NULL, 527, '2021-04-08 05:40:38', '2021-04-08 05:41:01');
INSERT INTO `status_upload` VALUES (528, 'ra', 'data_RA_080421_03-20210408061257.xlsx', 'file/data_RA_080421_03-20210408061257.xlsx', 2, 18, NULL, 528, '2021-04-08 06:12:57', '2021-04-08 06:13:01');
INSERT INTO `status_upload` VALUES (529, 'mau', 'DATA 08APRIL21 0802-20210408080231.xlsx', 'file/DATA 08APRIL21 0802-20210408080231.xlsx', 2, 9, NULL, 529, '2021-04-08 08:02:31', '2021-04-08 08:03:01');
INSERT INTO `status_upload` VALUES (530, 'ra', 'data_RA_080421_04-20210408132858.xlsx', 'file/data_RA_080421_04-20210408132858.xlsx', 2, 18, NULL, 530, '2021-04-08 13:28:58', '2021-04-08 13:29:01');
INSERT INTO `status_upload` VALUES (531, 'mau', 'DATA 08april21 1213-20210408183659.xlsx', 'file/DATA 08april21 1213-20210408183659.xlsx', 2, 9, NULL, 531, '2021-04-08 18:36:59', '2021-04-08 18:37:02');
INSERT INTO `status_upload` VALUES (532, 'ra', 'D 8713 FH 2040684-20210408231929.xlsx', 'file/D 8713 FH 2040684-20210408231929.xlsx', 2, 18, NULL, 532, '2021-04-08 23:19:29', '2021-04-08 23:20:02');
INSERT INTO `status_upload` VALUES (533, 'mau', 'DATA 09APRIL21 0000-20210409013242.xlsx', 'file/DATA 09APRIL21 0000-20210409013242.xlsx', 2, 9, NULL, 533, '2021-04-09 01:32:42', '2021-04-09 01:33:01');
INSERT INTO `status_upload` VALUES (534, '', 'D 8713 FH 2040685-20210409020250.xlsx', 'file/D 8713 FH 2040685-20210409020250.xlsx', 0, NULL, NULL, 534, '2021-04-09 02:02:50', '2021-04-09 02:02:50');
INSERT INTO `status_upload` VALUES (535, 'ra', 'D 8713 FH 2040685-20210409020519.xlsx', 'file/D 8713 FH 2040685-20210409020519.xlsx', 2, 18, NULL, 535, '2021-04-09 02:05:19', '2021-04-09 02:06:01');
INSERT INTO `status_upload` VALUES (536, 'ra', 'D 8713 FH 2040686-20210409041612.xlsx', 'file/D 8713 FH 2040686-20210409041612.xlsx', 2, 18, NULL, 536, '2021-04-09 04:16:12', '2021-04-09 04:17:01');
INSERT INTO `status_upload` VALUES (537, 'mau', 'DATA 09APRIL21 0430-20210409054745.xlsx', 'file/DATA 09APRIL21 0430-20210409054745.xlsx', 2, 9, NULL, 537, '2021-04-09 05:47:45', '2021-04-09 05:48:01');
INSERT INTO `status_upload` VALUES (538, 'ra', 'data_RA_090421_01-20210409063904.xlsx', 'file/data_RA_090421_01-20210409063904.xlsx', 2, 18, NULL, 538, '2021-04-09 06:39:04', '2021-04-09 06:40:01');
INSERT INTO `status_upload` VALUES (539, 'ra', 'data_RA_300321_13-20210409102639.xlsx', 'file/data_RA_300321_13-20210409102639.xlsx', 2, 18, NULL, 539, '2021-04-09 10:26:39', '2021-04-09 10:27:01');
INSERT INTO `status_upload` VALUES (540, 'mau', 'DATA 09APRIL21 1100-20210409145532.xlsx', 'file/DATA 09APRIL21 1100-20210409145532.xlsx', 2, 9, NULL, 540, '2021-04-09 14:55:32', '2021-04-09 14:56:01');
INSERT INTO `status_upload` VALUES (541, 'mau', 'DATA 09APRIL21 1200-20210409145702.xlsx', 'file/DATA 09APRIL21 1200-20210409145702.xlsx', 2, 9, NULL, 541, '2021-04-09 14:57:02', '2021-04-09 14:58:01');
INSERT INTO `status_upload` VALUES (542, 'ra', 'data_RA_100421_01-20210410012621.xlsx', 'file/data_RA_100421_01-20210410012621.xlsx', 2, 18, NULL, 542, '2021-04-10 01:26:21', '2021-04-10 01:27:01');
INSERT INTO `status_upload` VALUES (543, 'mau', 'DATA 10APRIL21 0301-20210410030209.xlsx', 'file/DATA 10APRIL21 0301-20210410030209.xlsx', 2, 9, NULL, 543, '2021-04-10 03:02:09', '2021-04-10 03:03:02');
INSERT INTO `status_upload` VALUES (544, 'ra', 'data_RA_100421_02-20210410041119.xlsx', 'file/data_RA_100421_02-20210410041119.xlsx', 2, 18, NULL, 544, '2021-04-10 04:11:19', '2021-04-10 04:12:01');
INSERT INTO `status_upload` VALUES (545, 'mau', 'DATA 10APRIL21 0529-20210410052934.xlsx', 'file/DATA 10APRIL21 0529-20210410052934.xlsx', 2, 9, NULL, 545, '2021-04-10 05:29:34', '2021-04-10 05:30:01');
INSERT INTO `status_upload` VALUES (546, 'ra', 'data_RA_110321_04-20210410063806.xlsx', 'file/data_RA_110321_04-20210410063806.xlsx', 2, 18, NULL, 546, '2021-04-10 06:38:06', '2021-04-10 06:39:02');
INSERT INTO `status_upload` VALUES (547, 'ra', 'data_RA_100421_03-20210410063933.xlsx', 'file/data_RA_100421_03-20210410063933.xlsx', 2, 18, NULL, 547, '2021-04-10 06:39:33', '2021-04-10 06:40:01');
INSERT INTO `status_upload` VALUES (548, 'ra', 'data_RA_100421_04-20210410112425.xlsx', 'file/data_RA_100421_04-20210410112425.xlsx', 2, 18, NULL, 548, '2021-04-10 11:24:25', '2021-04-10 11:25:01');
INSERT INTO `status_upload` VALUES (549, 'mau', 'DATA 10APRIL21 1200-20210410170117.xlsx', 'file/DATA 10APRIL21 1200-20210410170117.xlsx', 2, 9, NULL, 549, '2021-04-10 17:01:17', '2021-04-10 17:02:01');
INSERT INTO `status_upload` VALUES (550, 'mau', 'DATA 10APRIL21 1500-20210410170231.xlsx', 'file/DATA 10APRIL21 1500-20210410170231.xlsx', 2, 9, NULL, 550, '2021-04-10 17:02:31', '2021-04-10 17:03:01');
INSERT INTO `status_upload` VALUES (551, 'ra', 'data_RA_100421_05-20210410215211.xlsx', 'file/data_RA_100421_05-20210410215211.xlsx', 2, 18, NULL, 551, '2021-04-10 21:52:11', '2021-04-10 21:53:01');
INSERT INTO `status_upload` VALUES (552, 'mau', 'DATA 11APRIL21 0053-20210411005431.xlsx', 'file/DATA 11APRIL21 0053-20210411005431.xlsx', 2, 9, NULL, 552, '2021-04-11 00:54:31', '2021-04-11 00:55:01');
INSERT INTO `status_upload` VALUES (553, 'ra', 'data_RA_110421_01-20210411012344.xlsx', 'file/data_RA_110421_01-20210411012344.xlsx', 2, 18, NULL, 553, '2021-04-11 01:23:44', '2021-04-11 01:24:01');
INSERT INTO `status_upload` VALUES (554, 'mau', 'DATA 11APRIL21 0229-20210411022935.xlsx', 'file/DATA 11APRIL21 0229-20210411022935.xlsx', 2, 9, NULL, 554, '2021-04-11 02:29:35', '2021-04-11 02:30:01');
INSERT INTO `status_upload` VALUES (555, '', 'DATA 11 APRIL 2021 0523-20210411052344.xlsx', 'file/DATA 11 APRIL 2021 0523-20210411052344.xlsx', 0, NULL, NULL, 555, '2021-04-11 05:23:44', '2021-04-11 05:23:44');
INSERT INTO `status_upload` VALUES (556, 'mau', 'DATA 11 APRIL 2021 0523-20210411052402.xlsx', 'file/DATA 11 APRIL 2021 0523-20210411052402.xlsx', 2, 9, NULL, 556, '2021-04-11 05:24:02', '2021-04-11 05:25:01');
INSERT INTO `status_upload` VALUES (557, 'mau', 'DATA 11 APRIL21 0700-20210411090542.xlsx', 'file/DATA 11 APRIL21 0700-20210411090542.xlsx', 2, 9, NULL, 557, '2021-04-11 09:05:42', '2021-04-11 09:06:01');
INSERT INTO `status_upload` VALUES (558, 'mau', 'DATA 11 APRIL21 1200-20210411121634.xlsx', 'file/DATA 11 APRIL21 1200-20210411121634.xlsx', 2, 9, NULL, 558, '2021-04-11 12:16:34', '2021-04-11 12:17:01');
INSERT INTO `status_upload` VALUES (559, 'mau', 'DATA 11 APRIL21 1300-20210411140016.xlsx', 'file/DATA 11 APRIL21 1300-20210411140016.xlsx', 2, 9, NULL, 559, '2021-04-11 14:00:16', '2021-04-11 14:01:01');
INSERT INTO `status_upload` VALUES (560, 'ra', 'data_RA_300321_14-20210412002019.xlsx', 'file/data_RA_300321_14-20210412002019.xlsx', 2, 18, NULL, 560, '2021-04-12 00:20:19', '2021-04-12 00:21:01');
INSERT INTO `status_upload` VALUES (561, 'mau', 'DATA 12APRIL21 0300-20210412033311.xlsx', 'file/DATA 12APRIL21 0300-20210412033311.xlsx', 2, 9, NULL, 561, '2021-04-12 03:33:11', '2021-04-12 03:34:01');
INSERT INTO `status_upload` VALUES (562, 'mau', 'DATA 12APRIL21 0400-20210412054248.xlsx', 'file/DATA 12APRIL21 0400-20210412054248.xlsx', 2, 9, NULL, 562, '2021-04-12 05:42:48', '2021-04-12 05:43:01');
INSERT INTO `status_upload` VALUES (563, 'ra', 'data_RA_300321_15-20210412055511.xlsx', 'file/data_RA_300321_15-20210412055511.xlsx', 2, 18, NULL, 563, '2021-04-12 05:55:11', '2021-04-12 05:56:01');
INSERT INTO `status_upload` VALUES (564, 'mau', 'DATA 12APRIL21 0600-20210412064812.xlsx', 'file/DATA 12APRIL21 0600-20210412064812.xlsx', 2, 9, NULL, 564, '2021-04-12 06:48:12', '2021-04-12 06:49:01');
INSERT INTO `status_upload` VALUES (565, 'mau', 'DATA 12APRIL21 0630-20210412071831.xlsx', 'file/DATA 12APRIL21 0630-20210412071831.xlsx', 2, 9, NULL, 565, '2021-04-12 07:18:31', '2021-04-12 07:19:01');
INSERT INTO `status_upload` VALUES (566, 'ra', 'data_RA_300321_16-20210412074000.xlsx', 'file/data_RA_300321_16-20210412074000.xlsx', 2, 18, NULL, 566, '2021-04-12 07:40:00', '2021-04-12 07:40:01');
INSERT INTO `status_upload` VALUES (567, 'mau', 'DATA 13APRIL21 0300-20210413045625.xlsx', 'file/DATA 13APRIL21 0300-20210413045625.xlsx', 2, 9, NULL, 567, '2021-04-13 04:56:25', '2021-04-13 04:57:01');
INSERT INTO `status_upload` VALUES (568, 'mau', 'DATA 13APRIL21 0707-20210413070811.xlsx', 'file/DATA 13APRIL21 0707-20210413070811.xlsx', 2, 9, NULL, 568, '2021-04-13 07:08:11', '2021-04-13 07:09:01');
INSERT INTO `status_upload` VALUES (569, 'mau', 'DATA 13APRIL21 0843-20210413084316.xlsx', 'file/DATA 13APRIL21 0843-20210413084316.xlsx', 2, 9, NULL, 569, '2021-04-13 08:43:16', '2021-04-13 08:44:01');
INSERT INTO `status_upload` VALUES (570, 'mau', 'DATA 13APRIL21 1144-20210413114454.xlsx', 'file/DATA 13APRIL21 1144-20210413114454.xlsx', 2, 9, NULL, 570, '2021-04-13 11:44:54', '2021-04-13 11:45:01');
INSERT INTO `status_upload` VALUES (571, '', 'DATA 13APRIL21 1401-20210413140210.xlsx', 'file/DATA 13APRIL21 1401-20210413140210.xlsx', 0, NULL, NULL, 571, '2021-04-13 14:02:10', '2021-04-13 14:02:10');
INSERT INTO `status_upload` VALUES (572, 'mau', 'DATA 13APRIL21 1401-20210413140228.xlsx', 'file/DATA 13APRIL21 1401-20210413140228.xlsx', 2, 9, NULL, 572, '2021-04-13 14:02:28', '2021-04-13 14:03:01');
INSERT INTO `status_upload` VALUES (573, 'ra', 'qg40-20210413235935.xlsx', 'file/qg40-20210413235935.xlsx', 2, 18, NULL, 573, '2021-04-13 23:59:35', '2021-04-14 00:00:01');
INSERT INTO `status_upload` VALUES (574, 'mau', 'DATA 14APRIL21 0000-20210414022522.xlsx', 'file/DATA 14APRIL21 0000-20210414022522.xlsx', 2, 9, NULL, 574, '2021-04-14 02:25:22', '2021-04-14 02:26:01');
INSERT INTO `status_upload` VALUES (575, 'mau', 'DATA 14APRIL21 0200-20210414035934.xlsx', 'file/DATA 14APRIL21 0200-20210414035934.xlsx', 2, 9, NULL, 575, '2021-04-14 03:59:34', '2021-04-14 04:00:01');
INSERT INTO `status_upload` VALUES (576, '', 'DATA 14APRIL21 0704-20210414070439.xlsx', 'file/DATA 14APRIL21 0704-20210414070439.xlsx', 0, NULL, NULL, 576, '2021-04-14 07:04:39', '2021-04-14 07:04:39');
INSERT INTO `status_upload` VALUES (577, 'mau', 'DATA 14APRIL21 0704-20210414070501.xlsx', 'file/DATA 14APRIL21 0704-20210414070501.xlsx', 2, 9, NULL, 577, '2021-04-14 07:05:01', '2021-04-14 07:05:01');
INSERT INTO `status_upload` VALUES (578, 'mau', 'DATA 14APRIL21 0951-20210414095215.xlsx', 'file/DATA 14APRIL21 0951-20210414095215.xlsx', 2, 9, NULL, 578, '2021-04-14 09:52:15', '2021-04-14 09:53:01');
INSERT INTO `status_upload` VALUES (579, 'ra', 'data_RA_300321_17-20210414113440.xlsx', 'file/data_RA_300321_17-20210414113440.xlsx', 2, 18, NULL, 579, '2021-04-14 11:34:40', '2021-04-14 11:35:01');
INSERT INTO `status_upload` VALUES (580, 'mau', 'DATA 14APRIL21 1211-20210414121147.xlsx', 'file/DATA 14APRIL21 1211-20210414121147.xlsx', 2, 9, NULL, 580, '2021-04-14 12:11:47', '2021-04-14 12:12:02');
INSERT INTO `status_upload` VALUES (581, 'mau', 'DATA 15APRIL21 0000-20210415010613.xlsx', 'file/DATA 15APRIL21 0000-20210415010613.xlsx', 2, 9, NULL, 581, '2021-04-15 01:06:13', '2021-04-15 01:07:01');
INSERT INTO `status_upload` VALUES (582, 'ra', 'data_RA_300321_18-20210415094635.xlsx', 'file/data_RA_300321_18-20210415094635.xlsx', 2, 18, NULL, 582, '2021-04-15 09:46:35', '2021-04-15 09:47:01');
INSERT INTO `status_upload` VALUES (583, 'mau', 'DATA 15APRIL21 0800-20210415124848.xlsx', 'file/DATA 15APRIL21 0800-20210415124848.xlsx', 2, 9, NULL, 583, '2021-04-15 12:48:48', '2021-04-15 12:49:01');
INSERT INTO `status_upload` VALUES (584, 'mau', 'DATA 15APRIL21 1100-20210415124925.xlsx', 'file/DATA 15APRIL21 1100-20210415124925.xlsx', 2, 9, NULL, 584, '2021-04-15 12:49:25', '2021-04-15 12:50:01');
INSERT INTO `status_upload` VALUES (585, 'mau', 'DATA 15APRIL21 1400-20210415140832.xlsx', 'file/DATA 15APRIL21 1400-20210415140832.xlsx', 2, 9, NULL, 585, '2021-04-15 14:08:32', '2021-04-15 14:09:01');
INSERT INTO `status_upload` VALUES (586, 'mau', 'DATA 16APRIL21 0129-20210416012941.xlsx', 'file/DATA 16APRIL21 0129-20210416012941.xlsx', 2, 9, NULL, 586, '2021-04-16 01:29:41', '2021-04-16 01:30:01');
INSERT INTO `status_upload` VALUES (587, 'mau', 'DATA 16APRIL21 0314-20210416031435.xlsx', 'file/DATA 16APRIL21 0314-20210416031435.xlsx', 2, 9, NULL, 587, '2021-04-16 03:14:35', '2021-04-16 03:15:01');
INSERT INTO `status_upload` VALUES (588, 'ra', 'data_RA_300321_19-20210416042257.xlsx', 'file/data_RA_300321_19-20210416042257.xlsx', 2, 18, NULL, 588, '2021-04-16 04:22:57', '2021-04-16 04:23:02');
INSERT INTO `status_upload` VALUES (589, 'mau', 'DATA 16APRIL21 0556-20210416055616.xlsx', 'file/DATA 16APRIL21 0556-20210416055616.xlsx', 2, 9, NULL, 589, '2021-04-16 05:56:16', '2021-04-16 05:57:01');
INSERT INTO `status_upload` VALUES (590, 'ra', 'data_RA_300321_20-20210416055900.xlsx', 'file/data_RA_300321_20-20210416055900.xlsx', 2, 18, NULL, 590, '2021-04-16 05:59:00', '2021-04-16 05:59:01');
INSERT INTO `status_upload` VALUES (591, 'mau', 'DATA 16APRIL21 1414-20210416162819.xlsx', 'file/DATA 16APRIL21 1414-20210416162819.xlsx', 2, 9, NULL, 591, '2021-04-16 16:28:19', '2021-04-16 16:29:01');
INSERT INTO `status_upload` VALUES (592, 'mau', 'DATA 17APRIL21 0039-20210417004012.xlsx', 'file/DATA 17APRIL21 0039-20210417004012.xlsx', 2, 9, NULL, 592, '2021-04-17 00:40:12', '2021-04-17 00:41:01');
INSERT INTO `status_upload` VALUES (593, 'mau', 'DATA 17APRIL21 0224-20210417022513.xlsx', 'file/DATA 17APRIL21 0224-20210417022513.xlsx', 2, 9, NULL, 593, '2021-04-17 02:25:13', '2021-04-17 02:26:01');
INSERT INTO `status_upload` VALUES (594, 'mau', 'DATA 17DATA21 0334-20210417033509.xlsx', 'file/DATA 17DATA21 0334-20210417033509.xlsx', 2, 9, NULL, 594, '2021-04-17 03:35:09', '2021-04-17 03:36:01');
INSERT INTO `status_upload` VALUES (595, 'ra', 'data_RA_300321_21-20210417043421.xlsx', 'file/data_RA_300321_21-20210417043421.xlsx', 2, 18, NULL, 595, '2021-04-17 04:34:21', '2021-04-17 04:35:02');
INSERT INTO `status_upload` VALUES (596, 'mau', 'DATA 17APRIL21 0512-20210417051238.xlsx', 'file/DATA 17APRIL21 0512-20210417051238.xlsx', 2, 9, NULL, 596, '2021-04-17 05:12:38', '2021-04-17 05:13:01');
INSERT INTO `status_upload` VALUES (597, 'mau', 'DATA 17APRIL21 0646-20210417064634.xlsx', 'file/DATA 17APRIL21 0646-20210417064634.xlsx', 2, 9, NULL, 597, '2021-04-17 06:46:34', '2021-04-17 06:47:02');
INSERT INTO `status_upload` VALUES (598, 'ra', 'QG41-20210417115919.xlsx', 'file/QG41-20210417115919.xlsx', 2, 18, NULL, 598, '2021-04-17 11:59:19', '2021-04-17 12:00:02');
INSERT INTO `status_upload` VALUES (599, 'ra', 'data_RA_300321_22-20210418015405.xlsx', 'file/data_RA_300321_22-20210418015405.xlsx', 2, 18, NULL, 599, '2021-04-18 01:54:05', '2021-04-18 01:55:01');
INSERT INTO `status_upload` VALUES (600, 'ra', 'data_RA_300321_23-20210418044706.xlsx', 'file/data_RA_300321_23-20210418044706.xlsx', 2, 18, NULL, 600, '2021-04-18 04:47:06', '2021-04-18 04:48:01');
INSERT INTO `status_upload` VALUES (601, 'mau', 'DATA 18APRIL21 0300-20210418055249.xlsx', 'file/DATA 18APRIL21 0300-20210418055249.xlsx', 2, 9, NULL, 601, '2021-04-18 05:52:49', '2021-04-18 05:53:01');
INSERT INTO `status_upload` VALUES (602, 'mau', 'DATA 18APRIL0500-20210418065255.xlsx', 'file/DATA 18APRIL0500-20210418065255.xlsx', 2, 9, NULL, 602, '2021-04-18 06:52:55', '2021-04-18 06:53:02');
INSERT INTO `status_upload` VALUES (603, 'ra', 'data_RA_190421_01-20210419015405.xlsx', 'file/data_RA_190421_01-20210419015405.xlsx', 2, 18, NULL, 603, '2021-04-19 01:54:05', '2021-04-19 01:55:01');
INSERT INTO `status_upload` VALUES (604, 'ra', 'data_RA_190421_02-20210419060245.xlsx', 'file/data_RA_190421_02-20210419060245.xlsx', 2, 18, NULL, 604, '2021-04-19 06:02:45', '2021-04-19 06:03:01');
INSERT INTO `status_upload` VALUES (605, 'mau', 'DATA 19APRIL21 0806-20210419080644.xlsx', 'file/DATA 19APRIL21 0806-20210419080644.xlsx', 2, 9, NULL, 605, '2021-04-19 08:06:44', '2021-04-19 08:07:01');
INSERT INTO `status_upload` VALUES (606, 'ra', 'data_RA_190421_03-20210419104824.xlsx', 'file/data_RA_190421_03-20210419104824.xlsx', 2, 18, NULL, 606, '2021-04-19 10:48:24', '2021-04-19 10:49:01');
INSERT INTO `status_upload` VALUES (607, 'mau', 'DATA 19APRIL21 1159-20210419120015.xlsx', 'file/DATA 19APRIL21 1159-20210419120015.xlsx', 2, 9, NULL, 607, '2021-04-19 12:00:15', '2021-04-19 12:01:01');
INSERT INTO `status_upload` VALUES (608, 'ra', 'data_RA_190421_04-20210419231856.xlsx', 'file/data_RA_190421_04-20210419231856.xlsx', 2, 18, NULL, 608, '2021-04-19 23:18:56', '2021-04-19 23:19:02');
INSERT INTO `status_upload` VALUES (609, '', 'DATA 20 APRIL21 0001-20210420013739.xlsx', 'file/DATA 20 APRIL21 0001-20210420013739.xlsx', 0, NULL, NULL, 609, '2021-04-20 01:37:39', '2021-04-20 01:37:39');
INSERT INTO `status_upload` VALUES (610, 'mau', 'DATA 20 APRIL21 0001-20210420014834.xlsx', 'file/DATA 20 APRIL21 0001-20210420014834.xlsx', 2, 9, NULL, 610, '2021-04-20 01:48:34', '2021-04-20 01:49:01');
INSERT INTO `status_upload` VALUES (611, '', 'data_RA_200421_01-20210420014851.xlsx', 'file/data_RA_200421_01-20210420014851.xlsx', 0, NULL, NULL, 611, '2021-04-20 01:48:51', '2021-04-20 01:48:51');
INSERT INTO `status_upload` VALUES (612, 'ra', 'data_RA_200421_01-20210420014919.xlsx', 'file/data_RA_200421_01-20210420014919.xlsx', 2, 18, NULL, 612, '2021-04-20 01:49:19', '2021-04-20 01:50:01');
INSERT INTO `status_upload` VALUES (613, 'ra', 'data_RA_200421_02-20210420043130.xlsx', 'file/data_RA_200421_02-20210420043130.xlsx', 2, 18, NULL, 613, '2021-04-20 04:31:30', '2021-04-20 04:32:01');
INSERT INTO `status_upload` VALUES (614, 'ra', 'data_RA_200421_03-20210420055835.xlsx', 'file/data_RA_200421_03-20210420055835.xlsx', 2, 18, NULL, 614, '2021-04-20 05:58:35', '2021-04-20 05:59:01');
INSERT INTO `status_upload` VALUES (615, 'mau', 'DATA 20 APRIL21 0300-20210420060107.xlsx', 'file/DATA 20 APRIL21 0300-20210420060107.xlsx', 2, 9, NULL, 615, '2021-04-20 06:01:07', '2021-04-20 06:02:01');
INSERT INTO `status_upload` VALUES (616, 'ra', 'data_RA_200421_04-20210420063914.xlsx', 'file/data_RA_200421_04-20210420063914.xlsx', 2, 18, NULL, 616, '2021-04-20 06:39:14', '2021-04-20 06:40:02');
INSERT INTO `status_upload` VALUES (617, 'mau', 'DATA 20APRIL21 0751-20210420075809.xlsx', 'file/DATA 20APRIL21 0751-20210420075809.xlsx', 2, 9, NULL, 617, '2021-04-20 07:58:09', '2021-04-20 07:59:01');
INSERT INTO `status_upload` VALUES (618, 'ra', 'data_RA_300321_24-20210420113511.xlsx', 'file/data_RA_300321_24-20210420113511.xlsx', 2, 18, NULL, 618, '2021-04-20 11:35:11', '2021-04-20 11:36:01');
INSERT INTO `status_upload` VALUES (619, '', 'DATA 20APRIL21 1213-20210420121409.xlsx', 'file/DATA 20APRIL21 1213-20210420121409.xlsx', 0, NULL, NULL, 619, '2021-04-20 12:14:09', '2021-04-20 12:14:09');
INSERT INTO `status_upload` VALUES (620, 'mau', 'DATA 20APRIL21 1213-20210420121422.xlsx', 'file/DATA 20APRIL21 1213-20210420121422.xlsx', 2, 9, NULL, 620, '2021-04-20 12:14:22', '2021-04-20 12:15:01');
INSERT INTO `status_upload` VALUES (621, 'mau', 'DATA 20APRIL21 1351-20210420135205.xlsx', 'file/DATA 20APRIL21 1351-20210420135205.xlsx', 2, 9, NULL, 621, '2021-04-20 13:52:05', '2021-04-20 13:53:01');
INSERT INTO `status_upload` VALUES (622, 'mau', 'DATA 20 APRIL21 1420-20210420142018.xlsx', 'file/DATA 20 APRIL21 1420-20210420142018.xlsx', 2, 9, NULL, 622, '2021-04-20 14:20:18', '2021-04-20 14:21:01');
INSERT INTO `status_upload` VALUES (623, 'ra', 'data_RA_210421_01-20210421001208.xlsx', 'file/data_RA_210421_01-20210421001208.xlsx', 2, 18, NULL, 623, '2021-04-21 00:12:08', '2021-04-21 00:13:01');
INSERT INTO `status_upload` VALUES (624, 'ra', 'data_RA_210421_02-20210421023711.xlsx', 'file/data_RA_210421_02-20210421023711.xlsx', 2, 18, NULL, 624, '2021-04-21 02:37:11', '2021-04-21 02:38:02');
INSERT INTO `status_upload` VALUES (625, 'ra', 'data_RA_210421_03-20210421045509.xlsx', 'file/data_RA_210421_03-20210421045509.xlsx', 2, 18, NULL, 625, '2021-04-21 04:55:09', '2021-04-21 04:56:01');
INSERT INTO `status_upload` VALUES (626, 'ra', 'data_RA_210421_04-20210421053841.xlsx', 'file/data_RA_210421_04-20210421053841.xlsx', 2, 18, NULL, 626, '2021-04-21 05:38:41', '2021-04-21 05:39:01');
INSERT INTO `status_upload` VALUES (627, 'ra', 'data_RA_300321_25-20210421123956.xlsx', 'file/data_RA_300321_25-20210421123956.xlsx', 2, 18, NULL, 627, '2021-04-21 12:39:56', '2021-04-21 12:40:01');
INSERT INTO `status_upload` VALUES (628, 'ra', 'data_RA_210421_05-20210421173353.xlsx', 'file/data_RA_210421_05-20210421173353.xlsx', 2, 18, NULL, 628, '2021-04-21 17:33:53', '2021-04-21 17:34:01');
INSERT INTO `status_upload` VALUES (629, 'ra', 'data_RA_210421_06-20210421233158.xlsx', 'file/data_RA_210421_06-20210421233158.xlsx', 2, 18, NULL, 629, '2021-04-21 23:31:58', '2021-04-21 23:32:02');
INSERT INTO `status_upload` VALUES (630, 'mau', 'DATA 21APRIL2342-20210421234329.xlsx', 'file/DATA 21APRIL2342-20210421234329.xlsx', 2, 9, NULL, 630, '2021-04-21 23:43:29', '2021-04-21 23:44:01');
INSERT INTO `status_upload` VALUES (631, 'mau', 'DATA 22APRIL21 0113-20210422011327.xlsx', 'file/DATA 22APRIL21 0113-20210422011327.xlsx', 2, 9, NULL, 631, '2021-04-22 01:13:27', '2021-04-22 01:14:01');
INSERT INTO `status_upload` VALUES (632, 'ra', 'data_RA_220421_01-20210422013116.xlsx', 'file/data_RA_220421_01-20210422013116.xlsx', 2, 18, NULL, 632, '2021-04-22 01:31:16', '2021-04-22 01:32:02');
INSERT INTO `status_upload` VALUES (633, 'mau', 'DATA 22APRIL21 0250-20210422025016.xlsx', 'file/DATA 22APRIL21 0250-20210422025016.xlsx', 2, 9, NULL, 633, '2021-04-22 02:50:16', '2021-04-22 02:51:01');
INSERT INTO `status_upload` VALUES (634, 'mau', 'DATA 22APRIL21 0341-20210422034127.xlsx', 'file/DATA 22APRIL21 0341-20210422034127.xlsx', 2, 9, NULL, 634, '2021-04-22 03:41:27', '2021-04-22 03:42:01');
INSERT INTO `status_upload` VALUES (635, '', 'data_RA_220421_02-20210422041639.xlsx', 'file/data_RA_220421_02-20210422041639.xlsx', 0, NULL, NULL, 635, '2021-04-22 04:16:39', '2021-04-22 04:16:39');
INSERT INTO `status_upload` VALUES (636, 'ra', 'data_RA_220421_02-20210422041715.xlsx', 'file/data_RA_220421_02-20210422041715.xlsx', 2, 18, NULL, 636, '2021-04-22 04:17:15', '2021-04-22 04:18:01');
INSERT INTO `status_upload` VALUES (637, '', 'DATA 22APRIL21 0604-20210422060448.xlsx', 'file/DATA 22APRIL21 0604-20210422060448.xlsx', 0, NULL, NULL, 637, '2021-04-22 06:04:48', '2021-04-22 06:04:48');
INSERT INTO `status_upload` VALUES (638, 'mau', 'DATA 22APRIL21 0604-20210422060502.xlsx', 'file/DATA 22APRIL21 0604-20210422060502.xlsx', 2, 9, NULL, 638, '2021-04-22 06:05:02', '2021-04-22 06:06:01');
INSERT INTO `status_upload` VALUES (639, 'ra', 'data_RA_220421_03-20210422122302.xlsx', 'file/data_RA_220421_03-20210422122302.xlsx', 2, 18, NULL, 639, '2021-04-22 12:23:02', '2021-04-22 12:24:01');
INSERT INTO `status_upload` VALUES (640, 'mau', 'DATA 22APRIL21 1300-20210422170228.xlsx', 'file/DATA 22APRIL21 1300-20210422170228.xlsx', 2, 9, NULL, 640, '2021-04-22 17:02:28', '2021-04-22 17:03:01');
INSERT INTO `status_upload` VALUES (641, 'mau', 'DATA 23APRIL21 0042-20210423004323.xlsx', 'file/DATA 23APRIL21 0042-20210423004323.xlsx', 2, 9, NULL, 641, '2021-04-23 00:43:23', '2021-04-23 00:44:01');
INSERT INTO `status_upload` VALUES (642, '', 'data_RA_300321_26-20210423034356.xlsx', 'file/data_RA_300321_26-20210423034356.xlsx', 0, NULL, NULL, 642, '2021-04-23 03:43:56', '2021-04-23 03:43:56');
INSERT INTO `status_upload` VALUES (643, 'mau', 'DATA 23 APRIL21 0357-20210423035740.xlsx', 'file/DATA 23 APRIL21 0357-20210423035740.xlsx', 2, 9, NULL, 643, '2021-04-23 03:57:40', '2021-04-23 03:58:01');
INSERT INTO `status_upload` VALUES (644, 'ra', 'data_RA_300321_26-20210423043314.xlsx', 'file/data_RA_300321_26-20210423043314.xlsx', 2, 18, NULL, 644, '2021-04-23 04:33:14', '2021-04-23 04:34:01');
INSERT INTO `status_upload` VALUES (645, 'mau', 'DATA 23 APRIL21 0522-20210423052212.xlsx', 'file/DATA 23 APRIL21 0522-20210423052212.xlsx', 2, 9, NULL, 645, '2021-04-23 05:22:12', '2021-04-23 05:23:01');
INSERT INTO `status_upload` VALUES (646, 'mau', 'DATA 23 APRIL21 0700-20210423084438.xlsx', 'file/DATA 23 APRIL21 0700-20210423084438.xlsx', 2, 9, NULL, 646, '2021-04-23 08:44:38', '2021-04-23 08:45:01');
INSERT INTO `status_upload` VALUES (647, 'ra', 'data_RA_240421_01-20210424011636.xlsx', 'file/data_RA_240421_01-20210424011636.xlsx', 2, 18, NULL, 647, '2021-04-24 01:16:36', '2021-04-24 01:17:01');
INSERT INTO `status_upload` VALUES (648, 'ra', 'data_RA_240421_02-20210424072005.xlsx', 'file/data_RA_240421_02-20210424072005.xlsx', 2, 18, NULL, 648, '2021-04-24 07:20:05', '2021-04-24 07:21:02');
INSERT INTO `status_upload` VALUES (649, 'ra', 'D 8713 FH 2040811-20210424101735.xlsx', 'file/D 8713 FH 2040811-20210424101735.xlsx', 2, 18, NULL, 649, '2021-04-24 10:17:35', '2021-04-24 10:18:02');
INSERT INTO `status_upload` VALUES (650, '', 'D 8714 FH 2040812-20210424131511.xlsx', 'file/D 8714 FH 2040812-20210424131511.xlsx', 0, NULL, NULL, 650, '2021-04-24 13:15:11', '2021-04-24 13:15:11');
INSERT INTO `status_upload` VALUES (651, 'ra', 'D 8714 FH 2040812-20210424131550.xlsx', 'file/D 8714 FH 2040812-20210424131550.xlsx', 2, 18, NULL, 651, '2021-04-24 13:15:50', '2021-04-24 13:16:01');
INSERT INTO `status_upload` VALUES (652, 'mau', 'DATA 25APRIL21 0500-20210425052051.xlsx', 'file/DATA 25APRIL21 0500-20210425052051.xlsx', 2, 9, NULL, 652, '2021-04-25 05:20:51', '2021-04-25 05:21:02');
INSERT INTO `status_upload` VALUES (653, 'ra', 'qg42-20210425073714.xlsx', 'file/qg42-20210425073714.xlsx', 2, 18, NULL, 653, '2021-04-25 07:37:14', '2021-04-25 07:38:02');
INSERT INTO `status_upload` VALUES (654, 'mau', 'DATA 25APRIL21 0859-20210425085943.xlsx', 'file/DATA 25APRIL21 0859-20210425085943.xlsx', 2, 9, NULL, 654, '2021-04-25 08:59:43', '2021-04-25 09:00:01');
INSERT INTO `status_upload` VALUES (655, 'mau', 'DATA 25APRIL21 083-20210425093354.xlsx', 'file/DATA 25APRIL21 083-20210425093354.xlsx', 2, 9, NULL, 655, '2021-04-25 09:33:54', '2021-04-25 09:34:02');
INSERT INTO `status_upload` VALUES (656, 'ra', 'data_RA_250421_01-20210425113342.xlsx', 'file/data_RA_250421_01-20210425113342.xlsx', 2, 18, NULL, 656, '2021-04-25 11:33:42', '2021-04-25 11:34:01');
INSERT INTO `status_upload` VALUES (657, 'mau', 'DATA 25APRIL21 1214-20210425121415.xlsx', 'file/DATA 25APRIL21 1214-20210425121415.xlsx', 2, 9, NULL, 657, '2021-04-25 12:14:15', '2021-04-25 12:15:01');
INSERT INTO `status_upload` VALUES (658, 'mau', 'DATA 25APRIL21 1408-20210425140913.xlsx', 'file/DATA 25APRIL21 1408-20210425140913.xlsx', 2, 9, NULL, 658, '2021-04-25 14:09:13', '2021-04-25 14:10:01');
INSERT INTO `status_upload` VALUES (659, 'mau', 'DATA 26 APRIL21 0100-20210426041317.xlsx', 'file/DATA 26 APRIL21 0100-20210426041317.xlsx', 2, 9, NULL, 659, '2021-04-26 04:13:17', '2021-04-26 04:14:01');
INSERT INTO `status_upload` VALUES (660, 'ra', 'qg43-20210426062327.xlsx', 'file/qg43-20210426062327.xlsx', 2, 18, NULL, 660, '2021-04-26 06:23:27', '2021-04-26 06:24:02');
INSERT INTO `status_upload` VALUES (661, 'mau', 'DATA 26APRIL21 0803-20210426080352.xlsx', 'file/DATA 26APRIL21 0803-20210426080352.xlsx', 2, 9, NULL, 661, '2021-04-26 08:03:52', '2021-04-26 08:04:01');
INSERT INTO `status_upload` VALUES (662, 'ra', 'qg44-20210426132246.xlsx', 'file/qg44-20210426132246.xlsx', 2, 18, NULL, 662, '2021-04-26 13:22:46', '2021-04-26 13:23:01');
INSERT INTO `status_upload` VALUES (663, 'mau', 'DATA 26APRIL21 1407-20210426140802.xlsx', 'file/DATA 26APRIL21 1407-20210426140802.xlsx', 2, 9, NULL, 663, '2021-04-26 14:08:02', '2021-04-26 14:09:01');
INSERT INTO `status_upload` VALUES (664, 'mau', 'DATA 26APRIL21 1431-20210426143212.xlsx', 'file/DATA 26APRIL21 1431-20210426143212.xlsx', 2, 9, NULL, 664, '2021-04-26 14:32:12', '2021-04-26 14:33:01');
INSERT INTO `status_upload` VALUES (665, 'ra', 'qg45-20210426153644.xlsx', 'file/qg45-20210426153644.xlsx', 2, 18, NULL, 665, '2021-04-26 15:36:44', '2021-04-26 15:37:01');
INSERT INTO `status_upload` VALUES (666, 'ra', 'qg46-20210427011151.xlsx', 'file/qg46-20210427011151.xlsx', 2, 18, NULL, 666, '2021-04-27 01:11:51', '2021-04-27 01:12:01');
INSERT INTO `status_upload` VALUES (667, 'mau', 'DATA 27 APRIL21 0100-20210427033808.xlsx', 'file/DATA 27 APRIL21 0100-20210427033808.xlsx', 2, 9, NULL, 667, '2021-04-27 03:38:08', '2021-04-27 03:39:01');
INSERT INTO `status_upload` VALUES (668, 'ra', 'qg47-20210427041202.xlsx', 'file/qg47-20210427041202.xlsx', 2, 18, NULL, 668, '2021-04-27 04:12:02', '2021-04-27 04:13:01');
INSERT INTO `status_upload` VALUES (669, 'ra', 'qg48-20210427073312.xlsx', 'file/qg48-20210427073312.xlsx', 2, 18, NULL, 669, '2021-04-27 07:33:12', '2021-04-27 07:34:02');
INSERT INTO `status_upload` VALUES (670, 'ra', 'data_RA_300321_27-20210427134611.xlsx', 'file/data_RA_300321_27-20210427134611.xlsx', 2, 18, NULL, 670, '2021-04-27 13:46:11', '2021-04-27 13:47:01');
INSERT INTO `status_upload` VALUES (671, 'mau', 'DATA 27APRIL21 1000-20210427163614.xlsx', 'file/DATA 27APRIL21 1000-20210427163614.xlsx', 2, 9, NULL, 671, '2021-04-27 16:36:14', '2021-04-27 16:37:02');
INSERT INTO `status_upload` VALUES (672, 'ra', 'data_RA_300321_28-20210427183837.xlsx', 'file/data_RA_300321_28-20210427183837.xlsx', 2, 18, NULL, 672, '2021-04-27 18:38:37', '2021-04-27 18:39:01');
INSERT INTO `status_upload` VALUES (673, 'ra', 'qg49-20210428013857.xlsx', 'file/qg49-20210428013857.xlsx', 2, 18, NULL, 673, '2021-04-28 01:38:57', '2021-04-28 01:39:01');
INSERT INTO `status_upload` VALUES (674, 'mau', 'DATA 28APRIL21 0326-20210428032653.xlsx', 'file/DATA 28APRIL21 0326-20210428032653.xlsx', 2, 9, NULL, 674, '2021-04-28 03:26:53', '2021-04-28 03:27:01');
INSERT INTO `status_upload` VALUES (675, 'ra', 'qg50-20210428043137.xlsx', 'file/qg50-20210428043137.xlsx', 2, 18, NULL, 675, '2021-04-28 04:31:37', '2021-04-28 04:32:01');
INSERT INTO `status_upload` VALUES (676, 'mau', 'DATA 28APRIL21 0629-20210428062925.xlsx', 'file/DATA 28APRIL21 0629-20210428062925.xlsx', 2, 9, NULL, 676, '2021-04-28 06:29:25', '2021-04-28 06:30:01');
INSERT INTO `status_upload` VALUES (677, 'ra', 'qg51-20210428071256.xlsx', 'file/qg51-20210428071256.xlsx', 2, 18, NULL, 677, '2021-04-28 07:12:56', '2021-04-28 07:13:01');
INSERT INTO `status_upload` VALUES (678, 'ra', 'data_RA_300321_29-20210429014506.xlsx', 'file/data_RA_300321_29-20210429014506.xlsx', 2, 18, NULL, 678, '2021-04-29 01:45:06', '2021-04-29 01:46:01');
INSERT INTO `status_upload` VALUES (679, 'mau', 'DATA 29APRIL21 0337-20210429033731.xlsx', 'file/DATA 29APRIL21 0337-20210429033731.xlsx', 2, 9, NULL, 679, '2021-04-29 03:37:31', '2021-04-29 03:38:01');
INSERT INTO `status_upload` VALUES (680, 'ra', 'data_RA_300321_30-20210429064103.xlsx', 'file/data_RA_300321_30-20210429064103.xlsx', 2, 18, NULL, 680, '2021-04-29 06:41:03', '2021-04-29 06:42:01');
INSERT INTO `status_upload` VALUES (681, 'ra', 'data_RA_300321_31-20210430001817.xlsx', 'file/data_RA_300321_31-20210430001817.xlsx', 2, 18, NULL, 681, '2021-04-30 00:18:17', '2021-04-30 00:19:01');
INSERT INTO `status_upload` VALUES (682, 'mau', 'DATA 01 MEI 2021 0806-20210501080656.xlsx', 'file/DATA 01 MEI 2021 0806-20210501080656.xlsx', 2, 9, NULL, 682, '2021-05-01 08:06:56', '2021-05-01 08:07:01');
INSERT INTO `status_upload` VALUES (683, 'mau', 'DATA 01 MEI 2021 1232-20210501123255.xlsx', 'file/DATA 01 MEI 2021 1232-20210501123255.xlsx', 2, 9, NULL, 683, '2021-05-01 12:32:55', '2021-05-01 12:33:02');
INSERT INTO `status_upload` VALUES (684, 'ra', 'D 8713 FH 2040872-20210501130345.xlsx', 'file/D 8713 FH 2040872-20210501130345.xlsx', 2, 18, NULL, 684, '2021-05-01 13:03:45', '2021-05-01 13:04:01');
INSERT INTO `status_upload` VALUES (685, 'mau', 'DATA 01 MEI 2021 1428-20210501142834.xlsx', 'file/DATA 01 MEI 2021 1428-20210501142834.xlsx', 2, 9, NULL, 685, '2021-05-01 14:28:34', '2021-05-01 14:29:01');
INSERT INTO `status_upload` VALUES (686, 'mau', 'DATA 02 MEI 2021 0000-20210502023939.xlsx', 'file/DATA 02 MEI 2021 0000-20210502023939.xlsx', 2, 9, NULL, 686, '2021-05-02 02:39:39', '2021-05-02 02:40:01');
INSERT INTO `status_upload` VALUES (687, 'ra', 'QG52-20210502064336.xlsx', 'file/QG52-20210502064336.xlsx', 2, 18, NULL, 687, '2021-05-02 06:43:36', '2021-05-02 06:44:01');
INSERT INTO `status_upload` VALUES (688, 'ra', 'D 8714 FH 2040878-20210502103634.xlsx', 'file/D 8714 FH 2040878-20210502103634.xlsx', 0, 18, NULL, 688, '2021-05-02 10:36:34', '2021-05-02 10:36:34');
INSERT INTO `status_upload` VALUES (689, 'mau', 'DATA 02 MEI 2021 1124-20210502112454.xlsx', 'file/DATA 02 MEI 2021 1124-20210502112454.xlsx', 2, 9, NULL, 689, '2021-05-02 11:24:54', '2021-05-02 11:25:01');
INSERT INTO `status_upload` VALUES (690, 'mau', 'DATA 02 MEI 2021 1412-20210502141230.xlsx', 'file/DATA 02 MEI 2021 1412-20210502141230.xlsx', 2, 9, NULL, 690, '2021-05-02 14:12:30', '2021-05-02 14:13:01');
INSERT INTO `status_upload` VALUES (691, 'mau', 'DATA 04MEI2021 0304-20210504030450.xlsx', 'file/DATA 04MEI2021 0304-20210504030450.xlsx', 9, 9, NULL, 691, '2021-05-04 03:04:50', '2021-05-04 03:05:01');
INSERT INTO `status_upload` VALUES (692, 'mau', 'DATA 04MEI2021 0304-20210504033728.xlsx', 'file/DATA 04MEI2021 0304-20210504033728.xlsx', 9, 9, NULL, 692, '2021-05-04 03:37:28', '2021-05-04 03:38:01');
INSERT INTO `status_upload` VALUES (693, 'mau', 'DATA 04MEI2021 0304-20210504034010.xlsx', 'file/DATA 04MEI2021 0304-20210504034010.xlsx', 9, 9, NULL, 693, '2021-05-04 03:40:10', '2021-05-04 03:41:01');
INSERT INTO `status_upload` VALUES (694, 'mau', 'DATA 04MEI2021 0520-20210504052104.xlsx', 'file/DATA 04MEI2021 0520-20210504052104.xlsx', 2, 9, NULL, 694, '2021-05-04 05:21:04', '2021-05-04 05:22:01');
INSERT INTO `status_upload` VALUES (695, 'ra', 'data_RA_300321_32-20210504061357.xlsx', 'file/data_RA_300321_32-20210504061357.xlsx', 2, 18, NULL, 695, '2021-05-04 06:13:57', '2021-05-04 06:14:02');
INSERT INTO `status_upload` VALUES (696, 'ra', 'data_RA_300321_33-20210505012743.xlsx', 'file/data_RA_300321_33-20210505012743.xlsx', 2, 18, NULL, 696, '2021-05-05 01:27:43', '2021-05-05 01:28:02');
INSERT INTO `status_upload` VALUES (697, 'mau', 'DATA 05MEI21 0343-20210505034353.xlsx', 'file/DATA 05MEI21 0343-20210505034353.xlsx', 2, 9, NULL, 697, '2021-05-05 03:43:53', '2021-05-05 03:44:01');
INSERT INTO `status_upload` VALUES (698, 'mau', 'DATA 05 MEI 2021 0507-20210505050818.xlsx', 'file/DATA 05 MEI 2021 0507-20210505050818.xlsx', 2, 9, NULL, 698, '2021-05-05 05:08:18', '2021-05-05 05:09:01');
INSERT INTO `status_upload` VALUES (699, 'ra', 'data_RA_300321_34-20210506012303.xlsx', 'file/data_RA_300321_34-20210506012303.xlsx', 2, 18, NULL, 699, '2021-05-06 01:23:03', '2021-05-06 01:24:02');
INSERT INTO `status_upload` VALUES (700, 'ra', 'QG53-20210506184739.xlsx', 'file/QG53-20210506184739.xlsx', 2, 18, NULL, 700, '2021-05-06 18:47:39', '2021-05-06 18:48:02');
INSERT INTO `status_upload` VALUES (701, 'ra', 'QG54-20210507025325.xlsx', 'file/QG54-20210507025325.xlsx', 2, 18, NULL, 701, '2021-05-07 02:53:25', '2021-05-07 02:54:01');
INSERT INTO `status_upload` VALUES (702, 'ra', 'QG55-20210507141808.xlsx', 'file/QG55-20210507141808.xlsx', 2, 18, NULL, 702, '2021-05-07 14:18:08', '2021-05-07 14:19:01');
INSERT INTO `status_upload` VALUES (703, 'ra', 'QG56-20210508084451.xlsx', 'file/QG56-20210508084451.xlsx', 2, 18, NULL, 703, '2021-05-08 08:44:51', '2021-05-08 08:45:02');
INSERT INTO `status_upload` VALUES (704, 'ra', 'QG57-20210509120040.xlsx', 'file/QG57-20210509120040.xlsx', 2, 18, NULL, 704, '2021-05-09 12:00:40', '2021-05-09 12:01:02');
INSERT INTO `status_upload` VALUES (705, 'ra', 'QG58-20210510045716.xlsx', 'file/QG58-20210510045716.xlsx', 2, 18, NULL, 705, '2021-05-10 04:57:16', '2021-05-10 04:58:01');
INSERT INTO `status_upload` VALUES (706, 'ra', 'QG59-20210510142136.xlsx', 'file/QG59-20210510142136.xlsx', 2, 18, NULL, 706, '2021-05-10 14:21:36', '2021-05-10 14:22:01');
INSERT INTO `status_upload` VALUES (707, 'ra', 'data_RA_300321_35-20210511054419.xlsx', 'file/data_RA_300321_35-20210511054419.xlsx', 2, 18, NULL, 707, '2021-05-11 05:44:19', '2021-05-11 05:45:01');
INSERT INTO `status_upload` VALUES (708, '', 'data_RA_300321_36-20210512051057.xlsx', 'file/data_RA_300321_36-20210512051057.xlsx', 0, NULL, NULL, 708, '2021-05-12 05:10:57', '2021-05-12 05:10:57');
INSERT INTO `status_upload` VALUES (709, 'ra', 'data_RA_300321_36-20210512051156.xlsx', 'file/data_RA_300321_36-20210512051156.xlsx', 2, 18, NULL, 709, '2021-05-12 05:11:56', '2021-05-12 05:12:01');
INSERT INTO `status_upload` VALUES (710, 'ra', 'data_RA_300321_37-20210517050225.xlsx', 'file/data_RA_300321_37-20210517050225.xlsx', 2, 18, NULL, 710, '2021-05-17 05:02:25', '2021-05-17 05:03:01');
INSERT INTO `status_upload` VALUES (711, 'mau', 'DATA 06-18-20210518024946.xlsx', 'file/DATA 06-18-20210518024946.xlsx', 2, 9, NULL, 711, '2021-05-18 02:49:46', '2021-05-18 02:50:03');
INSERT INTO `status_upload` VALUES (712, 'ra', 'data_RA_300321_38-20210518034532.xlsx', 'file/data_RA_300321_38-20210518034532.xlsx', 2, 18, NULL, 712, '2021-05-18 03:45:32', '2021-05-18 03:46:01');
INSERT INTO `status_upload` VALUES (713, 'ra', 'qg60-20210518133037.xlsx', 'file/qg60-20210518133037.xlsx', 2, 18, NULL, 713, '2021-05-18 13:30:37', '2021-05-18 13:31:01');
INSERT INTO `status_upload` VALUES (714, 'mau', 'DATA 19MEI2021 0859-20210519085947.xlsx', 'file/DATA 19MEI2021 0859-20210519085947.xlsx', 2, 9, NULL, 714, '2021-05-19 08:59:47', '2021-05-19 09:00:02');
INSERT INTO `status_upload` VALUES (715, 'mau', 'DATA 19MEI2021 0956-20210519095645.xlsx', 'file/DATA 19MEI2021 0956-20210519095645.xlsx', 2, 9, NULL, 715, '2021-05-19 09:56:45', '2021-05-19 09:57:01');
INSERT INTO `status_upload` VALUES (716, '', 'DATA 19MEI21 1413-20210519141334.xlsx', 'file/DATA 19MEI21 1413-20210519141334.xlsx', 0, NULL, NULL, 716, '2021-05-19 14:13:34', '2021-05-19 14:13:34');
INSERT INTO `status_upload` VALUES (717, 'mau', 'DATA 19MEI21 1413-20210519141349.xlsx', 'file/DATA 19MEI21 1413-20210519141349.xlsx', 2, 9, NULL, 717, '2021-05-19 14:13:49', '2021-05-19 14:14:01');
INSERT INTO `status_upload` VALUES (718, 'mau', 'DATA 19MEI21 1500-20210519150104.xlsx', 'file/DATA 19MEI21 1500-20210519150104.xlsx', 2, 9, NULL, 718, '2021-05-19 15:01:04', '2021-05-19 15:02:01');
INSERT INTO `status_upload` VALUES (719, 'ra', 'data_RA_300321_39-20210519181914.xlsx', 'file/data_RA_300321_39-20210519181914.xlsx', 2, 18, NULL, 719, '2021-05-19 18:19:14', '2021-05-19 18:20:01');
INSERT INTO `status_upload` VALUES (720, '', 'DATA 20 MEI 21 0130-20210520022222.xlsx', 'file/DATA 20 MEI 21 0130-20210520022222.xlsx', 0, NULL, NULL, 720, '2021-05-20 02:22:22', '2021-05-20 02:22:22');
INSERT INTO `status_upload` VALUES (721, 'mau', 'DATA 20 MEI 21 0130-20210520022258.xlsx', 'file/DATA 20 MEI 21 0130-20210520022258.xlsx', 2, 9, NULL, 721, '2021-05-20 02:22:58', '2021-05-20 02:23:01');
INSERT INTO `status_upload` VALUES (722, 'mau', 'DATA 20MEI21 1310-20210520131056.xlsx', 'file/DATA 20MEI21 1310-20210520131056.xlsx', 2, 9, NULL, 722, '2021-05-20 13:10:56', '2021-05-20 13:11:02');
INSERT INTO `status_upload` VALUES (723, 'mau', 'DATA 20MEI21 1511-20210520151708.xlsx', 'file/DATA 20MEI21 1511-20210520151708.xlsx', 2, 9, NULL, 723, '2021-05-20 15:17:08', '2021-05-20 15:18:02');
INSERT INTO `status_upload` VALUES (724, 'mau', 'DATA 21MEI21 1311-20210521175020.xlsx', 'file/DATA 21MEI21 1311-20210521175020.xlsx', 2, 9, NULL, 724, '2021-05-21 17:50:20', '2021-05-21 17:51:01');
INSERT INTO `status_upload` VALUES (725, 'mau', 'DATA 22MEI21 0157-20210522015757.xlsx', 'file/DATA 22MEI21 0157-20210522015757.xlsx', 2, 9, NULL, 725, '2021-05-22 01:57:57', '2021-05-22 01:58:01');
INSERT INTO `status_upload` VALUES (726, 'mau', 'DATA 22MEI21 0510-20210522051024.xlsx', 'file/DATA 22MEI21 0510-20210522051024.xlsx', 2, 9, NULL, 726, '2021-05-22 05:10:24', '2021-05-22 05:11:01');
INSERT INTO `status_upload` VALUES (727, 'mau', 'DATA 22MEI21 0627-20210522062717.xlsx', 'file/DATA 22MEI21 0627-20210522062717.xlsx', 2, 9, NULL, 727, '2021-05-22 06:27:17', '2021-05-22 06:28:01');
INSERT INTO `status_upload` VALUES (728, 'ra', 'qg61-20210522162625.xlsx', 'file/qg61-20210522162625.xlsx', 2, 18, NULL, 728, '2021-05-22 16:26:25', '2021-05-22 16:27:02');
INSERT INTO `status_upload` VALUES (729, 'mau', 'DATA 22MEI21 1430-20210522171336.xlsx', 'file/DATA 22MEI21 1430-20210522171336.xlsx', 2, 9, NULL, 729, '2021-05-22 17:13:36', '2021-05-22 17:14:01');
INSERT INTO `status_upload` VALUES (730, 'mau', 'DATA 23 MEI 21 0258-20210523025914.xlsx', 'file/DATA 23 MEI 21 0258-20210523025914.xlsx', 2, 9, NULL, 730, '2021-05-23 02:59:14', '2021-05-23 03:00:01');
INSERT INTO `status_upload` VALUES (731, '', 'DATA 23 MEI 21 0525-20210523052532.xlsx', 'file/DATA 23 MEI 21 0525-20210523052532.xlsx', 0, NULL, NULL, 731, '2021-05-23 05:25:32', '2021-05-23 05:25:32');
INSERT INTO `status_upload` VALUES (732, 'mau', 'DATA 23 MEI 21 0525-20210523052553.xlsx', 'file/DATA 23 MEI 21 0525-20210523052553.xlsx', 2, 9, NULL, 732, '2021-05-23 05:25:53', '2021-05-23 05:26:02');
INSERT INTO `status_upload` VALUES (733, 'ra', 'data_RA_300321_40-20210523055709.xlsx', 'file/data_RA_300321_40-20210523055709.xlsx', 2, 18, NULL, 733, '2021-05-23 05:57:09', '2021-05-23 05:58:01');
INSERT INTO `status_upload` VALUES (734, 'mau', 'DATA 23MEI2021 0659-20210523065946.xlsx', 'file/DATA 23MEI2021 0659-20210523065946.xlsx', 2, 9, NULL, 734, '2021-05-23 06:59:46', '2021-05-23 07:00:01');
INSERT INTO `status_upload` VALUES (735, 'ra', 'data_RA_300321_41-20210523130726.xlsx', 'file/data_RA_300321_41-20210523130726.xlsx', 2, 18, NULL, 735, '2021-05-23 13:07:26', '2021-05-23 13:08:01');
INSERT INTO `status_upload` VALUES (736, 'mau', 'DATA 24MEI21 0430-20210524044306.xlsx', 'file/DATA 24MEI21 0430-20210524044306.xlsx', 2, 9, NULL, 736, '2021-05-24 04:43:06', '2021-05-24 04:44:02');
INSERT INTO `status_upload` VALUES (737, 'ra', 'data_RA_300321_42-20210524053035.xlsx', 'file/data_RA_300321_42-20210524053035.xlsx', 2, 18, NULL, 737, '2021-05-24 05:30:35', '2021-05-24 05:31:02');
INSERT INTO `status_upload` VALUES (738, 'ra', 'qg62-20210524105139.xlsx', 'file/qg62-20210524105139.xlsx', 2, 18, NULL, 738, '2021-05-24 10:51:39', '2021-05-24 10:52:02');
INSERT INTO `status_upload` VALUES (739, 'ra', 'qg63-20210525024415.xlsx', 'file/qg63-20210525024415.xlsx', 2, 18, NULL, 739, '2021-05-25 02:44:15', '2021-05-25 02:45:01');
INSERT INTO `status_upload` VALUES (740, 'ra', 'qg64-20210525044017.xlsx', 'file/qg64-20210525044017.xlsx', 2, 18, NULL, 740, '2021-05-25 04:40:17', '2021-05-25 04:41:01');
INSERT INTO `status_upload` VALUES (741, 'ra', 'qg65-20210525062355.xlsx', 'file/qg65-20210525062355.xlsx', 2, 18, NULL, 741, '2021-05-25 06:23:55', '2021-05-25 06:24:01');
INSERT INTO `status_upload` VALUES (742, 'mau', 'DATA 25MEI21 0727-20210525072822.xlsx', 'file/DATA 25MEI21 0727-20210525072822.xlsx', 2, 9, NULL, 742, '2021-05-25 07:28:22', '2021-05-25 07:29:01');
INSERT INTO `status_upload` VALUES (743, 'mau', 'DATA 25MEI21 0834-20210525083444.xlsx', 'file/DATA 25MEI21 0834-20210525083444.xlsx', 2, 9, NULL, 743, '2021-05-25 08:34:44', '2021-05-25 08:35:01');
INSERT INTO `status_upload` VALUES (744, 'mau', 'DATA 25MEI21 0958-20210525095902.xlsx', 'file/DATA 25MEI21 0958-20210525095902.xlsx', 2, 9, NULL, 744, '2021-05-25 09:59:02', '2021-05-25 10:00:02');
INSERT INTO `status_upload` VALUES (745, '', 'DATA 25MEI21 1249-20210525124940.xlsx', 'file/DATA 25MEI21 1249-20210525124940.xlsx', 0, NULL, NULL, 745, '2021-05-25 12:49:40', '2021-05-25 12:49:40');
INSERT INTO `status_upload` VALUES (746, 'mau', 'DATA 25MEI21 1249-20210525124954.xlsx', 'file/DATA 25MEI21 1249-20210525124954.xlsx', 2, 9, NULL, 746, '2021-05-25 12:49:54', '2021-05-25 12:50:02');
INSERT INTO `status_upload` VALUES (747, 'mau', 'DATA 25MEI21 1646-20210525164721.xlsx', 'file/DATA 25MEI21 1646-20210525164721.xlsx', 2, 9, NULL, 747, '2021-05-25 16:47:21', '2021-05-25 16:48:02');
INSERT INTO `status_upload` VALUES (748, 'ra', 'qg66-20210525172022.xlsx', 'file/qg66-20210525172022.xlsx', 2, 18, NULL, 748, '2021-05-25 17:20:22', '2021-05-25 17:21:02');
INSERT INTO `status_upload` VALUES (749, 'ra', 'qg67-20210526020242.xlsx', 'file/qg67-20210526020242.xlsx', 2, 18, NULL, 749, '2021-05-26 02:02:42', '2021-05-26 02:03:02');
INSERT INTO `status_upload` VALUES (750, 'mau', 'DATA 26 MEI 2021 0000-20210526053722.xlsx', 'file/DATA 26 MEI 2021 0000-20210526053722.xlsx', 2, 9, NULL, 750, '2021-05-26 05:37:22', '2021-05-26 05:38:01');
INSERT INTO `status_upload` VALUES (751, '', 'qg68-20210526061905.xlsx', 'file/qg68-20210526061905.xlsx', 0, NULL, NULL, 751, '2021-05-26 06:19:05', '2021-05-26 06:19:05');
INSERT INTO `status_upload` VALUES (752, 'ra', 'qg68-20210526061931.xlsx', 'file/qg68-20210526061931.xlsx', 2, 18, NULL, 752, '2021-05-26 06:19:31', '2021-05-26 06:20:02');
INSERT INTO `status_upload` VALUES (753, 'mau', 'DATA 26MEI21 0957-20210526095818.xlsx', 'file/DATA 26MEI21 0957-20210526095818.xlsx', 2, 9, NULL, 753, '2021-05-26 09:58:18', '2021-05-26 09:59:01');
INSERT INTO `status_upload` VALUES (754, '', 'D 8713 FH 2041046-20210526105930.xlsx', 'file/D 8713 FH 2041046-20210526105930.xlsx', 0, NULL, NULL, 754, '2021-05-26 10:59:30', '2021-05-26 10:59:30');
INSERT INTO `status_upload` VALUES (755, 'ra', 'D 8713 FH 2041046-20210526110019.xlsx', 'file/D 8713 FH 2041046-20210526110019.xlsx', 2, 18, NULL, 755, '2021-05-26 11:00:19', '2021-05-26 11:01:01');
INSERT INTO `status_upload` VALUES (756, 'mau', 'DATA 26MEI21 1211-20210526121151.xlsx', 'file/DATA 26MEI21 1211-20210526121151.xlsx', 2, 9, NULL, 756, '2021-05-26 12:11:51', '2021-05-26 12:12:01');
INSERT INTO `status_upload` VALUES (757, 'mau', 'DATA 26MEI21 1358-20210526135823.xlsx', 'file/DATA 26MEI21 1358-20210526135823.xlsx', 2, 9, NULL, 757, '2021-05-26 13:58:23', '2021-05-26 13:59:01');
INSERT INTO `status_upload` VALUES (758, 'mau', 'DATA 26MEI21 1608-20210526160842.xlsx', 'file/DATA 26MEI21 1608-20210526160842.xlsx', 2, 9, NULL, 758, '2021-05-26 16:08:42', '2021-05-26 16:09:01');
INSERT INTO `status_upload` VALUES (759, 'ra', 'data_RA_300321_43-20210527055334.xlsx', 'file/data_RA_300321_43-20210527055334.xlsx', 2, 18, NULL, 759, '2021-05-27 05:53:34', '2021-05-27 05:54:01');
INSERT INTO `status_upload` VALUES (760, 'mau', 'DATA 27MEI21 1000-20210527154632.xlsx', 'file/DATA 27MEI21 1000-20210527154632.xlsx', 2, 9, NULL, 760, '2021-05-27 15:46:32', '2021-05-27 15:47:01');
INSERT INTO `status_upload` VALUES (761, 'mau', 'DATA 27MEI21 1500-20210527154939.xlsx', 'file/DATA 27MEI21 1500-20210527154939.xlsx', 2, 9, NULL, 761, '2021-05-27 15:49:39', '2021-05-27 15:50:02');
INSERT INTO `status_upload` VALUES (762, 'mau', 'DATA 28MEI2021 0316-20210528031722.xlsx', 'file/DATA 28MEI2021 0316-20210528031722.xlsx', 2, 9, NULL, 762, '2021-05-28 03:17:22', '2021-05-28 03:18:01');
INSERT INTO `status_upload` VALUES (763, 'mau', 'DATA 28MEI2021 0546-20210528054630.xlsx', 'file/DATA 28MEI2021 0546-20210528054630.xlsx', 2, 9, NULL, 763, '2021-05-28 05:46:30', '2021-05-28 05:47:01');
INSERT INTO `status_upload` VALUES (764, 'ra', 'data_RA_300321_44-20210528064201.xlsx', 'file/data_RA_300321_44-20210528064201.xlsx', 2, 18, NULL, 764, '2021-05-28 06:42:01', '2021-05-28 06:42:01');
INSERT INTO `status_upload` VALUES (765, 'mau', 'DATA 28MEI2021 0700-20210528083349.xlsx', 'file/DATA 28MEI2021 0700-20210528083349.xlsx', 2, 9, NULL, 765, '2021-05-28 08:33:49', '2021-05-28 08:34:01');
INSERT INTO `status_upload` VALUES (766, 'ra', 'qg69-20210528141800.xlsx', 'file/qg69-20210528141800.xlsx', 2, 18, NULL, 766, '2021-05-28 14:18:00', '2021-05-28 14:18:01');
INSERT INTO `status_upload` VALUES (767, 'mau', 'DATA 29 MEI 2021 0143-20210529014339.xlsx', 'file/DATA 29 MEI 2021 0143-20210529014339.xlsx', 2, 9, NULL, 767, '2021-05-29 01:43:39', '2021-05-29 01:44:02');
INSERT INTO `status_upload` VALUES (768, 'mau', 'DATA 29 MEI 2021 0332-20210529033301.xlsx', 'file/DATA 29 MEI 2021 0332-20210529033301.xlsx', 2, 9, NULL, 768, '2021-05-29 03:33:01', '2021-05-29 03:34:01');
INSERT INTO `status_upload` VALUES (769, 'mau', 'DATA 29 MEI 2021 0535-20210529053559.xlsx', 'file/DATA 29 MEI 2021 0535-20210529053559.xlsx', 2, 9, NULL, 769, '2021-05-29 05:35:59', '2021-05-29 05:36:01');
INSERT INTO `status_upload` VALUES (770, 'ra', 'data_RA_300321_45-20210529063502.xlsx', 'file/data_RA_300321_45-20210529063502.xlsx', 2, 18, NULL, 770, '2021-05-29 06:35:02', '2021-05-29 06:36:02');
INSERT INTO `status_upload` VALUES (771, 'mau', 'DATA 31MEI21 0500-20210531050113.xlsx', 'file/DATA 31MEI21 0500-20210531050113.xlsx', 2, 9, NULL, 771, '2021-05-31 05:01:13', '2021-05-31 05:02:01');
INSERT INTO `status_upload` VALUES (772, 'mau', 'DATA 31MEI21 0630-20210531063753.xlsx', 'file/DATA 31MEI21 0630-20210531063753.xlsx', 2, 9, NULL, 772, '2021-05-31 06:37:53', '2021-05-31 06:38:01');
INSERT INTO `status_upload` VALUES (773, 'mau', 'DATA 31 MEI 2021 1123-20210531112419.xlsx', 'file/DATA 31 MEI 2021 1123-20210531112419.xlsx', 2, 9, NULL, 773, '2021-05-31 11:24:19', '2021-05-31 11:25:01');
INSERT INTO `status_upload` VALUES (774, 'mau', 'DATA 31MEI21 1347-20210531134807.xlsx', 'file/DATA 31MEI21 1347-20210531134807.xlsx', 2, 9, NULL, 774, '2021-05-31 13:48:07', '2021-05-31 13:49:01');
INSERT INTO `status_upload` VALUES (775, 'mau', 'DATA 01JUNI 21 0130-20210601062444.xlsx', 'file/DATA 01JUNI 21 0130-20210601062444.xlsx', 2, 9, NULL, 775, '2021-06-01 06:24:44', '2021-06-01 06:25:01');
INSERT INTO `status_upload` VALUES (776, 'ra', 'data_RA_300321_46-20210601074623.xlsx', 'file/data_RA_300321_46-20210601074623.xlsx', 2, 18, NULL, 776, '2021-06-01 07:46:23', '2021-06-01 07:47:01');
INSERT INTO `status_upload` VALUES (777, 'mau', 'DATA 01JUNE2021 0848-20210601084832.xlsx', 'file/DATA 01JUNE2021 0848-20210601084832.xlsx', 2, 9, NULL, 777, '2021-06-01 08:48:32', '2021-06-01 08:49:02');
INSERT INTO `status_upload` VALUES (778, 'mau', 'DATA 01JUNE2021 0939-20210601093953.xlsx', 'file/DATA 01JUNE2021 0939-20210601093953.xlsx', 9, 9, NULL, 778, '2021-06-01 09:39:53', '2021-06-01 09:40:01');
INSERT INTO `status_upload` VALUES (779, 'mau', 'DATA 01JUNE2021 0939-20210601094017.xlsx', 'file/DATA 01JUNE2021 0939-20210601094017.xlsx', 9, 9, NULL, 779, '2021-06-01 09:40:17', '2021-06-01 09:41:01');
INSERT INTO `status_upload` VALUES (780, 'mau', 'DATA 01JUNE2021 0939-20210601094140.xlsx', 'file/DATA 01JUNE2021 0939-20210601094140.xlsx', 9, 9, NULL, 780, '2021-06-01 09:41:40', '2021-06-01 09:42:01');
INSERT INTO `status_upload` VALUES (781, 'mau', 'DATA 01JUNE2021 0939-20210601094301.xlsx', 'file/DATA 01JUNE2021 0939-20210601094301.xlsx', 9, 9, NULL, 781, '2021-06-01 09:43:01', '2021-06-01 09:43:01');
INSERT INTO `status_upload` VALUES (782, 'mau', 'DATA 01JUNE 09390-20210601094341.xlsx', 'file/DATA 01JUNE 09390-20210601094341.xlsx', 9, 9, NULL, 782, '2021-06-01 09:43:41', '2021-06-01 09:44:01');
INSERT INTO `status_upload` VALUES (783, 'mau', 'DATA 01JUNE2021 0939-20210601094459.xlsx', 'file/DATA 01JUNE2021 0939-20210601094459.xlsx', 9, 9, NULL, 783, '2021-06-01 09:44:59', '2021-06-01 09:45:01');
INSERT INTO `status_upload` VALUES (784, 'mau', 'DATA 01JUNE 09390-20210601094533.xlsx', 'file/DATA 01JUNE 09390-20210601094533.xlsx', 9, 9, NULL, 784, '2021-06-01 09:45:33', '2021-06-01 09:46:01');
INSERT INTO `status_upload` VALUES (785, 'mau', 'DATA 01JUNE2021 0939-20210601094918.xlsx', 'file/DATA 01JUNE2021 0939-20210601094918.xlsx', 9, 9, NULL, 785, '2021-06-01 09:49:18', '2021-06-01 09:50:01');
INSERT INTO `status_upload` VALUES (786, 'mau', 'DATA 01JUNE2021 1141-20210601114140.xlsx', 'file/DATA 01JUNE2021 1141-20210601114140.xlsx', 2, 9, NULL, 786, '2021-06-01 11:41:40', '2021-06-01 11:42:01');
INSERT INTO `status_upload` VALUES (787, 'mau', 'DATA 01JUNE2021 0939-20210601131350.xlsx', 'file/DATA 01JUNE2021 0939-20210601131350.xlsx', 9, 9, NULL, 787, '2021-06-01 13:13:50', '2021-06-01 13:14:01');
INSERT INTO `status_upload` VALUES (788, 'mau', 'DATA 01JUNE2021 1348-20210601134828.xlsx', 'file/DATA 01JUNE2021 1348-20210601134828.xlsx', 2, 9, NULL, 788, '2021-06-01 13:48:28', '2021-06-01 13:49:02');
INSERT INTO `status_upload` VALUES (789, 'mau', 'DATA 02JUNE2021 0900-20210602153656.xlsx', 'file/DATA 02JUNE2021 0900-20210602153656.xlsx', 2, 9, NULL, 789, '2021-06-02 15:36:56', '2021-06-02 15:37:01');
INSERT INTO `status_upload` VALUES (790, 'mau', 'DATA 02JUNE2021 1500-20210602164430.xlsx', 'file/DATA 02JUNE2021 1500-20210602164430.xlsx', 2, 9, NULL, 790, '2021-06-02 16:44:30', '2021-06-02 16:45:01');
INSERT INTO `status_upload` VALUES (791, 'mau', 'DATA 02 JUNE 2021 2200-20210603005817.xlsx', 'file/DATA 02 JUNE 2021 2200-20210603005817.xlsx', 2, 9, NULL, 791, '2021-06-03 00:58:17', '2021-06-03 00:59:01');
INSERT INTO `status_upload` VALUES (792, 'mau', 'DATA 03JUNE2021 0446-20210603044701.xlsx', 'file/DATA 03JUNE2021 0446-20210603044701.xlsx', 2, 9, NULL, 792, '2021-06-03 04:47:01', '2021-06-03 04:48:01');
INSERT INTO `status_upload` VALUES (793, 'ra', 'data_RA_300321_47-20210603045658.xlsx', 'file/data_RA_300321_47-20210603045658.xlsx', 2, 18, NULL, 793, '2021-06-03 04:56:58', '2021-06-03 04:57:02');
INSERT INTO `status_upload` VALUES (794, 'mau', 'DATA 03JUNE2021 0558-20210603055831.xlsx', 'file/DATA 03JUNE2021 0558-20210603055831.xlsx', 2, 9, NULL, 794, '2021-06-03 05:58:31', '2021-06-03 05:59:01');
INSERT INTO `status_upload` VALUES (795, 'mau', 'DATA 03JUNE2021 0621-20210603062131.xlsx', 'file/DATA 03JUNE2021 0621-20210603062131.xlsx', 2, 9, NULL, 795, '2021-06-03 06:21:31', '2021-06-03 06:22:01');
INSERT INTO `status_upload` VALUES (796, 'mau', 'DATA 03JUNE2021 0900-20210603092700.xlsx', 'file/DATA 03JUNE2021 0900-20210603092700.xlsx', 2, 9, NULL, 796, '2021-06-03 09:27:00', '2021-06-03 09:27:01');
INSERT INTO `status_upload` VALUES (797, 'mau', 'DATA 03JUNE2021 1400-20210603145122.xlsx', 'file/DATA 03JUNE2021 1400-20210603145122.xlsx', 2, 9, NULL, 797, '2021-06-03 14:51:22', '2021-06-03 14:52:01');
INSERT INTO `status_upload` VALUES (798, 'mau', 'DATA 04JUNE2021 0131-20210604013132.xlsx', 'file/DATA 04JUNE2021 0131-20210604013132.xlsx', 2, 9, NULL, 798, '2021-06-04 01:31:32', '2021-06-04 01:32:01');
INSERT INTO `status_upload` VALUES (799, 'mau', 'DATA 04JUNE2021 0337-20210604033756.xlsx', 'file/DATA 04JUNE2021 0337-20210604033756.xlsx', 2, 9, NULL, 799, '2021-06-04 03:37:56', '2021-06-04 03:38:02');
INSERT INTO `status_upload` VALUES (800, 'mau', 'DATA 04JUNE2021 0543-20210604054408.xlsx', 'file/DATA 04JUNE2021 0543-20210604054408.xlsx', 2, 9, NULL, 800, '2021-06-04 05:44:08', '2021-06-04 05:45:02');
INSERT INTO `status_upload` VALUES (801, 'ra', 'qg70-20210604062751.xlsx', 'file/qg70-20210604062751.xlsx', 2, 18, NULL, 801, '2021-06-04 06:27:51', '2021-06-04 06:28:01');
INSERT INTO `status_upload` VALUES (802, 'ra', 'data_RA_300321_48-20210605072601.xlsx', 'file/data_RA_300321_48-20210605072601.xlsx', 2, 18, NULL, 802, '2021-06-05 07:26:01', '2021-06-05 07:27:02');
INSERT INTO `status_upload` VALUES (803, 'ra', 'qg71-20210605230405.xlsx', 'file/qg71-20210605230405.xlsx', 2, 18, NULL, 803, '2021-06-05 23:04:05', '2021-06-05 23:05:01');
INSERT INTO `status_upload` VALUES (804, 'mau', 'DATA 06JUN21 0500-20210606052139.xlsx', 'file/DATA 06JUN21 0500-20210606052139.xlsx', 2, 9, NULL, 804, '2021-06-06 05:21:39', '2021-06-06 05:22:02');
INSERT INTO `status_upload` VALUES (805, 'mau', 'DATA 06JUNE2021 0814-20210606081440.xlsx', 'file/DATA 06JUNE2021 0814-20210606081440.xlsx', 2, 9, NULL, 805, '2021-06-06 08:14:40', '2021-06-06 08:15:02');
INSERT INTO `status_upload` VALUES (806, 'mau', 'DATA 06JUNE2021 1127-20210606112801.xlsx', 'file/DATA 06JUNE2021 1127-20210606112801.xlsx', 2, 9, NULL, 806, '2021-06-06 11:28:01', '2021-06-06 11:28:01');
INSERT INTO `status_upload` VALUES (807, 'mau', 'DATA 06JUNE2021 1202-20210606120247.xlsx', 'file/DATA 06JUNE2021 1202-20210606120247.xlsx', 2, 9, NULL, 807, '2021-06-06 12:02:47', '2021-06-06 12:03:01');
INSERT INTO `status_upload` VALUES (808, 'mau', 'DATA 06JUNE2021 1417-20210606141720.xlsx', 'file/DATA 06JUNE2021 1417-20210606141720.xlsx', 2, 9, NULL, 808, '2021-06-06 14:17:20', '2021-06-06 14:18:01');
INSERT INTO `status_upload` VALUES (809, 'ra', 'data_RA_300321_49-20210606204706.xlsx', 'file/data_RA_300321_49-20210606204706.xlsx', 2, 18, NULL, 809, '2021-06-06 20:47:06', '2021-06-06 20:48:01');
INSERT INTO `status_upload` VALUES (810, 'mau', 'DATA 07 JUNE 2021 0200-20210607055230.xlsx', 'file/DATA 07 JUNE 2021 0200-20210607055230.xlsx', 2, 9, NULL, 810, '2021-06-07 05:52:30', '2021-06-07 05:53:01');
INSERT INTO `status_upload` VALUES (811, 'ra', 'data_RA_300321_50-20210607070024.xlsx', 'file/data_RA_300321_50-20210607070024.xlsx', 2, 18, NULL, 811, '2021-06-07 07:00:24', '2021-06-07 07:01:01');
INSERT INTO `status_upload` VALUES (812, 'mau', 'DATA 7JUNE2021 0900-20210607090022.xlsx', 'file/DATA 7JUNE2021 0900-20210607090022.xlsx', 2, 9, NULL, 812, '2021-06-07 09:00:22', '2021-06-07 09:01:01');
INSERT INTO `status_upload` VALUES (813, '', 'DATA 07JUNE2021 1218-20210607121843.xlsx', 'file/DATA 07JUNE2021 1218-20210607121843.xlsx', 0, NULL, NULL, 813, '2021-06-07 12:18:43', '2021-06-07 12:18:43');
INSERT INTO `status_upload` VALUES (814, 'mau', 'DATA 07JUNE2021 1218-20210607121911.xlsx', 'file/DATA 07JUNE2021 1218-20210607121911.xlsx', 2, 9, NULL, 814, '2021-06-07 12:19:11', '2021-06-07 12:20:01');
INSERT INTO `status_upload` VALUES (815, '', 'DATA 07JUNE2021 1436-20210607143649.xlsx', 'file/DATA 07JUNE2021 1436-20210607143649.xlsx', 0, NULL, NULL, 815, '2021-06-07 14:36:49', '2021-06-07 14:36:49');
INSERT INTO `status_upload` VALUES (816, 'mau', 'DATA 07JUNE2021 1436-20210607143707.xlsx', 'file/DATA 07JUNE2021 1436-20210607143707.xlsx', 2, 9, NULL, 816, '2021-06-07 14:37:07', '2021-06-07 14:38:01');
INSERT INTO `status_upload` VALUES (817, 'mau', 'DATA 08JUNE21 1000-20210608102935.xlsx', 'file/DATA 08JUNE21 1000-20210608102935.xlsx', 2, 9, NULL, 817, '2021-06-08 10:29:35', '2021-06-08 10:30:02');
INSERT INTO `status_upload` VALUES (818, 'mau', 'DATA 08JUNE21 1500-20210608171446.xlsx', 'file/DATA 08JUNE21 1500-20210608171446.xlsx', 2, 9, NULL, 818, '2021-06-08 17:14:46', '2021-06-08 17:15:01');
INSERT INTO `status_upload` VALUES (819, 'mau', 'DATA 09JUNE2021 0133-20210609013349.xlsx', 'file/DATA 09JUNE2021 0133-20210609013349.xlsx', 2, 9, NULL, 819, '2021-06-09 01:33:49', '2021-06-09 01:34:01');
INSERT INTO `status_upload` VALUES (820, 'ra', 'data_RA_300321_51-20210609021810.xlsx', 'file/data_RA_300321_51-20210609021810.xlsx', 2, 18, NULL, 820, '2021-06-09 02:18:10', '2021-06-09 02:19:02');
INSERT INTO `status_upload` VALUES (821, 'mau', 'DATA 09JUNE2021 0328-20210609032945.xlsx', 'file/DATA 09JUNE2021 0328-20210609032945.xlsx', 2, 9, NULL, 821, '2021-06-09 03:29:45', '2021-06-09 03:30:02');
INSERT INTO `status_upload` VALUES (822, 'mau', 'DATA 09JUNE2021 0514-20210609051501.xlsx', 'file/DATA 09JUNE2021 0514-20210609051501.xlsx', 2, 9, NULL, 822, '2021-06-09 05:15:01', '2021-06-09 05:15:02');
INSERT INTO `status_upload` VALUES (823, 'ra', 'data_RA_300321_52-20210609064023.xlsx', 'file/data_RA_300321_52-20210609064023.xlsx', 2, 18, NULL, 823, '2021-06-09 06:40:23', '2021-06-09 06:41:01');
INSERT INTO `status_upload` VALUES (824, 'mau', 'DATA 09JUNE2021 0514-20210609160956.xlsx', 'file/DATA 09JUNE2021 0514-20210609160956.xlsx', 2, 9, NULL, 824, '2021-06-09 16:09:56', '2021-06-09 16:10:01');
INSERT INTO `status_upload` VALUES (825, 'mau', 'DATA 09JUNE2021 1640-20210609164748.xlsx', 'file/DATA 09JUNE2021 1640-20210609164748.xlsx', 2, 9, NULL, 825, '2021-06-09 16:47:48', '2021-06-09 16:48:01');
INSERT INTO `status_upload` VALUES (826, 'mau', 'DATA 10JUNE2021 0211-20210610021221.xlsx', 'file/DATA 10JUNE2021 0211-20210610021221.xlsx', 2, 9, NULL, 826, '2021-06-10 02:12:21', '2021-06-10 02:13:01');
INSERT INTO `status_upload` VALUES (827, 'mau', 'DATA 10JUNE2021 0333-20210610033312.xlsx', 'file/DATA 10JUNE2021 0333-20210610033312.xlsx', 2, 9, NULL, 827, '2021-06-10 03:33:12', '2021-06-10 03:34:02');
INSERT INTO `status_upload` VALUES (828, '', 'DATA 10JUNE2021 0545-20210610054549.xlsx', 'file/DATA 10JUNE2021 0545-20210610054549.xlsx', 0, NULL, NULL, 828, '2021-06-10 05:45:49', '2021-06-10 05:45:49');
INSERT INTO `status_upload` VALUES (829, 'mau', 'DATA 10JUNE2021 0545-20210610054609.xlsx', 'file/DATA 10JUNE2021 0545-20210610054609.xlsx', 2, 9, NULL, 829, '2021-06-10 05:46:09', '2021-06-10 05:47:02');
INSERT INTO `status_upload` VALUES (830, 'ra', 'qg72-20210611065107.xlsx', 'file/qg72-20210611065107.xlsx', 2, 18, NULL, 830, '2021-06-11 06:51:07', '2021-06-11 06:52:02');
INSERT INTO `status_upload` VALUES (831, '', 'DATA 11JUNE2021 0600-20210611071508.xlsx', 'file/DATA 11JUNE2021 0600-20210611071508.xlsx', 0, NULL, NULL, 831, '2021-06-11 07:15:08', '2021-06-11 07:15:08');
INSERT INTO `status_upload` VALUES (832, 'mau', 'DATA 11JUNE2021 0600-20210611071535.xlsx', 'file/DATA 11JUNE2021 0600-20210611071535.xlsx', 2, 9, NULL, 832, '2021-06-11 07:15:35', '2021-06-11 07:16:02');
INSERT INTO `status_upload` VALUES (833, 'ra', 'data_RA_300321_54-20210612004027.xlsx', 'file/data_RA_300321_54-20210612004027.xlsx', 2, 18, NULL, 833, '2021-06-12 00:40:27', '2021-06-12 00:41:01');
INSERT INTO `status_upload` VALUES (834, 'mau', 'DATA 12JUNE21 0200-20210612023457.xlsx', 'file/DATA 12JUNE21 0200-20210612023457.xlsx', 2, 9, NULL, 834, '2021-06-12 02:34:57', '2021-06-12 02:35:01');
INSERT INTO `status_upload` VALUES (835, 'ra', 'data_RA_300321_55-20210612061428.xlsx', 'file/data_RA_300321_55-20210612061428.xlsx', 2, 18, NULL, 835, '2021-06-12 06:14:28', '2021-06-12 06:15:02');
INSERT INTO `status_upload` VALUES (836, 'mau', 'DATA 12JUNE2021 0747-20210612074728.xlsx', 'file/DATA 12JUNE2021 0747-20210612074728.xlsx', 2, 9, NULL, 836, '2021-06-12 07:47:28', '2021-06-12 07:48:01');
INSERT INTO `status_upload` VALUES (837, '', 'DATA 12JUNE2021 1215-20210612121601.xlsx', 'file/DATA 12JUNE2021 1215-20210612121601.xlsx', 0, NULL, NULL, 837, '2021-06-12 12:16:01', '2021-06-12 12:16:01');
INSERT INTO `status_upload` VALUES (838, 'mau', 'DATA 12JUNE2021 1215-20210612121619.xlsx', 'file/DATA 12JUNE2021 1215-20210612121619.xlsx', 2, 9, NULL, 838, '2021-06-12 12:16:19', '2021-06-12 12:17:01');
INSERT INTO `status_upload` VALUES (839, 'mau', 'DATA 12JUNE2021 1517-20210612151800.xlsx', 'file/DATA 12JUNE2021 1517-20210612151800.xlsx', 2, 9, NULL, 839, '2021-06-12 15:18:00', '2021-06-12 15:18:01');
INSERT INTO `status_upload` VALUES (840, 'ra', 'qg73-20210613043425.xlsx', 'file/qg73-20210613043425.xlsx', 2, 18, NULL, 840, '2021-06-13 04:34:25', '2021-06-13 04:35:01');
INSERT INTO `status_upload` VALUES (841, 'mau', 'DATA 13JUNE2021 1156-20210613115637.xlsx', 'file/DATA 13JUNE2021 1156-20210613115637.xlsx', 2, 9, NULL, 841, '2021-06-13 11:56:37', '2021-06-13 11:57:01');
INSERT INTO `status_upload` VALUES (842, '', 'DATA 13JUNE2021 1421-20210613142133.xlsx', 'file/DATA 13JUNE2021 1421-20210613142133.xlsx', 0, NULL, NULL, 842, '2021-06-13 14:21:33', '2021-06-13 14:21:33');
INSERT INTO `status_upload` VALUES (843, 'mau', 'DATA 13JUNE2021 1421-20210613142148.xlsx', 'file/DATA 13JUNE2021 1421-20210613142148.xlsx', 9, 9, NULL, 843, '2021-06-13 14:21:48', '2021-06-13 14:22:01');
INSERT INTO `status_upload` VALUES (844, 'mau', 'DATA 13JUNE2021 1421-20210613142555.xlsx', 'file/DATA 13JUNE2021 1421-20210613142555.xlsx', 9, 9, NULL, 844, '2021-06-13 14:25:55', '2021-06-13 14:26:01');
INSERT INTO `status_upload` VALUES (845, 'mau', 'DATA 13JUNE2021 1421-20210613142712.xlsx', 'file/DATA 13JUNE2021 1421-20210613142712.xlsx', 9, 9, NULL, 845, '2021-06-13 14:27:12', '2021-06-13 14:28:01');
INSERT INTO `status_upload` VALUES (846, 'ra', 'data_RA_300321_56-20210614060940.xlsx', 'file/data_RA_300321_56-20210614060940.xlsx', 2, 18, NULL, 846, '2021-06-14 06:09:40', '2021-06-14 06:10:01');
INSERT INTO `status_upload` VALUES (847, 'mau', 'DATA 14JUNE2021 1000-20210614100000.xlsx', 'file/DATA 14JUNE2021 1000-20210614100000.xlsx', 2, 9, NULL, 847, '2021-06-14 10:00:00', '2021-06-14 10:00:02');
INSERT INTO `status_upload` VALUES (848, 'mau', 'DATA 15JUNE2021 0601-20210615060150.xlsx', 'file/DATA 15JUNE2021 0601-20210615060150.xlsx', 2, 9, NULL, 848, '2021-06-15 06:01:50', '2021-06-15 06:02:01');
INSERT INTO `status_upload` VALUES (849, 'ra', 'qg74-20210615071346.xlsx', 'file/qg74-20210615071346.xlsx', 2, 18, NULL, 849, '2021-06-15 07:13:46', '2021-06-15 07:14:01');
INSERT INTO `status_upload` VALUES (850, 'mau', 'DATA 15JUNE2021 0700-20210615093543.xlsx', 'file/DATA 15JUNE2021 0700-20210615093543.xlsx', 2, 9, NULL, 850, '2021-06-15 09:35:43', '2021-06-15 09:36:02');
INSERT INTO `status_upload` VALUES (851, 'mau', 'DATA 15JUNE2021 1000-20210615102036.xlsx', 'file/DATA 15JUNE2021 1000-20210615102036.xlsx', 2, 9, NULL, 851, '2021-06-15 10:20:36', '2021-06-15 10:21:02');
INSERT INTO `status_upload` VALUES (852, 'mau', 'DATA 16JUNE2021 0059-20210616010018.xlsx', 'file/DATA 16JUNE2021 0059-20210616010018.xlsx', 2, 9, NULL, 852, '2021-06-16 01:00:18', '2021-06-16 01:01:01');
INSERT INTO `status_upload` VALUES (853, 'mau', 'DATA 16JUNE2021 0245-20210616024532.xlsx', 'file/DATA 16JUNE2021 0245-20210616024532.xlsx', 2, 9, NULL, 853, '2021-06-16 02:45:32', '2021-06-16 02:46:02');
INSERT INTO `status_upload` VALUES (854, 'mau', 'DATA 16JUNE2021 0434-20210616043433.xlsx', 'file/DATA 16JUNE2021 0434-20210616043433.xlsx', 2, 9, NULL, 854, '2021-06-16 04:34:33', '2021-06-16 04:35:02');
INSERT INTO `status_upload` VALUES (855, 'mau', 'DATA 16JUNE2021 0529-20210616053002.xlsx', 'file/DATA 16JUNE2021 0529-20210616053002.xlsx', 2, 9, NULL, 855, '2021-06-16 05:30:02', '2021-06-16 05:31:01');
INSERT INTO `status_upload` VALUES (856, 'ra', 'data_RA_300321_57-20210616072339.xlsx', 'file/data_RA_300321_57-20210616072339.xlsx', 2, 18, NULL, 856, '2021-06-16 07:23:39', '2021-06-16 07:24:02');
INSERT INTO `status_upload` VALUES (857, 'ra', 'qg75-20210617063225.xlsx', 'file/qg75-20210617063225.xlsx', 2, 18, NULL, 857, '2021-06-17 06:32:25', '2021-06-17 06:33:01');
INSERT INTO `status_upload` VALUES (858, 'mau', 'DATA 17JUNE2021 0700-20210617070630.xlsx', 'file/DATA 17JUNE2021 0700-20210617070630.xlsx', 2, 9, NULL, 858, '2021-06-17 07:06:30', '2021-06-17 07:07:01');
INSERT INTO `status_upload` VALUES (859, 'mau', 'DATA 17JUNE2021 0700-20210617103427.xlsx', 'file/DATA 17JUNE2021 0700-20210617103427.xlsx', 2, 9, NULL, 859, '2021-06-17 10:34:27', '2021-06-17 10:35:01');
INSERT INTO `status_upload` VALUES (860, 'ra', 'data_RA_300321_58-20210618063230.xlsx', 'file/data_RA_300321_58-20210618063230.xlsx', 2, 18, NULL, 860, '2021-06-18 06:32:30', '2021-06-18 06:33:01');
INSERT INTO `status_upload` VALUES (861, 'mau', 'DATA 18JUNE2021 0737-20210618073831.xlsx', 'file/DATA 18JUNE2021 0737-20210618073831.xlsx', 2, 9, NULL, 861, '2021-06-18 07:38:31', '2021-06-18 07:39:01');
INSERT INTO `status_upload` VALUES (862, 'mau', 'DATA 18JUNE2021 1226-20210618122704.xlsx', 'file/DATA 18JUNE2021 1226-20210618122704.xlsx', 2, 9, NULL, 862, '2021-06-18 12:27:04', '2021-06-18 12:28:01');
INSERT INTO `status_upload` VALUES (863, 'ra', 'data_RA_300321_59-20210619060835.xlsx', 'file/data_RA_300321_59-20210619060835.xlsx', 2, 18, NULL, 863, '2021-06-19 06:08:35', '2021-06-19 06:09:01');
INSERT INTO `status_upload` VALUES (864, 'mau', 'TONASE 19 JUNE  2021-20210619090758.xlsx', 'file/TONASE 19 JUNE  2021-20210619090758.xlsx', 9, 9, NULL, 864, '2021-06-19 09:07:58', '2021-06-19 09:08:01');
INSERT INTO `status_upload` VALUES (865, 'mau', 'DATA 19JUNE2021 1204-20210619120455.xlsx', 'file/DATA 19JUNE2021 1204-20210619120455.xlsx', 2, 9, NULL, 865, '2021-06-19 12:04:55', '2021-06-19 12:05:01');
INSERT INTO `status_upload` VALUES (866, 'mau', 'DATA 19JUNE2021 1400-20210619184713.xlsx', 'file/DATA 19JUNE2021 1400-20210619184713.xlsx', 2, 9, NULL, 866, '2021-06-19 18:47:13', '2021-06-19 18:48:01');
INSERT INTO `status_upload` VALUES (867, 'ra', 'qg76-20210620060042.xlsx', 'file/qg76-20210620060042.xlsx', 2, 18, NULL, 867, '2021-06-20 06:00:42', '2021-06-20 06:01:01');
INSERT INTO `status_upload` VALUES (868, 'mau', 'DATA 20JUNE2021 1000-20210620103334.xlsx', 'file/DATA 20JUNE2021 1000-20210620103334.xlsx', 2, 9, NULL, 868, '2021-06-20 10:33:34', '2021-06-20 10:34:01');
INSERT INTO `status_upload` VALUES (869, '', 'DATA 20JUNE2021 1000-20210620140913.xlsx', 'file/DATA 20JUNE2021 1000-20210620140913.xlsx', 0, NULL, NULL, 869, '2021-06-20 14:09:13', '2021-06-20 14:09:13');
INSERT INTO `status_upload` VALUES (870, 'mau', 'DATA 20JUNE2021 1300-20210620145828.xlsx', 'file/DATA 20JUNE2021 1300-20210620145828.xlsx', 2, 9, NULL, 870, '2021-06-20 14:58:28', '2021-06-20 14:59:01');
INSERT INTO `status_upload` VALUES (871, 'mau', 'DATA 21JUNE2021 0303-20210621030337.xlsx', 'file/DATA 21JUNE2021 0303-20210621030337.xlsx', 2, 9, NULL, 871, '2021-06-21 03:03:37', '2021-06-21 03:04:02');
INSERT INTO `status_upload` VALUES (872, 'mau', 'DATA 21JUNE2021 0335-20210621033603.xlsx', 'file/DATA 21JUNE2021 0335-20210621033603.xlsx', 2, 9, NULL, 872, '2021-06-21 03:36:03', '2021-06-21 03:37:01');
INSERT INTO `status_upload` VALUES (873, '', 'DATA 21JUNE2021 0619-20210621062009.xlsx', 'file/DATA 21JUNE2021 0619-20210621062009.xlsx', 0, NULL, NULL, 873, '2021-06-21 06:20:09', '2021-06-21 06:20:09');
INSERT INTO `status_upload` VALUES (874, 'mau', 'DATA 21JUNE2021 0619-20210621062024.xlsx', 'file/DATA 21JUNE2021 0619-20210621062024.xlsx', 2, 9, NULL, 874, '2021-06-21 06:20:24', '2021-06-21 06:21:01');
INSERT INTO `status_upload` VALUES (875, 'ra', 'data_RA_300321_60-20210621064050.xlsx', 'file/data_RA_300321_60-20210621064050.xlsx', 2, 18, NULL, 875, '2021-06-21 06:40:50', '2021-06-21 06:41:01');
INSERT INTO `status_upload` VALUES (876, 'mau', 'DATA 21JUNE2021 1400-20210621144013.xlsx', 'file/DATA 21JUNE2021 1400-20210621144013.xlsx', 2, 9, NULL, 876, '2021-06-21 14:40:13', '2021-06-21 14:41:01');
INSERT INTO `status_upload` VALUES (877, 'mau', 'DATA 21JUNE2021 1500-20210621160754.xlsx', 'file/DATA 21JUNE2021 1500-20210621160754.xlsx', 2, 9, NULL, 877, '2021-06-21 16:07:54', '2021-06-21 16:08:01');
INSERT INTO `status_upload` VALUES (878, 'mau', 'DATA 22JUNE2021 0305-20210622030532.xlsx', 'file/DATA 22JUNE2021 0305-20210622030532.xlsx', 2, 9, NULL, 878, '2021-06-22 03:05:32', '2021-06-22 03:06:01');
INSERT INTO `status_upload` VALUES (879, 'ra', 'QG77-20210622070834.xlsx', 'file/QG77-20210622070834.xlsx', 2, 18, NULL, 879, '2021-06-22 07:08:34', '2021-06-22 07:09:02');
INSERT INTO `status_upload` VALUES (880, 'ra', 'data_RA_300321_61-20210623064045.xlsx', 'file/data_RA_300321_61-20210623064045.xlsx', 2, 18, NULL, 880, '2021-06-23 06:40:45', '2021-06-23 06:41:01');
INSERT INTO `status_upload` VALUES (881, 'ra', 'QG78-20210623103945.xlsx', 'file/QG78-20210623103945.xlsx', 2, 18, NULL, 881, '2021-06-23 10:39:45', '2021-06-23 10:40:01');
INSERT INTO `status_upload` VALUES (882, 'mau', 'DATA 24JUNE2021 0630-20210624064514.xlsx', 'file/DATA 24JUNE2021 0630-20210624064514.xlsx', 2, 9, NULL, 882, '2021-06-24 06:45:14', '2021-06-24 06:46:02');
INSERT INTO `status_upload` VALUES (883, 'ra', 'data_RA_300321_62-20210624070613.xlsx', 'file/data_RA_300321_62-20210624070613.xlsx', 2, 18, NULL, 883, '2021-06-24 07:06:13', '2021-06-24 07:07:01');
INSERT INTO `status_upload` VALUES (884, 'mau', 'DATA 24JUNE2021 0827-20210624082816.xlsx', 'file/DATA 24JUNE2021 0827-20210624082816.xlsx', 2, 9, NULL, 884, '2021-06-24 08:28:16', '2021-06-24 08:29:01');
INSERT INTO `status_upload` VALUES (885, 'ra', 'QG79-20210624130824.xlsx', 'file/QG79-20210624130824.xlsx', 2, 18, NULL, 885, '2021-06-24 13:08:24', '2021-06-24 13:09:01');
INSERT INTO `status_upload` VALUES (886, 'mau', 'DATA 24JUNE2021 1414-20210624141456.xlsx', 'file/DATA 24JUNE2021 1414-20210624141456.xlsx', 2, 9, NULL, 886, '2021-06-24 14:14:56', '2021-06-24 14:15:01');
INSERT INTO `status_upload` VALUES (887, 'mau', 'DATA 24JUNE2021 1710-20210624171027.xlsx', 'file/DATA 24JUNE2021 1710-20210624171027.xlsx', 2, 9, NULL, 887, '2021-06-24 17:10:27', '2021-06-24 17:11:01');
INSERT INTO `status_upload` VALUES (888, 'ra', 'QG80-20210625033151.xlsx', 'file/QG80-20210625033151.xlsx', 2, 18, NULL, 888, '2021-06-25 03:31:51', '2021-06-25 03:32:01');
INSERT INTO `status_upload` VALUES (889, '', 'QG81-20210625060322.xlsx', 'file/QG81-20210625060322.xlsx', 0, NULL, NULL, 889, '2021-06-25 06:03:22', '2021-06-25 06:03:22');
INSERT INTO `status_upload` VALUES (890, 'ra', 'QG81-20210625060350.xlsx', 'file/QG81-20210625060350.xlsx', 2, 18, NULL, 890, '2021-06-25 06:03:50', '2021-06-25 06:04:01');
INSERT INTO `status_upload` VALUES (891, 'mau', 'DATA 25JUNE2021 0844-20210625094457.xlsx', 'file/DATA 25JUNE2021 0844-20210625094457.xlsx', 2, 9, NULL, 891, '2021-06-25 09:44:57', '2021-06-25 09:45:01');
INSERT INTO `status_upload` VALUES (892, 'mau', 'DATA 25JUNE2021 1605-20210625160556.xlsx', 'file/DATA 25JUNE2021 1605-20210625160556.xlsx', 2, 9, NULL, 892, '2021-06-25 16:05:56', '2021-06-25 16:06:01');
INSERT INTO `status_upload` VALUES (893, 'ra', 'data_RA_300321_63-20210625190833.xlsx', 'file/data_RA_300321_63-20210625190833.xlsx', 2, 18, NULL, 893, '2021-06-25 19:08:33', '2021-06-25 19:09:01');
INSERT INTO `status_upload` VALUES (894, 'ra', 'QG82-20210626023716.xlsx', 'file/QG82-20210626023716.xlsx', 2, 18, NULL, 894, '2021-06-26 02:37:16', '2021-06-26 02:38:02');
INSERT INTO `status_upload` VALUES (895, 'ra', 'QG83-20210626064557.xlsx', 'file/QG83-20210626064557.xlsx', 2, 18, NULL, 895, '2021-06-26 06:45:57', '2021-06-26 06:46:02');
INSERT INTO `status_upload` VALUES (896, 'mau', 'DATA 26JUNE2021 2201-20210626220234.xlsx', 'file/DATA 26JUNE2021 2201-20210626220234.xlsx', 2, 9, NULL, 896, '2021-06-26 22:02:34', '2021-06-26 22:03:01');
INSERT INTO `status_upload` VALUES (897, 'mau', 'DATA 27JUNE2021 0212-20210627021257.xlsx', 'file/DATA 27JUNE2021 0212-20210627021257.xlsx', 2, 9, NULL, 897, '2021-06-27 02:12:57', '2021-06-27 02:13:01');
INSERT INTO `status_upload` VALUES (898, 'ra', 'QG84-20210627022939.xlsx', 'file/QG84-20210627022939.xlsx', 2, 18, NULL, 898, '2021-06-27 02:29:39', '2021-06-27 02:30:01');
INSERT INTO `status_upload` VALUES (899, 'ra', 'QG85-20210627060413.xlsx', 'file/QG85-20210627060413.xlsx', 2, 18, NULL, 899, '2021-06-27 06:04:13', '2021-06-27 06:05:01');
INSERT INTO `status_upload` VALUES (900, 'mau', 'DATA 27JUNE0600-20210627154319.xlsx', 'file/DATA 27JUNE0600-20210627154319.xlsx', 2, 9, NULL, 900, '2021-06-27 15:43:19', '2021-06-27 15:44:01');
INSERT INTO `status_upload` VALUES (901, 'mau', 'DATA 27JUNE2021 1350-20210627163727.xlsx', 'file/DATA 27JUNE2021 1350-20210627163727.xlsx', 2, 9, NULL, 901, '2021-06-27 16:37:27', '2021-06-27 16:38:01');
INSERT INTO `status_upload` VALUES (902, 'mau', 'DATA 27JUNE2021 2240-20210627224101.xlsx', 'file/DATA 27JUNE2021 2240-20210627224101.xlsx', 2, 9, NULL, 902, '2021-06-27 22:41:01', '2021-06-27 22:41:02');
INSERT INTO `status_upload` VALUES (903, '', 'DATA 28JUNE2021 0053-20210628005335.xlsx', 'file/DATA 28JUNE2021 0053-20210628005335.xlsx', 0, NULL, NULL, 903, '2021-06-28 00:53:35', '2021-06-28 00:53:35');
INSERT INTO `status_upload` VALUES (904, 'mau', 'DATA 28JUNE2021 0053-20210628005349.xlsx', 'file/DATA 28JUNE2021 0053-20210628005349.xlsx', 2, 9, NULL, 904, '2021-06-28 00:53:49', '2021-06-28 00:54:01');
INSERT INTO `status_upload` VALUES (905, 'ra', 'QG86-20210628025314.xlsx', 'file/QG86-20210628025314.xlsx', 2, 18, NULL, 905, '2021-06-28 02:53:14', '2021-06-28 02:54:02');
INSERT INTO `status_upload` VALUES (906, 'mau', 'DATA 28JUNE2021 0444-20210628044451.xlsx', 'file/DATA 28JUNE2021 0444-20210628044451.xlsx', 2, 9, NULL, 906, '2021-06-28 04:44:51', '2021-06-28 04:45:01');
INSERT INTO `status_upload` VALUES (907, 'ra', 'QG87-20210628052653.xlsx', 'file/QG87-20210628052653.xlsx', 2, 18, NULL, 907, '2021-06-28 05:26:53', '2021-06-28 05:27:01');
INSERT INTO `status_upload` VALUES (908, 'mau', 'DATA 28JUNE2021 0629-20210628062946.xlsx', 'file/DATA 28JUNE2021 0629-20210628062946.xlsx', 2, 9, NULL, 908, '2021-06-28 06:29:46', '2021-06-28 06:30:01');
INSERT INTO `status_upload` VALUES (909, 'ra', 'qg88-20210629063837.xlsx', 'file/qg88-20210629063837.xlsx', 2, 18, NULL, 909, '2021-06-29 06:38:37', '2021-06-29 06:39:02');
INSERT INTO `status_upload` VALUES (910, 'ra', 'qg89-20210629125038.xlsx', 'file/qg89-20210629125038.xlsx', 2, 18, NULL, 910, '2021-06-29 12:50:38', '2021-06-29 12:51:01');
INSERT INTO `status_upload` VALUES (911, 'ra', 'qg90-20210629234115.xlsx', 'file/qg90-20210629234115.xlsx', 2, 18, NULL, 911, '2021-06-29 23:41:15', '2021-06-29 23:42:01');
INSERT INTO `status_upload` VALUES (912, '', 'qg91-20210630022048.xlsx', 'file/qg91-20210630022048.xlsx', 0, NULL, NULL, 912, '2021-06-30 02:20:48', '2021-06-30 02:20:48');
INSERT INTO `status_upload` VALUES (913, 'ra', 'qg91-20210630022114.xlsx', 'file/qg91-20210630022114.xlsx', 2, 18, NULL, 913, '2021-06-30 02:21:14', '2021-06-30 02:22:01');
INSERT INTO `status_upload` VALUES (914, 'mau', 'DATA 30JUNE2021 0330-20210630034308.xlsx', 'file/DATA 30JUNE2021 0330-20210630034308.xlsx', 2, 9, NULL, 914, '2021-06-30 03:43:08', '2021-06-30 03:44:01');
INSERT INTO `status_upload` VALUES (915, '', 'qg92-20210630055411.xlsx', 'file/qg92-20210630055411.xlsx', 0, NULL, NULL, 915, '2021-06-30 05:54:11', '2021-06-30 05:54:11');
INSERT INTO `status_upload` VALUES (916, 'ra', 'qg92-20210630055436.xlsx', 'file/qg92-20210630055436.xlsx', 2, 18, NULL, 916, '2021-06-30 05:54:36', '2021-06-30 05:55:02');
INSERT INTO `status_upload` VALUES (917, 'mau', 'DATA 30JUNE2021 0645-20210630064956.xlsx', 'file/DATA 30JUNE2021 0645-20210630064956.xlsx', 2, 9, NULL, 917, '2021-06-30 06:49:56', '2021-06-30 06:50:01');
INSERT INTO `status_upload` VALUES (918, 'mau', 'DATA 30JUNE2021 0850-20210630085115.xlsx', 'file/DATA 30JUNE2021 0850-20210630085115.xlsx', 2, 9, NULL, 918, '2021-06-30 08:51:15', '2021-06-30 08:52:01');
INSERT INTO `status_upload` VALUES (919, 'mau', 'DATA 30JUNE2021 1254-20210630125448.xlsx', 'file/DATA 30JUNE2021 1254-20210630125448.xlsx', 2, 9, NULL, 919, '2021-06-30 12:54:48', '2021-06-30 12:55:01');
INSERT INTO `status_upload` VALUES (920, 'ra', 'qg93-20210630133640.xlsx', 'file/qg93-20210630133640.xlsx', 2, 18, NULL, 920, '2021-06-30 13:36:40', '2021-06-30 13:37:01');
INSERT INTO `status_upload` VALUES (921, 'mau', 'DATA 30JUNE2021 1614-20210630161457.xlsx', 'file/DATA 30JUNE2021 1614-20210630161457.xlsx', 2, 9, NULL, 921, '2021-06-30 16:14:57', '2021-06-30 16:15:01');
INSERT INTO `status_upload` VALUES (922, 'ra', 'qg94-20210701053116.xlsx', 'file/qg94-20210701053116.xlsx', 2, 18, NULL, 922, '2021-07-01 05:31:16', '2021-07-01 05:32:02');
INSERT INTO `status_upload` VALUES (923, 'ra', 'qg95-20210701062828.xlsx', 'file/qg95-20210701062828.xlsx', 2, 18, NULL, 923, '2021-07-01 06:28:28', '2021-07-01 06:29:02');
INSERT INTO `status_upload` VALUES (924, 'mau', 'DATA 01JULI2021 0914-20210701091527.xlsx', 'file/DATA 01JULI2021 0914-20210701091527.xlsx', 2, 9, NULL, 924, '2021-07-01 09:15:27', '2021-07-01 09:16:01');
INSERT INTO `status_upload` VALUES (925, 'ra', 'qg96-20210701131740.xlsx', 'file/qg96-20210701131740.xlsx', 2, 18, NULL, 925, '2021-07-01 13:17:40', '2021-07-01 13:18:01');
INSERT INTO `status_upload` VALUES (926, 'mau', 'DATA 01JULI2021 1504-20210701150508.xlsx', 'file/DATA 01JULI2021 1504-20210701150508.xlsx', 2, 9, NULL, 926, '2021-07-01 15:05:08', '2021-07-01 15:06:01');
INSERT INTO `status_upload` VALUES (927, 'mau', 'DATA 01JULI2021 1549-20210701155011.xlsx', 'file/DATA 01JULI2021 1549-20210701155011.xlsx', 2, 9, NULL, 927, '2021-07-01 15:50:11', '2021-07-01 15:51:01');
INSERT INTO `status_upload` VALUES (928, 'mau', 'DATA 02JULI2021 1800-20210702181623.xlsx', 'file/DATA 02JULI2021 1800-20210702181623.xlsx', 2, 9, NULL, 928, '2021-07-02 18:16:23', '2021-07-02 18:17:03');
INSERT INTO `status_upload` VALUES (929, 'mau', 'DATA 03JULI2021 0257-20210703025801.xlsx', 'file/DATA 03JULI2021 0257-20210703025801.xlsx', 2, 9, NULL, 929, '2021-07-03 02:58:01', '2021-07-03 02:58:02');
INSERT INTO `status_upload` VALUES (930, 'ra', 'data_RA_300321_64-20210703055403.xlsx', 'file/data_RA_300321_64-20210703055403.xlsx', 2, 18, NULL, 930, '2021-07-03 05:54:03', '2021-07-03 05:55:03');
INSERT INTO `status_upload` VALUES (931, '', 'DATA 03JULI2021 0656-20210703065644.xlsx', 'file/DATA 03JULI2021 0656-20210703065644.xlsx', 0, NULL, NULL, 931, '2021-07-03 06:56:44', '2021-07-03 06:56:44');
INSERT INTO `status_upload` VALUES (932, 'mau', 'DATA 03JULI2021 0656-20210703065707.xlsx', 'file/DATA 03JULI2021 0656-20210703065707.xlsx', 2, 9, NULL, 932, '2021-07-03 06:57:07', '2021-07-03 06:58:01');
INSERT INTO `status_upload` VALUES (933, 'mau', 'DATA 03JULI2021 1500-20210703153227.xlsx', 'file/DATA 03JULI2021 1500-20210703153227.xlsx', 2, 9, NULL, 933, '2021-07-03 15:32:27', '2021-07-03 15:33:01');
INSERT INTO `status_upload` VALUES (934, 'mau', 'DATA 04JULI2021 0246-20210704024711.xlsx', 'file/DATA 04JULI2021 0246-20210704024711.xlsx', 2, 9, NULL, 934, '2021-07-04 02:47:11', '2021-07-04 02:48:02');
INSERT INTO `status_upload` VALUES (935, 'ra', 'data_RA_300321_67-20210704060221.xlsx', 'file/data_RA_300321_67-20210704060221.xlsx', 2, 18, NULL, 935, '2021-07-04 06:02:21', '2021-07-04 06:03:02');
INSERT INTO `status_upload` VALUES (936, 'mau', '04JULI2021 0644-20210704064520.xlsx', 'file/04JULI2021 0644-20210704064520.xlsx', 2, 9, NULL, 936, '2021-07-04 06:45:20', '2021-07-04 06:46:01');
INSERT INTO `status_upload` VALUES (937, 'ra', 'data_RA_300321_68-20210705060939.xlsx', 'file/data_RA_300321_68-20210705060939.xlsx', 2, 18, NULL, 937, '2021-07-05 06:09:39', '2021-07-05 06:10:02');
INSERT INTO `status_upload` VALUES (938, 'ra', 'qg97-20210706060617.xlsx', 'file/qg97-20210706060617.xlsx', 2, 18, NULL, 938, '2021-07-06 06:06:17', '2021-07-06 06:07:03');
INSERT INTO `status_upload` VALUES (939, 'mau', 'DATA 06JULI2021 0708-20210706070844.xlsx', 'file/DATA 06JULI2021 0708-20210706070844.xlsx', 2, 9, NULL, 939, '2021-07-06 07:08:44', '2021-07-06 07:09:02');
INSERT INTO `status_upload` VALUES (940, 'mau', 'DATA 06JULI2021 0831-20210706083132.xlsx', 'file/DATA 06JULI2021 0831-20210706083132.xlsx', 2, 9, NULL, 940, '2021-07-06 08:31:32', '2021-07-06 08:32:01');
INSERT INTO `status_upload` VALUES (941, 'mau', 'TONASE 07 JULY  2021-20210706174449.xlsx', 'file/TONASE 07 JULY  2021-20210706174449.xlsx', 9, 9, NULL, 941, '2021-07-06 17:44:49', '2021-07-06 17:45:01');
INSERT INTO `status_upload` VALUES (942, 'mau', 'DATA 06JULI2021 1500-20210706174545.xlsx', 'file/DATA 06JULI2021 1500-20210706174545.xlsx', 2, 9, NULL, 942, '2021-07-06 17:45:45', '2021-07-06 17:46:01');
INSERT INTO `status_upload` VALUES (943, 'mau', 'DATA 07JULI2021 0855-20210707085600.xlsx', 'file/DATA 07JULI2021 0855-20210707085600.xlsx', 2, 9, NULL, 943, '2021-07-07 08:56:00', '2021-07-07 08:56:01');
INSERT INTO `status_upload` VALUES (944, 'mau', 'DATA 07JULI2021 1200-20210707160438.xlsx', 'file/DATA 07JULI2021 1200-20210707160438.xlsx', 2, 9, NULL, 944, '2021-07-07 16:04:38', '2021-07-07 16:05:02');
INSERT INTO `status_upload` VALUES (945, 'mau', 'DATA 07JULI2021 TESTING-20210708122343.xlsx', 'file/DATA 07JULI2021 TESTING-20210708122343.xlsx', 2, 9, NULL, 945, '2021-07-08 12:23:43', '2021-07-08 12:24:18');
INSERT INTO `status_upload` VALUES (946, 'mau', 'DATA 08JULI2021 1245-20210708124857.xlsx', 'file/DATA 08JULI2021 1245-20210708124857.xlsx', 2, 9, NULL, 946, '2021-07-08 12:48:57', '2021-07-08 12:49:02');
INSERT INTO `status_upload` VALUES (947, 'mau', 'DATA 08JULI2021 1630-20210708163925.xlsx', 'file/DATA 08JULI2021 1630-20210708163925.xlsx', 2, 9, NULL, 947, '2021-07-08 16:39:26', '2021-07-08 16:40:01');
INSERT INTO `status_upload` VALUES (948, 'mau', 'DATA 09JULI2021 0055-20210709005606.xlsx', 'file/DATA 09JULI2021 0055-20210709005606.xlsx', 9, 9, NULL, 948, '2021-07-09 00:56:06', '2021-07-09 15:38:39');
INSERT INTO `status_upload` VALUES (949, 'mau', 'DATA 09JULI2021 0055-20210709030537.xlsx', 'file/DATA 09JULI2021 0055-20210709030537.xlsx', 9, 9, NULL, 949, '2021-07-09 03:05:37', '2021-07-09 15:38:21');
INSERT INTO `status_upload` VALUES (950, 'mau', 'DATA 09JULI2021 0307-20210709032021.xlsx', 'file/DATA 09JULI2021 0307-20210709032021.xlsx', 9, 9, NULL, 950, '2021-07-09 03:20:21', '2021-07-09 15:38:18');
INSERT INTO `status_upload` VALUES (951, 'mau', 'DATA 09JULI2021 0055-20210709043709.xlsx', 'file/DATA 09JULI2021 0055-20210709043709.xlsx', 9, 9, NULL, 951, '2021-07-09 04:37:09', '2021-07-09 15:38:01');
INSERT INTO `status_upload` VALUES (952, 'mau', 'DATA 09JULI2021 0307-20210709060323.xlsx', 'file/DATA 09JULI2021 0307-20210709060323.xlsx', 9, 9, NULL, 952, '2021-07-09 06:03:23', '2021-07-09 15:37:20');
INSERT INTO `status_upload` VALUES (953, 'mau', 'DATA 09JULI2021 TESTING-20210709153022.xlsx', 'file/DATA 09JULI2021 TESTING-20210709153022.xlsx', 9, 9, NULL, 953, '2021-07-09 15:30:22', '2021-07-09 15:37:16');
INSERT INTO `status_upload` VALUES (954, 'mau', 'DATA 09JULI2021 0600-20210709154611.xlsx', 'file/DATA 09JULI2021 0600-20210709154611.xlsx', 2, 9, NULL, 954, '2021-07-09 15:46:11', '2021-07-09 15:47:02');
INSERT INTO `status_upload` VALUES (955, 'mau', 'DATA 09JULI2021 0055A-20210709154959.xlsx', 'file/DATA 09JULI2021 0055A-20210709154959.xlsx', 2, 9, NULL, 955, '2021-07-09 15:49:59', '2021-07-09 15:50:02');
INSERT INTO `status_upload` VALUES (956, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 956, '2021-07-09 15:59:16', '2021-07-09 15:59:47');
INSERT INTO `status_upload` VALUES (957, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 957, '2021-07-09 16:19:40', '2021-07-09 16:20:01');
INSERT INTO `status_upload` VALUES (958, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 958, '2021-07-09 16:24:59', '2021-07-09 16:25:01');
INSERT INTO `status_upload` VALUES (959, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 959, '2021-07-09 16:25:31', '2021-07-09 16:26:01');
INSERT INTO `status_upload` VALUES (960, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 960, '2021-07-09 17:04:35', '2021-07-09 17:05:02');
INSERT INTO `status_upload` VALUES (961, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 961, '2021-07-09 17:06:38', '2021-07-09 17:07:01');
INSERT INTO `status_upload` VALUES (962, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 962, '2021-07-09 17:09:43', '2021-07-09 17:10:01');
INSERT INTO `status_upload` VALUES (963, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 963, '2021-07-09 17:12:26', '2021-07-09 17:13:01');
INSERT INTO `status_upload` VALUES (964, 'mau', 'File has been removed !', '------------------------', 9, 9, NULL, 964, '2021-07-09 17:17:09', '2021-07-09 17:18:02');
INSERT INTO `status_upload` VALUES (965, 'mau', 'DATA 09JULI2021 0307A-20210709171937.xlsx', 'file/DATA 09JULI2021 0307A-20210709171937.xlsx', 2, 9, NULL, 965, '2021-07-09 17:19:37', '2021-07-09 17:20:01');

-- ----------------------------
-- Table structure for t_number
-- ----------------------------
DROP TABLE IF EXISTS `t_number`;
CREATE TABLE `t_number`  (
  `IDnumber` int(11) NOT NULL AUTO_INCREMENT,
  `DescriptionCode` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `QueveNumber` int(4) NOT NULL,
  `totaldigit` int(11) NOT NULL,
  `Active` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`IDnumber`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of t_number
-- ----------------------------
INSERT INTO `t_number` VALUES (1, 'PLP_MOHON', 1, 6, 0);
INSERT INTO `t_number` VALUES (2, 'PLP_BATAL', 1, 6, 0);
INSERT INTO `t_number` VALUES (3, 'TPS_ONLINE', 679186, 6, 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL COMMENT '1 = OPERATOR, 2 = AIRLINES',
  `access_domain` enum('1','2') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '1 = MAU, 2 = RA',
  `is_active` int(1) NULL DEFAULT NULL,
  `date_created` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `email`(`email`) USING BTREE,
  INDEX `password`(`password`) USING BTREE,
  INDEX `active`(`is_active`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (9, 'Operational', 'mau@operational.com', 'default.png', '$2y$10$qYXMC0kEljfCqocZNOu85OKlgxcD1Pw96IIpBQB98wcgYqMA5gDby', 1, '1', 1, 1573567854);
INSERT INTO `user` VALUES (15, 'Airlines', 'mau@airlines.com', 'default.png', '$2y$10$HofLJc4vqf3RbNU/9GW/U./c086pag2ebVqdoKV76.ItxCFLoPD2O', 2, '1', 1, 1582022004);
INSERT INTO `user` VALUES (18, 'Operational', 'ra@operational.com', 'default.png', '$2y$10$RWFJ65LnOEfU9/UMCe7iwujtTRRteh836XxR1D9kDDvrR1Gc/Er6C', 1, '2', 1, 1573567854);
INSERT INTO `user` VALUES (19, 'Airlines', 'ra@airlines.com', 'default.png', '$2y$10$1rPJaGl.bWP7oR3eNdXKw.5PtmybWE/.kcchp6QlMd8bMA5tHEKH2', 2, '2', 1, 1582022004);

-- ----------------------------
-- Table structure for user_access_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_access_menu`;
CREATE TABLE `user_access_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `menu_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_access_menu
-- ----------------------------
INSERT INTO `user_access_menu` VALUES (6, 1, 5);
INSERT INTO `user_access_menu` VALUES (14, 1, 11);
INSERT INTO `user_access_menu` VALUES (18, 1, 12);
INSERT INTO `user_access_menu` VALUES (25, 1, 1);
INSERT INTO `user_access_menu` VALUES (34, 2, 1);
INSERT INTO `user_access_menu` VALUES (35, 2, 5);

-- ----------------------------
-- Table structure for user_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_menu`;
CREATE TABLE `user_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `controller` varchar(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `icon` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` int(1) NULL DEFAULT 1,
  `have_a_child` int(1) NULL DEFAULT NULL,
  `sort` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_menu
-- ----------------------------
INSERT INTO `user_menu` VALUES (1, 'Dashboard', 'dashboard', 'flaticon-line-graph', 'home/dashboard', 1, 0, '1');
INSERT INTO `user_menu` VALUES (5, 'Report', 'report', 'flaticon-interface-11', 'report', 1, 0, '4');
INSERT INTO `user_menu` VALUES (11, 'Update Status', 'updatestatus', 'flaticon-signs-1', 'status/updateStatus', 1, 0, '5');
INSERT INTO `user_menu` VALUES (12, 'Help', 'help', 'flaticon-share', 'help', 1, 0, '7');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 'Operational');
INSERT INTO `user_role` VALUES (2, 'Airlines');

-- ----------------------------
-- Table structure for user_sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_sub_menu`;
CREATE TABLE `user_sub_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NULL DEFAULT NULL,
  `title` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` int(1) NULL DEFAULT 1,
  `have_a_child` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_sub_sub_menu
-- ----------------------------
DROP TABLE IF EXISTS `user_sub_sub_menu`;
CREATE TABLE `user_sub_sub_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_menu_id` int(11) NULL DEFAULT NULL,
  `title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `is_active` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
