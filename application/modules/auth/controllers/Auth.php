<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->r_tracking = $this->load->database('r_db_tpsonline_tracking',true);
        $this->w_tracking = $this->load->database('w_db_tpsonline_tracking',true);
        // Deletes cache for the currently requested URI 
        $this->output->delete_cache();
    }

    public function index()
    {
        if ($this->session->userdata('email')) {
            redirect('home/dashboard');
        }

        $link = '';
        // $link = 'assets/img/logo_mau.png';
        $data = [
            'title' => 'Tracking - Login',
            'link'  => $link
        ];
        $this->load->view('template/auth_header', $data);
        $this->load->view('auth/login');
        $this->load->view('template/auth_footer');
    }

    public function login()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $return = $this->_login($email, $password);
        echo json_encode($return);
    }

    private function _login($email, $password)
    {
        $user = $this->r_tracking->get_where('user', ['email' => $email])->row_array();
        //Jika usernya ada
        if ($user) 
        {
            //jika usernya aktif
            if ($user['is_active'] == 1) 
            {
                //cek password nya
                if (password_verify($password, $user['password'])) 
                {
                    $data = [
                        'email' => $user['email'],
                        'role_id' => $user['role_id'],
                        'user_id' => $user['id'],
                        'user_nama' => $user['name'],
                        'user_email' => $user['email'],
                        'user_image' => $user['image'],
                        'user_date' => $user['date_created'],
                        'active_db' => $this->config->item('prefixdb'),
                        'access_domain' => $user['access_domain'] // 1 = MAU, 2 = RA
                    ];
                    $this->session->set_userdata($data);
                    $return = ['s' => 'success', 'm'=>'Login Success!'];
                } 
                else 
                {
                    $return = ['s' => 'fail', 'm'=>'Wrong password!'];
                }
            } 
            else 
            {
                $return = ['s' => 'fail', 'm'=>'This email has not been activated!'];
            }
        } 
        else 
        {
            $return = ['s' => 'fail', 'm'=>'Email is not registered!'];
        }
        return $return;
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('auth'));
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }

    function checkUser()
    {
    	$hostName = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getopt('h:');
    	// --------------
        $this->db->select('name, email, role_id, access_domain');
        $this->db->where(['email' => $this->input->post('email'), 'is_active' => '1']);
        $query = $this->db->get('user');
        $checkData = $query->num_rows();
        $data = $query->row_array();
        if ($checkData == 1)
        {
        	if ($hostName == 'www.mau-tracking.com') {
        		if ($data['access_domain'] == '1') {
        			$return = ['s' => 'success', 'm' => 'Ready'];
        		} else {
        			$return = ['s' => 'fail', 'm' => 'Failed !'];
        		}
        	}
        	elseif ($hostName == 'www.citralintas.com') {
        		if ($data['access_domain'] == '2') {
        			$return = ['s' => 'success', 'm' => 'Ready'];
        		} else {
        			$return = ['s' => 'fail', 'm' => 'Failed !'];
        		}
        	} 
        	else {
        		// melalui IP
        		$return = ['s' => 'success', 'm' => 'Ready'];
        	}
        }
        else
        {
            $return = ['s' => 'fail', 'm' => 'Not Found!'];
        }
        echo json_encode($return);
    }
}
