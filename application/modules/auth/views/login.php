<div class="m-grid m-grid--hor m-grid--root m-page">
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(assets/app/media/img//bg/bg-2.jpg);">
		<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
			<div class="m-login__container">
				<div class="m-login__logo" style="margin-bottom: 30px;">
					<a href="#">
						<div class="container">
							<div class="row">
								<div class="col-sm">
									<i class="fa fa-cogs" aria-hidden="true" style="font-size: 100px; color: #232a4d;"></i>
									<!-- <img alt="" src="<?php // echo base_url('assets/img/logoMau.png'); ?>" height="100"/> -->
									<!-- <img alt="" src="<?php // echo base_url('assets/img/logoRa.png'); ?>" height="100"/> -->
								</div>
							</div>
						</div>
					</a>
				</div>
				<div class="m-login__signin">
					<div class="m-login__head">
						<h3 class="m-login__title">Sign In</h3>
					</div>
					<form class="m-login__form m-form" action="">
						<div class="form-group">
							<input class="form-control m-input" type="text" placeholder="Email" name="email" onblur="check_email();" id="email" autocomplete="off">
						</div>
						<div class="form-group">
							<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" id="password">
						</div>
						<div class="form-group gudang-tps" style="display: none">
							<select class="form-control" name="id_tps" id="id_tps">

							</select>
						</div>
						<div class="row m-login__form-sub">
						</div>
						<div class="m-login__form-action">
							<button id="m_login_signin_submit" style="display: none" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">Sign In</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	@media (max-width: 768px) {
		.m-login__title {
			margin-bottom: 35px;
		}
	}
</style>
<?php $this->load->view('auth/ajax');?>