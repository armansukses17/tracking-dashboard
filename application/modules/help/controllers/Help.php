<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);
        
class Help extends MY_Controller 
{
    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata("user_id"))) {
            redirect(base_url('auth'));
        }
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
         $data = ['title' => ''];
        $this->home_library->main('errors/under_construction', $data); 
    }
}
