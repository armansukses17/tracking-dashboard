<div class="m-portlet m-portlet">
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12">
            	<form class="m-form m-form--fit m-form--label-align-right" id="formStatusPost">
					<div class="m-portlet__body">
						<div class="form-group m-form__group row">
							<label for="typeReport" class="col-2 col-form-label">MAWB / SMU</label>
							<div class="col-8" style="position: relative;">
								<img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="" id="loaderTracking">
								<input type="text" name="mawb" id="mawb" class="form-control input-sm" placeholder="Autocomplete mawb / waybill_smu ...">
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label for="typeReport" class="col-2 col-form-label">Status</label>
							<div class="col-8">
								<img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="" id="loaderTracking2">
								<select class="form-control" name="statusTracking" id="statusTracking">
								</select>
							</div>
						</div>
                        <div class="form-group m-form__group row" id="tot_item_reject" style="display: none;">
                            <label for="total_item" class="col-2 col-form-label">Total Item</label>
                            <div class="col-8">
                                <input type="text" name="total_item" id="total_item" class="form-control input-sm" placeholder="Total item rejected ..." autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group m-form__group row" id="desc_item_reject"  style="display: none;">
                            <label for="desc_item" class="col-2 col-form-label">Description Item Rejected</label>
                            <div class="col-8">
                                <textarea name="desc_item" id="desc_item" class="form-control input-sm" style="height: 90px" placeholder="Description ..."></textarea>
                            </div>
                        </div>
						<div class="form-group m-form__group row">
							<label for="date_time" class="col-2 col-form-label">Date</label>
							<div class="col-8">
								<input type="text" class="form-control m-input dateTimePicker" name="date_time" id="date_time" autocomplete="off" placeholder="yyyy-mm-dd hh:ii" />
								<span class="m-form__help"><small>Format time 24 hour. example: 2021-02-06 23:56</small></span>
							</div>
						</div>
						<div class="form-group m-form__group row mt-3" style="padding-top: 0;">
							<label for="example-date-input" class="col-2 col-form-label"></label>
							<div class="col-8">
								<button type="submit" class="btn btn-success">Submit</button>
							</div>
						</div>
					</div>
				</form>
				<hr>
 
            </div>
        </div>
    </div>
</div>

<style>
	.form-control.datepicker {
		width: 100%;
	}
	#loaderTracking, 
	#loaderTracking2 {
        display: none;
        width: 20px;
        position: absolute;
        right: 24px;
        top: 9px;
        bottom: 0;
        z-index: 999;
    }
</style>

<script>
$(document).ready(function() {
    // $('.datepicker').datepicker({
    //     format: 'yyyy-mm-dd',
    //     orientation: "bottom auto",
    //     autoclose: true
    // });
	$('.dateTimePicker').datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		autoclose: true,
	});
});

$("#mawb").autocomplete({
	source: function(request, response) {
		var term = $("#mawb").val();
        $('#tot_item_reject').hide();
        $('#desc_item_reject').hide();
		$.ajax({
			url: "<?php echo base_url(); ?>status/UpdateStatus/autocompleteMawb/?",
			type: 'post',
			dataType: "json",
			data: { term: term },
			beforeSend: function() {
	            $('#loaderTracking').show();
        	}, 
			success: function(data) {
				$('#loaderTracking').hide();
				response(data);
			}
		});
	},
    select: function(event, ui) {
        var value = ui.item.value;
        if (value == 'notFound') {
            alert('Data not found !');
        } 
        else {
			// Set selection
			$('#mawb').val(ui.item.label); // display the selected text
			getShipment($('#mawb').val());
			return false;
        }
    }
});

function getShipment(mawb) {
	$.ajax({
        type: "POST",
        url : "<?php echo base_url(); ?>status/UpdateStatus/getShipment/?mawb="+mawb,
        data: {},
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function() {
            $('#loaderTracking2').show();
    	}, 
        success: function(data) {
        	$('#loaderTracking2').hide();
            var s = '';
        	s += `<option value="">-- Pilih --</option>`;
        	for (var i = 0; i < data.d.length; i++) {  
        		s += `<option value="${data.d[i].status_code}">${data.d[i].status_name}</option>`;        
           }  
           $('#statusTracking').html(s);
        }
    });
}

$('#formStatusPost').on('submit',function(e) {
    e.preventDefault();
    var form = $(this).serialize();
    // return false;
    $.ajax({
        url : "<?php echo base_url(); ?>status/UpdateStatus/postStatus",
        type: 'post',
        data: form,
        dataType: 'json',
        beforeSend: function() {
        },
        success: function(hasil) {
            if (hasil.s == 'success') {
            	swal('Success !', hasil.m, 'success'); 
            	$('#formStatusPost')[0].reset(); // reset form
                $('#tot_item_reject').hide();
                $('#desc_item_reject').hide();
            } 
            else {
                swal('Error !', hasil.m, 'error'); 
                $('#formStatusPost')[0].reset(); // reset form
                $('#tot_item_reject').hide();
                $('#desc_item_reject').hide();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('error !!!!!!');
        }
    });
});

$("#statusTracking").change(function () {
    $( "#statusTracking option:selected" ).each(function() {
        if ($(this).val() == 'RA004') {
            $('#tot_item_reject').fadeIn(500);
            $('#desc_item_reject').fadeIn(500);
            $('#total_item').val('');
            $('#desc_item').val('');
        } else {
            $('#tot_item_reject').hide();
            $('#desc_item_reject').hide();
        }
    });
});
</script>
