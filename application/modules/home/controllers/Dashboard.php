<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata("user_id"))) {
			redirect(base_url('auth'));
		}
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$data = [
			'title' => 'Dashboard',
		];
		$this->home_library->main('home/index', $data);
	}
}
