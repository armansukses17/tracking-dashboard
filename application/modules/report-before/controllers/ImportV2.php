<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
        
class ImportV2 extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata("user_id"))) {
			redirect(base_url('auth'));
		}

        date_default_timezone_set('Asia/Jakarta');
	}

    function importInboundOutbondVERSI2() 
    {
        $fileName = $_FILES['fileExcel']['name'];
        $dateTime = date('YmdHis');

        if (empty($fileName))
        {
            $return = ['s' => 'fail', 'm' => 'Data cannot be empty !'];
        }
        else
        {
            $allowed_extension = array('xlsx', 'xls'); // 'xls', 'csv', etc

            $arr_file = explode('.', $fileName);
            $extension = end($arr_file);

            if (!in_array($extension, $allowed_extension))
            {
                $return = ['s' => 'fail', 'm' => 'Incorrect data format !. Only (xlxs & xls) format allowed'];
            }
            else
            {
                $createNewFileName = "$arr_file[0]-$dateTime.$extension";
                // move to directory file
                $targetPath = 'file/' . $createNewFileName;
                move_uploaded_file($_FILES['fileExcel']['tmp_name'], $targetPath);

                if ($extension == 'xlsx')
                {
                    // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }
                elseif ($extension == 'xls')
                {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                $spreadsheet = $reader->load($targetPath);
                $sheetCount = $spreadsheet->getSheetCount();

                if ($sheetCount != 12) // jumlah sheet ad 12
                {
                    $return = ['s' => 'fail', 'm' => 'Incorrect data format !'];
                    file_exists($targetPath) ? unlink($targetPath) : '';
                }
                else
                {
                    // -------------------------------------------------------------
                    // INBOUND
                    // -------------------------------------------------------------
                    $sheetInbound = $spreadsheet->getSheetByName('th_inbound')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound1 = [];

                    foreach ($sheetInbound as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound1, array(
                                'id_' => $row['A'],
                                'gate_type' => $row['B'],
                                'waybill_smu' => $row['C'],
                                'hawb' => $row['D'],
                                'koli' => $row['E'],
                                'netto' => $row['F'],
                                'volume' => $row['G'],
                                'kindofgood' => $row['H'],
                                'airline_code' => $row['I'],
                                'flight_no' => $row['J'],
                                'origin' => $row['K'],
                                'transit' => $row['L'],
                                'dest' => $row['M'],
                                'shipper_name' => $row['N'],
                                'consignee_name' => $row['O'],
                                '_is_active' => $row['P']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetInbound2 = $spreadsheet->getSheetByName('td_inbound_delivery')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound2 = [];

                    foreach ($sheetInbound2 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound2, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetInbound3 = $spreadsheet->getSheetByName('td_inbound_breakdown')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound3 = [];

                    foreach ($sheetInbound3 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound3, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetInbound4 = $spreadsheet->getSheetByName('td_inbound_storage')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound4 = [];

                    foreach ($sheetInbound4 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound4, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetInbound5 = $spreadsheet->getSheetByName('td_inbound_clearance')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound5 = [];

                    foreach ($sheetInbound5 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound5, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetInbound6 = $spreadsheet->getSheetByName('td_inbound_pod')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound6 = [];

                    foreach ($sheetInbound6 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound6, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }

                    ################################################################

                    // // -------------------------------------------------------------
                    // // OUTBOND
                    // // -------------------------------------------------------------
                    $sheetOutbond = $spreadsheet->getSheetByName('th_outbond')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond1 = [];

                    foreach ($sheetOutbond as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond1, array(
                                'id_' => $row['A'],
                                'gate_type' => $row['B'],
                                'waybill_smu' => $row['C'],
                                'hawb' => $row['D'],
                                'koli' => $row['E'],
                                'netto' => $row['F'],
                                'volume' => $row['G'],
                                'kindofgood' => $row['H'],
                                'airline_code' => $row['I'],
                                'flight_no' => $row['J'],
                                'origin' => $row['K'],
                                'transit' => $row['L'],
                                'dest' => $row['M'],
                                'shipper_name' => $row['N'],
                                'consignee_name' => $row['O'],
                                '_is_active' => $row['P']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond2 = $spreadsheet->getSheetByName('td_outbond_acceptance')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond2 = [];

                    foreach ($sheetOutbond2 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond2, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond3 = $spreadsheet->getSheetByName('td_outbond_weighing')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond3 = [];

                    foreach ($sheetOutbond3 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond3, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond4 = $spreadsheet->getSheetByName('td_outbond_manifest')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond4 = [];

                    foreach ($sheetOutbond4 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond4, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond5 = $spreadsheet->getSheetByName('td_outbond_storage')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond5 = [];

                    foreach ($sheetOutbond5 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond5, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond6 = $spreadsheet->getSheetByName('td_outbond_buildup')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond6 = [];

                    foreach ($sheetOutbond6 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond6, array(
                                'id_header' => $row['A'],
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // -------------------------------------------------------------
                    $all = [
                        'dataInbound1' => $dataInbound1,
                        'dataInbound2' => $dataInbound2,
                        'dataInbound3' => $dataInbound3,
                        'dataInbound4' => $dataInbound4,
                        'dataInbound5' => $dataInbound5,
                        'dataInbound6' => $dataInbound6,
                        'dataOutbond1' => $dataOutbond1,
                        'dataOutbond2' => $dataOutbond2,
                        'dataOutbond3' => $dataOutbond3,
                        'dataOutbond4' => $dataOutbond4,
                        'dataOutbond5' => $dataOutbond5,
                        'dataOutbond6' => $dataOutbond6

                    ];
                    // print_r($all); die;
                    $th_inbound = $this->db->insert_batch('th_inbound', $dataInbound1);
                    $td_inbound_delivery = $this->db->insert_batch('td_inbound_delivery', $dataInbound2);
                    $td_inbound_breakdown = $this->db->insert_batch('td_inbound_breakdown', $dataInbound3);
                    $td_inbound_storage = $this->db->insert_batch('td_inbound_storage', $dataInbound4);
                    $td_inbound_clearance = $this->db->insert_batch('td_inbound_clearance', $dataInbound5);
                    $td_inbound_pod = $this->db->insert_batch('td_inbound_pod', $dataInbound6);
                    $th_outbond = $this->db->insert_batch('th_outbond', $dataOutbond1);
                    $td_outbond_acceptance = $this->db->insert_batch('td_outbond_acceptance', $dataOutbond2);
                    $td_outbond_weighing = $this->db->insert_batch('td_outbond_weighing', $dataOutbond3);
                    $td_outbond_manifest = $this->db->insert_batch('td_outbond_manifest', $dataOutbond4);
                    $td_outbond_storage = $this->db->insert_batch('td_outbond_storage', $dataOutbond5);
                    $td_outbond_buildup = $this->db->insert_batch('td_outbond_buildup', $dataOutbond6);

                    if ($th_inbound AND 
                        $td_inbound_delivery AND 
                        $td_inbound_breakdown AND 
                        $td_inbound_storage AND 
                        $td_inbound_clearance AND 
                        $td_inbound_pod AND 
                        $th_outbond AND 
                        $td_outbond_acceptance AND 
                        $td_outbond_weighing AND 
                        $td_outbond_manifest AND 
                        $td_outbond_storage AND 
                        $td_outbond_buildup) 
                    {
                        $return = ['s' => 'success', 'm' => 'Import data successfully.'];
                    }
                    else
                    {
                        // DELETE INBOUND ---------------
                        foreach ($dataInbound1 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('th_inbound');
                        }

                        foreach ($dataInbound2 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_inbound_delivery');
                        }

                        foreach ($dataInbound3 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_inbound_breakdown');
                        }

                        foreach ($dataInbound4 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_inbound_storage');
                        }

                        foreach ($dataInbound5 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_inbound_clearance');
                        }

                        foreach ($dataInbound6 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_inbound_pod');
                        }
                        // DELETE OUTBOND ---------------
                        foreach ($dataOutbond1 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('th_outbond');
                        }

                        foreach ($dataOutbond2 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_outbond_acceptance');
                        }

                        foreach ($dataOutbond3 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_outbond_weighing');
                        }

                        foreach ($dataOutbond4 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_outbond_manifest');
                        }

                        foreach ($dataOutbond5 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_outbond_storage');
                        }

                        foreach ($dataOutbond6 as $row) {
                            $this->db->where(['id_' => $row['id_']]);
                            $this->db->delete('td_outbond_buildup');
                        }
                        
                        $return = ['s' => 'fail', 'm' => 'Data failed !.'];
                    }
                } // end: count sheet
            }
        }
        echo json_encode($return); 
    }
}
