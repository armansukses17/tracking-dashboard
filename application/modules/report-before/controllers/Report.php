<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
        
class Report extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata("user_id"))) {
			redirect(base_url('auth'));
		}
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
        $access_domain = $this->session->userdata['access_domain'];
		$getTypeReport = $this->getTypeReport();
		$data = [
			'title' => 'Report',
			'reportType' => $getTypeReport
		];

        if ($access_domain == 1)
        {
            $this->home_library->main('report/index', $data); // MAU
        }
        else
        {
            $this->home_library->main('report/index-ra', $data); // RA
        }
		
	}

	public function getTypeReport()
    {
        $access_domain = $this->session->userdata['access_domain'];
        if ($access_domain == 1)
        {
            $whereIn = "'1', '2', '3'"; // Inbound & outbond MAU
        }
        else
        {
            $whereIn = "'4', '5', '6'"; // RA
        }
		$getData = "SELECT * FROM m_report WHERE _id IN ($whereIn)";
		$get = $this->db->query($getData);
        $data = $get->result();
        return $data;
    }

    function tableReport()
    {
        $startDate = $this->input->post('startDate');
        $endDate = $this->input->post('endDate');
        $typeReport = trim($this->input->post('typeReport'));

        if ($typeReport == '1') 
        {	// Outbond
        	$select = 'waybill_smu, koli, "" weight, netto, kindofgood, _created_at';
            $table = "th_outbond";
            $andWhere = "AND gate_type IN ('outgoing', 'ekspor')";
        } 
        elseif ($typeReport == '2') 
        { // Inbond
            $select = 'waybill_smu, koli, "" weight, netto, kindofgood, _created_at';
            $table = "th_inbound";
            $andWhere = "AND gate_type IN ('incoming', 'import')";
        } 
        elseif ($typeReport == '3') 
        { // Inbond bagian transit
            $select = 'waybill_smu, koli, "" weight, netto, kindofgood, _created_at';
            $table = "th_inbound";
            $andWhere = "AND gate_type IN ('transit')";
        } 
        else 
        { // RA
            $select = 'waybill_smu, koli, "" weight, netto, kindofgood, _created_at';
            $table = "th_regulated";
            $andWhere = "";
        }

        $where = "(_created_at BETWEEN '".date('Y-m-d',strtotime($startDate))." 00:00:00' AND '".date('Y-m-d',strtotime($endDate))." 23:59:59') $andWhere";
        $column_order = array('');
        $column_search = array('');
        $order = array(null);
        $groupBy = [];

        $list = $this->crud->getDatatable2('r_tracking', $select, $table, [$where => null], $column_search, $column_order, $order, [], $groupBy);
        $myTable = $table;
        $join = [];
        $wkwk = $this->crud->dataTableCount2('r_tracking', $myTable, [$where => null], $join, $groupBy);
        // print_r($list); die;
        $data = array();
        $no = $_POST['start'] + 1;
        foreach ($list as $row) 
        {
            $sub_array = array();
            // $sub_array[] = $no++;
            $sub_array[] = $row->waybill_smu;
            $sub_array[] = $row->koli;
            $sub_array[] = $row->weight;
            $sub_array[] = $row->netto;
            $sub_array[] = $row->kindofgood;
            $sub_array[] = $row->_created_at;
            $data[] = $sub_array;
        }

		$output = array(
            'draw'	             => $_POST['draw'],
            'recordsTotal'	     => $wkwk,
            'recordsFiltered'	 => $this->crud->dataTableFilter2('r_tracking','*', $myTable, [$where => null], $column_search, $column_order, $order, $join, $groupBy),
            'data'		         => $data
		);
		echo json_encode($output);
    }

    function importInboundOutbond() 
    {
        $fileName = $_FILES['fileExcel']['name'];
        $dateTime = date('YmdHis');

        if (empty($fileName))
        {
            $return = ['s' => 'fail', 'm' => 'Data cannot be empty !'];
        }
        else
        {
            $allowed_extension = array('xlsx', 'xls'); // 'xls', 'csv', etc

            $arr_file = explode('.', $fileName);
            $extension = end($arr_file);

            if (!in_array($extension, $allowed_extension))
            {
                $return = ['s' => 'fail', 'm' => 'Incorrect data format !. Only (xlxs & xls) format allowed'];
            }
            else
            {
                $createNewFileName = "$arr_file[0]-$dateTime.$extension";
                // move to directory file
                $targetPath = 'file/' . $createNewFileName;
                move_uploaded_file($_FILES['fileExcel']['tmp_name'], $targetPath);

                if ($extension == 'xlsx')
                {
                    // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }
                elseif ($extension == 'xls')
                {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                $spreadsheet = $reader->load($targetPath);
                $sheetCount = $spreadsheet->getSheetCount();

                if ($sheetCount != 12) // jumlah sheet ad 12
                {
                    $return = ['s' => 'fail', 'm' => 'Incorrect data format !'];
                    file_exists($targetPath) ? unlink($targetPath) : '';
                }
                else
                {
                    // -------------------------------------------------------------
                    // INBOUND
                    // -------------------------------------------------------------
                    $sheetInbound = $spreadsheet->getSheetByName('th_inbound')->toArray();
                    for ($i = 1; $i < count($sheetInbound); $i++)
                    {
                        $data = [
                            'id_' => $sheetInbound[$i]['0'],
                            'gate_type' => $sheetInbound[$i]['1'],
                            'waybill_smu' => $sheetInbound[$i]['2'],
                            'hawb' => $sheetInbound[$i]['3'],
                            'koli' => $sheetInbound[$i]['4'],
                            'netto' => $sheetInbound[$i]['5'],
                            'volume' => $sheetInbound[$i]['6'],
                            'kindofgood' => $sheetInbound[$i]['7'],
                            'airline_code' => $sheetInbound[$i]['8'],
                            'flight_no' => $sheetInbound[$i]['9'],
                            'origin' => $sheetInbound[$i]['10'],
                            'transit' => $sheetInbound[$i]['11'],
                            'dest' => $sheetInbound[$i]['12'],
                            'shipper_name' => $sheetInbound[$i]['13'],
                            'consignee_name' => $sheetInbound[$i]['14'],
                            '_is_active' => $sheetInbound[$i]['15']
                        ];
                        $this->db->insert('th_inbound', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetInbound2 = $spreadsheet->getSheetByName('td_inbound_delivery')->toArray();
                    for ($i = 1; $i < count($sheetInbound2); $i++)
                    {
                        $data = [
                            'id_header' => $sheetInbound2[$i]['0'],
                            'status_date' => $sheetInbound2[$i]['1'],
                            'status_time' => $sheetInbound2[$i]['2'],
                            '_is_active' => $sheetInbound2[$i]['3']
                        ];
                        $this->db->insert('td_inbound_delivery', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetInbound3 = $spreadsheet->getSheetByName('td_inbound_breakdown')->toArray();
                    for ($i = 1; $i < count($sheetInbound3); $i++)
                    {
                        $data = [
                            'id_header' => $sheetInbound3[$i]['0'],
                            'status_date' => $sheetInbound3[$i]['1'],
                            'status_time' => $sheetInbound3[$i]['2'],
                            '_is_active' => $sheetInbound3[$i]['3']
                        ];
                        $this->db->insert('td_inbound_breakdown', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetInbound4 = $spreadsheet->getSheetByName('td_inbound_storage')->toArray();
                    for ($i = 1; $i < count($sheetInbound4); $i++)
                    {
                        $data = [
                            'id_header' => $sheetInbound4[$i]['0'],
                            'status_date' => $sheetInbound4[$i]['1'],
                            'status_time' => $sheetInbound4[$i]['2'],
                            '_is_active' => $sheetInbound4[$i]['3']
                        ];
                        $this->db->insert('td_inbound_storage', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetInbound5 = $spreadsheet->getSheetByName('td_inbound_clearance')->toArray();
                    for ($i = 1; $i < count($sheetInbound5); $i++)
                    {
                        $data = [
                            'id_header' => $sheetInbound5[$i]['0'],
                            'status_date' => $sheetInbound5[$i]['1'],
                            'status_time' => $sheetInbound5[$i]['2'],
                            '_is_active' => $sheetInbound5[$i]['3']
                        ];
                        $this->db->insert('td_inbound_clearance', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetInbound6 = $spreadsheet->getSheetByName('td_inbound_pod')->toArray();
                    for ($i = 1; $i < count($sheetInbound6); $i++)
                    {
                        $data = [
                            'id_header' => $sheetInbound6[$i]['0'],
                            'status_date' => $sheetInbound6[$i]['1'],
                            'status_time' => $sheetInbound6[$i]['2'],
                            '_is_active' => $sheetInbound6[$i]['3']
                        ];
                        $this->db->insert('td_inbound_pod', $data);
                    }

                    ################################################################

                    // // -------------------------------------------------------------
                    // // OUTBOND
                    // // -------------------------------------------------------------
                    $sheetOutbond = $spreadsheet->getSheetByName('th_outbond')->toArray();
                    for ($i = 1; $i < count($sheetOutbond); $i++)
                    {
                        $data = [
                            'id_' => $sheetOutbond[$i]['0'],
                            'gate_type' => $sheetOutbond[$i]['1'],
                            'waybill_smu' => $sheetOutbond[$i]['2'],
                            'hawb' => $sheetOutbond[$i]['3'],
                            'koli' => $sheetOutbond[$i]['4'],
                            'netto' => $sheetOutbond[$i]['5'],
                            'volume' => $sheetOutbond[$i]['6'],
                            'kindofgood' => $sheetOutbond[$i]['7'],
                            'airline_code' => $sheetOutbond[$i]['8'],
                            'flight_no' => $sheetOutbond[$i]['9'],
                            'origin' => $sheetOutbond[$i]['10'],
                            'transit' => $sheetOutbond[$i]['11'],
                            'dest' => $sheetOutbond[$i]['12'],
                            'shipper_name' => $sheetOutbond[$i]['13'],
                            'consignee_name' => $sheetOutbond[$i]['14'],
                            '_is_active' => $sheetOutbond[$i]['15']
                        ];
                        $this->db->insert('th_outbond', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond2 = $spreadsheet->getSheetByName('td_outbond_acceptance')->toArray();
                    for ($i = 1; $i < count($sheetOutbond2); $i++)
                    {
                        $data = [
                            'id_header' => $sheetOutbond2[$i]['0'],
                            'status_date' => $sheetOutbond2[$i]['1'],
                            'status_time' => $sheetOutbond2[$i]['2'],
                            '_is_active' => $sheetOutbond2[$i]['3']
                        ];
                        $this->db->insert('td_outbond_acceptance', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond3 = $spreadsheet->getSheetByName('td_outbond_weighing')->toArray();
                    for ($i = 1; $i < count($sheetOutbond3); $i++)
                    {
                        $data = [
                            'id_header' => $sheetOutbond3[$i]['0'],
                            'status_date' => $sheetOutbond3[$i]['1'],
                            'status_time' => $sheetOutbond3[$i]['2'],
                            '_is_active' => $sheetOutbond3[$i]['3']
                        ];
                        $this->db->insert('td_outbond_weighing', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond4 = $spreadsheet->getSheetByName('td_outbond_manifest')->toArray();
                    for ($i = 1; $i < count($sheetOutbond4); $i++)
                    {
                        $data = [
                            'id_header' => $sheetOutbond4[$i]['0'],
                            'status_date' => $sheetOutbond4[$i]['1'],
                            'status_time' => $sheetOutbond4[$i]['2'],
                            '_is_active' => $sheetOutbond4[$i]['3']
                        ];
                        $this->db->insert('td_outbond_manifest', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond5 = $spreadsheet->getSheetByName('td_outbond_storage')->toArray();
                    for ($i = 1; $i < count($sheetOutbond5); $i++)
                    {
                        $data = [
                            'id_header' => $sheetOutbond5[$i]['0'],
                            'status_date' => $sheetOutbond5[$i]['1'],
                            'status_time' => $sheetOutbond5[$i]['2'],
                            '_is_active' => $sheetOutbond5[$i]['3']
                        ];
                        $this->db->insert('td_outbond_storage', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetOutbond6 = $spreadsheet->getSheetByName('td_outbond_buildup')->toArray();
                    for ($i = 1; $i < count($sheetOutbond6); $i++)
                    {
                        $data = [
                            'id_header' => $sheetOutbond6[$i]['0'],
                            'status_date' => $sheetOutbond6[$i]['1'],
                            'status_time' => $sheetOutbond6[$i]['2'],
                            '_is_active' => $sheetOutbond6[$i]['3']
                        ];
                        $this->db->insert('td_outbond_buildup', $data);
                    }
                    // -------------------------------------------------------------
                    $return = ['s' => 'success', 'm' => 'Import data successfully.'];
                } // end: count sheet
            }
        }
        echo json_encode($return); 
    }

    function importRa() 
    {
        $fileName = $_FILES['fileExcel']['name'];
        $dateTime = date('YmdHis');

        if (empty($fileName))
        {
            $return = ['s' => 'fail', 'm' => 'Data cannot be empty !'];
        }
        else
        {
            $allowed_extension = array('xlsx', 'xls'); // 'xls', 'csv', etc

            $arr_file = explode('.', $fileName);
            $extension = end($arr_file);

            if (!in_array($extension, $allowed_extension))
            {
                $return = ['s' => 'fail', 'm' => 'Incorrect data format !. Only (xlxs & xls) format allowed'];
            }
            else
            {
                $createNewFileName = "$arr_file[0]-$dateTime.$extension";
                // move to directory file
                $targetPath = 'file/' . $createNewFileName;
                move_uploaded_file($_FILES['fileExcel']['tmp_name'], $targetPath);

                if ($extension == 'xlsx')
                {
                    // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }
                elseif ($extension == 'xls')
                {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
                }
                $spreadsheet = $reader->load($targetPath);
                $sheetCount = $spreadsheet->getSheetCount();

                if ($sheetCount != 10) // jumlah sheet ad 10
                {
                    $return = ['s' => 'fail', 'm' => 'Incorrect data format !'];
                    file_exists($targetPath) ? unlink($targetPath) : '';
                }
                else
                {
                    // -------------------------------------------------------------
                    // RA
                    // -------------------------------------------------------------
                    $sheetRegulated = $spreadsheet->getSheetByName('th_regulated')->toArray();
                    for ($i = 1; $i < count($sheetRegulated); $i++)
                    {
                        $data = [
                            'id_' => $sheetRegulated[$i]['0'],
                            'gate_type' => $sheetRegulated[$i]['1'],
                            'waybill_smu' => $sheetRegulated[$i]['2'],
                            'hawb' => $sheetRegulated[$i]['3'],
                            'koli' => $sheetRegulated[$i]['4'],
                            'netto' => $sheetRegulated[$i]['5'],
                            // 'volume' => $sheetRegulated[$i]['6'],
                            'kindofgood' => $sheetRegulated[$i]['6'],
                            'airline_code' => $sheetRegulated[$i]['7'],
                            'flight_no' => $sheetRegulated[$i]['8'],
                            'origin' => $sheetRegulated[$i]['9'],
                            'transit' => $sheetRegulated[$i]['10'],
                            'dest' => $sheetRegulated[$i]['11'],
                            'shipper_name' => $sheetRegulated[$i]['12'],
                            'consignee_name' => $sheetRegulated[$i]['13'],
                            '_is_active' => $sheetRegulated[$i]['14']
                        ];
                        $this->db->insert('th_regulated', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated2 = $spreadsheet->getSheetByName('td_regulated_acceptance')->toArray();
                    for ($i = 1; $i < count($sheetRegulated2); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated2[$i]['0'],
                            'status_date' => $sheetRegulated2[$i]['1'],
                            'status_time' => $sheetRegulated2[$i]['2'],
                            '_is_active' => $sheetRegulated2[$i]['3']
                        ];
                        $this->db->insert('td_regulated_acceptance', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated3 = $spreadsheet->getSheetByName('td_regulated_screening')->toArray();
                    for ($i = 1; $i < count($sheetRegulated3); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated3[$i]['0'],
                            'status_date' => $sheetRegulated3[$i]['1'],
                            'status_time' => $sheetRegulated3[$i]['2'],
                            '_is_active' => $sheetRegulated3[$i]['3']
                        ];
                        $this->db->insert('td_regulated_screening', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated4 = $spreadsheet->getSheetByName('td_regulated_weighing')->toArray();
                    for ($i = 1; $i < count($sheetRegulated4); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated4[$i]['0'],
                            'status_date' => $sheetRegulated4[$i]['1'],
                            'status_time' => $sheetRegulated4[$i]['2'],
                            '_is_active' => $sheetRegulated4[$i]['3']
                        ];
                        $this->db->insert('td_regulated_weighing', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated5 = $spreadsheet->getSheetByName('td_regulated_rejected')->toArray();
                    for ($i = 1; $i < count($sheetRegulated5); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated5[$i]['0'],
                            'status_date' => $sheetRegulated5[$i]['1'],
                            'status_time' => $sheetRegulated5[$i]['2'],
                            '_is_active' => $sheetRegulated5[$i]['3']
                        ];
                        $this->db->insert('td_regulated_rejected', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated6 = $spreadsheet->getSheetByName('td_regulated_storage')->toArray();
                    for ($i = 1; $i < count($sheetRegulated6); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated6[$i]['0'],
                            'status_date' => $sheetRegulated6[$i]['1'],
                            'status_time' => $sheetRegulated6[$i]['2'],
                            '_is_active' => $sheetRegulated6[$i]['3']
                        ];
                        $this->db->insert('td_regulated_storage', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated7 = $spreadsheet->getSheetByName('td_regulated_csd')->toArray();
                    for ($i = 1; $i < count($sheetRegulated7); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated7[$i]['0'],
                            'status_date' => $sheetRegulated7[$i]['1'],
                            'status_time' => $sheetRegulated7[$i]['2'],
                            '_is_active' => $sheetRegulated7[$i]['3']
                        ];
                        $this->db->insert('td_regulated_csd', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated8 = $spreadsheet->getSheetByName('td_regulated_loading')->toArray();
                    for ($i = 1; $i < count($sheetRegulated8); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated8[$i]['0'],
                            'status_date' => $sheetRegulated8[$i]['1'],
                            'status_time' => $sheetRegulated8[$i]['2'],
                            '_is_active' => $sheetRegulated8[$i]['3']
                        ];
                        $this->db->insert('td_regulated_loading', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated9 = $spreadsheet->getSheetByName('td_regulated_transport')->toArray();
                    for ($i = 1; $i < count($sheetRegulated9); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated9[$i]['0'],
                            'status_date' => $sheetRegulated9[$i]['1'],
                            'status_time' => $sheetRegulated9[$i]['2'],
                            '_is_active' => $sheetRegulated9[$i]['3']
                        ];
                        $this->db->insert('td_regulated_transport', $data);
                    }
                    // -------------------------------------------------------------
                    $sheetRegulated10 = $spreadsheet->getSheetByName('td_regulated_handover')->toArray();
                    for ($i = 1; $i < count($sheetRegulated10); $i++)
                    {
                        $data = [
                            'id_header' => $sheetRegulated10[$i]['0'],
                            'status_date' => $sheetRegulated10[$i]['1'],
                            'status_time' => $sheetRegulated10[$i]['2'],
                            '_is_active' => $sheetRegulated10[$i]['3']
                        ];
                        $this->db->insert('td_regulated_handover', $data);
                    }
                    ################################################################

                    $return = ['s' => 'success', 'm' => 'Import data successfully.'];
                } // end: count sheet
            }
        }
        echo json_encode($return); 
    }
}
