<div class="m-portlet m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Data Report
                </h3>
                 <div class="row">
                    <div class="col-md-12" style="position: absolute;right: 11px;top: 13px;">
                        <div class="ss">
                            <a href="#modalUpload" role="button" class="modalUploadExcel" data-toggle="modal" onclick="modalUploadExcel()">
                            	<button class="btn btn-primary pull-right" id="upload_excels" style="margin-left:8px">Upload</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12">
            	<form class="m-form m-form--fit m-form--label-align-right" id="formReport">
					<div class="m-portlet__body">
						<div class="form-group m-form__group row">
							<label for="typeReport" class="col-1 col-form-label">Type</label>
							<div class="col-11">
								<select class="form-control" name="typeReport" id="typeReport">
									<option value="">-- Pilih --</option>
									<?php foreach($reportType as $rw):?>
			                            <option value="<?php echo $rw->_id;?>"><?php echo $rw->TypeReport;?></option>
			                        <?php endforeach ?>
								</select>
							</div>
						</div>
					<!-- 	<div class="form-group m-form__group row">
							<label for="typeReport" class="col-2 col-form-label">Type</label>
							<div class="col-lg-4 col-md-9 col-sm-12">
								<select class="form-control m-select2" id="m_select2_3" name="param" multiple="multiple">
									<optgroup label="Mountain Time Zone">
										<option value="AZ">Arizona</option>
										<option value="CO">Colorado</option>
										<option value="ID">Idaho</option>
										<option value="MT" selected>Montana</option>
										<option value="UT">Utah</option>
										<option value="WY">Wyoming</option>
									</optgroup>
								</select>
							</div>
						</div> -->
						<div class="form-group m-form__group row">
							<label for="example-date-input" class="col-1 col-form-label">Date</label>
							<div class="col-11">
			                    <div class="row">
								    <div class="col">
								      <input type="text" class="form-control datepicker" name="startDate" id="startDate" placeholder="Startdate" value="<?=date('d-m-Y')?>" autocomplete="off">
								    </div>
								    <div class="col">
								      <input type="text" class="form-control datepicker" name="endDate" id="endDate" placeholder="Enddate" value="<?=date('d-m-Y')?>" autocomplete="off">
								    </div>
								  </div>
							</div>
						</div>
						<div class="form-group m-form__group row" style="padding-top: 0;">
							<label for="example-date-input" class="col-1 col-form-label"></label>
							<div class="col-11">
								<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Filter</button>
								<button type="button"  class="btn btn-danger" id="button_excels" style="margin-left:8px">Download</button>
							</div>
						</div>
					</div>
				</form>
				<hr>
                <table class="table table-hover mt-5" id="tableReport">
                    <thead>
                        <tr>
                            <th>Waybill / SMU</th>
                            <th>Koli</th>
                            <th>Weight</th>
                            <th>Netto</th>
                            <th>Kind Of Goods</th>
                            <th>Datetime</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modalUpload" class="modal animated pulse" tabindex="-1" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload File Excel (Format only: xlxs, xls)</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
            	<div id="onProccess">Waitting ...</div>
                <div class="row">
                   <!--  <div class="col-md-12">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
						    <li class="nav-item">
						        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Inbound</a>
						    </li>
						    <li class="nav-item">
						        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Outbond</a>
						    </li>
						    <li class="nav-item">
						        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Regulated</a>
						    </li>
						</ul>
						<div class="tab-content" id="myTabContent">
						    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						    	<form id="uploadExcelsInbound">
			                        <div class="form-group m-form__group">
										<label for="exampleInputEmail1">Inbound</label>
										<div></div>
										<div class="custom-file">
											<input type="file" class="custom-file-input" id="customFile">
											<label class="custom-file-label" for="customFile">Choose file</label>
										</div>
									</div>
									<button type="submit" class="btn btn-success">Import</button>
								</form>
						    </div>
						    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						    	<form id="uploadExcelsOutbond">
			                        <div class="form-group m-form__group">
										<label for="exampleInputEmail1">Outbond</label>
										<div></div>
										<div class="custom-file">
											<input type="file" class="custom-file-input" id="customFile">
											<label class="custom-file-label" for="customFile">Choose file</label>
										</div>
									</div>
									<button type="submit" class="btn btn-success">Import</button>
								</form>
						    </div>
						    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						    	<form id="uploadExcelsRA">
			                        <div class="form-group m-form__group">
										<label for="exampleInputEmail1">Regulated</label>
										<div></div>
										<div class="custom-file">
											<input type="file" class="custom-file-input" id="customFile">
											<label class="custom-file-label" for="customFile">Choose file</label>
										</div>
									</div>
									<button type="submit" class="btn btn-success">Import</button>
								</form>
						    </div>
						</div>
                    </div> -->
                    <div class="col-md-12">
                    	<div class="wrap-alert"></div>
                    	<form id="importData">
							<div class="form-group">
						        <label for="exampleInputFile">Upload File (Inbound & Outbond)</label>
						        <input type="file" name="fileExcel" id="fileExcel" class="form-control" style="border-color: #405eab;">
						        <span class="m-form__help">
						        	<small>if you don't have the upload format, please download the format <a href="<?php echo base_url('file/format/formatReportMau.xls'); ?>" title="Download format excel">here</a></small>
						        </span>
						    </div>
						    <button type="submit" class="btn btn-primary">Import</button>
						    <!-- <a href="<?php // echo base_url('file/format/formatExcel.xls'); ?>"><button type="button" class="btn btn-danger">Download Format</button></a> -->
						</form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div><!-- End: modal -->

<style>
	.form-control.datepicker {
		width: 100%;
	}
	#myTab .nav-link.active {
		color: #4f72ef;
	}

	#onProccess {
		display: none;
		position: absolute;
		z-index: 2;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		background: #0000006b;
		text-align: center;
		font-size: 20px;
		color: #fff;
		line-height: 188px;
	}
</style>

<script src="<?= base_url()?>assets/js/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/js/dataTables.checkboxes.min.js" type="text/javascript"></script>
<!-- <script src="<?= base_url()?>assets/js/select2.js" type="text/javascript"></script> -->
<script>

$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        orientation: "bottom auto",
        autoclose: true
    });
});

function modalUploadExcel() {
	$('#importData')[0].reset(); // reset form
}

$('#importData').on('submit', function(e) {
	e.preventDefault(); 
	var data = new FormData(this);
	if ($('#fileExcel').val() == '') {
		swal('Error !', 'Please select the file first!', 'error'); 
		return false;
	} 
	else {
    	$.ajax({
			url: '<?php echo base_url();?>report/importInboundOutbond',
			type: "post",
			data: data,
			dataType: "json",
			cache: false,
			processData: false,
			contentType: false,
			async: false,
			beforeSend: function(jqXHR, options) {
		        setTimeout(function() {
		            $.ajax($.extend(options, {beforeSend: $.noop}));
		        }, 1000);
		        $('#onProccess').show();
		        return false;
		    },
			success: function(data) {
				$('#onProccess').hide();
	         	if (data.s == 'success') {
	                swal('Success !', data.m, 'success'); 
	                $('.wrap-alert')
	                .html(`<div class="alert alert-success alert-dismissible fade show" role="alert">	
	                			<span>${data.m}</span>
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							`);
	                $('.alert').delay(5000).fadeOut(500);
	                $('#importData')[0].reset(); // reset form
	            } 
	            else {
	                swal('Error !', data.m, 'error'); 
	            }
	       },
	        error: function (xhr, ajaxOptions, thrownError) {
	            swal('Error !', 'Error !!!', 'error'); 
	        }
	    });
	}
});

function listData(startDate='',endDate='', typeReport='')
{
    var oTable = $("#tableReport").on('processing.dt', function (e, settings, processing) {
    	if (processing == true) {
            swal({
                showCancelButton: false,
                showConfirmButton: false,
                imageUrl: '<?= base_url();?>assets/img/loading.gif'
            });
    	} else {
            swal.close()
    	}
    }).DataTable({
        responsive: !0,
        paging: !0,
        bDestroy : !0,
        ajax : {
            type: 'POST',
            data: {startDate:startDate, endDate:endDate, typeReport:typeReport},
            url : '<?php echo base_url();?>report/tableReport'
        },
        serverSide: !0,
        processing: !0,
        columnDefs: [
        ],
        order: [],
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });
}

$('#formReport').on('submit',function(e){
    e.preventDefault();
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    var typeReport = $("#typeReport").val();
    if (typeReport == '') {
        swal('Error !', 'Type report cannot be null !', 'error'); 
    } else {
        listData(startDate, endDate, typeReport);
    }
});
</script>