<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
        
class Report extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata("user_id"))) {
			redirect(base_url('auth'));
		}
        date_default_timezone_set('Asia/Jakarta');
	}

    public function index()
    {
        $access_domain = $this->session->userdata['access_domain'];
        // $flight = $this->flight();

        if ($access_domain == 1)
        {
            $data = [
                'title' => 'Report',
                'type' => 1, // inbound / outbond
            ];
            $this->home_library->main('report/index', $data); // MAU
        }
        else
        {
            $data = [
                'title' => 'Report',
                'type' => 2, // RA
            ];
            $this->home_library->main('report/index-ra', $data); // RA
        }
    }

    public function getFlightByType()
    {
        $typeReport = $this->input->post('typeReport');
        // print_r($typeReport); die;
        if ($typeReport == 'INBOUND') {
           $table = 'th_inbound a';
        }
        elseif ($typeReport == 'OUTBOND') {
            $table = 'th_outbond a';
        }
        elseif ($typeReport == 'REGULATED') {
            $table = 'th_regulated a';
        }
        else {
             $table = '';
             echo json_encode(['s' => 'empty']);
             die;
        }
        $getData = "SELECT a.airline_code, a.flight_no, b.airlinesname
                    FROM $table
                    LEFT JOIN m_airlines b ON b.airlinescode = a.airline_code
                    WHERE a.airline_code = 'QG'
                    GROUP BY a.flight_no
                    ORDER BY a.flight_no ASC";
        $get = $this->db->query($getData);
        $data = $get->result();
        $result = ['s' => 'success', 'd' => $data];
        echo json_encode($result);
    }

    function tableReport()
    {
        $flight = $this->input->post('flight');
        $typeReport = $this->input->post('typeReport');

        if ($typeReport == 'INBOUND') 
        {   
            $table = "th_inbound";
        } 
        elseif ($typeReport == 'OUTBOND') 
        { 
            $table = "th_outbond";
        } 
        elseif ($typeReport == 'REGULATED') 
        { 
            $table = "th_regulated";
        } 

        $select = 'flight_no, waybill_smu, koli, netto as weight, netto, kindofgood, _created_at, "" status';
        $table = $table;
        $where = "flight_no = '$flight' AND _is_active = 1";

        $column_order = array('flight_no', 'waybill_smu', 'koli', 'netto', 'netto', 'kindofgood', '""', '""');
        $column_search =  array('flight_no', 'waybill_smu', 'koli', 'netto', 'netto', 'kindofgood', '""', '""');
        $order = 'id_ DESC';  // or setting in ajax
        $groupBy = 'waybill_smu';

        $list = $this->crud->getDatatable3('r_tracking', $select, $table, [$where => null], $column_search, $column_order, $order, [], $groupBy);
        $join = [];
        $wkwk = $this->crud->dataTableCount3('r_tracking', $table, [$where => null], $join, $groupBy);
        // print_r($list); die;
        $data = array();
        $no = $_POST['start'] + 1;
        foreach ($list as $row) 
        {
            if ($typeReport == 'INBOUND' || $typeReport == 'OUTBOND') 
            {   
                $infoLastStatus = $this->updateInfoLastStatusMau($row->waybill_smu);
            } 
            elseif ($typeReport == 'REGULATED') 
            { 
                $infoLastStatus = $this->updateInfoLastStatusRa($row->waybill_smu);
            } 
            
            $getLastStatus = explode('^^^', $infoLastStatus);

            $sub_array = array();
            $sub_array[] = $row->flight_no;
            $sub_array[] = $row->waybill_smu;
            $sub_array[] = $row->koli;
            $sub_array[] = $row->weight;
            $sub_array[] = $row->netto;
            $sub_array[] = $row->kindofgood;
            $sub_array[] = $getLastStatus[1];
            $sub_array[] = $getLastStatus[0];
            $data[] = $sub_array;
        }

        $output = array(
            'draw'               => $_POST['draw'],
            'recordsTotal'       => $wkwk,
            'recordsFiltered'    => $this->crud->dataTableFilter3('r_tracking','*', $table, [$where => null], $column_search, $column_order, $order, $join, $groupBy),
            'data'               => $data
        );
        echo json_encode($output);
    }

    public function updateInfoLastStatusMau($mawb) // untuk di header
    {
        // ------------------------------------------
        // INBOUND BAGIAN AWAL
        // ------------------------------------------
        $this->db->select('id_, gate_type');
        $this->db->where(['waybill_smu' => $mawb]);
        $getHeaderInbound = $this->db->get('th_inbound');
        $checkHeaderInbound = $getHeaderInbound->num_rows();
        $HeaderInbound = $getHeaderInbound->row_array();

        if ($HeaderInbound['gate_type'] == 'import' || $HeaderInbound['gate_type'] == 'incoming')
        {
            // td_inbound_delivery_aircarft
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query1 = $this->db->get('td_inbound_delivery_aircarft a');
            $check1 = $query1->num_rows();
            $dataArr1 = $query1->row_array();

            // td_inbound_breakdown
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query2 = $this->db->get('td_inbound_breakdown a');
            $check2 = $query2->num_rows();
            $dataArr2 = $query2->row_array();

            // td_inbound_storage
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query3 = $this->db->get('td_inbound_storage a');
            $check3 = $query3->num_rows();
            $dataArr3 = $query3->row_array();

            // td_inbound_clearance
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query4 = $this->db->get('td_inbound_clearance a');
            $check4 = $query4->num_rows();
            $dataArr4 = $query4->row_array();

            // td_inbound_pod
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query5 = $this->db->get('td_inbound_pod a');
            $check5 = $query5->num_rows();
            $dataArr5 = $query5->row_array();

            // ------------------------------------------------------------------
            
            if ($check5 > 0) {
                $statusName = 'Received by consignee';
                $dateTime = $dataArr5['status_date'] .' '. $dataArr5['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            } 
            elseif ($check4 > 0) {
                $statusName = 'Custom & quarantine clearance';
                $dateTime = $dataArr4['status_date'] .' '. $dataArr4['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check3 > 0) {
                $statusName = 'Storage';
                $dateTime = $dataArr3['status_date'] .' '. $dataArr3['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check2 > 0) {
                $statusName = 'Arrival at incoming warehouse';
                $dateTime = $dataArr2['status_date'] .' '. $dataArr2['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check1 > 0) {
                $statusName = 'Delivery from aircraft to incoming warehouse';
                $dateTime = $dataArr1['status_date'] .' '. $dataArr1['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            else
            {
                $status_last_update = '';
            }
            // Update info last status di header
            // $this->db->where('waybill_smu', $mawb);
            // $update = $this->db->update('th_inbound', ['status_last_update' => $status_last_update]); 
            $update = $info_last_status;
        }
        elseif ($HeaderInbound['gate_type'] == 'transit')
        {
            // ------------------------------------------
            // INBOUND BAGIAN TRANSIT/TRANSFER
            // ------------------------------------------
            // td_inbound_delivery_aircarft
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query1 = $this->db->get('td_inbound_delivery_aircarft a');
            $check1 = $query1->num_rows();
            $dataArr1 = $query1->row_array();

            // td_inbound_breakdown
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query2 = $this->db->get('td_inbound_breakdown a');
            $check2 = $query2->num_rows();
            $dataArr2 = $query2->row_array();

            // td_inbound_storage
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query3 = $this->db->get('td_inbound_storage a');
            $check3 = $query3->num_rows();
            $dataArr3 = $query3->row_array();

            // td_inbound_transit
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query4 = $this->db->get('td_inbound_transit a');
            $check4 = $query4->num_rows();
            $dataArr4 = $query4->row_array();

            // td_inbound_transit_buildup
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query5 = $this->db->get('td_inbound_transit_buildup a');
            $check5 = $query5->num_rows();
            $dataArr5 = $query5->row_array();

            // td_inbound_transit_delivery_staging
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query6 = $this->db->get('td_inbound_transit_delivery_staging a');
            $check6 = $query6->num_rows();
            $dataArr6 = $query6->row_array();

            // td_inbound_delivery_aircarft
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query7 = $this->db->get('td_inbound_delivery_aircarft a');
            $check7 = $query7->num_rows();
            $dataArr7 = $query7->row_array();

            // td_inbound_loading_aircarft
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_inbound b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query8 = $this->db->get('td_inbound_loading_aircarft a');
            $check8 = $query8->num_rows();
            $dataArr8 = $query8->row_array();

            // ------------------------------------------------------------------
            
            if ($check8 > 0) {
                $statusName = 'Loading to aircraft';
                $dateTime = $dataArr8['status_date'] .' '. $dataArr8['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            } 
            elseif ($check7 > 0) {
                $statusName = 'Delivery to aircraft';
                $dateTime = $dataArr7['status_date'] .' '. $dataArr7['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check6 > 0) {
                $statusName = 'Delivery to staging area';
                $dateTime = $dataArr6['status_date'] .' '. $dataArr6['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check5 > 0) {
                $statusName = 'Build Up process';
                $dateTime = $dataArr5['status_date'] .' '. $dataArr5['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check4 > 0) {
                $statusName = 'Manifesting';
                $dateTime = $dataArr4['status_date'] .' '. $dataArr4['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check3 > 0) {
                $statusName = 'Storage';
                $dateTime = $dataArr3['status_date'] .' '. $dataArr3['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check2 > 0) {
                $statusName = 'Arrival at incoming warehouse';
                $dateTime = $dataArr2['status_date'] .' '. $dataArr2['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check1 > 0) {
                $statusName = 'Delivery from aircraft to incoming warehouse';
                $dateTime = $dataArr1['status_date'] .' '. $dataArr1['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            else
            {
                $status_last_update = '';
            }
            // Update info last status di header
            // $this->db->where('waybill_smu', $mawb);
            // $update = $this->db->update('th_inbound', ['status_last_update' => $status_last_update]);
            $update = $info_last_status; 
        }

        // ------------------------------------------
        // OUTBOND
        // ------------------------------------------
        $this->db->select('id_, gate_type');
        $this->db->where(['waybill_smu' => $mawb]);
        $getHeaderOutbond = $this->db->get('th_outbond');
        $checkHeaderOutbond = $getHeaderOutbond->num_rows();
        $HeaderOutbond = $getHeaderOutbond->row_array();

        if ($checkHeaderOutbond > 0)
        {
            if ($HeaderOutbond['gate_type'] == 'outgoing' || $HeaderOutbond['gate_type'] == 'ekspor')
            {
                // td_outbond_acceptance
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query1 = $this->db->get('td_outbond_acceptance a');
                $check1 = $query1->num_rows();
                $dataArr1 = $query1->row_array();

                // td_outbond_weighing
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query2 = $this->db->get('td_outbond_weighing a');
                $check2 = $query2->num_rows();
                $dataArr2 = $query2->row_array();

                // td_outbond_manifest
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query3 = $this->db->get('td_outbond_manifest a');
                $check3 = $query3->num_rows();
                $dataArr3 = $query3->row_array();

                // td_outbond_storage
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query4 = $this->db->get('td_outbond_storage a');
                $check4 = $query4->num_rows();
                $dataArr4 = $query4->row_array();

                // td_outbond_buildup
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query5 = $this->db->get('td_outbond_buildup a');
                $check5 = $query5->num_rows();
                $dataArr5 = $query5->row_array();

                // td_outbond_delivery_staging
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query6 = $this->db->get('td_outbond_delivery_staging a');
                $check6 = $query6->num_rows();
                $dataArr6 = $query6->row_array();

                // td_outbond_delivery_aircarft
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query7 = $this->db->get('td_outbond_delivery_aircarft a');
                $check7 = $query7->num_rows();
                $dataArr7 = $query7->row_array();

                // td_outbond_loading_aircarft
                $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
                $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
                $this->db->where(['b.waybill_smu' => $mawb]);
                $this->db->order_by('a._created_at', 'DESC');
                $query8 = $this->db->get('td_outbond_loading_aircarft a');
                $check8 = $query8->num_rows();
                $dataArr8 = $query8->row_array();

                // -----------------------------------
                
                if ($check8 > 0) {
                    $statusName = 'Loading to aircraft';
                    $dateTime = $dataArr8['status_date'] .' '. $dataArr8['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                } 
                elseif ($check7 > 0) {
                    $statusName = 'Delivery to aircraft';
                    $dateTime = $dataArr7['status_date'] .' '. $dataArr7['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                }
                elseif ($check6 > 0) {
                    $statusName = 'Delivery to Staging Area';
                    $dateTime = $dataArr6['status_date'] .' '. $dataArr6['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                }
                elseif ($check5 > 0) {
                    $statusName = 'Build Up process';
                    $dateTime = $dataArr5['status_date'] .' '. $dataArr5['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                }
                elseif ($check4 > 0) {
                    $statusName = 'Storage Position';
                    $dateTime = $dataArr4['status_date'] .' '. $dataArr4['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                }
                elseif ($check3 > 0) {
                    $statusName = 'Manifesting';
                    $dateTime = $dataArr3['status_date'] .' '. $dataArr3['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                }
                elseif ($check2 > 0) {
                    $statusName = 'Weighing Scale';
                    $dateTime = $dataArr2['status_date'] .' '. $dataArr2['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                }
                elseif ($check1 > 0) {
                    $statusName = 'Acceptance confirmation';
                    $dateTime = $dataArr1['status_date'] .' '. $dataArr1['status_time'];
                    $info_last_status = $statusName .'^^^'. $dateTime;
                    $status_last_update = [
                        'status_last_update' => $info_last_status
                    ];
                }
                else
                {
                    $status_last_update = '';
                }
                // Update info last status di header
                // $this->db->where('waybill_smu', $mawb);
                // $update = $this->db->update('th_outbond', ['status_last_update' => $status_last_update]); 
                $update = $info_last_status;
            }
        }
        return $update;
    }

    public function updateInfoLastStatusRa($mawb) // untuk di header
    {  
        // ------------------------------------------
        // REGULATED
        // ------------------------------------------
        $this->db->select('id_, gate_type');
        $this->db->where(['waybill_smu' => $mawb]);
        $getHeaderRegulated = $this->db->get('th_regulated');
        $checkHeaderRegulated = $getHeaderRegulated->num_rows();
        $HeaderRegulated = $getHeaderRegulated->row_array();

        if ($checkHeaderRegulated > 0)
        {
            // td_regulated_acceptance
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query1 = $this->db->get('td_regulated_acceptance a');
            $check1 = $query1->num_rows();
            $dataArr1 = $query1->row_array();

            // td_regulated_screening
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query2 = $this->db->get('td_regulated_screening a');
            $check2 = $query2->num_rows();
            $dataArr2 = $query2->row_array();

            // td_regulated_weighing
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query3 = $this->db->get('td_regulated_weighing a');
            $check3 = $query3->num_rows();
            $dataArr3 = $query3->row_array();

            // td_regulated_rejected
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query4 = $this->db->get('td_regulated_rejected a');
            $check4 = $query4->num_rows();
            $dataArr4 = $query4->row_array();

            // td_regulated_storage
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query5 = $this->db->get('td_regulated_storage a');
            $check5 = $query5->num_rows();
            $dataArr5 = $query5->row_array();

            // td_regulated_csd
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query6 = $this->db->get('td_regulated_csd a');
            $check6 = $query6->num_rows();
            $dataArr6 = $query6->row_array();

            // td_regulated_loading
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query7 = $this->db->get('td_regulated_loading a');
            $check7 = $query7->num_rows();
            $dataArr7 = $query7->row_array();

            // td_regulated_transport
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_outbond b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query8 = $this->db->get('td_regulated_transport a');
            $check8 = $query8->num_rows();
            $dataArr8 = $query8->row_array();

            // td_regulated_handover
            $this->db->select('a.*, b.waybill_smu, b.shipper_name, b.consignee_name');
            $this->db->join('th_regulated b', 'b.id_ = a.id_header', 'LEFT');
            $this->db->where(['b.waybill_smu' => $mawb]);
            $this->db->order_by('a._created_at', 'DESC');
            $query9 = $this->db->get('td_regulated_handover a');
            $check9 = $query9->num_rows();
            $dataArr9 = $query9->row_array();

            // -----------------------------------
            
            if ($check9 > 0) {
                $statusName = 'Hand-over to airline';
                $dateTime = $dataArr9['status_date'] .' '. $dataArr9['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            } 
            elseif ($check8 > 0) {
                $statusName = 'Transportation to warehouse lini-1';
                $dateTime = $dataArr8['status_date'] .' '. $dataArr8['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            } 
            elseif ($check7 > 0) {
                $statusName = 'Loading process to vehicle';
                $dateTime = $dataArr7['status_date'] .' '. $dataArr7['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check6 > 0) {
                $statusName = 'Consignment security declaration (CSD) issuance';
                $dateTime = $dataArr6['status_date'] .' '. $dataArr6['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check5 > 0) {
                $statusName = 'RA storage position';
                $dateTime = $dataArr5['status_date'] .' '. $dataArr5['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check4 > 0) {
                $statusName = 'Updated rejected item';
                $dateTime = $dataArr4['status_date'] .' '. $dataArr4['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check3 > 0) {
                $statusName = 'Digital weighing scale';
                $dateTime = $dataArr3['status_date'] .' '. $dataArr3['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check2 > 0) {
                $statusName = 'Security screening process';
                $dateTime = $dataArr2['status_date'] .' '. $dataArr2['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            elseif ($check1 > 0) {
                $statusName = 'Acceptance confirmation (before security screening)';
                $dateTime = $dataArr1['status_date'] .' '. $dataArr1['status_time'];
                $info_last_status = $statusName .'^^^'. $dateTime;
                $status_last_update = [
                    'status_last_update' => $info_last_status
                ];
            }
            else
            {
                $status_last_update = '';
            }
            // Update info last status di header
            // $this->db->where('waybill_smu', $mawb);
            // $update = $this->db->update('th_regulated', ['status_last_update' => $status_last_update]); 
            $update = $info_last_status;
            
        }
        return $update;
    }
}
