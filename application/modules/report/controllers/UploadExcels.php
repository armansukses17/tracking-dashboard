<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
        
class UploadExcels extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
	}

    public function index()
    {
        echo 'Testing !!!!';
    }

    function loop_extractEcxelMau()
    {
        $time_start = microtime(true);
        set_time_limit(60);
        error_reporting('E_NONE');
        $file = fopen("loop_extractExcelMau.txt", "w+");
        if (flock($file, LOCK_EX)) {
            fwrite($file, "Write something too");
            for ($i = 0; $i < 100; $i++) {
                echo "loop ke " . $i . " ";
                $this->extractExcelMau();
                sleep(1);
                $time_end = microtime(true);
                $time = $time_end - $time_start;
                if ($time >= 60) {
                    exit();
                }
            }
            flock($file, LOCK_UN);
        } else {
            echo "Error locking file!";
        }
        fclose($file);
    }

    function loop_extractEcxelRa()
    {
        $time_start = microtime(true);
        set_time_limit(60);
        error_reporting('E_NONE');
        $file = fopen("loop_extractExcelRa.txt", "w+");
        if (flock($file, LOCK_EX)) {
            fwrite($file, "Write something too");
            for ($i = 0; $i < 100; $i++) {
                echo "loop ke " . $i . " ";
                $this->extractExcelRa();
                sleep(1);
                $time_end = microtime(true);
                $time = $time_end - $time_start;
                if ($time >= 60) {
                    exit();
                }
            }
            flock($file, LOCK_UN);
        } else {
            echo "Error locking file!";
        }
        fclose($file);
    }

    // ##############################################################################################################

    public function typeLogin()
    {
        $access_domain = $this->session->userdata['access_domain'];
        if ($access_domain == 1) {
            $type = 'mau';
        } 
        elseif ($access_domain == 2) {
            $type = 'ra';
        } else {
            $type = '';
        }
        return $type;
    }

    public function uploadFile()
    {
        $type = $this->typeLogin();
        // -------------------------------------------------
        $name_file      = trim($_FILES['fileExcel']['name']);
        $type_file      = $_FILES['fileExcel']['type'];
        $tmp_name       = $_FILES['fileExcel']['tmp_name'];
        $files          = $_FILES['fileExcel'];
        $file_path      = 'file';
        $dateTime       = date('YmdHis');
        // create new file name -----------
        $arr_file = explode('.', $name_file);
        $extension = end($arr_file);
        $getLongString = strlen(".$extension");
        $getNameOri = substr($name_file, 0, "-$getLongString");
        $createNewFileName = "$getNameOri-$dateTime.$extension";
        // print_r($createNewFileName); die;
        // -----------
        $file_ext = pathinfo($_FILES['fileExcel']['name'], PATHINFO_EXTENSION);
        if ($file_ext == 'xlsx' || $file_ext == 'xls') 
        {
            if (move_uploaded_file($tmp_name, "$file_path/$createNewFileName")) 
            {
                $checkLastKey = "SELECT MAX(key_upload) AS lastKey FROM status_upload";
                $queryNumb = $this->db->query($checkLastKey);
                $getLastKey = $queryNumb->row_array();
                $lastKey = (int)$getLastKey['lastKey'];
                $lastKey++;

                $data_insert = [
                    'type' => $type,
                    'file_name' => $createNewFileName,
                    'file_path' => "$file_path/$createNewFileName",
                    'cron_status' => 0, // pertama upload statu 0
                    'user' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'key_upload' => $lastKey
                ];
                $this->db->insert('status_upload', $data_insert);

                $return = array('s' =>'success', 'm' => 'Your file was uploaded successfully. Please wait for a while until the data storage process is complete.');
            }
            else
            {
                $return = array('s' =>'fail', 'm' => 'Upload File Error. Please Contact Admin');
            }
        }
        else
        {
            $return = array('s' =>'fail', 'm' => 'File upload must xlsx or xls');
        }
        echo json_encode($return);
    }

    public function extractExcelMau()
    {
        $this->db->select('a.*, b.role_id, b.access_domain, b.is_active ');
        $this->db->join('user b', 'b.id = a.user');
        $this->db->where(['a.cron_status' => 0, 'b.role_id' => '1', 'b.access_domain' => '1', 'b.is_active' => '1']); // operational MAU
        // $this->db->where_in('user', [9, 15]); // user access MAU
        $this->db->order_by('a.id', 'DESC');
        $sql = $this->db->get('status_upload a');
        $getData = $sql->row_array();
        $checkData = $sql->num_rows();
        // Get extention
        $fileName = $getData['file_name'];
        $arr_file = explode('.', $fileName);
        $extension = end($arr_file);
        // check extentioin
        if ($extension == 'xlsx')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }
        elseif ($extension == 'xls')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        }

        if ($checkData > 0) 
        {
            $spreadsheet = $reader->load($getData['file_path']);
            $sheetCount = $spreadsheet->getSheetCount();

            if ($sheetCount != 12) // jumlah sheet ad 12
            {
                $updateIfFailed = [
                    'cron_status '=> 9, 
                    'updated_at' => date('Y-m-d H:i:s'),
                    'file_name' => $fileName,
                    'file_path' => 'File has been removed !'
                ];
                // update status cron jk gagal upload status = 9
                $this->db->where(['cron_status' => 0, 'file_name' => $fileName]);
                $this->db->update('status_upload', $updateIfFailed); // failled

                if (file_exists('./file/'.$fileName) AND (!empty($fileName))) { // check if file exists and available in database
                    $moveFileTo = '../tracking-trash-file/';
                    if (copy('./file/'.$fileName, $moveFileTo.$fileName)) // move file to another directory
                    {
                        unlink('./file/'.$fileName); // then, remove file in directory
                    }
                }
                echo 'The number of sheets does not match !!! ';
                die;
            }
            else
            {
                // #################################################################
                $this->db->trans_begin(); // start
                // #################################################################
                // GET LAST KEY UPLOAD
                $checkLastKey = "SELECT MAX(key_upload) AS lastKey FROM status_upload";
                $queryNumb = $this->db->query($checkLastKey);
                $getLastKey = $queryNumb->row_array();
                $lastKey = (int)$getLastKey['lastKey'];

                // -------------------------------------------------------------
                // INBOUND
                // -------------------------------------------------------------
                $sheetInbound = $spreadsheet->getSheetByName('th_inbound')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataInbound1 = [];
                $countRecord = [];
                foreach ($sheetInbound as $row) 
                {
                    if ($numrow > 1) 
                    {
                        $dataInbound1 = array(
                            // 'id_' => $row['A'],
                            'gate_type' => $row['B'],
                            'waybill_smu' => $row['C'],
                            'hawb' => $row['D'],
                            'koli' => $row['E'],
                            'netto' => $row['F'],
                            'volume' => $row['G'],
                            'kindofgood' => $row['H'],
                            'airline_code' => $row['I'],
                            'flight_no' => $row['J'],
                            'origin' => $row['K'],
                            'transit' => $row['L'],
                            'dest' => $row['M'],
                            'shipper_name' => $row['N'],
                            'consignee_name' => $row['O'],
                            '_is_active' => $row['P'],
                            'key_upload' => $lastKey
                        );

                        if (!empty($row['B']) || !empty($row['C'])) {
                            $this->db->insert('th_inbound', $dataInbound1);
                            // get coun all
                            array_push($countRecord, array(
                                'gate_type' => $row['B'],
                                'waybill_smu' => $row['C'],
                            ));
                            $insertData = TRUE;
                            $msg = "insert th_inbound successfully \n";
                        }
                        else 
                        {
                            $insertData = FALSE;
                            $msg = "Could not save the data INBOUND ! \n";
                        }
                    } 
                    $numrow++;
                }

                echo $msg;
                echo "<br>\n";
                // *****************************************

                if ($insertData == TRUE) {
                    // $count = count($dataInbound1);
                    // $th_inbound = $this->db->insert_batch('th_inbound', $dataInbound1);
                    $LastIdIn = $this->db->insert_id(); 

                    $startInsertIDIn2 = ($LastIdIn - count($countRecord)) + 1;
                    $startInsertIDIn3 = ($LastIdIn - count($countRecord)) + 1;
                    $startInsertIDIn4 = ($LastIdIn - count($countRecord)) + 1;
                    $startInsertIDIn5 = ($LastIdIn - count($countRecord)) + 1;
                    $startInsertIDIn6 = ($LastIdIn - count($countRecord)) + 1;
                    
                    // $firstID = ($startInsertIDIn2 - count($countRecord)) + 1;
                    // $getAll = [$startInsertIDIn2, $startInsertIDIn3, $startInsertIDIn4, $startInsertIDIn5, $startInsertIDIn6];
                    // print_r($getAll); die;

                    $sheetInbound2 = $spreadsheet->getSheetByName('td_inbound_delivery')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound2 = [];
                    foreach ($sheetInbound2 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound2, array(
                                'id_header' => $startInsertIDIn2++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    // print_r($dataInbound2[0]['status_time']); die;
                    if ($dataInbound2[0]['status_date'] != '' || $dataInbound2[0]['status_time'] != '' || $dataInbound2[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_inbound_delivery', $dataInbound2);
                        $msg = "insert td_inbound_delivery successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_inbound_delivery ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetInbound3 = $spreadsheet->getSheetByName('td_inbound_breakdown')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound3 = [];
                    foreach ($sheetInbound3 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound3, array(
                                'id_header' => $startInsertIDIn3++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataInbound3[0]['status_date'] != '' || $dataInbound3[0]['status_time'] != '' || $dataInbound3[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_inbound_breakdown', $dataInbound3);
                        $msg = "insert td_inbound_breakdown successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_inbound_breakdown ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------    
                    $sheetInbound4 = $spreadsheet->getSheetByName('td_inbound_storage')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound4 = [];
                    foreach ($sheetInbound4 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound4, array(
                                'id_header' => $startInsertIDIn4++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataInbound4[0]['status_date'] != '' || $dataInbound4[0]['status_time'] != '' || $dataInbound4[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_inbound_storage', $dataInbound4);
                        $msg = "insert td_inbound_storage successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_inbound_storage ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------  
                    $sheetInbound5 = $spreadsheet->getSheetByName('td_inbound_clearance')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound5 = [];
                    foreach ($sheetInbound5 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound5, array(
                                'id_header' => $startInsertIDIn5++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataInbound5[0]['status_date'] != '' || $dataInbound5[0]['status_time'] != '' || $dataInbound5[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_inbound_clearance', $dataInbound5);
                        $msg = "insert td_inbound_clearance successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_inbound_clearance ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetInbound6 = $spreadsheet->getSheetByName('td_inbound_pod')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataInbound6 = [];
                    foreach ($sheetInbound6 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataInbound6, array(
                                'id_header' => $startInsertIDIn6++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataInbound6[0]['status_date'] != '' || $dataInbound6[0]['status_time'] != '' || $dataInbound6[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_inbound_pod', $dataInbound6);  
                        $msg = "insert td_inbound_pod successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_inbound_pod ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                } // END: check if insert header success\
                // #############################################################
                // -------------------------------------------------------------
                // OUTBOND
                // -------------------------------------------------------------
                $sheetOutbond = $spreadsheet->getSheetByName('th_outbond')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataOutbond1 = [];
                $countRecord = [];
                foreach ($sheetOutbond as $row) 
                {
                    if ($numrow > 1) 
                    {
                        $dataOutbond1 = array(
                            // 'id_' => $row['A'],
                            'gate_type' => $row['B'],
                            'waybill_smu' => $row['C'],
                            'hawb' => $row['D'],
                            'koli' => $row['E'],
                            'netto' => $row['F'],
                            'volume' => $row['G'],
                            'kindofgood' => $row['H'],
                            'airline_code' => $row['I'],
                            'flight_no' => $row['J'],
                            'origin' => $row['K'],
                            'transit' => $row['L'],
                            'dest' => $row['M'],
                            'shipper_name' => $row['N'],
                            'consignee_name' => $row['O'],
                            '_is_active' => $row['P'],
                            'key_upload' => $lastKey
                        );

                        if (!empty($row['B']) || !empty($row['C'])) {
                            $this->db->insert('th_outbond', $dataOutbond1);
                            // get coun all
                            array_push($countRecord, array(
                                'gate_type' => $row['B'],
                                'waybill_smu' => $row['C'],
                            ));
                            $insertData2 = TRUE;
                            $msg2 = "insert th_outbond successfully \n";
                        }
                        else 
                        {
                            $insertData2 = FALSE;
                            $msg2 = "Could not save the data OUTBOND ! \n";
                        }
                    }
                    $numrow++;
                }

                echo $msg2;
                echo "<br>\n";
                // *****************************************

                if ($insertData2 == TRUE)
                {
                    // $th_outbond = $this->db->insert_batch('th_outbond', $dataOutbond1);
                    $LastIdOut = $this->db->insert_id(); 

                    $startInsertIDOut2 = ($LastIdOut - count($countRecord)) + 1;
                    $startInsertIDOut3 = ($LastIdOut - count($countRecord)) + 1;
                    $startInsertIDOut4 = ($LastIdOut - count($countRecord)) + 1;
                    $startInsertIDOut5 = ($LastIdOut - count($countRecord)) + 1;
                    $startInsertIDOut6 = ($LastIdOut - count($countRecord)) + 1;

                    $sheetOutbond2 = $spreadsheet->getSheetByName('td_outbond_acceptance')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond2 = [];
                    foreach ($sheetOutbond2 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond2, array(
                                'id_header' => $startInsertIDOut2++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataOutbond2[0]['status_date'] != '' || $dataOutbond2[0]['status_time'] != '' || $dataOutbond2[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_outbond_acceptance', $dataOutbond2);
                        $msg2 = "insert td_outbond_acceptance successfully \n";
                    }
                    else
                    {
                        $msg2 = "Could not save the data td_outbond_acceptance ! \n";
                    }
                    echo $msg2;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetOutbond3 = $spreadsheet->getSheetByName('td_outbond_weighing')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond3 = [];
                    foreach ($sheetOutbond3 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond3, array(
                                'id_header' => $startInsertIDOut3++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataOutbond3[0]['status_date'] != '' || $dataOutbond3[0]['status_time'] != '' || $dataOutbond3[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_outbond_weighing', $dataOutbond3);
                        $msg2 = "insert td_outbond_weighing successfully \n";
                    }
                    else
                    {
                        $msg2 = "Could not save the data td_outbond_weighing ! \n";
                    }
                    echo $msg2;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetOutbond4 = $spreadsheet->getSheetByName('td_outbond_manifest')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond4 = [];
                    foreach ($sheetOutbond4 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond4, array(
                                'id_header' => $startInsertIDOut4++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataOutbond4[0]['status_date'] != '' || $dataOutbond4[0]['status_time'] != '' || $dataOutbond4[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_outbond_manifest', $dataOutbond4);
                        $msg2 = "insert td_outbond_manifest successfully \n";
                    }
                    else
                    {
                        $msg2 = "Could not save the data td_outbond_manifest ! \n";
                    }
                    echo $msg2;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetOutbond5 = $spreadsheet->getSheetByName('td_outbond_storage')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond5 = [];
                    foreach ($sheetOutbond5 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond5, array(
                                'id_header' => $startInsertIDOut5++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataOutbond5[0]['status_date'] != '' || $dataOutbond5[0]['status_time'] != '' || $dataOutbond5[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_outbond_storage', $dataOutbond5);
                        $msg2 = "insert td_outbond_storage successfully \n";
                    }
                    else
                    {
                        $msg2 = "Could not save the data td_outbond_storage ! \n";
                    }
                    echo $msg2;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetOutbond6 = $spreadsheet->getSheetByName('td_outbond_buildup')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataOutbond6 = [];
                    foreach ($sheetOutbond6 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataOutbond6, array(
                                'id_header' => $startInsertIDOut6++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataOutbond6[0]['status_date'] != '' || $dataOutbond6[0]['status_time'] != '' || $dataOutbond6[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_outbond_buildup', $dataOutbond6);
                        $msg2 = "insert td_outbond_buildup successfully \n";
                    }
                    else
                    {
                        $msg2 = "Could not save the data td_outbond_buildup ! \n";
                    }
                    echo $msg2;
                    echo "<br>\n";
                }
                // -------------------------------------------------------------
                // #################################################################
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback(); // balikin jk gagal
                    $this->db->where(['cron_status' => 0, 'file_name' => $fileName]);
                    $this->db->update('status_upload', ['cron_status '=> 11, 'updated_at' => date('Y-m-d H:i:s')]);
                    echo 'Import data failed !!!.';
                    die;
                }
                else
                {
                    // update alert status cron jk success
                    $this->db->where(['cron_status' => 0, 'file_name' => $fileName]);
                    $this->db->update('status_upload', ['cron_status '=> 1, 'updated_at' => date('Y-m-d H:i:s')]); // data sudah ditransfer
                    $this->db->trans_commit(); // finish
                    echo 'Import data successfully.';
                    die;
                }
            } // END
        } 
        else 
        {
            echo "No data ready to extract !";
            die;
        }
    }

    public function extractExcelRa()
    {
        $this->db->select('a.*, b.role_id, b.access_domain, b.is_active ');
        $this->db->join('user b', 'b.id = a.user');
        $this->db->where(['a.cron_status' => 0, 'b.role_id' => '1', 'b.access_domain' => '2', 'b.is_active' => '1']); // operational RA 
        // $this->db->where_in('user', [18, 19]); // user access RA
        $this->db->order_by('a.id', 'DESC');
        $sql = $this->db->get('status_upload a');
        $getData = $sql->row_array();
        $checkData = $sql->num_rows();
        // Get extention
        $fileName = $getData['file_name'];
        $arr_file = explode('.', $fileName);
        $extension = end($arr_file);
        // check extentioin
        if ($extension == 'xlsx')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }
        elseif ($extension == 'xls')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        }

        if ($checkData > 0) 
        {
            $spreadsheet = $reader->load($getData['file_path']);
            $sheetCount = $spreadsheet->getSheetCount();

            if ($sheetCount != 10) // jumlah sheet ad 10
            {
                $updateIfFailed = [
                    'cron_status '=> 9, 
                    'updated_at' => date('Y-m-d H:i:s'),
                    'file_name' => $fileName,
                    'file_path' => 'File has been removed !'
                ];
                // update status cron jk gagal upload status = 9
                $this->db->where(['cron_status' => 0, 'file_name' => $fileName]);
                $this->db->update('status_upload', $updateIfFailed); // failled

                if (file_exists('./file/'.$fileName) AND (!empty($fileName))) { // check if file exists and available in database
                    $moveFileTo = '../tracking-trash-file/';
                    if (copy('./file/'.$fileName, $moveFileTo.$fileName)) // move file to another directory
                    {
                        unlink('./file/'.$fileName); // then, remove file in directory
                    }
                }
                echo 'The number of sheets does not match !!! ';
                die;
            }
            else
            {
                // #################################################################
                $this->db->trans_begin(); // start
                // #################################################################
                // GET LAST KEY UPLOAD
                $checkLastKey = "SELECT MAX(key_upload) AS lastKey FROM status_upload";
                $queryNumb = $this->db->query($checkLastKey);
                $getLastKey = $queryNumb->row_array();
                $lastKey = (int)$getLastKey['lastKey'];
               // -------------------------------------------------------------
                // RA
                // -------------------------------------------------------------
                $sheetRa = $spreadsheet->getSheetByName('th_regulated')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa1 = [];
                $countRecord = [];
                foreach ($sheetRa as $row) 
                {
                    if ($numrow > 1) 
                    {
                        $dataRa1 = array(
                            // 'id_' => $row['A'],
                            'gate_type' => $row['B'],
                            'waybill_smu' => $row['C'],
                            'hawb' => $row['D'],
                            'koli' => $row['E'],
                            'netto' => $row['F'],
                            'volume' => $row['G'],
                            'kindofgood' => $row['H'],
                            'airline_code' => $row['I'],
                            'flight_no' => $row['J'],
                            'origin' => $row['K'],
                            'transit' => $row['L'],
                            'dest' => $row['M'],
                            'shipper_name' => $row['N'],
                            'consignee_name' => $row['O'],
                            '_is_active' => $row['P'],
                            'key_upload' => $lastKey
                        );

                        if (!empty($row['B']) || !empty($row['C'])) {
                            $this->db->insert('th_regulated', $dataRa1);
                            // get coun all
                            array_push($countRecord, array(
                                'gate_type' => $row['B'],
                                'waybill_smu' => $row['C'],
                            ));
                            $insertData = TRUE;
                            $msg = "insert th_regulated successfully \n";
                        }
                        else 
                        {
                            $insertData = FALSE;
                            $msg = "Could not save the data REGULATED AGENT ! \n";
                        }
                    }
                    $numrow++;
                }

                echo $msg;
                echo "<br>\n";
                // *****************************************

                if ($insertData == TRUE) {
                    // $th_regulated = $this->db->insert_batch('th_regulated', $dataRa1);
                    $LastIdRa = $this->db->insert_id(); 

                    $startInsertID2 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID3 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID4 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID5 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID6 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID7 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID8 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID9 = ($LastIdRa - count($countRecord)) + 1;
                    $startInsertID10 = ($LastIdRa - count($countRecord)) + 1;

                    // if ($th_regulated)
                    // {
                    $sheetRa2 = $spreadsheet->getSheetByName('td_regulated_acceptance')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa2 = [];
                    foreach ($sheetRa2 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa2, array(
                                'id_header' => $startInsertID2++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa2[0]['status_date'] != '' || $dataRa2[0]['status_time'] != '' || $dataRa2[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_acceptance', $dataRa2);
                        $msg = "insert td_regulated_acceptance successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_acceptance ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa3 = $spreadsheet->getSheetByName('td_regulated_screening')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa3 = [];
                    foreach ($sheetRa3 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa3, array(
                                'id_header' => $startInsertID3++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa3[0]['status_date'] != '' || $dataRa3[0]['status_time'] != '' || $dataRa3[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_screening', $dataRa3);
                        $msg = "insert td_regulated_screening successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_screening ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa4 = $spreadsheet->getSheetByName('td_regulated_weighing')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa4 = [];
                    foreach ($sheetRa4 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa4, array(
                                'id_header' => $startInsertID4++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa4[0]['status_date'] != '' || $dataRa4[0]['status_time'] != '' || $dataRa4[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_weighing', $dataRa4);
                        $msg = "insert td_regulated_weighing successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_weighing ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa5 = $spreadsheet->getSheetByName('td_regulated_rejected')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa5 = [];
                    foreach ($sheetRa5 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa5, array(
                                'id_header' => $startInsertID5++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa5[0]['status_date'] != '' || $dataRa5[0]['status_time'] != '' || $dataRa5[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_rejected', $dataRa5);
                        $msg = "insert td_regulated_rejected successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_rejected ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa6 = $spreadsheet->getSheetByName('td_regulated_storage')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa6 = [];
                    foreach ($sheetRa6 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa6, array(
                                'id_header' => $startInsertID6++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa6[0]['status_date'] != '' || $dataRa6[0]['status_time'] != '' || $dataRa6[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_storage', $dataRa6);
                        $msg = "insert td_regulated_storage successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_storage ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa7 = $spreadsheet->getSheetByName('td_regulated_csd')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa7 = [];
                    foreach ($sheetRa7 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa7, array(
                                'id_header' => $startInsertID7++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa7[0]['status_date'] != '' || $dataRa7[0]['status_time'] != '' || $dataRa7[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_csd', $dataRa7);
                        $msg = "insert td_regulated_csd successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_csd ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa8 = $spreadsheet->getSheetByName('td_regulated_loading')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa8 = [];
                    foreach ($sheetRa8 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa8, array(
                                'id_header' => $startInsertID8++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa8[0]['status_date'] != '' || $dataRa8[0]['status_time'] != '' || $dataRa8[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_loading', $dataRa8);
                        $msg = "insert td_regulated_loading successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_loading ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa9 = $spreadsheet->getSheetByName('td_regulated_transport')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa9 = [];
                    foreach ($sheetRa9 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa9, array(
                                'id_header' => $startInsertID9++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa9[0]['status_date'] != '' || $dataRa9[0]['status_time'] != '' || $dataRa9[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_transport', $dataRa9);
                        $msg = "insert td_regulated_transport successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_transport ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                    // -------------------------------------------------------------
                    $sheetRa10 = $spreadsheet->getSheetByName('td_regulated_handover')->toArray(null, true, true ,true);
                    $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                    $dataRa10 = [];
                    foreach ($sheetRa10 as $row) 
                    {
                        if ($numrow > 1) 
                        {
                            array_push($dataRa10, array(
                                'id_header' => $startInsertID10++,
                                'status_date' => $row['B'],
                                'status_time' => $row['C'],
                                '_is_active' => $row['D']
                            ));
                        }
                        $numrow++;
                    }
                    if ($dataRa10[0]['status_date'] != '' || $dataRa10[0]['status_time'] != '' || $dataRa10[0]['_is_active'] != '')
                    {
                        $this->db->insert_batch('td_regulated_handover', $dataRa10);
                        $msg = "insert td_regulated_handover successfully \n";
                    }
                    else
                    {
                        $msg = "Could not save the data td_regulated_handover ! \n";
                    }
                    echo $msg;
                    echo "<br>\n";
                } // END: check if header success insert
                // -------------------------------------------------------------
                // #################################################################
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback(); // balikin jk gagal
                    $this->db->where(['cron_status' => 0, 'file_name' => $fileName]);
                    $this->db->update('status_upload', ['cron_status '=> 11, 'updated_at' => date('Y-m-d H:i:s')]);
                    echo 'Import data failed !!!.';
                    die;
                }
                else
                {
                    // update alert status cron jk success
                    $this->db->where(['cron_status' => 0, 'file_name' => $fileName]);
                    $this->db->update('status_upload', ['cron_status '=> 1, 'updated_at' => date('Y-m-d H:i:s')]); // data sudah ditransfer
                    $this->db->trans_commit(); // finish
                    echo 'Import data successfully.';
                    die;
                }
            } // END
        } 
        else 
        {
            echo "No data ready to extract !";
            die;
        }
    }

    function statusFile()
    {
        $type = $this->typeLogin();
        // --------------------------------------------------------
        $this->db->select('*');
        $this->db->where(['type' => $type, 'created_at LIKE "'.date('Y-m-d').'%"' => NULL]);
        $this->db->order_by('id', 'DESC');
        $sql = $this->db->get('status_upload');
        $getData = $sql->row_array();
        $checkData = $sql->num_rows();

        if ($checkData > 0) 
        {
            if ($getData['cron_status'] == 1) 
            {
                $respon = ['s' => 'success', 'd' => $getData];
            } 
            elseif ($getData['cron_status'] == 9) 
            {
                $respon = ['s' => 'fail', 'm' => 'Error, The number of sheets does not match the format. Please try again !'];
            } 
            elseif ($getData['cron_status'] == 11) 
            {
                $respon = ['s' => 'fail', 'm' => 'Failed to import data!, please check your data !'];
            } 
            else 
            {
                $respon = ['s' => 'no', 'm' => '-'];
            }
        } 
        else 
        {
            $respon = ['s' => 'notFound', 'm' => 'No new data yet!'];
        }
        echo json_encode($respon);
    }

    public function updateStatusCron()
    {
        $updateCron = $this->input->post('updateCron');
        if ($updateCron == 'updateCron')
        {
            // Update status cron semua yg bernilai 1 / telah success ke 2
            $this->db->select('*');
            $this->db->where(['cron_status ' => 1]);
            $sql = $this->db->get('status_upload');
            $getData = $sql->result_array();
            $checkData = $sql->num_rows();

            if ($checkData > 0) 
            {
                foreach ($getData as $row) 
                {
                    $this->db->where(['cron_status' => 1]);
                    $this->db->update('status_upload', ['cron_status '=> 2]); // complete
                }
                $return = ['s' => 'success', 'm' => 'ok !'];
            }
            else
            {
                $return = ':)'; // kosongkan
            }
        } 
        else
        {
            $return = ['s' => 'fail', 'm' => 'Failled !'];
        }
        echo json_encode($return);
    }
}
