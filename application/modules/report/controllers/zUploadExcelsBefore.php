<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
        
class UploadExcels extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
	}

    public function index()
    {
        echo 'Testing !!!!';
    }

    function loop_extractEcxelMau()
    {
        $time_start = microtime(true);
        set_time_limit(60);
        error_reporting('E_NONE');
        $file = fopen("loop_extractExcelMau.txt", "w+");
        if (flock($file, LOCK_EX)) {
            fwrite($file, "Write something too");
            for ($i = 0; $i < 100; $i++) {
                echo "loop ke " . $i . " ";
                $this->extractExcelMau();
                sleep(1);
                $time_end = microtime(true);
                $time = $time_end - $time_start;
                if ($time >= 60) {
                    exit();
                }
            }
            flock($file, LOCK_UN);
        } else {
            echo "Error locking file!";
        }
        fclose($file);
    }

    function loop_extractEcxelRa()
    {
        $time_start = microtime(true);
        set_time_limit(60);
        error_reporting('E_NONE');
        $file = fopen("loop_extractExcelRa.txt", "w+");
        if (flock($file, LOCK_EX)) {
            fwrite($file, "Write something too");
            for ($i = 0; $i < 100; $i++) {
                echo "loop ke " . $i . " ";
                $this->extractExcelRa();
                sleep(1);
                $time_end = microtime(true);
                $time = $time_end - $time_start;
                if ($time >= 60) {
                    exit();
                }
            }
            flock($file, LOCK_UN);
        } else {
            echo "Error locking file!";
        }
        fclose($file);
    }

    // ##############################################################################################################

    public function uploadFile()
    {
        $name_file      = trim($_FILES['fileExcel']['name']);
        $type_file      = $_FILES['fileExcel']['type'];
        $tmp_name       = $_FILES['fileExcel']['tmp_name'];
        $files          = $_FILES['fileExcel'];
        $file_path      = 'file';
        $dateTime       = date('YmdHis');
        // create new file name -----------
        $arr_file = explode('.', $name_file);
        $extension = end($arr_file);
        $getLongString = strlen(".$extension");
        $getNameOri = substr($name_file, 0, "-$getLongString");
        $createNewFileName = "$getNameOri-$dateTime.$extension";
        // print_r($createNewFileName); die;
        // -----------
        $file_ext = pathinfo($_FILES['fileExcel']['name'], PATHINFO_EXTENSION);
        if ($file_ext == 'xlsx' || $file_ext == 'xls') 
        {
            if (move_uploaded_file($tmp_name, "$file_path/$createNewFileName")) 
            {
                $data_insert = [
                    'file_name' => $createNewFileName,
                    'file_path' => "$file_path/$createNewFileName",
                    'cron_status' => 0, 
                    'user' => $this->session->userdata('user_id'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
                $this->db->insert('status_upload', $data_insert);

                $return = array('s' =>'success', 'm' => 'Your file was uploaded successfully. Please wait for a while until the data storage process is complete.');
            }
            else
            {
                $return = array('s' =>'fail', 'm' => 'Upload File Error. Please Contact Admin');
            }
        }
        else
        {
            $return = array('s' =>'fail', 'm' => 'File upload must xlsx or xls');
        }
        echo json_encode($return);
    }

    public function extractExcelMau()
    {
        $this->db->select('*');
        $this->db->where(['cron_status' => 0]);
        $sql = $this->db->get('status_upload');
        $getData = $sql->row_array();
        $checkData = $sql->num_rows();
        // Get extention
        $fileName = $getData['file_name'];
        $arr_file = explode('.', $fileName);
        $extension = end($arr_file);
        // check extentioin
        if ($extension == 'xlsx')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }
        elseif ($extension == 'xls')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        }

        if ($checkData > 0) 
        {
            $spreadsheet = $reader->load($getData['file_path']);
            $sheetCount = $spreadsheet->getSheetCount();

            if ($sheetCount != 12) // jumlah sheet ad 12
            {
                $return = ['s' => 'fail', 'm' => 'Incorrect data format !'];
                file_exists($getData['file_path']) ? unlink($getData['file_path']) : '';

                // delete data di db
                $this->db->where(['id' => $getData['id']]);
                $this->db->delete('status_upload');

                // $return = ['s' => 'fail', 'm' => 'The number of sheets does not match !'];
                echo 'The number of sheets does not match !';
            }
            else
            {
                // -------------------------------------------------------------
                // INBOUND
                // -------------------------------------------------------------
                $sheetInbound = $spreadsheet->getSheetByName('th_inbound')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataInbound1 = [];

                foreach ($sheetInbound as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataInbound1, array(
                            'id_' => $row['A'],
                            'gate_type' => $row['B'],
                            'waybill_smu' => $row['C'],
                            'hawb' => $row['D'],
                            'koli' => $row['E'],
                            'netto' => $row['F'],
                            'volume' => $row['G'],
                            'kindofgood' => $row['H'],
                            'airline_code' => $row['I'],
                            'flight_no' => $row['J'],
                            'origin' => $row['K'],
                            'transit' => $row['L'],
                            'dest' => $row['M'],
                            'shipper_name' => $row['N'],
                            'consignee_name' => $row['O'],
                            '_is_active' => $row['P']
                        ));
                    }
                    echo "loop th_inbound data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetInbound2 = $spreadsheet->getSheetByName('td_inbound_delivery')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataInbound2 = [];

                foreach ($sheetInbound2 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataInbound2, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_inbound_delivery data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetInbound3 = $spreadsheet->getSheetByName('td_inbound_breakdown')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataInbound3 = [];

                foreach ($sheetInbound3 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataInbound3, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_inbound_breakdown data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetInbound4 = $spreadsheet->getSheetByName('td_inbound_storage')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataInbound4 = [];

                foreach ($sheetInbound4 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataInbound4, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_inbound_storage data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetInbound5 = $spreadsheet->getSheetByName('td_inbound_clearance')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataInbound5 = [];

                foreach ($sheetInbound5 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataInbound5, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_inbound_clearance data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetInbound6 = $spreadsheet->getSheetByName('td_inbound_pod')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataInbound6 = [];

                foreach ($sheetInbound6 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataInbound6, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_inbound_pod data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }

                ################################################################

                // // -------------------------------------------------------------
                // // OUTBOND
                // // -------------------------------------------------------------
                $sheetOutbond = $spreadsheet->getSheetByName('th_outbond')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataOutbond1 = [];

                foreach ($sheetOutbond as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataOutbond1, array(
                            'id_' => $row['A'],
                            'gate_type' => $row['B'],
                            'waybill_smu' => $row['C'],
                            'hawb' => $row['D'],
                            'koli' => $row['E'],
                            'netto' => $row['F'],
                            'volume' => $row['G'],
                            'kindofgood' => $row['H'],
                            'airline_code' => $row['I'],
                            'flight_no' => $row['J'],
                            'origin' => $row['K'],
                            'transit' => $row['L'],
                            'dest' => $row['M'],
                            'shipper_name' => $row['N'],
                            'consignee_name' => $row['O'],
                            '_is_active' => $row['P']
                        ));
                    }
                    echo "loop th_outbond data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetOutbond2 = $spreadsheet->getSheetByName('td_outbond_acceptance')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataOutbond2 = [];

                foreach ($sheetOutbond2 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataOutbond2, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_outbond_acceptance data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetOutbond3 = $spreadsheet->getSheetByName('td_outbond_weighing')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataOutbond3 = [];

                foreach ($sheetOutbond3 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataOutbond3, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_outbond_weighing data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetOutbond4 = $spreadsheet->getSheetByName('td_outbond_manifest')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataOutbond4 = [];

                foreach ($sheetOutbond4 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataOutbond4, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_outbond_manifest data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetOutbond5 = $spreadsheet->getSheetByName('td_outbond_storage')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataOutbond5 = [];

                foreach ($sheetOutbond5 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataOutbond5, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_outbond_storage data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetOutbond6 = $spreadsheet->getSheetByName('td_outbond_buildup')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataOutbond6 = [];

                foreach ($sheetOutbond6 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataOutbond6, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_outbond_buildup data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $all = [
                    'dataInbound1' => $dataInbound1,
                    'dataInbound2' => $dataInbound2,
                    'dataInbound3' => $dataInbound3,
                    'dataInbound4' => $dataInbound4,
                    'dataInbound5' => $dataInbound5,
                    'dataInbound6' => $dataInbound6,
                    'dataOutbond1' => $dataOutbond1,
                    'dataOutbond2' => $dataOutbond2,
                    'dataOutbond3' => $dataOutbond3,
                    'dataOutbond4' => $dataOutbond4,
                    'dataOutbond5' => $dataOutbond5,
                    'dataOutbond6' => $dataOutbond6

                ];
                // print_r($all); die;
                $th_inbound = $this->db->insert_batch('th_inbound', $dataInbound1);
                $td_inbound_delivery = $this->db->insert_batch('td_inbound_delivery', $dataInbound2);
                $td_inbound_breakdown = $this->db->insert_batch('td_inbound_breakdown', $dataInbound3);
                $td_inbound_storage = $this->db->insert_batch('td_inbound_storage', $dataInbound4);
                $td_inbound_clearance = $this->db->insert_batch('td_inbound_clearance', $dataInbound5);
                $td_inbound_pod = $this->db->insert_batch('td_inbound_pod', $dataInbound6);
                $th_outbond = $this->db->insert_batch('th_outbond', $dataOutbond1);
                $td_outbond_acceptance = $this->db->insert_batch('td_outbond_acceptance', $dataOutbond2);
                $td_outbond_weighing = $this->db->insert_batch('td_outbond_weighing', $dataOutbond3);
                $td_outbond_manifest = $this->db->insert_batch('td_outbond_manifest', $dataOutbond4);
                $td_outbond_storage = $this->db->insert_batch('td_outbond_storage', $dataOutbond5);
                $td_outbond_buildup = $this->db->insert_batch('td_outbond_buildup', $dataOutbond6);

                if ($th_inbound AND 
                    $td_inbound_delivery AND 
                    $td_inbound_breakdown AND 
                    $td_inbound_storage AND 
                    $td_inbound_clearance AND 
                    $td_inbound_pod AND 
                    $th_outbond AND 
                    $td_outbond_acceptance AND 
                    $td_outbond_weighing AND 
                    $td_outbond_manifest AND 
                    $td_outbond_storage AND 
                    $td_outbond_buildup) 
                {
                    // Update status cron success
                    $this->db->where(['id' => $getData['id']]);
                    $this->db->update('status_upload', ['cron_status' => 1]); 
                
                    // $return = ['s' => 'success', 'm' => 'Import data successfully.'];
                    echo 'Import data successfully.';
                }
                else
                {
                    // $sheetInbound = $spreadsheet->getSheetByName('th_inbound')->toArray(null, true, true ,true);
                    // $sheetInbound2 = $spreadsheet->getSheetByName('td_inbound_delivery')->toArray(null, true, true ,true);
                    // $sheetInbound3 = $spreadsheet->getSheetByName('td_inbound_breakdown')->toArray(null, true, true ,true);
                    // $sheetInbound4 = $spreadsheet->getSheetByName('td_inbound_storage')->toArray(null, true, true ,true);
                    // $sheetInbound5 = $spreadsheet->getSheetByName('td_inbound_clearance')->toArray(null, true, true ,true);
                    // $sheetInbound6 = $spreadsheet->getSheetByName('td_inbound_pod')->toArray(null, true, true ,true);
                    // $sheetOutbond = $spreadsheet->getSheetByName('th_outbond')->toArray(null, true, true ,true);
                    // $sheetOutbond2 = $spreadsheet->getSheetByName('td_outbond_acceptance')->toArray(null, true, true ,true);
                    // $sheetOutbond3 = $spreadsheet->getSheetByName('td_outbond_weighing')->toArray(null, true, true ,true);
                    // $sheetOutbond4 = $spreadsheet->getSheetByName('td_outbond_manifest')->toArray(null, true, true ,true);
                    // $sheetOutbond5 = $spreadsheet->getSheetByName('td_outbond_storage')->toArray(null, true, true ,true);
                    // $sheetOutbond6 = $spreadsheet->getSheetByName('td_outbond_buildup')->toArray(null, true, true ,true);
                    
                    // DELETE INBOUND ---------------
                    // foreach ($sheetInbound as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('th_inbound');
                    // }

                    // foreach ($sheetInbound2 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_inbound_delivery');
                    // }

                    // foreach ($sheetInbound3 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_inbound_breakdown');
                    // }

                    // foreach ($sheetInbound4 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_inbound_storage');
                    // }

                    // foreach ($sheetInbound5 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_inbound_clearance');
                    // }

                    // foreach ($sheetInbound6 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_inbound_pod');
                    // }
                    // // DELETE OUTBOND ---------------
                    // foreach ($sheetOutbond as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('th_outbond');
                    // }

                    // foreach ($sheetOutbond2 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_outbond_acceptance');
                    // }

                    // foreach ($sheetOutbond3 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_outbond_weighing');
                    // }

                    // foreach ($sheetOutbond4 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_outbond_manifest');
                    // }

                    // foreach ($sheetOutbond5 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_outbond_storage');
                    // }

                    // foreach ($sheetOutbond6 as $row) {
                    //     $this->db->where(['id_' => $row['id_']]);
                    //     $this->db->delete('td_outbond_buildup');
                    // }
                    
                    // $return = ['s' => 'fail', 'm' => 'Data failed !.'];
                    echo 'Data failed !';
                }
            } // END
        } 
        else 
        {
            // $return = ['s' => 'fail', 'm' => 'No data ready to extract !'];
            echo "No data ready to extract !";
        }
        // echo json_encode($return);
    }

    public function extractExcelRa()
    {
        $this->db->select('*');
        $this->db->where(['cron_status' => 0]);
        $sql = $this->db->get('status_upload');
        $getData = $sql->row_array();
        $checkData = $sql->num_rows();
        // Get extention
        $fileName = $getData['file_name'];
        $arr_file = explode('.', $fileName);
        $extension = end($arr_file);
        // check extentioin
        if ($extension == 'xlsx')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }
        elseif ($extension == 'xls')
        {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        }

        if ($checkData > 0) 
        {
            $spreadsheet = $reader->load($getData['file_path']);
            $sheetCount = $spreadsheet->getSheetCount();

            if ($sheetCount != 10) // jumlah sheet ad 10
            {
                $return = ['s' => 'fail', 'm' => 'Incorrect data format !'];
                file_exists($getData['file_path']) ? unlink($getData['file_path']) : '';

                // delete data di db
                $this->db->where(['id' => $getData['id']]);
                $this->db->delete('status_upload');

                // $return = ['s' => 'fail', 'm' => 'The number of sheets does not match !'];
                echo 'The number of sheets does not match !';
            }
            else
            {
                // -------------------------------------------------------------
                // RA
                // -------------------------------------------------------------
                $sheetRa = $spreadsheet->getSheetByName('th_regulated')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa1 = [];

                foreach ($sheetRa as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa1, array(
                            'id_' => $row['A'],
                            'gate_type' => $row['B'],
                            'waybill_smu' => $row['C'],
                            'hawb' => $row['D'],
                            'koli' => $row['E'],
                            'netto' => $row['F'],
                            'volume' => $row['G'],
                            'kindofgood' => $row['H'],
                            'airline_code' => $row['I'],
                            'flight_no' => $row['J'],
                            'origin' => $row['K'],
                            'transit' => $row['L'],
                            'dest' => $row['M'],
                            'shipper_name' => $row['N'],
                            'consignee_name' => $row['O'],
                            '_is_active' => $row['P']
                        ));
                    }
                    echo "loop th_regulated data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // ------------------------------------------------------------- 
                $sheetRa2 = $spreadsheet->getSheetByName('td_regulated_acceptance')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa2 = [];

                foreach ($sheetRa2 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa2, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_acceptance data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                $sheetRa3 = $spreadsheet->getSheetByName('td_regulated_screening')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa3 = [];

                foreach ($sheetRa3 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa3, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_screening data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------  
                $sheetRa4 = $spreadsheet->getSheetByName('td_regulated_weighing')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa4 = [];

                foreach ($sheetRa4 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa4, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_weighing data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // ------------------------------------------------------------- 
                $sheetRa5 = $spreadsheet->getSheetByName('td_regulated_storage')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa5 = [];

                foreach ($sheetRa5 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa5, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_storage data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // ------------------------------------------------------------- 
                $sheetRa6 = $spreadsheet->getSheetByName('td_regulated_csd')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa6 = [];

                foreach ($sheetRa6 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa6, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_csd data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // ------------------------------------------------------------- 
                $sheetRa7 = $spreadsheet->getSheetByName('td_regulated_loading')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa7 = [];

                foreach ($sheetRa7 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa7, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_loading data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // ------------------------------------------------------------- 
                $sheetRa8 = $spreadsheet->getSheetByName('td_regulated_transport')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa8 = [];

                foreach ($sheetRa8 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa8, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_transport data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // ------------------------------------------------------------- 
                $sheetRa9 = $spreadsheet->getSheetByName('td_regulated_handover')->toArray(null, true, true ,true);
                $numrow = 1; // kolom dimulai dari baris ke 2 / index ke 1
                $dataRa9 = [];

                foreach ($sheetRa9 as $row) 
                {
                    if ($numrow > 1) 
                    {
                        array_push($dataRa9, array(
                            'id_header' => $row['A'],
                            'status_date' => $row['B'],
                            'status_time' => $row['C'],
                            '_is_active' => $row['D']
                        ));
                    }
                    echo "loop td_regulated_handover data ke ".$numrow."\n";
                    echo "<br>\n";
                    $numrow++;
                }
                // -------------------------------------------------------------
                
                $th_regulated = $this->db->insert_batch('th_regulated', $dataRa1);
                $td_regulated_acceptance = $this->db->insert_batch('td_regulated_acceptance', $dataRa2);
                $td_regulated_screening = $this->db->insert_batch('td_regulated_screening', $dataRa3);
                $td_regulated_weighing = $this->db->insert_batch('td_regulated_weighing', $dataRa4);
                $td_regulated_storage = $this->db->insert_batch('td_regulated_storage', $dataRa5);
                $td_regulated_csd = $this->db->insert_batch('td_regulated_csd', $dataRa6);
                $td_regulated_loading = $this->db->insert_batch('td_regulated_loading', $dataRa7);
                $td_regulated_transport = $this->db->insert_batch('td_regulated_transport', $dataRa8);
                $td_regulated_handover = $this->db->insert_batch('td_inbound_pod', $dataRa9);

                if ($th_regulated AND 
                    $td_regulated_acceptance AND 
                    $td_regulated_screening AND 
                    $td_regulated_weighing AND 
                    $td_regulated_storage AND 
                    $td_regulated_csd AND 
                    $td_regulated_loading AND 
                    $td_regulated_transport AND 
                    $td_regulated_handover) 
                {
                    // Update status cron success
                    $this->db->where(['id' => $getData['id']]);
                    $this->db->update('status_upload', ['cron_status' => 1]); 
                
                    // $return = ['s' => 'success', 'm' => 'Import data successfully.'];
                    echo 'Import data successfully.';
                }
                else
                {
                    echo 'Data failed !';
                }
            } // END
        } 
        else 
        {
            // $return = ['s' => 'fail', 'm' => 'No data ready to extract !'];
            echo "No data ready to extract !";
        }
        // echo json_encode($return);
    }

    function statusFile()
    {
        $this->db->select('*');
        $this->db->where(['cron_status != 0'=> NULL, 'created_at LIKE "'.date('Y-m-d').'%"'=>NULL]);
        $this->db->order_by('id', 'DESC');
        $sql = $this->db->get('status_upload');
        $getData = $sql->row_array();
        $checkData = $sql->num_rows();

        if ($checkData > 0) 
        {
            if ($getData['cron_status'] != 1) 
            {
                $respon = ['s' => 'fail', 'd' => $getData, 'm' => 'There is an error, please upload it again.'];
            } 
            else 
            {
                $respon = ['s' => 'success', 'd' => $getData];
            }
        } 
        else 
        {
            $respon = ['s' => 'notFound', 'm' => 'No new data yet!'];
        }
        echo json_encode($respon);
    }

    // function importInboundOutbond() 
    // {
    //     $fileName = $_FILES['fileExcel']['name'];
    //     $dateTime = date('YmdHis');

    //     if (empty($fileName))
    //     {
    //         $return = ['s' => 'fail', 'm' => 'Data cannot be empty !'];
    //     }
    //     else
    //     {
    //         $allowed_extension = array('xlsx', 'xls'); // 'xls', 'csv', etc

    //         $arr_file = explode('.', $fileName);
    //         $extension = end($arr_file);

    //         if (!in_array($extension, $allowed_extension))
    //         {
    //             $return = ['s' => 'fail', 'm' => 'Incorrect data format !. Only (xlxs & xls) format allowed'];
    //         }
    //         else
    //         {
    //             $createNewFileName = "$arr_file[0]-$dateTime.$extension";
    //             // move to directory file
    //             $targetPath = 'file/' . $createNewFileName;
    //             move_uploaded_file($_FILES['fileExcel']['tmp_name'], $targetPath);

    //             if ($extension == 'xlsx')
    //             {
    //                 // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
    //                 $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    //             }
    //             elseif ($extension == 'xls')
    //             {
    //                 $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
    //             }
    //             $spreadsheet = $reader->load($targetPath);
    //             $sheetCount = $spreadsheet->getSheetCount();

    //             if ($sheetCount != 12) // jumlah sheet ad 12
    //             {
    //                 $return = ['s' => 'fail', 'm' => 'Incorrect data format !'];
    //                 file_exists($targetPath) ? unlink($targetPath) : '';
    //             }
    //             else
    //             {
    //                 // -------------------------------------------------------------
    //                 // INBOUND
    //                 // -------------------------------------------------------------
    //                 $sheetInbound = $spreadsheet->getSheetByName('th_inbound')->toArray();
    //                 for ($i = 1; $i < count($sheetInbound); $i++)
    //                 {
    //                     $data = [
    //                         'id_' => $sheetInbound[$i]['0'],
    //                         'gate_type' => $sheetInbound[$i]['1'],
    //                         'waybill_smu' => $sheetInbound[$i]['2'],
    //                         'hawb' => $sheetInbound[$i]['3'],
    //                         'koli' => $sheetInbound[$i]['4'],
    //                         'netto' => $sheetInbound[$i]['5'],
    //                         'volume' => $sheetInbound[$i]['6'],
    //                         'kindofgood' => $sheetInbound[$i]['7'],
    //                         'airline_code' => $sheetInbound[$i]['8'],
    //                         'flight_no' => $sheetInbound[$i]['9'],
    //                         'origin' => $sheetInbound[$i]['10'],
    //                         'transit' => $sheetInbound[$i]['11'],
    //                         'dest' => $sheetInbound[$i]['12'],
    //                         'shipper_name' => $sheetInbound[$i]['13'],
    //                         'consignee_name' => $sheetInbound[$i]['14'],
    //                         '_is_active' => $sheetInbound[$i]['15']
    //                     ];
    //                     $this->db->insert('th_inbound', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetInbound2 = $spreadsheet->getSheetByName('td_inbound_delivery')->toArray();
    //                 for ($i = 1; $i < count($sheetInbound2); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetInbound2[$i]['0'],
    //                         'status_date' => $sheetInbound2[$i]['1'],
    //                         'status_time' => $sheetInbound2[$i]['2'],
    //                         '_is_active' => $sheetInbound2[$i]['3']
    //                     ];
    //                     $this->db->insert('td_inbound_delivery', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetInbound3 = $spreadsheet->getSheetByName('td_inbound_breakdown')->toArray();
    //                 for ($i = 1; $i < count($sheetInbound3); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetInbound3[$i]['0'],
    //                         'status_date' => $sheetInbound3[$i]['1'],
    //                         'status_time' => $sheetInbound3[$i]['2'],
    //                         '_is_active' => $sheetInbound3[$i]['3']
    //                     ];
    //                     $this->db->insert('td_inbound_breakdown', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetInbound4 = $spreadsheet->getSheetByName('td_inbound_storage')->toArray();
    //                 for ($i = 1; $i < count($sheetInbound4); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetInbound4[$i]['0'],
    //                         'status_date' => $sheetInbound4[$i]['1'],
    //                         'status_time' => $sheetInbound4[$i]['2'],
    //                         '_is_active' => $sheetInbound4[$i]['3']
    //                     ];
    //                     $this->db->insert('td_inbound_storage', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetInbound5 = $spreadsheet->getSheetByName('td_inbound_clearance')->toArray();
    //                 for ($i = 1; $i < count($sheetInbound5); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetInbound5[$i]['0'],
    //                         'status_date' => $sheetInbound5[$i]['1'],
    //                         'status_time' => $sheetInbound5[$i]['2'],
    //                         '_is_active' => $sheetInbound5[$i]['3']
    //                     ];
    //                     $this->db->insert('td_inbound_clearance', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetInbound6 = $spreadsheet->getSheetByName('td_inbound_pod')->toArray();
    //                 for ($i = 1; $i < count($sheetInbound6); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetInbound6[$i]['0'],
    //                         'status_date' => $sheetInbound6[$i]['1'],
    //                         'status_time' => $sheetInbound6[$i]['2'],
    //                         '_is_active' => $sheetInbound6[$i]['3']
    //                     ];
    //                     $this->db->insert('td_inbound_pod', $data);
    //                 }

    //                 ################################################################

    //                 // // -------------------------------------------------------------
    //                 // // OUTBOND
    //                 // // -------------------------------------------------------------
    //                 $sheetOutbond = $spreadsheet->getSheetByName('th_outbond')->toArray();
    //                 for ($i = 1; $i < count($sheetOutbond); $i++)
    //                 {
    //                     $data = [
    //                         'id_' => $sheetOutbond[$i]['0'],
    //                         'gate_type' => $sheetOutbond[$i]['1'],
    //                         'waybill_smu' => $sheetOutbond[$i]['2'],
    //                         'hawb' => $sheetOutbond[$i]['3'],
    //                         'koli' => $sheetOutbond[$i]['4'],
    //                         'netto' => $sheetOutbond[$i]['5'],
    //                         'volume' => $sheetOutbond[$i]['6'],
    //                         'kindofgood' => $sheetOutbond[$i]['7'],
    //                         'airline_code' => $sheetOutbond[$i]['8'],
    //                         'flight_no' => $sheetOutbond[$i]['9'],
    //                         'origin' => $sheetOutbond[$i]['10'],
    //                         'transit' => $sheetOutbond[$i]['11'],
    //                         'dest' => $sheetOutbond[$i]['12'],
    //                         'shipper_name' => $sheetOutbond[$i]['13'],
    //                         'consignee_name' => $sheetOutbond[$i]['14'],
    //                         '_is_active' => $sheetOutbond[$i]['15']
    //                     ];
    //                     $this->db->insert('th_outbond', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetOutbond2 = $spreadsheet->getSheetByName('td_outbond_acceptance')->toArray();
    //                 for ($i = 1; $i < count($sheetOutbond2); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetOutbond2[$i]['0'],
    //                         'status_date' => $sheetOutbond2[$i]['1'],
    //                         'status_time' => $sheetOutbond2[$i]['2'],
    //                         '_is_active' => $sheetOutbond2[$i]['3']
    //                     ];
    //                     $this->db->insert('td_outbond_acceptance', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetOutbond3 = $spreadsheet->getSheetByName('td_outbond_weighing')->toArray();
    //                 for ($i = 1; $i < count($sheetOutbond3); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetOutbond3[$i]['0'],
    //                         'status_date' => $sheetOutbond3[$i]['1'],
    //                         'status_time' => $sheetOutbond3[$i]['2'],
    //                         '_is_active' => $sheetOutbond3[$i]['3']
    //                     ];
    //                     $this->db->insert('td_outbond_weighing', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetOutbond4 = $spreadsheet->getSheetByName('td_outbond_manifest')->toArray();
    //                 for ($i = 1; $i < count($sheetOutbond4); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetOutbond4[$i]['0'],
    //                         'status_date' => $sheetOutbond4[$i]['1'],
    //                         'status_time' => $sheetOutbond4[$i]['2'],
    //                         '_is_active' => $sheetOutbond4[$i]['3']
    //                     ];
    //                     $this->db->insert('td_outbond_manifest', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetOutbond5 = $spreadsheet->getSheetByName('td_outbond_storage')->toArray();
    //                 for ($i = 1; $i < count($sheetOutbond5); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetOutbond5[$i]['0'],
    //                         'status_date' => $sheetOutbond5[$i]['1'],
    //                         'status_time' => $sheetOutbond5[$i]['2'],
    //                         '_is_active' => $sheetOutbond5[$i]['3']
    //                     ];
    //                     $this->db->insert('td_outbond_storage', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetOutbond6 = $spreadsheet->getSheetByName('td_outbond_buildup')->toArray();
    //                 for ($i = 1; $i < count($sheetOutbond6); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetOutbond6[$i]['0'],
    //                         'status_date' => $sheetOutbond6[$i]['1'],
    //                         'status_time' => $sheetOutbond6[$i]['2'],
    //                         '_is_active' => $sheetOutbond6[$i]['3']
    //                     ];
    //                     $this->db->insert('td_outbond_buildup', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $return = ['s' => 'success', 'm' => 'Import data successfully.'];
    //             } // end: count sheet
    //         }
    //     }
    //     echo json_encode($return); 
    // }

    // function importRa() 
    // {
    //     $fileName = $_FILES['fileExcel']['name'];
    //     $dateTime = date('YmdHis');

    //     if (empty($fileName))
    //     {
    //         $return = ['s' => 'fail', 'm' => 'Data cannot be empty !'];
    //     }
    //     else
    //     {
    //         $allowed_extension = array('xlsx', 'xls'); // 'xls', 'csv', etc

    //         $arr_file = explode('.', $fileName);
    //         $extension = end($arr_file);

    //         if (!in_array($extension, $allowed_extension))
    //         {
    //             $return = ['s' => 'fail', 'm' => 'Incorrect data format !. Only (xlxs & xls) format allowed'];
    //         }
    //         else
    //         {
    //             $createNewFileName = "$arr_file[0]-$dateTime.$extension";
    //             // move to directory file
    //             $targetPath = 'file/' . $createNewFileName;
    //             move_uploaded_file($_FILES['fileExcel']['tmp_name'], $targetPath);

    //             if ($extension == 'xlsx')
    //             {
    //                 // $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
    //                 $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    //             }
    //             elseif ($extension == 'xls')
    //             {
    //                 $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
    //             }
    //             $spreadsheet = $reader->load($targetPath);
    //             $sheetCount = $spreadsheet->getSheetCount();

    //             if ($sheetCount != 10) // jumlah sheet ad 10
    //             {
    //                 $return = ['s' => 'fail', 'm' => 'Incorrect data format !'];
    //                 file_exists($targetPath) ? unlink($targetPath) : '';
    //             }
    //             else
    //             {
    //                 // -------------------------------------------------------------
    //                 // RA
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated = $spreadsheet->getSheetByName('th_regulated')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated); $i++)
    //                 {
    //                     $data = [
    //                         'id_' => $sheetRegulated[$i]['0'],
    //                         'gate_type' => $sheetRegulated[$i]['1'],
    //                         'waybill_smu' => $sheetRegulated[$i]['2'],
    //                         'hawb' => $sheetRegulated[$i]['3'],
    //                         'koli' => $sheetRegulated[$i]['4'],
    //                         'netto' => $sheetRegulated[$i]['5'],
    //                         // 'volume' => $sheetRegulated[$i]['6'],
    //                         'kindofgood' => $sheetRegulated[$i]['6'],
    //                         'airline_code' => $sheetRegulated[$i]['7'],
    //                         'flight_no' => $sheetRegulated[$i]['8'],
    //                         'origin' => $sheetRegulated[$i]['9'],
    //                         'transit' => $sheetRegulated[$i]['10'],
    //                         'dest' => $sheetRegulated[$i]['11'],
    //                         'shipper_name' => $sheetRegulated[$i]['12'],
    //                         'consignee_name' => $sheetRegulated[$i]['13'],
    //                         '_is_active' => $sheetRegulated[$i]['14']
    //                     ];
    //                     $this->db->insert('th_regulated', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated2 = $spreadsheet->getSheetByName('td_regulated_acceptance')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated2); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated2[$i]['0'],
    //                         'status_date' => $sheetRegulated2[$i]['1'],
    //                         'status_time' => $sheetRegulated2[$i]['2'],
    //                         '_is_active' => $sheetRegulated2[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_acceptance', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated3 = $spreadsheet->getSheetByName('td_regulated_screening')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated3); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated3[$i]['0'],
    //                         'status_date' => $sheetRegulated3[$i]['1'],
    //                         'status_time' => $sheetRegulated3[$i]['2'],
    //                         '_is_active' => $sheetRegulated3[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_screening', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated4 = $spreadsheet->getSheetByName('td_regulated_weighing')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated4); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated4[$i]['0'],
    //                         'status_date' => $sheetRegulated4[$i]['1'],
    //                         'status_time' => $sheetRegulated4[$i]['2'],
    //                         '_is_active' => $sheetRegulated4[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_weighing', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated5 = $spreadsheet->getSheetByName('td_regulated_rejected')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated5); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated5[$i]['0'],
    //                         'status_date' => $sheetRegulated5[$i]['1'],
    //                         'status_time' => $sheetRegulated5[$i]['2'],
    //                         '_is_active' => $sheetRegulated5[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_rejected', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated6 = $spreadsheet->getSheetByName('td_regulated_storage')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated6); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated6[$i]['0'],
    //                         'status_date' => $sheetRegulated6[$i]['1'],
    //                         'status_time' => $sheetRegulated6[$i]['2'],
    //                         '_is_active' => $sheetRegulated6[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_storage', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated7 = $spreadsheet->getSheetByName('td_regulated_csd')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated7); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated7[$i]['0'],
    //                         'status_date' => $sheetRegulated7[$i]['1'],
    //                         'status_time' => $sheetRegulated7[$i]['2'],
    //                         '_is_active' => $sheetRegulated7[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_csd', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated8 = $spreadsheet->getSheetByName('td_regulated_loading')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated8); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated8[$i]['0'],
    //                         'status_date' => $sheetRegulated8[$i]['1'],
    //                         'status_time' => $sheetRegulated8[$i]['2'],
    //                         '_is_active' => $sheetRegulated8[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_loading', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated9 = $spreadsheet->getSheetByName('td_regulated_transport')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated9); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated9[$i]['0'],
    //                         'status_date' => $sheetRegulated9[$i]['1'],
    //                         'status_time' => $sheetRegulated9[$i]['2'],
    //                         '_is_active' => $sheetRegulated9[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_transport', $data);
    //                 }
    //                 // -------------------------------------------------------------
    //                 $sheetRegulated10 = $spreadsheet->getSheetByName('td_regulated_handover')->toArray();
    //                 for ($i = 1; $i < count($sheetRegulated10); $i++)
    //                 {
    //                     $data = [
    //                         'id_header' => $sheetRegulated10[$i]['0'],
    //                         'status_date' => $sheetRegulated10[$i]['1'],
    //                         'status_time' => $sheetRegulated10[$i]['2'],
    //                         '_is_active' => $sheetRegulated10[$i]['3']
    //                     ];
    //                     $this->db->insert('td_regulated_handover', $data);
    //                 }
    //                 ################################################################

    //                 $return = ['s' => 'success', 'm' => 'Import data successfully.'];
    //             } // end: count sheet
    //         }
    //     }
    //     echo json_encode($return); 
    // }
}
