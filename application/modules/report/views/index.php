<div class="m-portlet m-portlet">
<!--     <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                 <div class="row">
                    <div class="col-md-12" style="position: absolute;right: 11px;top: 13px;">
                        <div class="ss">
                            <a href="#modalUpload" role="button" class="modalUploadExcel" data-toggle="modal" onclick="modalUploadExcel()">
                            	<button class="btn btn-primary pull-right" id="upload_excels" style="margin-left:8px">Upload</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12">
            	<form class="m-form m-form--fit m-form--label-align-right" id="formReport">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <label for="example-date-input" class="col-2 col-form-label">Type Report</label>
                            <div class="col-8">
                                <div class="row">
                                    <div class="col">
                                        <?php if ($type == 1) : ?>
                                            <select class="form-control" name="typeReport" id="typeReport">
                                                <option value="">-- Pilih --</option>
                                                <option value="INBOUND">INBOUND</option>
                                                <option value="OUTBOND">OUTBOND</option>
                                            </select>
                                        <?php endif; ?>
                                    </div>
                                  </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label for="flight" class="col-2 col-form-label">Flight</label>
                            <div class="col-8" style="position: relative;">
                                <img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="" id="loaderTracking">
                                <select class="form-control" name="flight" id="flight">
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-form__group row" style="padding-top: 0;">
                            <label for="example-date-input" class="col-2 col-form-label"></label>
                            <div class="col-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Filter</button>
                                <!-- <button type="button"  class="btn btn-danger" id="button_excels" style="margin-left:8px">Download</button> -->
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <table class="table table-hover mt-5" id="tableReport">
                    <thead>
                        <tr>
                            <th>FlightNo</th>
                            <th>Waybill / SMU</th>
                            <th>Koli</th>
                            <th>Weight</th>
                            <th>Netto</th>
                            <th>Kind Of Goods</th>
                            <th>Datetime</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modalUpload" class="modal animated pulse" tabindex="-1" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload File Excel (Format only: xlxs, xls)</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
            	<div id="onProccess">Waitting ...</div>
                <div class="row">
                    <div class="col-md-12">
                    	<div class="wrap-alert"></div>
                    	<form id="importData">
							<div class="form-group">
						        <label for="exampleInputFile">Upload File (Inbound & Outbond)</label>
						        <input type="file" name="fileExcel" id="fileExcel" class="form-control" style="border-color: #405eab;">
						        <span class="m-form__help">
						        	<small>if you don't have the upload format, please download the format <a href="<?php echo base_url('file/format/formatReportMau.xls'); ?>" title="Download format excel">here</a></small>
						        </span>
						    </div>
						    <button type="submit" class="btn btn-primary">Import</button>

						    <div id="infoProccessUpload" class="mt-5"></div>
						    <!-- <a href="<?php // echo base_url('file/format/formatExcel.xls'); ?>"><button type="button" class="btn btn-danger">Download Format</button></a> -->
						</form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div><!-- End: modal -->

<style>
	.form-control.datepicker {
		width: 100%;
	}
	#myTab .nav-link.active {
		color: #4f72ef;
	}
    #loaderTracking, 
    #loaderTracking2 {
        display: none;
        width: 20px;
        position: absolute;
        right: 24px;
        top: 9px;
        bottom: 0;
        z-index: 999;
    }
	#onProccess {
		display: none;
		position: absolute;
		z-index: 2;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		background: #0000006b;
		text-align: center;
		font-size: 20px;
		color: #fff;
		line-height: 188px;
	}
    #tableReport thead {
        background: #212834;
        color: #fff;
        font-size: 12px;
    }
    #tableReport tbody {
        font-size: 10px;
    }
</style>

<script>
var interval = null;

$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        orientation: "bottom auto",
        autoclose: true
    });
    // update uploaded complete
    setTimeout(function() {
	     updateStatusCron(); // update status cron complete
	}, 3000);
});

// function getStatusUpload()
// {
//     $.ajax({
//         url : '<?= base_url(); ?>report/UploadExcels/statusFile',
//         type : 'post',
//         dataType: 'json',
//         success: function(hasil) {
//             // console.log(hasil);
//             var rows ='';
//             var status ='';
//             $('table tbody').empty();
//             if (hasil.s == 'success') {
//             	$('#importData')[0].reset(); // reset form
//                 $('#onProccess').hide();
//                 $('.wrap-alert').html(`<div class="alert alert-success alert-dismissible fade show" role="alert">	
// 	                			<span>Import Data successfuly.</span>
// 								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
// 									<span aria-hidden="true">&times;</span>
// 								</button>
// 							</div>
// 							`);
//                 $('.alert').delay(15000).fadeOut(1000);
//                 clearInterval(interval); // stop the interval
//             }
//             else if (hasil.s == 'fail') {
//             	$('#importData')[0].reset(); // reset form
//             	$('#onProccess').hide();
//             	$('.wrap-alert').html(`<div class="alert alert-danger alert-dismissible fade show" role="alert">	
// 	                			<span>${hasil.m}</span>
// 								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
// 									<span aria-hidden="true">&times;</span>
// 								</button>
// 							</div>
// 							`);
//                 $('.alert').delay(15000).fadeOut(1000);
//                 // $('#infoProccessUpload').html('<span class="badge badge-warning infoStatus">'+hasil.m+'</span>');
//                 // $('.infoStatus').delay(15000).fadeOut(1000);
//                 clearInterval(interval); // stop the interval
//             }
//         },
//         error: function (xhr, ajaxOptions, thrownError) {
//             console.log(xhr.status);
//             console.log(thrownError);
//             // alert(xhr.status);
//             // alert(thrownError);
//         }
//     });
// }

// function modalUploadExcel() {
// 	$('#importData')[0].reset(); // reset form
// 	// updateStatusCron(); // update status cron complete
// }

// function updateStatusCron()
// {
// 	$.ajax({
// 		url: '<?php echo base_url();?>report/UploadExcels/updateStatusCron',
// 		type: "post",
// 		data: {'updateCron': 'updateCron'},
// 		dataType: "json",
// 		success: function(data) {
//          	if (data.s == 'success') {
//          		console.log(data.m);
//             } 
//             else {
//                 console.log(data.m);
//             }
//        },
//         error: function (xhr, ajaxOptions, thrownError) {
//             swal('Error !', 'Error !!!', 'error'); 
//         }
//     });
// }

// $('#importData').on('submit', function(e) {
// 	e.preventDefault(); 
// 	var data = new FormData(this);
// 	if ($('#fileExcel').val() == '') {
// 		swal('Error !', 'Please select the file first!', 'error'); 
// 		return false;
// 	} 
// 	else {
//     	$.ajax({
// 			url: '<?php echo base_url();?>report/UploadExcels/uploadFile',
// 			type: "post",
// 			data: data,
// 			dataType: "json",
// 			cache: false,
// 			processData: false,
// 			contentType: false,
// 			async: false,
// 			beforeSend: function(jqXHR, options) {
// 		        setTimeout(function() {
// 		            $.ajax($.extend(options, {beforeSend: $.noop}));
// 		        }, 1000);
// 		        $('#onProccess').show();
// 		        return false;
// 		    },
// 			success: function(data) {
// 				// $('#onProccess').hide();
// 	         	if (data.s == 'success') {
// 	         		interval = setInterval(getStatusUpload,5000);
// 	            } 
// 	            else {
// 	                swal('Error !', data.m, 'error'); 
// 	                $('#onProccess').hide();
// 	            }
// 	       },
// 	        error: function (xhr, ajaxOptions, thrownError) {
// 	            swal('Error !', 'Error !!!', 'error'); 
// 	        }
// 	    });
// 	}
// });

function listData(typeReport='', flight='')
{
    var oTable = $("#tableReport").on('processing.dt', function (e, settings, processing) {
        if (processing == true) {
            swal({
                showCancelButton: false,
                showConfirmButton: false,
                imageUrl: '<?= base_url();?>assets/img/loading.gif'
            });
        } else {
            swal.close()
        }
    }).DataTable({
        responsive: !0,
        paging: !0,
        bDestroy : !0,
        ajax : {
            type: 'POST',
            data: {typeReport:typeReport, flight:flight},
            url : '<?php echo base_url();?>report/tableReport'
        },
        serverSide: !0,
        processing: !0,
        columnDefs: [
        ],
        order: [],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]]
    });
}

$('#formReport').on('submit',function(e){
    e.preventDefault();
    var typeReport = $("#typeReport").val();
    var flight = $("#flight").val();
    if (typeReport == '') {
        swal('Error !', 'Type report cannot be null !', 'error'); 
    } 
    else if (flight == '') {
        swal('Error !', 'FlightNo cannot be null !', 'error'); 
    } 
    else {
        listData(typeReport, flight);
    }
});

$('#typeReport').on('change', function() {
    var InOut = $('#typeReport').val();
    var data = '';
    if (InOut == 'INBOUND') {
        data =  {'typeReport': 'INBOUND'}
    }
    else if (InOut == 'OUTBOND') {
        data =  {'typeReport': 'OUTBOND'}
    }
    $.ajax({
        url: '<?php echo base_url();?>report/getFlightByType',
        type: "post",
        data: data,
        dataType: "json",
        beforeSend: function() {
            $('#loaderTracking').show();
        }, 
        success: function(data) {
            $('#loaderTracking').hide();
            if (data.s == 'success') {
                var s = '';
                s += `<option value="">-- Pilih --</option>`;
                for (var i = 0; i < data.d.length; i++) {  
                    s += `<option value="${data.d[i].flight_no}">${data.d[i].flight_no} - ${data.d[i].airlinesname}</option>`;        
               }  
               $('#flight').html(s);
            } 
            else if (data.s = 'empty') {
                $('#flight').empty();
            }
            else {
                swal('Error !', data.m, 'error'); 
            }
       },
        error: function (xhr, ajaxOptions, thrownError) {
            swal('Error !', 'Error !!!', 'error'); 
        }
    });
});
</script>