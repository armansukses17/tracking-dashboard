<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);
        
class UpdateStatus extends MY_Controller 
{
    public function __construct()
    {
        parent::__construct();
        if (empty($this->session->userdata("user_id"))) {
            redirect(base_url('auth'));
        }
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        $access_domain = $this->session->userdata['access_domain'];
        $data = ['title' => 'Update Status'];

        if ($access_domain == 1)
        {
            $this->home_library->main('status/index-mau', $data); // MAU
        }
        else
        {
            $this->home_library->main('status/index-ra', $data); // RA
        }
    }

    public function getMawb($param)
    {
        $access_domain = $this->session->userdata['access_domain'];
        if ($access_domain == 1)
        {
            $endPoint = 'http://149.129.227.63/tracking-rest/RestServerMau/mawb/?param='; // MAU
        }
        else
        {
            $endPoint = 'http://149.129.227.63/tracking-rest/RestServerRa/mawb/?param='; // RA
        }
        // -----------------------------------------------
	    $ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, $endPoint . $param);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    $output = curl_exec($ch); 
	    curl_close($ch);      
	    return $output;
    }

    public function autocompleteMawb()
    {
        if (isset($_POST['term'])) 
        {
            $term = $this->getMawb($_POST['term']);
            $result = json_decode($term, true);
            $data = $result['d'];
            // print_r(count($data)); die;
            if (count($data) > 0) 
            {
                foreach ($data as $row)
                {
                	$arr_result[] = [
                        'key' => $row['waybill_smu'], 
                        'label' => $row['waybill_smu']
                    ];
                }
            }
            else
            {
                $arr_result[] = 'notFound';
            }
            echo json_encode($arr_result);
        }
    }

    public function getShipment()
    {
        $access_domain = $this->session->userdata['access_domain'];
        if ($access_domain == 1)
        {
            $endPoint = 'http://149.129.227.63/tracking-rest/RestServerMau/status/?mawb='; // MAU
        }
        else
        {
            $endPoint = 'http://149.129.227.63/tracking-rest/RestServerRa/status/?mawb='; // RA
        }
        // -----------------------------------------------
		$getShipment = $this->input->get('mawb');
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $endPoint . $getShipment);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch); 
		curl_close($ch);      
		echo $output;
    }

    public function postStatus()
    {
        $access_domain = $this->session->userdata['access_domain'];
        if ($access_domain == 1)
        {
            $endPoint = 'http://149.129.227.63/tracking-rest/RestServerMau/create'; // MAU
        }
        else
        {
            $endPoint = 'http://149.129.227.63/tracking-rest/RestServerRa/create'; // RA
        }
        // -----------------------------------------------
    	$mawb = $this->input->post('mawb');
    	$status_code = $this->input->post('statusTracking');
    	$total_item = $this->input->post('total_item');
        $desc_item = $this->input->post('desc_item');
        $date_time = $this->input->post('date_time');
    	// ---------------------------------------------------
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $endPoint);
		curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, "mawb=$mawb&status_code=$status_code&date_time=$date_time");

		// In real life you should use something like:
		curl_setopt($ch, CURLOPT_POSTFIELDS, 
	         http_build_query([
         		'mawb' => $mawb,
         		'status_code' => $status_code,
         		'total_item' => $total_item,
                'desc_item' => $desc_item,
                'date_time' => $date_time
         	])
	     );

		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);
		curl_close ($ch);
		echo $server_output;
        die;
    }
}
