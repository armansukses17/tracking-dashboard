<div class="m-portlet m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                 <div class="row">
                    <div class="col-md-12" style="position: absolute;right: 11px;top: 13px;">
                        <div class="ss">
                            <a href="#modalUpload" role="button" class="modalUploadExcel" data-toggle="modal" onclick="modalUploadExcel()">
                                <button class="btn btn-primary pull-right" id="upload_excels" style="margin-left:8px"><i class="fa fa-upload" aria-hidden="true"></i> Import</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row">
            <div class="col-md-12">
            	<form class="m-form m-form--fit m-form--label-align-right" id="formStatusPost">
					<div class="m-portlet__body">
						<div class="form-group m-form__group row">
							<label for="typeReport" class="col-md-2 col-sm-12 col-form-label">MAWB / SMU</label>
							<div class="col-md-8 col-sm-12" style="position: relative;">
								<img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="" id="loaderTracking">
								<input type="text" name="mawb" id="mawb" class="form-control input-sm" placeholder="Autocomplete mawb / waybill_smu ...">
							</div>
						</div>
						<div class="form-group m-form__group row">
							<label for="typeReport" class="col-md-2 col-sm-12 col-form-label">Status</label>
							<div class="col-md-8 col-sm-12">
								<img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="" id="loaderTracking2">
								<select class="form-control" name="statusTracking" id="statusTracking">
								</select>
							</div>
						</div>
						<div class="form-group m-form__group row">
                            <label for="typeReport" class="col-md-2 col-sm-12 col-form-label">Date</label>
                            <div class="col-md-8 col-sm-12">
                                 <input type="text" class="form-control m-input dateTimePicker" name="date_time" id="date_time" autocomplete="off" placeholder="yyyy-mm-dd hh:ii" />
                                 <span class="m-form__help"><small>Format time 24 hour. example: 2021-02-06 23:56</small></span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row mt-3" style="padding-top: 0;">
                            <label for="example-date-input" class="col-md-2 col-sm-12 col-form-label"></label>
                            <div class="col-md-8 col-sm-12">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
					</div>
				</form>
				<hr>
 
            </div>
        </div>
    </div>
</div>

<div id="modalUpload" class="modal animated pulse" tabindex="-1" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Import File Excel (Format only: xlsx, xls)</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div id="onProccess">Waitting ...</div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap-alert"></div>
                        <form id="importData">
                            <div class="form-group">
                                <label for="exampleInputFile">Import File (Inbound & Outbond)</label>
                                <input type="file" name="fileExcel" id="fileExcel" class="form-control" style="border-color: #405eab;">
                                <span class="m-form__help">
                                    <small>if you don't have the upload format, please download the format <a href="<?php echo base_url('file/format/formatReportMau.xls'); ?>" title="Download format excel">here</a></small>
                                </span>
                            </div>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Import</button>

                            <!-- <div id="infoProccessUpload" class="mt-5"></div> -->
                            <!-- <a href="<?php // echo base_url('file/format/formatExcel.xls'); ?>"><button type="button" class="btn btn-danger">Download Format</button></a> -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div><!-- End: modal -->

<style>
	.form-control.datepicker {
		width: 100%;
	}
	#loaderTracking, 
	#loaderTracking2 {
        display: none;
        width: 20px;
        position: absolute;
        right: 24px;
        top: 9px;
        bottom: 0;
        z-index: 999;
    }
    #onProccess {
        display: none;
        position: absolute;
        z-index: 2;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        background: #0000006b;
        text-align: center;
        font-size: 20px;
        color: #fff;
        line-height: 188px;
    }
</style>

<script>
$(document).ready(function() {
    // $('.datepicker').datepicker({
    //     format: 'yyyy-mm-dd',
    //     orientation: "bottom auto",
    //     autoclose: true
    // });
	$('.dateTimePicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
    });
});

$("#mawb").autocomplete({
	source: function(request, response) {
		var term = $("#mawb").val();
		$.ajax({
			url: "<?php echo base_url(); ?>status/UpdateStatus/autocompleteMawb/?",
			type: 'post',
			dataType: "json",
			data: { term: term },
			beforeSend: function() {
	            $('#loaderTracking').show();
        	}, 
			success: function(data) {
				$('#loaderTracking').hide();
				response(data);
			}
		});
	},
    select: function(event, ui) {
        var value = ui.item.value;
        if (value == 'notFound') {
            alert('Data not found !');
        } 
        else {
			// Set selection
			$('#mawb').val(ui.item.label); // display the selected text
			getShipment($('#mawb').val());
			return false;
        }
    }
});

function getShipment(mawb) {
	$.ajax({
        type: "POST",
        url : "<?php echo base_url(); ?>status/UpdateStatus/getShipment/?mawb="+mawb,
        data: {},
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function() {
            $('#loaderTracking2').show();
    	}, 
        success: function(data) {
        	$('#loaderTracking2').hide();
            var s = '';
        	s += `<option value="">-- Pilih --</option>`;
        	for (var i = 0; i < data.d.length; i++) {  
        		s += `<option value="${data.d[i].status_code}">${data.d[i].status_name}</option>`;        
           }  
           $('#statusTracking').html(s);
        }
    });
}

$('#formStatusPost').on('submit',function(e) {
    e.preventDefault();
    var form = $(this).serialize();
    // return false;
    $.ajax({
        url : "<?php echo base_url(); ?>status/UpdateStatus/postStatus",
        type: 'post',
        data: form,
        dataType: 'json',
        beforeSend: function() {
        },
        success: function(hasil) {
            if (hasil.s == 'success') {
            	swal('Success !', hasil.m, 'success'); 
            	$('#formStatusPost')[0].reset(); // reset form
            } 
            else {
                swal('Error !', hasil.m, 'error'); 
                $('#formStatusPost')[0].reset(); // reset form
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert('error !!!!!!');
        }
    });
});

// UPLOAD EXCELS ---------------
function getStatusUpload()
{
    $.ajax({
        url : '<?= base_url(); ?>report/UploadExcels/statusFile',
        type : 'post',
        dataType: 'json',
        success: function(hasil) {
            // console.log(hasil);
            var rows ='';
            var status ='';
            $('table tbody').empty();
            if (hasil.s == 'success') {
                $('#importData')[0].reset(); // reset form
                $('#onProccess').hide();
                $('.wrap-alert').html(`<div class="alert alert-success alert-dismissible fade show" role="alert">   
                                <span>Import Data successfuly.</span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            `);
                // $('.alert').delay(30000).fadeOut(1000);
                swal('Success !', 'Import Data successfuly.', 'success'); 
                clearInterval(interval); // stop the interval
            }
            else if (hasil.s == 'fail') {
                $('#importData')[0].reset(); // reset form
                $('#onProccess').hide();
                $('.wrap-alert').html(`<div class="alert alert-danger alert-dismissible fade show" role="alert">    
                                <span>${hasil.m}</span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            `);
                // $('.alert').delay(30000).fadeOut(1000);
                swal('Error !', hasil.m, 'error'); 
                clearInterval(interval); // stop the interval
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            // alert(xhr.status);
            // alert(thrownError);
        }
    });
}

function modalUploadExcel() {
    $('#importData')[0].reset(); // reset form
    updateStatusCron(); // update status cron complete
}

function updateStatusCron()
{
    $.ajax({
        url: '<?php echo base_url();?>report/UploadExcels/updateStatusCron',
        type: "post",
        data: {'updateCron': 'updateCron'},
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                console.log(data.m);
            } 
            else {
                console.log(data.m);
            }
       },
        error: function (xhr, ajaxOptions, thrownError) {
            swal('Error !', 'Error !!!', 'error'); 
        }
    });
}

$('#importData').on('submit', function(e) {
    e.preventDefault(); 
    $('.alert').hide();
    var data = new FormData(this);
    if ($('#fileExcel').val() == '') {
        swal('Error !', 'Please select the file first!', 'error'); 
        return false;
    } 
    else {
        $.ajax({
            url: '<?php echo base_url();?>report/UploadExcels/uploadFile',
            type: "post",
            data: data,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            async: false,
            beforeSend: function(jqXHR, options) {
                setTimeout(function() {
                    $.ajax($.extend(options, {beforeSend: $.noop}));
                }, 1000);
                $('#onProccess').show();
                return false;
            },
            success: function(data) {
                // $('#onProccess').hide();
                if (data.s == 'success') {
                    interval = setInterval(getStatusUpload,5000);
                } 
                else {
                    swal('Error !', data.m, 'error'); 
                    $('#onProccess').hide();
                }
           },
            error: function (xhr, ajaxOptions, thrownError) {
                swal('Error !', 'Error !!!', 'error'); 
            }
        });
    }
});
// END: UPLOAD
</script>
