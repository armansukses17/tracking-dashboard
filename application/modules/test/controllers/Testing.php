<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
ini_set("memory_limit","-1");
set_time_limit(-1);

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class Testing extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata("user_id"))) {
			redirect(base_url('auth'));
		}
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		echo 'ssss';
	}

	public function testExcel()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');

        // $writer = new Xlsx($spreadsheet);
        // $writer->save('hello world.xlsx');

        $writer = new Xlsx($spreadsheet);
        $filename = 'asdsad';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
        header('Cache-Control: max-age=0');

        $writer->save('php://output'); // download file 
    }
}
