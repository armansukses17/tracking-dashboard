<script>
function processing_modal(){
    mApp.block("#modal_review .modal-content", {
        overlayColor: "#000000",
        type: "loader",
        state: "primary",
        message: "Processing..."
    });
}

function stop_processing_modal(){
    setTimeout(function() {
        mApp.unblock("#modal_review .modal-content")
    }, 800)
}

function processing(){
    // mApp.blockPage({
    //     overlayColor:"#000000",
    //     type:"loader",state:"primary",
    //     message:"Processing..."
    // });
    swal('Processing..')
}

function stop_processing(){
    // setTimeout(function(){
    //     mApp.unblockPage()
    // },1000);
    swal.close()
}
</script>