<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>TRACKING | DASHBOARD</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
        </script>
        <?php  
		$access_domain = $this->session->userdata['access_domain'];
		if ($access_domain == 1) {  
			 $favicon = 'assets/demo/default/media/img/logo/faviconMau.png';
		} 
		elseif ($access_domain == 2) 
		{  
			$favicon = 'assets/demo/default/media/img/logo/faviconRa.png';
		} 
		else 
		{ 
			$favicon = 'assets/demo/default/media/img/logo/faviconMau.png';
		    // $favicon = 'assets/demo/default/media/img/logo/faviconRa.png'; 
		}
		?>
		<link href="<?= base_url()?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?= base_url()?>assets/css/jquery-ui.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="<?= base_url() . $favicon; ?>" />
		<link href="<?= base_url()?>assets/css/custom.css" rel="stylesheet" type="text/css" />

		<script src="<?= base_url()?>assets/js/jquery/jquery.min.js" type="text/javascript"></script>
		<script src="<?= base_url()?>assets/js/jquery/jquery-ui.js" type="text/javascript"></script>
	</head>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default m-header">
		<div class="m-grid m-grid--hor m-grid--root m-page">