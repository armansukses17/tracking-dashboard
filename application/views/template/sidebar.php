<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
	<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow" style="padding-top: 0;">
		<div class="m-card-user m-card-user--skin-dark" style="padding: 40px 0 40px 15px; background: #212834;">
			<div class="m-card-user__pic">
				<img src="<?= base_url();?>assets/img/<?= $_SESSION['user_image']?>" class="m--img-rounded m--marginless" alt="" style="max-width: 50px !important;">
			</div>
			<div class="m-card-user__details">
				<span class="m-card-user__name m--font-weight-500" style="color: #716aca; font-size: 14px;"><?= $_SESSION['user_nama']?></span>
				<a href="" class="m-card-user__email m--font-weight-300 m-link" style="font-size: 10px;"><?= $_SESSION['user_email']?></a>
			</div>
		</div>
		<li class="m-menu__section " style="margin-top: 0;">
			<h4 class="m-menu__section-text">NAVIGATION</h4>
			<i class="m-menu__section-icon flaticon-more-v2"></i>
		</li>
		<?php
		$role_id = $_SESSION['role_id'];
		$this->db->join('user_menu','user_access_menu.menu_id = user_menu.id','');
		$this->db->order_by('user_menu.sort','asc');
		$get_menu = $this->db->get_where('user_access_menu',['role_id' => $role_id,'is_active'=> 1])->result();
		// print_r($get_menu);die;
		foreach($get_menu as $menu):
		?>
		<!-- m-menu__item--open m-menu__item--expanded -->
		<?php 
		// $config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		// $config['base_url'] .= "://" . ((array_key_exists('HTTP_HOST',$_SERVER)) ? $_SERVER['HTTP_HOST'] : 'app');
		// $config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
		$uri = $_SERVER['REQUEST_URI'];
		$menus = str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']).$menu->url;
		if($uri == $menus):
		?>
		<li class="m-menu__item  m-menu__item--submenu m-menu__item--active" aria-haspopup="true" m-menu-submenu-toggle="hover">
		<?php else : ?>
		<li class="m-menu__item  m-menu__item--submenu " aria-haspopup="true" m-menu-submenu-toggle="hover">
		<?php endif ?>
			<?php if($menu->have_a_child == 1) :?>
			<a href="<?=$menu->url;?>" class="m-menu__link m-menu__toggle">
			<?php else :?>
			<a href="<?=base_url().$menu->url;?>" class="m-menu__link m-menu__toggle">
			<?php endif?>
				<!-- <i class="m-menu__link-icon flaticon-web"></i> -->
				<i class="m-menu__link-icon <?=$menu->icon?>"></i>
				<span class="m-menu__link-text"><?=$menu->menu?></span>
				<?php if($menu->have_a_child == 1) :?>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				<?php endif?>
			</a>
			<?php
			$get_submenu = $this->db->get_where('user_sub_menu',['menu_id' =>$menu->id,'is_active'=>1])->result();
			foreach($get_submenu as $sm):
			?>
			<div class="m-menu__submenu ">
				<span class="m-menu__arrow"></span>
				<ul class="m-menu__subnav">
					<!-- m-menu__item--open m-menu__item--expanded -->
					<li class="m-menu__item  m-menu__item--submenu " aria-haspopup="true" m-menu-submenu-toggle="hover">
						<?php if($sm->have_a_child == 1) :?>
						<a href="<?=$sm->url;?>" class="m-menu__link m-menu__toggle">
						<?php else :?>
						<a href="<?=base_url().$sm->url;?>" class="m-menu__link m-menu__toggle">
						<?php endif?>
							<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
							<span class="m-menu__link-text"><?=$sm->title?></span>
							<?php if($sm->have_a_child == 1) :?>
								<i class="m-menu__ver-arrow la la-angle-right"></i>
							<?php endif?>
						</a>
						<?php
						$get_subsubmenu = $this->db->get_where('user_sub_sub_menu',['sub_menu_id' =>$sm->id,'is_active'=>1])->result();
						foreach($get_subsubmenu as $ssm):
						?>
						<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
							<ul class="m-menu__subnav">
								<!--  m-menu__item--active -->
								<li class="m-menu__item" aria-haspopup="true">
									<a href="<?=base_url().$ssm->url?>" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot"></i>
										<span class="m-menu__link-text"><?=$ssm->title?></span>
									</a>
								</li>
							</ul>
						</div>
						<?php endforeach ?>
					</li>
				</ul>
			</div>
			<?php endforeach ?>
		</li>
		<?php endforeach ?>
		
		<li class="m-menu__item" aria-haspopup="true">
			<a href="<?= base_url('auth/logout')?>" class="m-menu__link ">
				<i class="m-menu__link-icon flaticon-logout"></i>
				<span class="m-menu__link-title"> 
					<span class="m-menu__link-wrap"> 
						<span class="m-menu__link-text">Logout</span>
					</span>
				</span>
			</a>
		</li>
	</ul>
</div>